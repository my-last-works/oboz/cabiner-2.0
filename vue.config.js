const path = require("path")
const webpack = require("webpack")
const ENVRMNT = process.env.ENVRMNT

const resolve = pathString => {
	return path.resolve(__dirname, pathString)
}

const publicPath = process.env.OBOZ_FRONT_ROOT_URL || '/'

const plugins = [
	new webpack.ProvidePlugin({
		mapGetters: ["vuex", "mapGetters"],
		mapMutations: ["vuex", "mapMutations"],
		mapActions: ["vuex", "mapActions"],
		mapState: ["vuex", "mapState"],
	}),
]

module.exports = {
	assetsDir: 'static',
	css: {
		loaderOptions: {
			sass: {
				prependData: 
				`
				@import "@/static/css/variables.scss";			
				`
			}
		}
	},
	publicPath,
	lintOnSave: "error",

	devServer: {},

	chainWebpack: (config) => {
		config.plugin('html').tap((definitions) => {
			const srcFunc = definitions[0].templateParameters
			definitions[0].templateParameters = () => {
				const result = srcFunc.apply(this, arguments)
				if (publicPath === "/") {
					result.OBOZ_API_HOST = ""
				} else {
					result.OBOZ_API_HOST = publicPath
				}
				return result
			}
			return definitions
		});
	},

	configureWebpack: {
		plugins,
		resolve: {
			alias: {
				'@RestClient': path.resolve(__dirname, 'src/api/RestClient.js'),
				src: resolve("src/")
			}
		},
		performance: {
			hints: false
		},
		optimization: {
			splitChunks: {
				chunks: 'all',
				minSize: 30000,
				maxSize: 0,
				cacheGroups: {
					defaultVendors: {
						test: /[\\/]node_modules[\\/]/,
						priority: -10
					},
					default: {
						minChunks: 2,
						priority: -20,
						reuseExistingChunk: true
					},
					styles: {
						name: 'styles',
						test: /\.css$/,
						chunks: 'all',
						enforce: true
					}
				}
			}
		},
		devtool: process.env.NODE_ENV !== 'production' ? 'eval-source-map' : 'none'
	}
}
