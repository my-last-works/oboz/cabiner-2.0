export const indicators = [
  {
    from: 'ЦФО (Центральный федеральный округ)',
    to: 'Везде',
    service: 'Перевозка фурой (все), перевозка контейнеровозом (длиннобазный)',
    characteristic: '22 т / 90 м3 / 33 пал, 14 т / 55 м3 / 24 пал',
    email: 'sergeytimofeevich@mail.ru',
    phone: '+7(923) 233-11-45',
    active: true,
    activeFrom: '27.10.2020',
    activeTo: '31.12.2020'
  },
  {
    from: 'Нижний Новгород',
    to: 'Пермь',
    service: 'Перевозка фурой',
    characteristic: 'Все',
    email: 'sergeytimofeevich@mail.ru',
    phone: '',
    active: true,
    activeFrom: '27.10.2020',
    activeTo: '31.12.2020'
  },
  {
    from: 'ЦФО (Центральный федеральный округ)',
    to: 'ЮФО (Южный федеральный округ)',
    service: 'Перевозка автосцепкой',
    email: 'sergeytimofeevich@mail.ru',
    phone: '+7(923) 233-11-45',
    active: true,
    activeFrom: '27.10.2020',
    activeTo: '31.12.2020'
  }
];