class DraftErrors {
  constructor() {
    this.errors = {
      'CARGO_ERRORS': []
    }
  }

  has(field) {
    return this.errors.hasOwnProperty(field);
  }

  any () {
    return Object.keys(this.errors).length > 0;
  }

  get(field) {
    if (this.errors[field]) {
      return this.errors[field]
    }
  }

  record(field, error) {
    this.errors[field] = error;
  }

  clear(field) {
    if (field) {
      delete this.errors[field];
      return;
    }

    this.errors = {
      'CARGO_ERRORS': []
    };
  }
}

export default DraftErrors;