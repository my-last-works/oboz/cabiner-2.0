export const contractors = [
  {
    name: 'ООО «Абсоюлют сервис»',
    inn: '7715962150',
    phone: '+7 (495) 856-5555',
    email: 'info@rom.ru',
    description: 'Возим рефами по Москве',
    address: 'г. Москва, ул. Докукина, д.8, оф. 3',
    uuid: '1',
    fio: 'Андреев Вадим',
    groups: ['Авто. Только по ЦАО', 'Минимальные затраты', 'Работали в 2019'],
  },
  {
    name: 'ООО «Автосибирь»',
    inn: '7715962659',
    phone: '+7 (499) 258-3321, доб. 24',
    email: 'vasilyok114@yandex.ru',
    description: 'Скорость, надежность, качество',
    address: 'г. Москва, ул. Летчика Бабушкина, д.8, оф. 3',
    uuid: '2',
    fio: 'Куршева Елена',
    groups: ['Авто. Только по ЦАО', 'Любимые поставщики Евгинько', 'Минимальные затраты'],
  },
  {
    name: 'ООО «Атмаш»',
    inn: '7715962569',
    phone: '+7 903 555-1782',
    email: 'info@soltrans.ru',
    description: 'Аренда фур, аутсорс водителей',
    address: 'г. Москва, пр. Ленина, д.8, оф. 100',
    uuid: '3',
    fio: 'Жукова Анастасия',
    groups: ['Авто. Только по ЦАО', 'Любимые поставщики Евгинько'],
  },
  {
    name: 'ООО «Байкальская нерпа»',
    inn: '3415962158',
    phone: '+7 (499) 851-1218, доб. 153',
    email: 'info@jupiter-1.ru',
    description: 'Наш приоритет — Южное направление',
    address: 'г. Москва, Новорязанское ш., д.104',
    uuid: '4',
    fio: 'Иванов Владимир',
    groups: ['Любимые поставщики Евгинько', 'Минимальные затраты', 'Работали в 2019'],
  },
  {
    name: 'ООО «Бетонстрой»',
    inn: '0815499989',
    phone: '+7 (8712) 51-1218, доб. 8',
    email: '—',
    description: '—',
    address: 'г. Грозный, ул. Курсантов, д.75, оф. 153',
    uuid: '5',
    fio: 'Кулаков Николай',
    groups: ['Любимые поставщики Евгинько'],
  },
  {
    name: 'ООО «Один»',
    inn: '7715962150',
    phone: '+7 (495) 856-5555',
    email: 'info@rom.ru',
    description: 'Возим рефами по Москве',
    address: 'г. Москва, ул. Докукина, д.8, оф. 3',
    uuid: '6',
    fio: 'Андреев Вадим',
    groups: ['Группа 1', 'Группа 2', 'Группа 3'],
  },
  {
    name: 'ООО «Два»',
    inn: '7715962150',
    phone: '+7 (495) 856-5555',
    email: 'info@rom.ru',
    description: 'Возим рефами по Москве',
    address: 'г. Москва, ул. Докукина, д.8, оф. 3',
    uuid: '7',
    fio: 'Андреев Вадим',
    groups: ['Группа 1', 'Группа 2'],
  },
  {
    name: 'ООО «Три»',
    inn: '7715962150',
    phone: '+7 (495) 856-5555',
    email: 'info@rom.ru',
    description: 'Возим рефами по Москве',
    address: 'г. Москва, ул. Докукина, д.8, оф. 3',
    uuid: '8',
    fio: 'Андреев Вадим',
    groups: ['Группа 1'],
  },
  {
    name: 'ООО «Четыре»',
    inn: '7715962150',
    phone: '+7 (495) 856-5555',
    email: 'info@rom.ru',
    description: 'Возим рефами по Москве',
    address: 'г. Москва, ул. Докукина, д.8, оф. 3',
    uuid: '9',
    fio: 'Андреев Вадим',
    groups: ['Группа 2', 'Группа 3'],
  }
];

export const fields = [
  {
    title: 'Страна регистрации',
    value: 'Россия',
    group: 'Регистрационные данные'
  },
  {
    title: 'Форма организации',
    value: 'Юридическое лицо',
    group: 'Регистрационные данные'
  },
  {
    title: 'Наименование',
    value: 'ООО “Волжский круиз”',
    group: 'Регистрационные данные'
  },
  {
    title: 'Роль',
    value: 'Заказчик-Исполнитель',
    group: 'Регистрационные данные'
  },
  {
    title: 'ИНН',
    value: '4835445786864',
    group: 'Регистрационные данные'
  },
  {
    title: 'КПП',
    value: '0495398500',
    group: 'Регистрационные данные'
  },
  


  {
    title: 'Предоставляемые услуги',
    value: 'Перевозки фурами по югу России: продукты и промтовары',
    group: 'Данные для тендеров'
  },
  {
    title: 'Производственные мощности',
    value: '40 фур, 52 водителя',
    group: 'Данные для тендеров'
  },
  {
    title: 'Регионы присутствия',
    value: 'Весь Северо-Кавказский, Южный ФО и Воронежская область',
    group: 'Данные для тендеров'
  },
  {
    title: 'Приоритетные направления',
    value: 'Астрахань-Ростов-на-Дону и обратно, Астрахань-Москва, Астрахань-Воронеж',
    group: 'Данные для тендеров'
  },
  


  {
    title: 'Полное наименование',
    value: '',
    group: 'Основные данные'
  },
  {
    title: 'Ген. директор',
    value: 'Ивановский Александр Александрович',
    group: 'Основные данные'
  },
  {
    title: 'Паспорт гендира',
    value: '',
    group: 'Основные данные'
  },
  {
    title: 'Существование организации',
    value: 'Существует',
    group: 'Основные данные'
  },
  {
    title: 'Задолженности',
    value: '',
    group: 'Основные данные'
  },
  {
    title: 'Судебные тяжбы/сумма',
    value: '20 000 000',
    group: 'Основные данные'
  },
  {
    title: 'Плательщик НДС',
    value: 'Да',
    group: 'Основные данные'
  },
  {
    title: 'Данные о судимости генерального директора',
    value: '4.2',
    group: 'Основные данные'
  },
  {
    title: 'Паспорт гендира валиден',
    value: 'Да',
    group: 'Основные данные'
  },
  {
    title: 'Телефон гендира',
    value: '',
    group: 'Основные данные'
  },
  {
    title: 'Паспорт гендира в реестре недействительных не значится',
    value: 'Нет',
    group: 'Основные данные'
  },
  {
    title: 'Отчество гендира',
    value: '',
    group: 'Основные данные'
  },
  {
    title: 'Фамилия главбуха',
    value: '',
    group: 'Основные данные'
  },
  {
    title: 'Паспорт главбуха',
    value: '',
    group: 'Основные данные'
  },
  {
    title: 'Дебиторская задолженность',
    value: '100 500',
    group: 'Основные данные'
  }
];