import uploaderMixin from './uploaderMixin'
import FormValidationMixin from 'common-components/src/obComponents/obForm/FormValidationMixin'
import { required, requiredIf, minLength } from 'vuelidate/lib/validators'
import { uuid } from 'vue-uuid'
import { mapGetters } from 'vuex'
import api from '@/api'
import moment from 'moment'
import Documents from '../components/Documents'
import OwnerField from '../components/OwnerField'

import ObForm from 'common-components/src/obComponents/obForm'
import ObInput from 'common-components/src/obComponents/obInput'
import ObSelect from 'common-components/src/obComponents/obSelect'
import ObUploader from 'common-components/src/obComponents/obUploader'
import ObCheckbox from 'common-components/src/obComponents/obCheckbox'
import ObControlGroup from 'common-components/src/obComponents/obControlGroup'
import ObLoader from 'common-components/src/obComponents/obLoader'
import ObAlert from 'common-components/src/obComponents/obAlert'

export default {
  components: {
    Documents,
    OwnerField,
    ObForm,
    ObInput,
    ObSelect,
    ObUploader,
    ObCheckbox,
    ObControlGroup,
    ObLoader,
    ObAlert
  },
  mixins: [
    uploaderMixin,
    FormValidationMixin
  ],
  props: {
    resourceTitle: {
      type: String,
      required: true
    }
  },
  data () {
    return {
      isLoaded: false,
      countries: [],
      isRegistered: false,
      classUuid: null,
      speciesUuid: null,
      ownershipForms: [],
      selectedOwnershipForm: null,
      rentOwnershipFormCode: [],
      isRequestPending: false,
      isGovernmentRegnum: true,
      regnumMaskTokens: {
        '0': { pattern: /\d/ },
        'A': {
          pattern: /[ABEKMHOPCTXYАВЕКМНОРСТХУ]/i,
          transform: symbol => this.prepareRegnum(symbol)
        }
      }
    }
  },
  computed: {
    ...mapGetters({
      isModerator: 'user/isModerator',
      participantUuid: 'user/participantUuid'
    }),
    submitButtonState () {
      return this.isFilesPending || this.isRequestPending || undefined
    },
    regnumMask () {
      switch (this.resourceName) {
        case 'Van':
        case 'Tractor':
          return 'A 000 AA 00(0)'

        default:
          return 'AA 0000 00(0)'
      }
    },
    commonFormModel () {
      return {
        ownerUuid: {
          validator: {
            required: requiredIf(() => this.isModerator)
          }
        },
        countryCode: {
          value: 'rus',
          validator: {
            required
          }
        },
        props: {
          nested_fields: {
            regnum: {
              validator: {
                required,
                checkLength: {
                  method: this.checkRegnumLength,
                  error: 'Регистрационный номер введён не полностью'
                }
              }
            },
            payload_max: {
              validator: {
                required
              }
            },
            vehicle_in_pledge: {
              value: false
            },
            has_casco_insurance: {
              value: false
            }
          }
        },
        links: {
          nested_fields: {
            RENT: {
              value: [],
              validator: {
                required: requiredIf(() => {
                  return this.selectedOwnershipForm === this.rentOwnershipFormCode
                })
              }
            },
            VEHICLE_PASSPORT: {
              value: [],
              validator: {
                required,
                minLength: minLength(2)
              }
            },
            PTS: {
              value: []
            }
          }
        }
      }
    }
  },
  watch: {
    'formData.countryCode' (countryCode) {
      this.isGovernmentRegnum = countryCode === 'rus'
    }
  },
  async mounted () {
    this.countries = await api.registration.register.getCountriesDropdown()

    const { resourceSpecies } = await api.resources.getResourceSpeciesDropdown()
    this.classUuid = resourceSpecies.find(item => item.title === this.resourceTitle).classUuid
  },
  methods: {
    async getSpeciesUuid () {
      // Added 'size' parameter to prevent pagination
      const { content } = await api.resources.getResourceSpecies({ size: 40 })

      this.speciesUuid = content.find(item => item.name === this.resourceTitle).uuid
    },
    async getOwnershipForms () {
      const { ownershipForms } = await api.resources.getOwnershipForms()

      this.ownershipForms = ownershipForms.map((item) => {
        return {
          label: item.title,
          value: item.uuid,
          code: item.code
        }
      })

      this.selectedOwnershipForm = this.ownershipForms[0].value
      this.rentOwnershipFormCode = this.ownershipForms.find(item => item.code === 'rent').value
    },
    async onUpload (code, data, side = 'FRONT') {
      if (!data.length) {
        this.formData.links[code] = this.formData.links[code].filter(link => link.file_side !== side)

        return
      }

      const found = this.formData.links[code].find(link => link.file_side === side)

      if (found) {
        found.fileUuid = data[0].uuid
      } else {
        this.formData.links[code].push({
          fileUuid: data[0].uuid,
          file_side: side
        })
      }

      this.isFilesPending = false
    },
    // Limit maximum length for 'payload_max' with
    // the type 'INTEGER' to prevent overflow
    checkLength (value) {
      if (value.length > 9) {
        setTimeout(() => {
          this.formData.props.payload_max = value.slice(0, 9)
        }, 0)
      }
    },
    async onSubmit () {
      this.isRequestPending = true;

      try {
        if (this.resourceName === 'Driver') {
          // Fixed dates (https://oboz.myjetbrains.com/youtrack/issue/SB_UAT-627)
          this.formData.props.birthdate = moment(this.formData.props.birthdate).utcOffset(0, true).format()
          this.formData.props.passport_date = moment(this.formData.props.passport_date).utcOffset(0, true).format()
        } else {
          this.formData.props.ownership_form = this.selectedOwnershipForm
          this.formData.props.payload_max = Number(this.formData.props.payload_max)
        }

        await api.resources.registerResource(uuid.v4(), {
          ...this.formData,
          resourceSpecies: this.resourceName,
          ownerUuid: this.isModerator ? this.formData.ownerUuid : this.participantUuid,
          classUuid: this.classUuid,
          speciesUuid: this.speciesUuid,
          isActive: false,
          links: Object.entries(this.formData.links).reduce((result, item) => {
            const [ code, links ] = [ item[0], item[1] ]

            if (links.length) {
              links.forEach(link => result.push({ ...link, code }))
            }

            return result
          }, [])
        })

        this.$emit('success-submit')

        this.$notification({
          type: 'system_notification_success',
          message: 'Новый ресурс успешно добавлен'
        })
      } catch (e) {
        let message = 'Ресурс не создан'

        switch (e.status) {
          case 409:
            message = 'Ресурс с такими данными уже существует'
            break
        }

        this.$notification({
          type: 'system_notification_error',
          message: message
        })
      }

      this.isRequestPending = false;
    },
    prepareRegnum (symbol) {
      // Convert cyrillic symbols to latin
      const mappedSymbol = {
        'А': 'A',
        'В': 'B',
        'Е': 'E',
        'К': 'K',
        'М': 'M',
        'Н': 'H',
        'О': 'O',
        'Р': 'P',
        'С': 'C',
        'Т': 'T',
        'Х': 'X',
        'У': 'Y'
      }[symbol] || symbol // Fallback if there is no match for conversion

      return mappedSymbol.toLocaleUpperCase()
    },
    checkRegnumLength (value) {
      if (!this.isGovernmentRegnum || !value) {
        return true
      }

      // -3 to remove non-required number in brackets
      return value.length >= this.regnumMask.replace(/ /g, '').length - 3
    },
    changeOwner (uuid) {
      this.formData.ownerUuid = uuid
    }
  }
}
