import api from '@/api'

export default {
  data () {
    return {
      uploadMethod: api.files.postSecurityFileV2,
      uploadOptions: null,
      isFilesPending: false,
      domainUuid: '54d0de87-99a2-4771-9c33-e9ac4ebf5fb1' // TODO remove (needed for 'postSecurityFile' method)
    }
  },
  created () {
    this.uploadOptions = {
      participant_uuid: this.$store.getters['user/userUuid'],
      domain_uuid: this.domainUuid
    }
  },
  methods: {
    getUploadOptions (side = 'FRONT') {
      return {
        ...this.uploadOptions,
        file_side: side
      }
    },
    onFilesProcessStart () {
      this.isFilesPending = true
    },
    isFileUploaded (code, side = 'FRONT') {
      const links = this.formData?.links || this.links

      return links[code].find(link => link.file_side === side)
    }
  }
}
