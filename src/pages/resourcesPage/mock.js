import moment from 'moment';

const getValue = (value, isActive) => value ? isActive ? value : `<span class="text-disabled text-crossed">${value}</span>` : null;
const getDate = (date, isActive) => date ? 
  `<div class="field-datetime${!isActive ? ' text-disabled': ''}">
    <div>${moment(date).locale('ru').format('L')}</div>
    <div class="field-time">${moment(date).locale('ru').format('LT')}</div>
  </div>` : null;

const trailerColumns = [
  {
    title: 'Тип ресурса',
    key: 'resourceTypeTitle',
    html: ({ resourceTypeTitle, isActive }) => getValue(resourceTypeTitle, isActive)
  },
  {
    title: "Регистрационный номер",
    key: "number",
    html: ({ number, isActive }) => getValue(number, isActive)
  },
  {
    title: "Год выпуска",
    key: "issueYear",
    html: ({ issueYear, isActive }) => getValue(issueYear, isActive)
  },
  {
    title: "Право владения",
    key: "holdTypeTitle",
    html: ({ holdTypeTitle, isActive }) => getValue(holdTypeTitle, isActive)
  },
  {
    title: "Группа риска",
    key: "riskGroupColor",
    slotName: 'risks'
  },
  {
    title: 'Начальная регистрация',
    key: 'registeredAt',
    html: ({ registeredAt, isActive }) => getDate(registeredAt, isActive)
  },
  {
    title: 'Запуск проверки',
    key: 'verificationStartedAt',
    html: ({ verificationStartedAt, isActive }) => getDate(verificationStartedAt, isActive)
  },
  {
    title: "Окончание проверки",
    key: "assessedBySecurityAt",
    html: ({ assessedBySecurityAt, isActive }) => getDate(assessedBySecurityAt, isActive)
  },
  {
    title: "Статус",
    key: "isActive",
    html: ({ isActive }) => isActive ? 'Активно' : '<span class="text-disabled">Неактивно</span>'
  }
]

export const columns = {
  driver: [
    {
      title: "ФИО",
      key: "fio",
      html: ({ fio, isActive }) => getValue(fio, isActive)
    },
    {
      title: "Номер паспорта",
      key: "passportNumber",
      html: ({ passportNumber, isActive }) => getValue(passportNumber, isActive)
    },
    {
      title: 'Группа риска',
      key: 'riskGroupColor',
      slotName: 'risks'
    },
    {
      title: 'Начальная регистрация',
      key: 'registeredAt',
      html: ({ registeredAt, isActive }) => getDate(registeredAt, isActive)
    },
    {
      title: 'Запуск проверки',
      key: 'verificationStartedAt',
      html: ({ verificationStartedAt, isActive }) => getDate(verificationStartedAt, isActive)
    },
    {
      title: "Окончание проверки",
      key: "assessedBySecurityAt",
      html: ({ assessedBySecurityAt, isActive }) => getDate(assessedBySecurityAt, isActive)
    },
    {
      title: "Статус",
      key: "isActive",
      html: ({ isActive }) => isActive ? 'Активно' : '<span class="text-disabled">Неактивно</span>'
    }
  ],
  tractor: [
    {
      title: "Марка",
      key: "mark",
      html: ({ mark, isActive }) => getValue(mark, isActive)
    },
    {
      title: "Регистрационный номер",
      key: "number",
      html: ({ number, isActive }) => getValue(number, isActive)
    },
    {
      title: "Цвет",
      key: "colorTitle",
      html: ({ colorTitle, isActive }) => getValue(colorTitle, isActive)
    },
    {
      title: "Год выпуска",
      key: "issueYear",
      html: ({ issueYear, isActive }) => getValue(issueYear, isActive)
    },
    {
      title: "Право владения",
      key: "holdTypeTitle",
      html: ({ holdTypeTitle, isActive }) => getValue(holdTypeTitle, isActive)
    },
    {
      title: "Группа риска",
      key: "riskGroupColor",
      slotName: 'risks'
    },
    {
      title: 'Начальная регистрация',
      key: 'registeredAt',
      html: ({ registeredAt, isActive }) => getDate(registeredAt, isActive)
    },
    {
      title: 'Запуск проверки',
      key: 'verificationStartedAt',
      html: ({ verificationStartedAt, isActive }) => getDate(verificationStartedAt, isActive)
    },
    {
      title: "Окончание проверки",
      key: "assessedBySecurityAt",
      html: ({ assessedBySecurityAt, isActive }) => getDate(assessedBySecurityAt, isActive)
    },
    {
      title: "Статус",
      key: "isActive",
      html: ({ isActive }) => isActive ? 'Активно' : '<span class="text-disabled">Неактивно</span>'
    }
  ],
  wagon: trailerColumns,
  trailer: trailerColumns,
  semitrailer: trailerColumns,
  containertruck: trailerColumns,
  tral: trailerColumns,
  jumbo: trailerColumns,
  lst: trailerColumns
};