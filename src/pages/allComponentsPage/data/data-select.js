const dataSelectList = [
    {code: 1, uuid: 'uuid-select-1', title: 'Белый'},
    {code: 2, uuid: 'uuid-select-2', title: 'Красный'},
    {code: 3, uuid: 'uuid-select-3', title: 'Синий'},
    {code: 4, uuid: 'uuid-select-4', title: 'Черный'},
    {code: 5, uuid: 'uuid-select-5', title: 'Зеленый'},
    {code: 6, uuid: 'uuid-select-6', title: 'Розовый'},
];
export default dataSelectList
