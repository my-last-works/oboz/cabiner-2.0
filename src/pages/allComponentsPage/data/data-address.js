const dataAddressList = {
    uuid: "7c5fba70-2080-4a7c-b2f1-25ee6c209b6c",
    coordinates: {
        lat: 53.675060925331714,
        lon: 102.31049374931989
    },
    location_type_uuid: "4adf706e-0be2-4383-b680-a73c959b9e2c",
    russian_name: "Тыреть — Восточно-Сибирская ЖД — 930305",
    english_name: "1-Ya Tyret'",
    local_name: "Тыреть — Восточно-Сибирская ЖД — 930305",
    russian_address: "Россия, Иркутская область, Заларинский район, посёлок городского типа Тыреть 1-я",
    english_address: "1-Ya Tyret', Irkutsk Oblast, Russia",
    local_address: "Россия, Иркутская область, Заларинский район, посёлок городского типа Тыреть 1-я",
    location_info: null,
    location_radius: {
        vehicle_arrival_radius: 2,
        vehicle_departure_radius: 2,
        zero_interrogation_radius: 2
    },
    is_active: true
};
export default dataAddressList
