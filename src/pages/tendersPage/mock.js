import moment from 'moment';
import { STATUSES, PARTICIPATION_STATUSES } from './statuses';

export const columns = {
  client: [
    {
      title: '№ Тендера',
      key: 'number',
      html: ({ number }) => `<div class="tender-code-container">${ number }</div>`,
      width: 120
    },
    {
      title: 'Статус',
      key: 'status',
      html: ({ status }) => STATUSES[status]
    },
    {
      title: 'Название тендера',
      key: 'title',
      width: 570
    },
    {
      title: 'Ответственный',
      key: 'author',
      html({ author }) {
        const presence = author?.firstname && author?.lastname;
        return presence ? `${author?.firstname} ${author?.lastname}` : '–'
      }
    },
    {
      title: 'Изменен, utc+3',
      headerHtml: () => '<div title="Часовой пояс: Москва">Изменен, utc+3</div>',
      key: 'updatedAt',
      filter: 'datetime',
      html({ updatedAt }) {
        return `<div class="field-datetime">
          <div>${moment(updatedAt).locale('ru').format('L')}</div>
          <div class="field-time">${moment(updatedAt).locale('ru').format('LT')}</div>
        </div>`
      }
    },
    {
      title: 'Опубликован, utc+3',
      headerHtml: () => '<div title="Часовой пояс: Москва">Опубликован, utc+3</div>',
      key: 'publishedAt',
      filter: 'datetime',
      html({ publishedAt }) {
        return publishedAt ? `<div class="field-datetime">
          <div>${moment(publishedAt).locale('ru').format('L')}</div>
          <div class="field-time">${moment(publishedAt).locale('ru').format('LT')}</div>
        </div>` : "–"
      }
    },
    {
      title: 'Создан, utc+3',
      headerHtml: () => '<div title="Часовой пояс: Москва">Создан, utc+3</div>',
      key: 'createdAt',
      filter: 'datetime',
      html({ createdAt }) {
        return `<div class="field-datetime">
          <div>${moment(createdAt).locale('ru').format('L')}</div>
          <div class="field-time">${moment(createdAt).locale('ru').format('LT')}</div>
        </div>`
      }
    },
    {
      slotName: 'menu',
      sortDisable: true,
      isToggleDisabled: true,
      exactWidth: '70px'
    }
  ],
  carrier: [
    {
      title: '№ Тендера',
      key: 'number',
      html: ({ number, participationStatus, _seconds }) => `
        <div class="tender-code-container">
          ${participationStatus === 'NOT_VIEWED' ? '<div class="tender-active-triangle"></div>' : ''}
          ${(parseInt(_seconds) < 3600 ? '<i class="icon clock"></i>' : '<i class="icon clock transparent"></i>') + number}
        </div>
      `,
      width: 120
    },
    {
      title: 'Статус участия в тендере',
      key: 'participationStatus',
      html: ({ participationStatus }) => PARTICIPATION_STATUSES[participationStatus]
    },
    {
      title: 'Название тендера',
      key: 'title',
      // width: 570
    },
    {
      title: 'Организатор',
      key: 'organizer',
      html: ({ organizer, author }) => `
        <div class="organisator-container">
          <div class="organisator-name">${ organizer.title }</div>
          <div class="responsible-name">${ author.firstname } ${ author.lastname }</div>
        </div>
      `
    },
    {
      title: 'Статус тендера',
      key: 'status',
      html: ({ status }) => STATUSES[status]
    },
    {
      title: 'Тендер актуален',
      key: 'validityPeriod',
      html: ({ _seconds, validityPeriod }) => _seconds ? _seconds < 3600 ? `<span class="red-countdown">${ validityPeriod }</span>` : validityPeriod : '–'
    },
    {
      slotName: 'menu',
      sortDisable: true,
      isToggleDisabled: true,
      exactWidth: '70px'
    }
  ]
}