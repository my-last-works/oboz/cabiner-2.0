export const STATUSES = {
  DRAFT: 'Черновик',
  COMPLETED: 'Завершён',
  CANCELLED: 'Отменён',
  HELD: 'Сбор предложений',
  ON_CHECKING: 'На проверке',
  ON_APPROVAL: 'На утверждении',
  SUMMARIZING: 'Подведение итогов',
  RESULTS_APPROVAL: 'Утверждение итогов',
  ARCHIVED: 'Архивный'
}

export const PARTICIPATION_STATUSES = {
  REJECTED: 'Отказ от участия',
  REPLIED: 'Предложение отправлено',
  DRAFT: 'Черновик',
  VIEWED: 'Просмотрен',
  NOT_VIEWED: 'Не просмотрен'
}