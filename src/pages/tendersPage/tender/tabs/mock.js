export const offers_columns = {
  all: [
    {
      title: '№ лота',
      key: 'lot_number'
    },
    {
      title: 'Откуда',
      key: 'from'
    },
    {
      title: 'Куда',
      key: 'to'
    },
    {
      title: 'Ресурс',
      key: 'resource'
    },
    {
      title: 'Грузоподъемность',
      key: 'max_load'
    },
    {
      title: 'Объем закуп.',
      key: 'volume'
    },
    {
      title: 'Период',
      key: 'span'
    },
    {
      title: 'Поставщик',
      html: ({ contractor }) => `
        <div class="offer-comment-wrapper">
          ${contractor.title}
          <div class="offer-comment">
            Дмитрий Андреев<br>
            +7 495 123 4567, доб. 112<br>
            <a href="mailto:d.andreev@oboz.com">d.andreev@oboz.com</a>
          </div>
        </div>`
    },
    {
      title: 'Гарант. объемы',
      key: 'confirmed_volume'
    },
    {
      title: 'Ставка поставщика, ₽',
      key: 'price'
    },
    {
      title: 'Коммент. пост-ка',
      html: ({ comment }) => comment ? `<div class="offer-comment-wrapper"><i class="icon event"></i><div class="offer-comment">${comment}</div></div>` : ''
    }
  ],
  lots: [
    {
      title: '№ лота',
      key: 'lot_number'
    },
    {
      title: 'Откуда',
      key: 'from'
    },
    {
      title: 'Куда',
      key: 'to'
    },
    {
      title: 'Ресурс',
      key: 'resource'
    },
    {
      title: 'Грузоподъемность',
      key: 'max_load'
    },
    {
      title: 'Объем закуп.',
      key: 'volume'
    },
    {
      title: 'Период',
      key: 'span'
    },
    {
      title: 'Ненул. ставок',
      key: 'not_empty_bids'
    },
    {
      title: 'Предл. с гарант. объемом',
      key: 'confirmed_volume_quantity'
    },
    {
      title: 'Индик-р дефицита',
      key: 'not_enough_identy'
    },
    {
      title: 'Min ставка',
      key: 'min_bid'
    },
    {
      title: 'Max ставка',
      key: 'max_bid'
    },
    {
      title: 'Средняя ставка',
      key: 'average_bid'
    },
    {
      title: 'Вес лота, %',
      key: 'lot_weight'
    }
  ],
  contractors: [
    {
      title: 'Поставщик',
      html: ({ contractor }) => contractor.title
    },
    {
      title: 'Участвует в лотах',
      key: 'participating_in_quantity'
    },
    {
      title: 'Не участвует в лотах',
      key: 'not_participating_in_quantity'
    },
    {
      title: 'Лотов с гар. объемом',
      key: 'confirmed_volume'
    },
    {
      title: 'Дефицитных лотов',
      key: 'not_enough_volume'
    }
  ]
};

export const questions = [
  {
    uuid: '1',
    value: false,
    group: 'Ресурсы',
    title: 'Количество ТС Рефрижераторов 1.5 т',
    type: '1-5, 5-15, 15-30, 30-100, Более 100'
  },
  {
    uuid: '2',
    value: false,
    group: 'Ресурсы',
    title: 'Количество ТС Рефрижераторов 3 т',
    type: '1-5, 5-15, 15-30, 30-100, Более 100'
  },
  {
    uuid: '3',
    value: true,
    group: 'Контакты',
    title: 'Количество ТС Рефрижераторов 1.5 т',
    type: '1-5, 5-15, 15-30, 30-100, Более 100'
  },
  {
    uuid: '4',
    value: false,
    group: 'Контакты',
    title: 'Количество ТС Рефрижераторов 3 т',
    type: '1-5, 5-15, 15-30, 30-100, Более 100'
  },
  {
    uuid: '5',
    value: true,
    group: 'Общие сведения',
    title: 'Количество ТС Рефрижераторов 1.5 т',
    type: '1-5, 5-15, 15-30, 30-100, Более 100'
  },
  {
    uuid: '6',
    value: true,
    group: 'Общие сведения',
    title: 'Количество ТС Рефрижераторов 3 т',
    type: '1-5, 5-15, 15-30, 30-100, Более 100'
  }
];