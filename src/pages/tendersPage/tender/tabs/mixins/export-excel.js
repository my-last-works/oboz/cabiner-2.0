import ObMenuDropdown from 'common-components/src/obComponents/obMenuDropdown'
import SuccessExport from '../../../modal/successExport'

export default {
  components: {
    ObMenuDropdown,
    SuccessExport
  },
  data () {
    return {
      exportResult: {}
    }
  },
  watch: {
    exportResult () {
      this.$refs.successExportModal.open()
    }
  },
  computed: {
    excelDropdownItems () {
      return [
        {
          title: 'Выгрузить заполненные данные в Excel...',
          method: this.exportToExcel
        }
      ]
    }
  }
}