import ObTable from 'common-components/src/obComponents/obTable'
import api from '@/api'
import moment from 'moment'
import { mapGetters } from 'vuex'
import { STATUSES, PARTICIPATION_STATUSES } from '../../../statuses'

export default {
  components: {
    ObTable
  },
  computed: {
    ...mapGetters({
      currentRole: 'tenders/currentRole'
    }),
    columns () {
      // 1% column width to fit column width to content width
      const commonColumns = [
        {
          title: '№ Этапа',
          key: 'stageLvl',
          isAlignRight: true,
          exactWidth: '61px'
        },
        {
          title: '№ Тендера',
          key: 'number'
        },
        {
          title: 'Статус',
          key: 'status',
          html: ({ status }) => STATUSES[status]
        },
        {
          title: 'Название тендера',
          key: 'title'
        }
      ]

      if (this.currentRole === 'client') {
        return commonColumns.concat([
          {
            title: 'Ответственный',
            html: ({ author }) => `${author.firstname} ${author.lastname}`
          },
          {
            title: 'Изменён',
            key: 'updatedAt',
            html: ({ updatedAt }) => this.parseDate(updatedAt),
            exactWidth: '1%'
          },
          {
            title: 'Создан',
            key: 'createdAt',
            html: ({ createdAt }) => this.parseDate(createdAt),
            exactWidth: '1%'
          }
        ])
      } else {
        return commonColumns.concat([
          {
            title: 'Организатор',
            html: ({ organizer }) => organizer.title
          },
          {
            title: 'Статус тендера',
            key: 'participationStatus',
            html: ({ participationStatus }) => PARTICIPATION_STATUSES[participationStatus]
          },
          {
            title: 'Тендер актуален',
            key: 'validityPeriod',
            slotName: 'validity'
          }
        ])
      }
    }
  },
  methods: {
    async getStages () {
      const uuid = this.$route.params.uuid
      const response = await api.tenders.getStages(uuid)

      // Exclude current stage
      response.tenders = response.tenders.filter(tender => tender.uuid !== uuid)

      return response
    },
    openTender ({ uuid }) {
      this.$router.push({ path: `/tenders/${uuid}`, query: { ...this.$route.query } })

      this.$emit('reloadTender')
    },
    parseDate (date) {
      return `${moment(date).format('DD.MM.YYYY')} <span class="-gray-slate">${moment(date).format('HH:mm')}</span>`
    }
  }
}