import { mapGetters, mapActions } from 'vuex'
import api from '@/api'

export default {
  data () {
    return {
      lotsData: [],
      checklists: [],
      isColumnsModified: false,
      initialColumns: [
        {
          key: 'pos',
          title: '№ Лота',
          isAlignRight: true,
          exactWidth: '61px',
          headerHtml: (data) => this.getHeader(data.title)
        }
      ],
      columns: [],
      lotsFilters: [],
      lotsFiltersCodes: [
        'locality_from',
        'locality_to',
        'vehicle_type'
      ],
      lotsFiltersInit: false,
      isTenderEditable: true,
      initialFieldsTypes: {},
      fieldsStatuses: {},
      optionsTableSaveItem: {
        onSubmit: async (data) => {
          // Saving row data to store row uuid
          this.selectedData = data
          this.currentAction = 'edit'

          await this.onSave(data, false)
        }
      }
    }
  },
  computed: {
    ...mapGetters({
      tenderInfo: 'tenders/tenderInfo'
    })
  },
  created () {
    this.columns = this.initialColumns
  },
  methods: {
    ...mapActions({
      updateTenderLots: 'tenders/updateTenderLots'
    }),
    async fetchLots ({ query, sort, page, size }) {
      this.lotsData = await api.tenders.getLots(this.tenderInfo.uuid, this.tenderInfo.requestUuid, {
        params: {
          q: query,
          sort,
          size,
          page,
          filter: this.lotsFilters.length ? this.lotsFilters.reduce((filters, item) => {
            if (item.code === 'vehicle_type' && item.value === 'all') {
              return filters
            }

            if (item.value) {
              filters.push(`${item.headerUuid},${item.value}`)
            }

            return filters
          }, []).join(';') : ''
        }
      })

      if (!this.lotsData) {
        this.$notification({
          type: 'system_notification_error',
          message: 'Ошибка получения лотов'
        })

        return this.lotsData
      }

      // Лотов нет - обработку запускать не нужно
      if (!this.lotsData.lots.length) {
        return this.lotsData
      }

      if (!this.isColumnsModified) {
        this.columns = []

        // Получаю данные для выпадающих списков
        this.checklists = await api.tenders.getLotsChecklists(this.tenderInfo.requestUuid)

        let categories = []

        this.lotsData.lots[0].headers.map((header) => {
          const index = categories.findIndex((category) => {
            return category.title === header.category.title
          })

          // If items already contains in this category
          if (~index) {
            categories[index].items.push(header)
          } else {
            categories.push({
              ...header.category,
              items: [header]
            })
          }

          if (!this.lotsFiltersInit && this.userType === 'carrier') {
            if (this.lotsFiltersCodes.includes(header.code)) {
              this.lotsFilters.push({
                headerUuid: header.uuid,
                code: header.code,
                valueType: header.valueType,
                title: header.category.title,
                value: header.code === 'vehicle_type' ? 'all' : '',
                pos: this.lotsFiltersCodes.indexOf(header.code) // Чтобы сортировать фильтры в соответствии с порядком, указанным в 'lotsFiltersCodes'
              })
            }
          }
        })

        // Сортировка категорий
        categories.sort((a, b) => {
          return a.pos - b.pos
        })

        // Сортировка элементов в категории
        categories = categories.map((category) => {
          return {
            ...category,
            items: category.items.sort((a, b) => {
              return a.pos - b.pos
            })
          }
        })

        // Сортировка фильтров
        if (!this.lotsFiltersInit && this.userType === 'carrier') {
          this.lotsFilters.sort((a, b) => {
            return a.pos - b.pos
          })
        }

        const newColumns = []

        categories.forEach((category) => {
          category.items.forEach((item) => {
            const newColumn = {
              key: item.uuid,
              title: item.title,
              code: item.code, // Using for some inner logic
              headerHtml: (column) => this.getHeader(column.title, category.title, column.isRequired, !column.isEditColumn),
              highlight: (value) => {
                if (value) {
                  return
                }

                if (this.userType === 'carrier') {
                  switch (item.code) {
                    case 'price':
                      return this.isSomePriceFilled ? 'warning' : 'error'

                    case 'guaranteed_number_of_shipments':
                      return this.isSomePriceFilled ? 'warning' : null

                    default:
                      return item.isRequired ? 'error' : null
                  }
                } else {
                  return item.isRequired
                }
              },
              isEditColumn: !item.isDisabled && this.isTenderEditable,
              isDisabled: this.userType === 'carrier' ? item.isDisabled && item.code === 'price' : item.isDisabled,
              isClearable: true,
              isGrayedWhenDisabled: item.isDisabled,
              isAlignRight: ['INTEGER', 'FLOAT'].includes(item.valueType),
              valueType: item.valueType
            }

            switch (item.valueType) {
              case 'CHECKLIST':
              case 'CHECKLIST_LIST':
                newColumn.html = (column) => this.getChecklistValue(column, item.uuid)
                newColumn.items = this.getChecklist(item.uuid)

                if (item.valueType === 'CHECKLIST_LIST') {
                  newColumn.isCompactChecklist = true
                }
                break

              case 'BOOLEAN':
                if (this.userType === 'buyer') {
                  newColumn.isEditColumn = true // To showing checkboxes even if column is disabled
                  newColumn.isDisabled = item.isDisabled || !this.isTenderEditable
                } else {
                  newColumn.html = (column) => column[item.uuid] ? 'Да' : 'Нет'
                }

                break

              case 'FLOAT':
                newColumn.handler = (data) => Math.abs(data)
                break
            }

            // Запоминаем типы ('TEXT', 'INTEGER', ...)
            // Используется в случае, когда пользователь редактирует
            // лоты прямо в таблице, и для этого нужно при инициализации
            // запомнить типы полей
            this.initialFieldsTypes[item.uuid] = item.valueType
            // Запоминаем статусы, чтобы при сохранении черновика
            // лота не отправлять выключенные столбцы, то есть те
            // столбцы, которые пользователь не может редактировать
            this.fieldsStatuses[item.uuid] = item.isDisabled

            // Special case for https://oboz.myjetbrains.com/youtrack/issue/TND-1069
            if (this.userType === 'carrier' && ['desired_rate', 'consent_desired_rate', 'price'].includes(item.code)) {
              this.fieldsInfo[item.code] = item.uuid

              // Enabled editing for the 'price' fields for carriers
              if (item.code === 'price') {
                this.fieldsStatuses[item.uuid] = false
              }
            }

            newColumns.push(newColumn)
          })
        })

        if (this.userType === 'buyer' && this.isTenderEditable) {
          newColumns.push({ slotName: 'edit-row', width: 100, isToggleDisabled: true })
        }

        this.columns = this.initialColumns.concat(newColumns)

        this.isColumnsModified = true
        this.lotsFiltersInit = true
      }

      // Меняю формат данных для таблицы
      const lots = this.lotsData.lots.map((field) => {
        field.headers = field.headers.map((header) => {
          // TODO: move to backend
          // Changed format for 'CHECKLIST_LIST' values:
          // 'uuid,uuid,uuid' to ['uuid', 'uuid', 'uuid']
          return {
            ...header,
            value: header.valueType === 'CHECKLIST_LIST' && header.value ? header.value.split(',') : header.value
          }
        })

        return Object.assign({ pos: field.pos, uuid: field.uuid }, field.headers.reduce((obj, item) => Object.assign(obj, { [item.uuid]: item.value }), {}))
      })

      return {
        ...this.lotsData,
        lots
      }
    },
    getChecklist (columnUuid) {
      const checklist = this.checklists.headers.find((header) => {
        return header.uuid === columnUuid
      })

      if (checklist) {
        return checklist.options
      }

      return []
    },
    getChecklistValue (column, columnUuid) {
      const optionUuids = column[columnUuid]

      if (!optionUuids) {
        return ''
      }

      const checklist = this.checklists.headers.find(header => header.uuid === columnUuid)

      const options = checklist.options.reduce((result, option) => {
        if (optionUuids.includes(option.uuid)) {
          result.push(option.title)
        }

        return result
      }, [])

      return options ? options.join(', ') : ''
    },
    getHeader (title, subtitle = null, isRequired = false, isDisabled = false) {
      let result = title

      if (subtitle) {
        result = `<div class="text-gray">${subtitle}</div> ${result}`
      }

      if (isRequired && !isDisabled) {
        result += `<span class="table-header-required ${isDisabled ? 'disabled' : ''}">*</span>`
      }

      return result
    },
    // Limit maximum length for field to prevent overflow
    checkLength (value) {
      return value.length > 9 ? value.slice(0, 9) : value
    },
    getLotValues (data, types, uuidPropertyName = 'uuid') {
      return Object.entries(data).filter((field) => {
        switch (true) {
          case field[0] === 'uuid': // Remove unwanted columns
          case field[0] === 'pos':
          case this.fieldsStatuses[field[0]]: // Remove field which isn't available to editing
            return false

          default:
            return true
        }
      }).map((field) => {
        return {
          [uuidPropertyName]: field[0],
          value: this.getFieldByType(types[field[0]], field[1])
        }
      })
    },
    getFieldByType (type, value) {
      switch (type) {
        case 'FLOAT':
        case 'INTEGER':
          // 'value' can be equal to 0 - it's allowed, so checking for 'undefined'
          return value ? Number(this.checkLength(value)) : null

        case 'BOOLEAN':
          return Boolean(value)

        case 'TEXT':
          return value ? value : null

        case 'CHECKLIST':
          return typeof value !== 'undefined' ? value : null

        case 'CHECKLIST_LIST':
          return value?.length ? String(value) : null

        default:
          return value ? String(value) : null
      }
    }
  }
}
