import api from '@/api'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      questionnaireValue: [],
      files: [],
      fileUuids: []
    }
  },
  computed: {
    ...mapGetters({
      tenderInfo: 'tenders/tenderInfo'
    })
  },
  async created () {
    await this.getQuestionnaire()
  },
  methods: {
    async getQuestionnaire () {
      const { questionGroups, files } = await api.tenders.getQuestionnaire(this.tenderInfo.questionnaireUuid)

      this.questionnaireValue = questionGroups
      this.files = files

      if (this.files && this.files.length) {
        this.fileUuids = this.files.reduce((filtered, file) => {
          filtered.push(file.uuid)

          return filtered
        }, [])
      }

      // This actions is needed only for buyers
      if (this.currentRole === 'client') {
        this.storeQuestions()

        const userQuestionsGroup = this.questionnaireValue.find(group => group.isUserQuestionsAvailable)

        // Saving user question group uuid to sending it with user questions changes
        if (userQuestionsGroup) {
          this.isUserQuestionsEnabled = true
          this.userQuestionsGroupUuid = userQuestionsGroup.uuid
        }
      }
    }
  }
}
