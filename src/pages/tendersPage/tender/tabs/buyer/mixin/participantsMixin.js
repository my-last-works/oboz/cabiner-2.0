import { plural } from '@/helpers'

export default {
  data () {
    return {
      participantsGroups: [],
      isShowGroups: true
    }
  },
  watch: {
    isShowGroups () {
      // Reset selected uuids, because otherwise
      // checkboxes will be unchecked, but some participants are still
      // contains in selected uuid array
      this.selectedUuids = []
    }
  },
  methods: {
    plural,
    parseParticipants (data) {
      if (this.isShowGroups) {
        this.participantsGroups = []

        return {
          ...data,
          participants: data.groups.reduce((filtered, group, i) => {
            this.participantsGroups[i] = group.uuid

            return filtered.concat(group.participants.map(item => ({
              ...item,
              group: group.title || 'Без группы',
              checked: this.selectedUuids.includes(item.uuid)
            })))
          }, []).concat(
            data.participantsWithoutGroups.map(item => ({
              ...item,
              group: 'Без группы',
              checked: this.selectedUuids.includes(item.uuid)
            }))
          )
        }
      } else {
        return {
          ...data,
          participants: data.participants.map(item => ({
            ...item,
            checked: this.selectedUuids.includes(item.uuid)
          }))
        }
      }
    },
    onToggle (data, selectedUuids) {
      this.selectedUuids = selectedUuids
    },
    stringifyGroupsList (data) {
      if (!data.groups) {
        return null
      }

      const groups = data.groups.reduce((groups, item) => {
        groups.push(item.title)

        return groups
      }, [])

      if (groups.length) {
        return groups.map(date => `«${date}»`).join(', ')
      }
    }
  }
}
