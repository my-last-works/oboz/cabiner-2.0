export const rfq_columns = {
  all: [
    {
      title: '№ лота',
      key: 'lotPos',
      isAlignRight: true,
      exactWidth: '61px'
    }
  ],
  lots: [
    {
      title: '№ лота',
      key: 'pos',
      isAlignRight: true,
      exactWidth: '61px'
    },
    {
      title: 'Ненул. ставок',
      key: 'nonZeroBidsCount',
      isAlignRight: true
    },
    {
      title: 'Предл. с гарант. объемом',
      key: 'withGuaranteedPurchaseVolume',
      isAlignRight: true
    },
    {
      title: 'Индик-р дефицита',
      key: 'deficitIndicator',
      isAlignRight: true
    },
    {
      title: 'Min ставка',
      key: 'minBid',
      isAlignRight: true
    },
    {
      title: 'Max ставка',
      key: 'maxBid',
      isAlignRight: true
    },
    {
      title: 'Средняя ставка',
      key: 'averageBid',
      isAlignRight: true
    },
    {
      title: 'Вес лота, %',
      key: 'lotWeight',
      isAlignRight: true
    }
  ],
  contractors: [
    {
      title: 'Поставщик',
      key: 'title',
      withFilter: true
    },
    {
      title: 'Участвует в лотах',
      key: 'partTakeLotsCount',
      isAlignRight: true
    },
    {
      title: 'Не участвует в лотах',
      key: 'noPartTakeLotsCount',
      isAlignRight: true
    },
    {
      title: 'Лотов с гар. объемом',
      key: 'guaranteedPurchaseVolumeLotsCount',
      isAlignRight: true
    },
    {
      title: 'Дефицитных лотов',
      key: 'deficitLotsCount',
      isAlignRight: true
    }
  ]
}