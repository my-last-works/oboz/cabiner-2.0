export const scenario_columns = {
  all: [
    {
      title: '№ лота',
      key: 'number',
      isAlignRight: true,
      exactWidth: '61px'
    },
    {
      title: 'Откуда',
      key: 'fromPlaceTitle',
      withFilter: true,
      headerHtml: () => `<div class="text-gray">Откуда</div>Описание`
    },
    {
      title: 'Куда',
      key: 'toPlaceTitle',
      withFilter: true,
      headerHtml: () => `<div class="text-gray">Куда</div>Описание`
    },
    {
      title: 'Ресурс',
      key: 'resourceTypeTitle',
      headerHtml: () => `<div class="text-gray">Ресурс</div>Тип ТС`
    },
    {
      title: 'Грузопод., т',
      key: 'carrying',
      isAlignRight: true
    },
    {
      title: 'Поставщик',
      key: 'carrierTitle',
      withFilter: true
    },
    {
      title: 'Гарант. объемы',
      key: 'carrierCarriageCount',
      isAlignRight: true
    },
    {
      title: 'Ставка, пост-ка ₽',
      key: 'bid',
      isAlignRight: true
    },
    {
      title: 'Квота в лоте',
      key: 'carrierCarriageQuote',
      isAlignRight: true,
      html: ({ carrierCarriageQuote }) => `<div class="highlighted-td"><span>${carrierCarriageQuote}</span></div>`
    },
    {
      title: 'Сумма затрат в лоте, ₽',
      key: 'cost',
      isAlignRight: true,
      html: ({ cost }) => `<div class="highlighted-td"><span>${cost}</span></div>`
    },
    {
      title: 'Доля в объеме, %',
      key: 'percentInTotalVolume',
      isAlignRight: true,
      html: ({ percentInTotalVolume }) => `<div class="highlighted-td"><span>${percentInTotalVolume}</span></div>`
    },
    {
      title: 'Доля в затр., %',
      key: 'partInTotalCosts',
      isAlignRight: true,
      html: ({ partInTotalCosts }) => `<div class="highlighted-td"><span>${partInTotalCosts}</span></div>`
    }
  ],

  lots: [
    {
      title: '№ лота',
      key: 'number',
      isAlignRight: true,
      exactWidth: '61px'
    },
    {
      title: 'Откуда',
      key: 'fromPlaceTitle',
      withFilter: true,
      headerHtml: () => `<div class="text-gray">Откуда</div>Описание`
    },
    {
      title: 'Куда',
      key: 'toPlaceTitle',
      withFilter: true,

      headerHtml: () => `<div class="text-gray">Куда</div>Описание`
    },
    {
      title: 'Ресурс',
      key: 'resourceTypeTitle',
      headerHtml: () => `<div class="text-gray">Ресурс</div>Тип ТС`
    },
    {
      title: 'Грузопод., т',
      key: 'carrying',
      isAlignRight: true
    },
    {
      title: 'Индик-р дефицита',
      key: 'deficitIndicator',
      isAlignRight: true
    },
    {
      title: 'Min ставка',
      key: 'minBid',
      isAlignRight: true
    },
    {
      title: 'Max ставка',
      key: 'maxBid',
      isAlignRight: true
    },
    {
      title: 'Средняя ставка',
      key: 'averageBid',
      isAlignRight: true
    },
    {
      title: 'Вес лота, %',
      key: 'lotWeight',
      isAlignRight: true
    },
    {
      title: 'Выбрано пост-в в лоте',
      key: 'carrierCount',
      isAlignRight: true,
      html: ({ carrierCount }) => `<div class="highlighted-td"><span>${carrierCount}</span></div>`
    },
    {
      title: 'Сумма затрат по лоту, ₽',
      key: 'lotCost',
      isAlignRight: true,
      html: ({ lotCost }) => `<div class="highlighted-td"><span>${lotCost}</span></div>`
    }
  ],

  contractors: [
    {
      title: 'Поставщик',
      key: 'title',
      withFilter: true
    },
    {
      title: 'Участвует в лотах',
      key: 'partTakeLotsCount',
      isAlignRight: true
    },
    {
      title: 'Не участвует в лотах',
      key: 'noPartTakeLotsCount',
      isAlignRight: true
    },
    {
      title: 'Лотов с гар. объемом',
      key: 'guaranteedPurchaseVolumeLotsCount',
      isAlignRight: true
    },
    {
      title: 'Дефицитных лотов',
      key: 'deficitLotsCount',
      isAlignRight: true
    },
    {
      title: 'Квота во всех лотах',
      key: 'carriageQuoteSum',
      isAlignRight: true,
      html: ({ carriageQuoteSum }) => `<div class="highlighted-td"><span>${carriageQuoteSum}</span></div>`
    },
    {
      title: 'Сумма затрат на все лоты, ₽',
      key: 'costSum',
      isAlignRight: true,
      html: ({ costSum }) => `<div class="highlighted-td"><span>${costSum}</span></div>`
    },
    {
      title: 'Доля в общем объеме, %',
      key: 'partInTotalVolume',
      isAlignRight: true,
      html: ({ partInTotalVolume }) => `<div class="highlighted-td"><span>${partInTotalVolume}</span></div>`
    },
    {
      title: 'Доля в общих затратах, %',
      key: 'partInTotalCosts',
      isAlignRight: true,
      html: ({ partInTotalCosts }) => `<div class="highlighted-td"><span>${partInTotalCosts}</span></div>`
    }
  ]
}

export const modal_columns = {
  lots: [
    {
      title: 'Поставщик',
      key: 'title',
      withFilter: true
    },
    {
      title: 'Гарант. объемы',
      key: 'carriageCount',
      isAlignRight: true
    },
    {
      title: 'Ставка пост-ка, ₽',
      key: 'bid',
      isAlignRight: true
    },
    {
      title: 'Квота в лоте',
      key: 'carriageQuote',
      isAlignRight: true,
      html: ({ carriageQuote }) => `<div class="highlighted-td"><span>${carriageQuote}</span></div>`
    },
    {
      title: 'Сумма затрат в лоте, ₽',
      key: 'cost',
      isAlignRight: true,
      html: ({ cost }) => `<div class="highlighted-td"><span>${cost}</span></div>`
    },
    {
      title: 'Доля в объеме, %',
      key: 'partInTotalVolume',
      isAlignRight: true,
      html: ({ partInTotalVolume }) => `<div class="highlighted-td"><span>${partInTotalVolume}</span></div>`
    },
    {
      title: 'Доля в затратах, %',
      key: 'partInTotalCosts',
      isAlignRight: true,
      html: ({ partInTotalCosts }) => `<div class="highlighted-td"><span>${partInTotalCosts}</span></div>`
    },
  ],

  contractors: [
    {
      title: '№ лота',
      key: 'number',
      isAlignRight: true,
      exactWidth: '61px'
    },
    {
      title: 'Откуда',
      key: 'fromPlaceTitle',
      withFilter: true,
      headerHtml: () => `<div class="text-gray">Откуда</div>Описание`
    },
    {
      title: 'Куда',
      key: 'toPlaceTitle',
      withFilter: true,
      headerHtml: () => `<div class="text-gray">Куда</div>Описание`
    },
    {
      title: 'Ресурс',
      key: 'resourceTypeTitle',
      headerHtml: () => `<div class="text-gray">Ресурс</div>Тип ТС`
    },
    {
      title: 'Грузопод., т',
      key: 'carriageCount',
      isAlignRight: true
    },

    {
      title: 'Гарант. объемы',
      key: 'carrierCarriageCount',
      isAlignRight: true
    },
    {
      title: 'Ставка пост-ка, ₽',
      key: 'bid',
      isAlignRight: true
    },
    {
      title: 'Квота в лоте',
      key: 'carrierCarriageQuote',
      isAlignRight: true,
      html: ({ carrierCarriageQuote }) => `<div class="highlighted-td"><span>${carrierCarriageQuote}</span></div>`
    },
    {
      title: 'Сумма затрат в лоте, ₽',
      key: 'cost',
      isAlignRight: true,
      html: ({ cost }) => `<div class="highlighted-td"><span>${cost}</span></div>`
    },
    {
      title: 'Доля в объеме, %',
      key: 'percentInTotalVolume',
      isAlignRight: true,
      html: ({ percentInTotalVolume }) => `<div class="highlighted-td"><span>${percentInTotalVolume}</span></div>`
    },
    {
      title: 'Доля в затр., %',
      key: 'partInTotalCosts',
      isAlignRight: true,
      html: ({ partInTotalCosts }) => `<div class="highlighted-td"><span>${partInTotalCosts}</span></div>`
    }
  ]
}