import ObModal from "common-components/src/obComponents/obModal";

export default {
  components: {
    ObModal
  },
  props: {
    tenderUuid: {
      type: String,
      required: true
    },
    scenarioUuid: {
      type: String,
      required: true
    }
  },
  methods: {
    open() {
      this.$refs.modal.open();
    },
    close() {
      this.$refs.modal.close();
    }
  }
}