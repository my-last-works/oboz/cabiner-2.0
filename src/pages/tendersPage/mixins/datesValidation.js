import ObDatepicker from 'common-components/src/obComponents/obDatePickerConstructor'

/*
  Checking for 'isEditable === false', because 'isEditable' property
  is available only in tender pages, but this mixin is using for tender
  creation via 'tendersPage/modal/add.vue' modal window
 */

export default {
  components: {
    ObDatepicker
  },
  data () {
    return {
      datepickers: [],
      formData: {},
      validationMessages: {
        summarizingDt: 'Дата окончания подведения итогов не может наступить раньше даты завершения сбора предложений',
        endContractDt: 'Дата окончания исполнения договора не может наступить раньше даты начала исполнения договора',
        general: 'Выбранная дата не может быть раньше текущей даты'
      }
    }
  },
  computed: {
    disabledDate () {
      const today = new Date()
      const yesterday = today.setDate(today.getDate() - 1)

      return [
        {
          from: new Date(1976, 0, 0),
          to: yesterday
        }
      ]
    },
    isSummarizingDtValid () {
      if (this.isEditable === false || !this.formData.targetFinishDt || !this.formData.summarizingDt) {
        return
      }

      if (!this.isDateValid('summarizingDt')) {
        return false
      }

      return new Date(this.formData.summarizingDt).getTime() - new Date(this.formData.targetFinishDt).getTime() >= 0
    },
    isEndContractDtValid () {
      if (!this.isEditable || !this.formData.startContractDt || !this.formData.endContractDt) {
        return
      }

      if (!this.isDateValid('endContractDt')) {
        return false
      }

      return new Date(this.formData.endContractDt).getTime() - new Date(this.formData.startContractDt).getTime() > 0
    },
    isAllDatesValid () {
      return this.isSummarizingDtValid !== false &&
             this.isEndContractDtValid !== false &&
             this.datepickers.every(item => item[1].value && this.isDateValid(item[0]))
    }
  },
  mounted () {
    this.datepickers = Object.entries(this.$refs).filter(item => item[0].includes('Dt'))

    if (this.isEditable !== false) {
      // Set 'dirty' on mounted() to showing 'error' state
      // for all datepickers with prefilled dates from API
      this.datepickers.forEach(item => item[1].dirty = !!item[1].value)
    }
  },
  methods: {
    isDateValid (component) {
      if (this.isEditable === false) {
        return
      }

      return !this.formData[component] || new Date(this.formData[component]).getTime() > Date.now()
    },
    getValidationMessage (component = null) {
      // 'general' error message is priority, so always checking for it earlier
      if (!component || !this.isDateValid(component)) {
        return this.validationMessages['general']
      } else if (component in this.validationMessages) {
        return this.validationMessages[component]
      }
    },
    setDateState (component) {
      // If don't set datepicker 'dirty' property to 'true'
      // at @open (for dropdown) or @focus (for manually input),
      // 'error' state will not be appearing without $nextTick()
      this.$refs[component].dirty = true
    }
  }
}