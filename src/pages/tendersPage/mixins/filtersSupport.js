export default {
  data () {
    return {
      tableParameters: {},
      filtersHandler: ({ filters }) => {
        // Remove deleted filters
        for (let key in this.tableParameters) {
          if (!filters.find(filter => filter.key === key)) {
            delete this.tableParameters[key]
          }
        }

        // https://oboz.myjetbrains.com/youtrack/issue/TND-1351
        return filters.filter((filter) => {
          if (
            ['fromPlaceTitle', 'toPlaceTitle', 'carrierTitle'].includes(filter.key) ||
            ['ByCarriers', 'contractors'].includes(this.viewFilter)
          ) {
            // Keys for filtering are different
            const KEYS_MAP = {
              fromPlaceTitle: 'localityFrom',
              toPlaceTitle: 'localityTo',
              carrierTitle: 'carrierTitle',
              title: 'carrierTitle'
            }

            this.$set(this.tableParameters, KEYS_MAP[filter.key], filter.value || null)

            return false
          }

          return true
        })
      }
    }
  },
  watch: {
    viewFilter () {
      this.tableParameters = {}
    }
  }
}
