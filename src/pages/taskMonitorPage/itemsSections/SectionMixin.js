// Нужно для нового меню itit для новых url ресурсов
import { getTokenData } from '@/tokenData'
import SectionHeading from './components/SectionHeading'
import SectionTab from './components/SectionTab'
import { mapGetters } from 'vuex'
import api from '@/api'

export default {
  components: {
    SectionHeading,
    SectionTab
  },
  props: {
    task: {
      type: Object,
      default: () => {}
    },
    taskInfo: {
      type: Object,
      default: () => {}
    }
  },
  data () {
    return {
      isLoading: false,
      isHeaderLoading: true,
      headerData: null,
      headerSections: [],
      userRole: null
    }
  },
  computed: {
    ...mapGetters({
      userUuid: 'user/userUuid'
    }),
    isNewTask () {
      return this.task.status.toLowerCase() === 'new'
    },
    isParticipant () {
      return this.taskInfo.role.toLowerCase() === 'participant'
    },
    currentKey: ({ taskInfo }) => {
      return taskInfo.role.toLowerCase() === 'participant' ? 'Participant' : 'Resource'
    },
    // Нужно для нового меню itit для новых url ресурсов
    isCarrier: ({ userRole }) => {
      return userRole === 'carrier'
    },
    isShowLoader () {
      return this.isLoading || this.isHeaderLoading
    }
  },
  async mounted () {
    // Нужно для нового меню itit для новых url ресурсов
    const { contractor_role_code } = { ...getTokenData() }
    this.userRole = contractor_role_code

    try {
      if (this.isParticipant) {
        const { data } = await api.taskMonitor.getExistingParticipantFields(this.task.card_uuid)

        this.headerData = data?.domains[0] || {}

        this.headerData?.participantInfo?.forEach((row) => {
          this.headerSections.push({
            title: row.fieldTitle,
            value: this.getUserReadableValue(row.fieldValue.data.value),
            uuid: row.fieldUuid,
            position: row.position || row.fieldPos
          })
        })
      } else {
        const { data } = await api.taskMonitor.getResourceTaskFields(this.task.uuid)

        this.headerData = data

        if (this.headerData.resourceInfoDto) {
          const info = this.headerData.resourceInfoDto

          const getValue = (value) => {
            return value || 'Неопределен'
          }

          this.headerSections = [
            {
              title: 'Класс',
              value: getValue(info.class_title)
            },
            {
              title: 'Код страны',
              value: getValue(this.getUserReadableValue(info.country_code))
            },
            {
              title: 'Вид',
              value: getValue(info.species_title)
            },
            {
              title: info.class_title === 'Человеческий ресурс' || info.class_title === 'Персонал' ? 'Номер паспорта' : 'Рег. номер',
              value: getValue(info.class_title)
            },
            {
              title: 'Тип',
              value: getValue(info.type_title)
            }
          ]

          if (this.headerData.ownerTitle) {
            this.task.ownerTitle = this.headerData.ownerTitle
          }

          if (this.headerData.ownerInn) {
            this.headerSections.push({
              title: 'ИНН владельца',
              value: this.headerData.ownerInn
            })
          }
        }
      }
    } catch (e) {
      console.error(e)

      this.$notification({
        type: 'system_notification_error',
        message: 'Ошибка загрузки данных'
      })
    }

    this.isHeaderLoading = false
  },
  methods: {
    onClose () {
      this.$emit('getTasks')
    },
    onTaskSuccess() {
      this.$notification({
        type: 'system_notification_success',
        message: 'Задача решена и перенесена в Архив'
      })
    },
    getUserReadableValue (value) {
      switch (value) {
        case 'rus':
          return 'Российская Федерация'

        default:
          return value
      }
    }
  }
}
