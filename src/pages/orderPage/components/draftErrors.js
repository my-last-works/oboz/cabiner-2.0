class DraftErrors {
  constructor() {
    this.errors = {
      'CARGO_ERRORS': null,
      'CONTRACTOR_ERRORS': null,
      'RISKS_ERRORS': null
    }
  }

  has(field) {
    return this.errors.hasOwnProperty(field);
  }

  any () {
    return Object.keys(this.errors).length > 0;
  }

  get(field) {
    if (this.errors[field]) {
      return this.errors[field]
    }
  }

  record(field, error) {
    this.errors[field] = error;
  }

  clear(field) {
    if (field) {
      this.errors[field] = null;
      return;
    }

    this.errors = {
      'CARGO_ERRORS': null,
      'CONTRACTOR_ERRORS': null,
      'RISKS_ERRORS': null,
    };
  }
}

export default DraftErrors;