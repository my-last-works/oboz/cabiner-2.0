import Vue from 'vue';

const state = Vue.observable({ 
  order: null,
  statusesMap: {
    'NEW': 'Принят в исполнение',
    'IN_PROCESS': 'Поехал',
    'PROCESSED': 'Исполнен',
    'CANCELLED': 'Отменён',
    'INVOICING': 'Инвойсирование',
    'INVOICED': 'Инвойсирован',
    'PAY_PROCESS': 'В процессе оплаты',
    'PAID': 'Оплачен',
  },
});

export const setState = (data) => {
  state.order = data ? {...data } : null;
};

export default state;