
import api from '@/api';

export default class ResourceTypeStream {
  constructor() {
    this.masks = []
    this.species = []
    this.types = []
    this.result = []
    this.pageNum = 0;
    this.pageSize = 0;
    this.totalElements = 0;
    this.totalPages = 0;
  }
  async loadData (pageNum, pageSize) {
    let res = await api.resources.getPatternMasks(0, 100, '');
    if (!res || res.error) {
        throw new Error('Ошибка загрузки масок шаблонов');
    } else {
        this.masks = res.data.content;
    }
    res = await api.resources.getResourceSpecies({page: pageNum, size: pageSize, query: '', sort: ''});
    if (!res || res.error) {
        throw new Error('Ошибка загрузки видов ресурсов');
    } else {
        this.species = res.content;
        this.pageNum = res.page;
        this.pageSize = res.size;
        this.totalElements = res.totalElements;
        this.totalPages = res.totalPages;
    }
    res = await api.resources.getResourceTypes(0, 1000);
    if (!res || res.error) {
        throw new Error('Ошибка загрузки типов ресурсов');
    } else {
        this.types = res.data.content;
    }
    this.species.forEach(({ uuid, name, isActive, resourceSubTypes }) => {
      this.result.push({
        uuid,
        title: name,
        isActive,
        _isOpen: false,
        parent: undefined
      })
      if (resourceSubTypes && resourceSubTypes.length) {
        resourceSubTypes.forEach(({uuid, title, specieUuid, maskUuid}) => {
          this.result.push({
            uuid,
            title,
            parent: specieUuid,
            mask: maskUuid,
            maskTitle: title,
            resourceSubTypeUuid: uuid,
            isMask: false
          })
        })
      }
    })



    this.types.forEach(({ uuid, resourceSubTypeDTO, resourcePatternDTO }) => {
      let spec = this.result.find(item => item.uuid === resourceSubTypeDTO.uuid && !item.isMask)
      if(resourcePatternDTO){
        let _mask
        if (this.masks.find(i => i.uuid === resourceSubTypeDTO.maskUuid)){
          _mask =  this.masks.find(i => i.uuid === resourceSubTypeDTO.maskUuid).mask
          for (let key in  resourcePatternDTO.parameters) {
            _mask = _mask.replaceAll(key, resourcePatternDTO.parameters[key] || '');
          }
        }

        if(spec) {
          this.result.push({
            ...spec,
            ...{ maskTitle: spec.title + ' ' + _mask,
              isMask: true,
              typeUuid: uuid,
              resourcePatternUuid: resourcePatternDTO.uuid
            }
          })
        }
      }
    })



    this.types.forEach(({ resourceSubTypeDTO }) => {
      let spec = this.result.find(item => item.uuid === resourceSubTypeDTO.uuid && !item.isMask)
      if (spec) {
        spec.parent = false
      }
    })
    return this.result;
  }
  getMasks() {
    return this.masks;
  }
  async getResources() {
    if (this.result.length === 0) {
      await this.loadData()
      return this.result
    }
    return this.result
  }
  getPagination() {
    return {
      pageNum: this.pageNum,
      pageSize: this.pageSize,
      totalElements: this.totalElements,
      totalPages: this.totalPages
    }
  }
}
