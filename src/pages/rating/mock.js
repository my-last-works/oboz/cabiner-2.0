export const indicators = [
  {
    uuid: '3b1dc5b9-e050-4335-a842-61c635053c45',
    startAddress: "Москва",
    endAddress: "Санкт-Петербург",
    number: 'PO20 714 8308',
    completionDt: '2021-01-01T06:12:17.982Z',
    shipmentPointsCount: 2,
    price: '+0,55',
    person: 'Иванов Сергей Петрович'
  },
  {
    uuid: '3b1dc5b9-e050-4335-a842-61c635053c41',
    startAddress: "Москва",
    endAddress: "Тверь",
    number: 'PO20 714 3311',
    completionDt: '2020-04-05T06:12:17.982Z',
    shipmentPointsCount: null,
    price: '-0,44',
    person: 'Николаев Роман Сергеевич'
  },
  {
    uuid: '3b1dc5b9-e050-4335-a842-61c635053c42',
    startAddress: "Тверь",
    endAddress: "Санкт-Петербург",
    number: 'PO20 714 8678',
    completionDt: '2020-11-12T06:12:17.982Z',
    shipmentPointsCount: 3,
    price: '+0,11',
    person: 'Дмитриев Андрей Владимирович'
  },
  {
    uuid: '3b1dc5b9-e050-4335-a842-61c635053c453',
    startAddress: "Москва",
    endAddress: "Тверь",
    number: 'PO20 714 5123',
    completionDt: '2020-04-10T06:12:17.982Z',
    shipmentPointsCount: null,
    price: '+0,23',
    person: 'Дмитриев Андрей Владимирович'
  },
  {
    uuid: '3b1dc5b9-e050-4335-a842-61c635053c44',
    startAddress: "Тверь",
    endAddress: "Москва",
    number: 'PO20 714 8312',
    completionDt: '2020-07-05T06:12:17.982Z',
    shipmentPointsCount: null,
    price: '-0,09',
    person: 'Дмитриев Андрей Владимирович'
  }
];