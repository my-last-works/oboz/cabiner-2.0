export default {
  methods: {
    transformRubles(sum) {
      sum = String(sum);
      const integer = Number(sum.slice(0, -2));
      return Math.round('.' + sum.substring(sum.length - 2)) ? integer + 1 : integer
    }
  }
}