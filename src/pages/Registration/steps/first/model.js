import { required, requiredIf, email, helpers } from 'vuelidate/lib/validators'

const checkLength = (value, { countrySymbolCode }) => {
  let length

  switch (countrySymbolCode) {
    case 'rus':
      length = [10, 12]
      break

    case 'kaz':
      length = [12, 14]
      break

    case 'blr':
      length = [9]
      break

    default:
      return true
  }

  return Boolean(!value || (value && length.includes(value.length)))
}

const checkedAcceptTerms = (value) => {
  return value
}

export default {
  email: {
    value: '',
    validator: {
      required,
      email: {
        method: email,
        error: 'Неверный адрес электронной почты'
      }
    }
  },
  phone: {
    value: '',
    validator: {
      required
    }
  },
  countrySymbolCode: {
    value: 'rus',
    validator: {
      required
    }
  },
  legalNameShort: {
    value: '',
    validator: {
      required: requiredIf(({ countrySymbolCode, card }) => {
        return ['blr', 'kaz'].includes(countrySymbolCode) || (Boolean(Object.keys(card).length) && !card.name)
      })
    }
  },
  inn: {
    value: '',
    validator: {
      minLength: {
        method: checkLength,
        error: 'Некорректный номер'
      }
    }
  },
  card: {
    value: {},
    nested_fields: {
      kpp: {
        validator: {
          required: requiredIf(({ kpp }) => {
            return typeof kpp === 'string'
          }),
          minLength: {
            method: value => !helpers.req(value) || value?.length === 9,
            error: 'КПП должен быть длиной 9 цифр'
          },
          isUnique: {
            method: function () {
              return !this.$parent.isKppExist
            },
            error: 'Такой КПП уже зарегистрирован, укажите другой'
          },
          isChecked: function (value) {
            return !helpers.req(value) || this.$parent.isKppExist === false
          }
        }
      }
    }
  },
  isAcceptTerms: {
    value: false,
    type: 'boolean',
    validator: {
      serviceDateAfter: {
        method: checkedAcceptTerms
      }
    }
  }
}
