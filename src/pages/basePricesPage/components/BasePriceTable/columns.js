export default [
  {
    title: 'Тип и характеристики ресурса',
    key: 'type',
    sortDisable: true,
    width: '500',
    slotName: 'type',
  },
  {
    title: 'Ед. Изм.',
    key: 'unit',
    slotName: 'unit',
    width: '35',
    sortDisable: true,
  },
  {
    title: 'Фиксированный тариф, ₽(Без НДС)',
    key: 'fix',
    slotName: 'fixed',
    width: '75',
    sortDisable: true,
  },
  {
    title: 'Усредненный тариф, ₽(Без НДС)',
    key: 'averaged_price',
    slotName: 'avg',
    width: '75',
    sortDisable: true,
  },
  {
    title: 'Экспертный тариф, ₽(Без НДС)',
    key: 'expert',
    slotName: 'expert',
    width: '75',
    sortDisable: true,
  },
  {
    title: 'Базовая цена, ₽(Без НДС)',
    key: 'base',
    slotName: 'base',
    width: '75',
    sortDisable: true,
  },
  {
    title: 'Достоверность БЦ',
    key: 'percentage',
    slotName: 'trust',
    width: '50',
    sortDisable: true,
  },
  //   {
  //       title: 'История/Экспертная оценка',
  //       key: 'history',
  //       slotName: 'progress',
  //       width: '75'
  //   },
];
