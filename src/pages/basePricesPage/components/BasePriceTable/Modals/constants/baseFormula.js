import { formatKopeykiToRubli } from '@/pages/basePricesPage/utils';

export default function formula({
  coef_a,
  averaged_price,
  coef_b,
  expert_price,
  season_coef,
  inflation_rate,
  week_number,
  base_price_fact,
}) {
  return [
    {
      value: coef_a ? coef_a.toString().replace('.', ',') : '0,0',
      tipBelow: 'Коэффициент доверия α',
      tipPositionClass: 'align-left',
      tipSizeClass: 'narrow',
    },
    {
      value: '×',
      isOperator: true,
    },
    {
      // value: formatKopeykiToRubli(averaged_price.price_stage),
      value: formatKopeykiToRubli(averaged_price),
      currency: '₽',
      tipAbove: 'УСРЕДНЕННЫЙ ТАРИФ',
      tipPositionClass: 'align-center',
      tipSizeClass: 'narrow',
      isBold: true,
    },
    {
      value: '+',
      isOperator: true,
    },
    {
      value: coef_b ? coef_b.toString().replace('.', ',') : '0,0',
      tipBelow: 'Коэффициент доверия β',
      tipPositionClass: 'align-left',
      tipSizeClass: 'narrow',
    },
    {
      value: '×',
      isOperator: true,
    },
    {
      value: formatKopeykiToRubli(expert_price),
      currency: '₽',
      tipAbove: 'ЭКСПЕРТНЫЙ ТАРИФ',
      tipPositionClass: 'align-left',
      tipSizeClass: 'narrow',
      isBold: true,
    },
    {
      value: '×',
      isOperator: true,
    },
    {
      value: season_coef ? season_coef.toString().replace('.', ',') : '0,0',
      tipBelow: 'Календарный коэффициет',
      tipPositionClass: 'align-left',
      tipSizeClass: 'narrow',
    },
    {
      value: '(',
      isOperator: true,
    },
    {
      value: '100%',
    },
    {
      value: inflation_rate < 0 ? '-' : '+',
      isOperator: true,
    },
    {
      value: (inflation_rate ? Math.abs(inflation_rate).toString().replace('.', ',') : '0,0') + '%',
      tipAbove: 'Процент инфляции',
      tipPositionClass: 'align-center',
      tipSizeClass: 'narrow',
    },
    {
      value: '×',
      isOperator: true,
    },
    {
      value: week_number || 0,
      tipBelow: 'номер недели после ввода экспертного тарифа',
      tipPositionClass: 'align-left',
      tipSizeClass: 'narrow',
    },
    {
      value: '/',
      isOperator: true,
    },
    {
      value: '52',
    },
    {
      value: ')',
      isOperator: true,
    },
    {
      value: '≈',
      isOperator: true,
    },
    {
      value: formatKopeykiToRubli(base_price_fact),
      currency: '₽',
      tipAbove: 'Базовая цена',
      tipPositionClass: 'align-left',
      tipSizeClass: 'narrow',
      valueSize: 'big',
      isBold: true,
    },
  ];
}
