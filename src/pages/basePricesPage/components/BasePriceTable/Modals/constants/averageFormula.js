import { formatKopeykiToRubli } from '@/pages/basePricesPage/utils';

export default function averageRateFormula({
  averaged_price_fact,
  coef_a,
  coef_b,
  coef_c,
  distant_price,
  nearest_price,
  recent_price,
}) {
  let coefA = +(+coef_a).toFixed(3);
  let coefB = +(+coef_b).toFixed(3);
  let coefC = +(+coef_c).toFixed(3);

  // sum of coefficients must be 1. With code below in formula this statement will be true
  const coefSum = coefA + coefB + coefC;
  const coefDiff = coefSum - 1;

  coefB += coefDiff;

  return [
    {
      value: coefC.toString().replace('.', ','),
      tipBelow: 'Коэффициент актуальности c',
      tipPositionClass: 'align-center',
      tipSizeClass: 'narrow',
    },
    {
      value: '×',
      isOperator: true,
    },
    {
      value: formatKopeykiToRubli(distant_price),
      currency: '₽',
      tipAbove: 'Среднее по давним тарифам',
      tipPositionClass: 'align-right',
      tipSizeClass: 'narrow',
      isBold: true,
    },
    {
      value: '+',
      isOperator: true,
    },
    {
      value: coefB.toString().replace('.', ','),
      tipBelow: 'Коэффициент актуальности b',
      tipPositionClass: 'align-center',
      tipSizeClass: 'narrow',
    },
    {
      value: '×',
      isOperator: true,
    },
    {
      value: formatKopeykiToRubli(recent_price),
      currency: '₽',
      tipAbove: 'Среднее по недавним тарифам',
      tipPositionClass: 'align-right',
      tipSizeClass: 'narrow',
      isBold: true,
    },
    {
      value: '+',
      isOperator: true,
    },
    {
      value: coefA.toString().replace('.', ','),
      tipBelow: 'Коэффициент актуальности a',
      tipPositionClass: 'align-center',
      tipSizeClass: 'narrow',
    },
    {
      value: '×',
      isOperator: true,
    },
    {
      value: formatKopeykiToRubli(nearest_price),
      currency: '₽',
      tipAbove: 'Среднее по ближайшим тарифам',
      tipPositionClass: 'align-right',
      tipSizeClass: 'narrow',
      isBold: true,
    },
    {
      value: '≈',
      isOperator: true,
    },
    {
      value: formatKopeykiToRubli(averaged_price_fact),
      currency: '₽',
      tipAbove: 'Усредненый тариф',
      tipPositionClass: 'align-left',
      tipSizeClass: 'narrow',
      valueSize: 'big',
      isBold: true,
    },
  ];
}
