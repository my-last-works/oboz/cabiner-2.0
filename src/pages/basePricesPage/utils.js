// IMPORTANT!
// This file should be moved to global utils later
// I'm not doing this now because there are possible problems with merging
// I'm going to do that right before merging RC_test1 to master branch

import numberWithSpaces from '@/utils/formatNumber';

export const formatKopeykiToRubli = (value, divider = 100) => {
  return numberWithSpaces(value, divider).replace('.', ',');
};
