// способы перевозки
export const transportationTypes = [
    {
        label: 'Автомашины',
        value: 'FTL',
    },
    {
        label: 'Контейнеры',
        value: 'CONTAINER',
    },
    {
        label: 'Вагоны',
        value: 'RAIL',
    },
];

// Статусы загрузки файла
export const fileStatus = {
    LOAD: 'Загрузка',
    LOADED: 'Загружен',
    ACTIVATION: 'Активация',
    ACTIVATED: 'Активирован',
};
