export default {
  /**
   * тип заказа - букинг и аукцион
   */
  AUCTION: 'auction',
  BOOKING: 'booking',
  OFFER: 'offer',
  /**
   * текущие состояние ставки аукциона currentState
   * WIN - выигрывает
   * LOSING - проигрывает
   * NO_BIDS - не делал ставки
   * текущие состояние аукциона lotStatus
   * ACTIVE - у участника есть выбор, участвовать или нет и позволяет делать ставки
   * COMPLETED - ставки сделаны, ожидает решение
   * CANCELLED - мы отменили участие в аукционе
   */
  WIN: 'WIN',
  LOSING: 'LOSING',
  NO_BIDS: 'NO_BIDS',
  /**
   * статусы типа заказа status
   * ACTIVE - аукцион (или букинга) начался, время аукциона (или букинга) еще не закончилось
   * COMPLETED - время аукциона закончилось, подводятся итоги
   * CANCELLED - аукцион (или букинг) отменен
   * ACCEPTED - участник согласился участвовать в букинга (время еще не закончилось)
   * REJECTED - участник отклонил букинг
   * WINNER - участник - победитель букинга
   * LOSER - букинг проигран
   * IN_PROCESS - в процессе перевозки
   * PROCESSED - в статусе, когда выиграл и завершилось (своего рода архив)
   * FAILED - отменили после назначения аукнциона или букинга
   */
  ACTIVE: 'ACTIVE',
  COMPLETED: 'COMPLETED',
  CANCELLED: 'CANCELLED',
  ACCEPTED: 'ACCEPTED',
  REJECTED: 'REJECTED',
  WINNER: 'WINNER',
  LOSER: 'LOSER',
  IN_PROCESS: 'IN_PROCESS',
  PROCESSED: 'PROCESSED',
  FAILED: 'FAILED',
  /**
   * статус заказа в случае выиграша orderStatus
   * NEW - ожидаем решения по аукциону (или букингу)
   * ON_DOER_SEARCH - тоже, что и статус NEW по аукциону (или букингу)
   * BOOKED - назначение ресурсов у аукциона (или букинга)
   * ALLOCATED - подписание заявки аукциона (или букинга)
   * CONTRACTED - заявка подписана, участник приступил к выполнению аукциона (или букинга)
   * также еще могут дублироваться такие статусы, как LOSER и CANCELLED
   * они расчитаны на несоблюдение условия или отказа в случае выигрыша
   */
  NEW: 'NEW',
  SEARCH: 'ON_DOER_SEARCH',
  BOOKED: 'BOOKED',
  ALLOCATED: 'ALLOCATED',
  CONTRACTED: 'CONTRACTED'
}
