export function getResourceClassByCode (code) {
  const classesMap = {
    tractor: {
      label: 'Тягач',
      labelGenitive: 'тягача',
      code: 'tractor'
    },
    trailer: {
      label: 'Прицеп',
      labelGenitive: 'прицепа',
      code: 'trailer'
    },
    semitrailer: {
      label: 'Полуприцеп',
      labelGenitive: 'полуприцепа',
      code: 'semitrailer'
    },
    container_truck: {
      label: 'Контейнеровоз',
      labelGenitive: 'контейнеровоза',
      code: 'container_truck'
    },
    driver: {
      label: 'Водитель',
      labelGenitive: 'водителя',
      code: 'driver'
    },
    van: {
      label: 'Фургон',
      labelGenitive: 'фургона',
      code: 'van'
    },
    jumbo: {
      label: 'Джамбо',
      labelGenitive: 'джамбо',
      code: 'jumbo'
    },
    trals: {
      label: 'Трал',
      labelGenitive: 'трала',
      code: 'trals'
    },
    lst: {
      label: 'Длинномер',
      labelGenitive: 'длинномера',
      code: 'lst'
    },
  }

  return classesMap[code.toLowerCase()]
}

export function getServiceIcon ({ uuid }) {
  const iconsMap = {
    '56eaa734-ad65-47e2-b961-80797818e935': {
      title: 'Перевозка фурой',
      icon: `<img src="${require('../assets/icons/fura.svg')}">`
    },
    '5e08d698-e12c-411d-a6ec-7e5ae74877b5': {
      title: 'Перевозка фургоном',
      icon: `<img src="${require('../assets/icons/furgon.svg')}">`
    },
    '0b36331f-7930-472e-892f-758dea34b038': {
      title: 'Перевозка длинномером',
      icon: `<img src="${require('../assets/icons/long.svg')}">`
    },
    'c4e10587-776d-493b-b34c-8f6bb2237554': {
      title: 'Перевозка тралом',
      icon: `<img src="${require('../assets/icons/tral-and-bulldozer.svg')}">`
    },
    'a4eec774-4998-42e3-9832-aaf9740b759e': {
      title: 'Перевозка автоконтейнеровозом',
      icon: `<img src="${require('../assets/icons/container.svg')}">`
    },
    '03384c79-953c-4406-a1b0-5507745de62b': {
      title: 'Перевозка ЖД вагоном',
      icon: `<img src="${require('../assets/icons/train.svg')}">`
    },
    '1591360d-51a3-46ef-9b9c-38b1e4830631': {
      title: 'Провозной тариф ЖД',
      icon: `<img src="${require('../assets/icons/train.svg')}">`
    },
    '774ff37f-330e-47a5-a90e-11f78ee00ab2': {
      title: 'Перевозка контейнером',
      icon: `<img src="${require('../assets/icons/container.svg')}">`
    },
    'ae088764-db00-4665-ab1a-d24905a97fe0': {
      title: 'Перевозка контейнера по ЖД',
      icon: `<img src="${require('../assets/icons/train-container.svg')}">`
    },
    '4ccffca2-89e3-49fc-8f7f-e730d176b833': {
      title: 'Перевозка автовозом',
      icon: `<img src="${require('../assets/icons/fura.svg')}">`
    },
    'f7b9fcd9-532f-4db0-bae1-16ad79d6b74f': {
      title: 'Перевозка эвакуатором',
      icon: `<img src="${require('../assets/icons/fura.svg')}">`
    },
    'cb114122-c298-4a7c-9b51-83ce52012b7a': {
      title: 'Перевозка манипулятором',
      icon: `<img src="${require('../assets/icons/fura.svg')}">`
    },
    '0e94ac65-72c6-4f10-be72-e9f9e94a8537': {
      title: 'Перевозка Джамбо',
      icon: `<img src="${require('../assets/icons/jumbo-simple.svg')}">`
    },
  }

  return iconsMap[uuid] || { icon: '' }
}

export const textContent = {
  isEditableIconTooltip: 'Изменения более недоступны, возможны только корректировки'
}
