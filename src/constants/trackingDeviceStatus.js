export function getAppStatus(code) {
  const map = {
    ACTIVE: 'Активно',
    WAIT: 'Ожидание подключения по МП',
    NODATA:'Отсутствие данных трекинга',
    INACTIVE: 'Не активно',
  }
  return map[code.toUpperCase()];
}

export function getGSMStatus(code) {
  const map = {
    WAIT: 'Ожидание подтверждения',
    OFF: 'Отсутствие данных трекинга',
    ERROR: 'Ошибка подключения',
    REJECTED: 'Водитель отказался от мониторинга',
    NEW: 'Новый',
    WRONG_OPERATOR: 'Неподдерживаемый оператор',
    OK: 'Активно',
    ERROR_EXISTS: 'Отслеживается другой компанией',
    OFF_NEW: 'Отсутствие данных трекинга',
    LIMITED: 'Ограничение на подключение',
    NO_CONFIRMED: 'Не подтверждено',
    WRONG_PHONE_NUMBER: 'Невалидный номер',
    INACTIVE: 'Не активно',
  }
  return code ? map[code.toUpperCase()] : '';
}
export function getSealStatus(code) {
  const map = {
    ACTIVE: 'Активно',
    WAIT: '-',
    NODATA: 'Отсутствие данных трекинга',
    INACTIVE: 'Не активно',
  }
  return code ? map[code.toUpperCase()] : '';
}