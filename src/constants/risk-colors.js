export function getIconByRiskColor (color) {
  const iconMap = {
    grey: 'risk-0-grey',
    blue: 'risk-2-green',
    green: 'risk-1-blue',
    yellow: 'risk-3-yellow',
    orange: 'risk-4-orange',
    red: 'risk-5-red',
    black: 'risk-6-black'
  }

  return iconMap[color.toLowerCase()]
}
