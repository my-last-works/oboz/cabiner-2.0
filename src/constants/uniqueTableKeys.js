export default {
	AccountOrdersAll: 'AccountOrdersAll',
	OrderDraftsList: 'OrderDraftsList',
	AccountArchiveClientsOrders: 'AccountArchiveClientsOrders',
	AccountArchiveCarriersOrders: 'AccountArchiveCarriersOrders',
	AccountShouldOrders: 'AccountShouldOrders',
	AccountDebtorOrders: 'AccountDebtorOrders',
	AccountPerformerOrdersCarrier: 'AccountPerformerOrdersCarrier',
	AccountPerformerOrdersClient: 'AccountPerformerOrdersClient',
	AccountClientsOrders: 'AccountClientsOrders',
}
