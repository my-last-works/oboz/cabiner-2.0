import { ERROR_CODES } from '@/resources/error_codes';

// Склонение слов titles - [ 1, 2, 5]
export const declOfNum = (number, titles) => {
  let cases = [2, 0, 1, 1, 1, 2];
  return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
};

export const formationParameters = (page, size, q, sort, filters) => {
  let parameters = '';
  if(page !== undefined) parameters += '&page=' + page;
  if(size !== undefined) parameters += '&size=' + size;
  if(sort && sort.length > 0) {
    sort.forEach(i => {
      let _sort = i.desc ? 'desc' : 'asc';
      parameters += '&sort=' + i.selector + ',' + _sort;
    });
  }
  if(filters) {
    for (let prop in filters) {
      if(filters[prop] !== undefined && filters[prop] !== null && filters[prop].length > 0) {
        parameters += '&' + prop + '=' + filters[prop];
      }
    }
  }
  if(q !== undefined && q.length > 0) parameters += '&q=' + q;
  if(parameters.length > 0) {
    parameters = '?' + parameters;
  }
  return parameters;
};

export const getIconByLocation = (name = '') => {
  const locationMap = {
    'bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4': 'airplane',
    '4adf706e-0be2-4383-b680-a73c959b9e2c': 'train',
    '4adf706e-0be2-4383-b680-a73c959b9e4c': 'container',
    '5919a99f-d840-4855-b158-5ba957076603': 'city',
    '4adf706e-0be2-4383-b680-a73c959b9e2f': 'ship',
    'a2166b4d-27a9-41ce-9fc8-8bfd21baa8d7': 'restrict',
    '4adf706e-0be2-4383-b680-a73c959b9e3c': 'police',
    'eb737130-7e9b-4614-86ce-e07d12181436': 'hand-box'
  }

  return locationMap[name]
}

export const getCountryFlag = (code = '') => {
  code = code.substr(0, 2).toLowerCase();
  try {
    return require(`src/static/images/countries/${code}.svg`);
  } catch (e) {
    // TODO Решить как храним сами картинки
    return '';
  }
}

export const getErrorByCode = (code) => {
  return ERROR_CODES[code] || 'Ошибка сервера';
}

export const CODEX_NAME = (type = 'default') => {
  const map = {
    criminal: {
        short: 'УК',
        full: 'Уголовный кодекс'
    },
    civil: {
        short: 'ГК',
        full: 'Гражданский кодекс'
    },
    default: {
        short: '-',
        full: '-'
    }
  }

  return map[type.toLowerCase()];
}

export const COUNTRY_CODE = (type = 'default') => {
  const map = {
    rus: {
      short: 'РФ',
      full: 'Россия'
    },
    default: {
      short: '-',
      full: '-'
    }
  }

  return map[type.toLowerCase()];
}

export const STATUS_LIST = [
  {
    value: true,
    text: 'Активен'
  },
  {
    value: false,
    text: 'Неактивен'
  }
];

export const isEmpty = obj => Object.keys(obj).length === 0 && obj.constructor === Object;

export const isNil = obj => obj == null;

export const compareValues = (key, order = 'asc') => {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      return 0;
    }

    const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];

    let result = 0;

    if (varA > varB) {
      result = 1;
    } else if ( varA < varB) {
      result = -1;
    }

    return (
      (order === 'desc') ? (result * -1) : result
    );
  };
}

// Function to sort an array of object by one or multiple fields. Takes an argument that should be an array of objects that describes fields for sorting in desired order [{field: 'fieldname', direction: 1(ASC) or -1(DESC}]
export const sortByFields = (fieldsArr) => {
  return function innerSort(a, b) {
    let i = 0;
    let result = 0;
    while (i < fieldsArr.length && result === 0) {
      result = fieldsArr[i].direction * (a[fieldsArr[i].field].toString() < b[fieldsArr[i].field].toString() ? -1 : (a[fieldsArr[i].field].toString() > b[fieldsArr[i].field].toString() ? 1 : 0));
      i += 1;
    }
    return result;
  }
}

export const plural = (value, unit) => {
  return value + ' ' + declOfNum(value, unit)
}

export const isOutOfVewport = (el) => {
  const bound = el.getBoundingClientRect();

  const out = {};

  out.top = bound.top < 0;
  out.bottom = bound.bottom > (window.innerHeight || document.documentElement.clientHeight);
  out.left = bound.left < 0;
  out.right = bound.right > (window.innerWidth || document.documentElement.clientWidth);

  out.any = out.top || out.bottom || out.left || out.right;
  out.all = out.top && out.bottom && out.left && out.right;

  return out;
}

export const getLocationIconByCode = (code) => {
  if (!code) return '';
  const map = {
    "railway_station" : "train-sm",
    airport: "airplane-sm",
    "container_port": "container-sm",
    "sea_port": "ship-sm",
    "reference_position": "city-sm",
    customs: "police-sm",
    "border_crossing_point": "restrict-sm",
    "railway_terminal": "hand-box"
  }
  return map[code.toLowerCase()];
}
