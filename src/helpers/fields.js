export const groupsFieldsSort = (groups = []) => {
  return groups.map((group) => {
    let fields = (group.fields || []).sort((a, b) => {
      return a.fieldPos - b.fieldPos
    })

    return {
      ...group,
      fields
    }
  })
}