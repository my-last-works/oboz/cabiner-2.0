export const parseNumber = (value, superclass) => {
  if (['driver'].includes(superclass) || !value) return {}
  let result
  value = value.replaceAll(' ', '')
  if ((/^(.{2})(\d{4})(\d{2,3})$/gi).test(value)) {
    result = value.replace(/^(.{2})(\d{4})(.+)/gi, '$1 $2|$3')
  } else if ((/^(.{1})(\d{3})(.{2})(\d{2,3})$/gi).test(value)) {
    result = value.replace(/^(.{1})(\d{3})(.{2})(.+)/gi, '$1 $2 $3|$4')
  } else {
    result = value
  }
  let resultArr = result.split('|')
  return {
    grzNum: resultArr[0],
    grzReg: resultArr[1]
  }
}
