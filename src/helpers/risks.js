import cloneDeep from 'clone-deep'
import { COLORS, COLORS_ORDER } from '@/resources/risk_colors'
import { TITLES } from '@/resources/risk_titles'
import { getValueFromPath } from 'common-components/src/helpers/common'


export const getRiskIcon = (code) => code ? `<i class="icon ${COLORS[code]}" title="${TITLES[code]}"></i>` : ''

export const getRiskClass = (color) => {
  return COLORS[color]
}

export const getNumberRisk = (color) => {
  return COLORS_ORDER[color]
}

export const sortRisks = ({ data = [], sort = 'asc', key = null }) => {
  return data.sort((a, b) => {
    const colorA = key ? getValueFromPath({ data: a, path: key }) : a
    const colorB = key ? getValueFromPath({ data: b, path: key }) : b
    const riskA = sort === 'asc' ? COLORS_ORDER[colorA] : COLORS_ORDER[colorB]
    const riskB = sort === 'asc' ? COLORS_ORDER[colorB] : COLORS_ORDER[colorA]
    return riskA - riskB
  })
}

export const riskGroupsProcessor = ({ data = [], sort = 'asc', role = '' }) => {
  let result = data.map((item) => {
    let groupInfo = item.cargo_loss_risk_group || {}
    let color = groupInfo.color || item.color || item.risk_color

    let roleUuids = {
      participant_payment_risk_group_uuid: null,
      cargo_loss_risk_group_uuid: null
    }

    item.uuid = item.uuid || item.loss_group_uuid

    if (role) {
      switch (role.toLowerCase()) {
        case 'client':
          roleUuids.participant_payment_risk_group_uuid = item.uuid
          role = 'CLIENT'
          break
        case 'carrier':
          roleUuids.cargo_loss_risk_group_uuid = item.uuid
          role = 'CARRIER'
      }
    }

    if (item.participant_payment_risk_group) {
      roleUuids.participant_payment_risk_group_uuid = item.participant_payment_risk_group.uuid
      item.uuid = item.participant_payment_risk_group.uuid
    }

    if (item.cargo_loss_risk_group) {
      roleUuids.cargo_loss_risk_group_uuid = item.cargo_loss_risk_group.uuid
      item.uuid = item.cargo_loss_risk_group.uuid
    }

    return {
      ...item,
      ...roleUuids,
      color,
      title: TITLES[color],
      icon: COLORS[color]
    }
  })
  return sortRisks({ data: result, sort, key: 'color' })
}

export const generateRisks = ({ data = [], type, sort = 'asc' }) => {
  if (!data.length) return []

  const typeMap = {
    boolean: 'boolean',
    numeric: 'numeric',
    checklist: 'ref_list',
    date: 'numeric',
    datetime: 'numeric'
  }
  let constraints
  let result = []

  if (['NUMERIC', 'DATE', 'DATETIME'].includes(type)) {
    constraints = {
      from: null,
      to: null,
      '@type': typeMap[type.toLowerCase()]
    }
  } else if (type === 'CHECKLIST') {
    constraints = {
      value: [],
      '@type': typeMap[type.toLowerCase()]
    }
  } else if (type === 'BOOLEAN') {
    constraints = {
      value: null,
      '@type': typeMap[type.toLowerCase()]
    }
  } else {
    return result
  }

  result = data.map((item) => {
    let groupInfo = item.cargo_loss_risk_group || item.participant_payment_risk_group || {}
    let color = groupInfo.color || item.color || item.risk_color
    let uuid = item.uuid || groupInfo.uuid || item.loss_group_uuid
    let { participant_payment_risk_group_uuid, cargo_loss_risk_group_uuid, participant_payment_risk_group, cargo_loss_risk_group} = item

    return {
      ...item,
      ...groupInfo,
      uuid,
      participant_payment_risk_group_uuid: participant_payment_risk_group && participant_payment_risk_group.uuid || participant_payment_risk_group_uuid,
      cargo_loss_risk_group_uuid: cargo_loss_risk_group && cargo_loss_risk_group.uuid || cargo_loss_risk_group_uuid,
      loss_group_uuid: uuid,
      constraints: cloneDeep(constraints),
      title: TITLES[color],
      icon:  COLORS[color],
      riskColor: item.cargo_loss_risk_group && item.cargo_loss_risk_group.color ? item.cargo_loss_risk_group.color : item.color || item.risk_color
    }
  })

  return sortRisks({ data: result, sort, key: 'riskColor' })
}
