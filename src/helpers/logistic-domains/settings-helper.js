import cloneDeep from 'clone-deep'
import deepEqual from 'deep-equal'

// Поиск настроек
export const FIND_SETTING = (array, viewDirection, role, targetRole, isCrossDomainScope, scope, deleteFlag) => {
    let _index;
    let _result = array.find((setting, index) => {
        if ((setting.viewDirection === viewDirection)
            && setting.participantRole === role
            && setting.targetParticipantRole === targetRole
            && (setting.isCrossDomainScope === isCrossDomainScope)) {
            if(scope && setting.scope !== scope) {
                return false;
            }
            _index = index;
            return true;
        }
    });
    if(deleteFlag && _result) {
        array.splice(_index, 1);
    }
    return _result;
};

// Изменение настроек видимости
export const GET_CHANGED_SETTINGS = (changedSettings, initialSettings, viewDirection, role, targetRoles, isCrossDomainScope, access, key, status, scope) => {
    let _changedSettings = cloneDeep(changedSettings);
    targetRoles.forEach(targetRole => {
        let _setting = FIND_SETTING(
            _changedSettings,
            viewDirection,
            role,
            targetRole,
            isCrossDomainScope,
            scope,
            true
        );
        let _initialSsetting = FIND_SETTING(
            initialSettings,
            viewDirection,
            role,
            targetRole,
            isCrossDomainScope,
            scope
        );
        if(!status && _setting) {
            _changedSettings.filter(setting => {
                return setting.uuid !== _setting.uuid
            })
        }
        else if(!deepEqual(_setting, _initialSsetting)) {
            _setting = _setting ? _setting : cloneDeep(_initialSsetting);
            _setting[key] = access;
            _changedSettings.push(_setting);
        }
    });
    return _changedSettings;
};