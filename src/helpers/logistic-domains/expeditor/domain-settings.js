import {
    VIEW_DIRECTION,
    PARTICIPANT_ROLES,
    SCOPE
} from '../../../resources/log_domains_const';
import {
    FIND_SETTING,
    GET_CHANGED_SETTINGS
} from '../../../helpers/logistic-domains/settings-helper';
import {uuid} from "vue-uuid";

// Получение настроек для сохранения
export const GET_PREPARED_SETTINGS = (viewDirection, initialSettings, settings, userData) => {
    let preparedSettings;
    if(viewDirection !== VIEW_DIRECTION.LINKS) {
        preparedSettings = [];
        settings.forEach(preparedSetting => {
            let _setting = FIND_SETTING(
                initialSettings,
                preparedSetting.viewDirection,
                preparedSetting.participantRole,
                preparedSetting.targetParticipantRole,
                preparedSetting.isCrossDomainScope
            );
            preparedSettings.push(
                changeScope(preparedSetting, _setting.uuid, userData)
            );
        });
    }
    else {
        preparedSettings = getLinksSettings( initialSettings, settings)
    }
    return preparedSettings;
};

// Изменение настроек видимости
export const GET_CHANGED_DOMAIN_SETTINGS = (changedSettings, initialSettings, row, access) => {
    let isCrossDomainScope;
    let targetParticipantRoles;
    switch (access.keyAccess) {
        case 0:
            isCrossDomainScope = false;
            targetParticipantRoles = row.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.MODERATOR] : [PARTICIPANT_ROLES.EXPEDITOR];
            break;
        case 1:
        case 3:
            isCrossDomainScope = access.keyAccess === 3;
            targetParticipantRoles = row.id === 1 ? [PARTICIPANT_ROLES.CARRIER, PARTICIPANT_ROLES.CLIENT] :
                row.id === 2 ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            break;
        case 2:
            isCrossDomainScope = true;
            targetParticipantRoles = row.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.EXPEDITOR] : [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR];
            break;
    }
    return GET_CHANGED_SETTINGS(
        changedSettings,
        initialSettings,
        row.viewDirection,
        row.role,
        targetParticipantRoles,
        isCrossDomainScope,
        access.access,
        'visibilityPermission'
    );
};

// Изменение уровня для настройки
const changeScope = (setting, settingUuid, userData) => {
    setting.uuid = settingUuid;
    if(setting.scope !== SCOPE.DOMAIN) {
        setting.uuid = uuid.v4();
        setting.scope = SCOPE.DOMAIN;
        setting.domainUuid = userData.contractor_uuid;
    }
    return setting;
};


// Получение настроек создания линков для сохранения
const getLinksSettings = (initialSettings, links) => {
    let _preparedSettings = [];
    links.forEach(link => {
        if(link.isCrossDomainScope !== null) {
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                link.targetParticipantRole,
                link.isCrossDomainScope,
                link.permission,
                'createLinkPermission'
            );
        }
        else {
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR],
                true,
                link.permission,
                'publicateLinkPermission'
            );
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                link.targetParticipantRole,
                false,
                link.permission,
                'publicateLinkPermission'
            );
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                link.targetParticipantRole,
                true,
                link.permission,
                'publicateLinkPermission'
            );
        }
    });
    _preparedSettings = _preparedSettings.map(preparedSetting => {
        return changeScope(preparedSetting, preparedSetting.uuid);
    });
    return _preparedSettings;
};

// Изменение настроек создания линков
export const GET_CHANGED_LINK_SETTING = (settingsObject) => {
    let _setting = {
        participantRole: settingsObject.setting.role,
        permission:  settingsObject.access.access
    };
    switch (settingsObject.access.keyAccess) {
        case 0:
        case 2:
            _setting.isCrossDomainScope = settingsObject.access.keyAccess === 2;
            _setting.targetParticipantRole = settingsObject.setting.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.CARRIER, PARTICIPANT_ROLES.CLIENT] :
                settingsObject.setting.role === PARTICIPANT_ROLES.CLIENT ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            break;
        case 1:
            _setting.isCrossDomainScope = true;
            _setting.targetParticipantRole = settingsObject.setting.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.EXPEDITOR] :
                [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR];
            break;
        case 3:
            _setting.isCrossDomainScope = null;
            _setting.targetParticipantRole = settingsObject.setting.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.CARRIER, PARTICIPANT_ROLES.CLIENT] :
                settingsObject.setting.role === PARTICIPANT_ROLES.CLIENT ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            break;
    }
    return _setting;
};