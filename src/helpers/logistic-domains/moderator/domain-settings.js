import {
    VIEW_DIRECTION,
    PARTICIPANT_ROLES,
    SCOPE
} from '@/resources/log_domains_const';
import {
    FIND_SETTING,
    GET_CHANGED_SETTINGS
} from '@/helpers/logistic-domains/settings-helper';
import deepEqual from 'deep-equal'


// Изменение настроек видимости
export const GET_CHANGED_DOMAIN_SETTINGS = (changedSettings, initialSettings, row, access, status) => {
    let isCrossDomainScope;
    let targetParticipantRoles;
    let scope = row.id < 3 ? SCOPE.DOMAIN : SCOPE.SYSTEM;
    switch (access.keyAccess) {
        case 0:
            isCrossDomainScope = false;
            targetParticipantRoles = row.id < 4 ? [PARTICIPANT_ROLES.MODERATOR] : [PARTICIPANT_ROLES.EXPEDITOR];
            break;
        case 1:
        case 3:
            isCrossDomainScope = access.keyAccess === 3;
            targetParticipantRoles = row.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.CARRIER, PARTICIPANT_ROLES.CLIENT] :
                row.role === PARTICIPANT_ROLES.CLIENT ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            break;
        case 2:
            isCrossDomainScope = true;
            targetParticipantRoles = row.id < 4 ? [PARTICIPANT_ROLES.EXPEDITOR] : [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR];
            break;
    }
    return GET_CHANGED_SETTINGS(
        changedSettings,
        initialSettings,
        row.viewDirection,
        row.role,
        targetParticipantRoles,
        isCrossDomainScope,
        access.access,
        'visibilityPermission',
        status,
        scope
    );
};

// Изменение настроек создания линков
export const GET_CHANGED_LINK_SETTING = (settings, row, access, status) => {
    let _setting = {
        participantRole: row.role,
        permission:  access.access
    };
    switch (access.keyAccess) {
        case 0:
        case 2:
            _setting.isCrossDomainScope = access.keyAccess === 2;
            _setting.targetParticipantRole = row.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.CARRIER, PARTICIPANT_ROLES.CLIENT] :
                row.role === PARTICIPANT_ROLES.CLIENT ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            break;
        case 1:
            _setting.isCrossDomainScope = true;
            _setting.targetParticipantRole = row.id < 4 ? [PARTICIPANT_ROLES.EXPEDITOR] : [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR];
            break;
        case 3:
            _setting.isCrossDomainScope = null;
            _setting.targetParticipantRole = row.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.CARRIER, PARTICIPANT_ROLES.CLIENT] :
                row.role === PARTICIPANT_ROLES.CLIENT ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            break;
    }
    if(status) {
        let _isSave = false
        settings.map(setting => {
            if (setting.isCrossDomainScope === _setting.isCrossDomainScope
                && setting.participantRole === _setting.participantRole
                && deepEqual(setting.targetParticipantRole, _setting.targetParticipantRole)) {
                setting.permission = _setting.permission
                _isSave = true
            }
            return setting
        })
        if (!_isSave) settings.push(_setting)
    }
    else {
        settings = settings.filter(setting => {
            return !(setting.isCrossDomainScope === _setting.isCrossDomainScope
                && setting.participantRole === _setting.participantRole
                && deepEqual(setting.targetParticipantRole, _setting.targetParticipantRole))
        })
    }
    return settings;
};

// Получение настроек для сохранения
export const GET_PREPARED_SETTINGS = (viewDirection, initialSettings, settings, systemSettings) => {
    let preparedSettings;
    if(viewDirection !== VIEW_DIRECTION.LINKS) {
        preparedSettings = [];
        settings.forEach(preparedSetting => {
            let _setting = FIND_SETTING(
                initialSettings,
                preparedSetting.viewDirection,
                preparedSetting.participantRole,
                preparedSetting.targetParticipantRole,
                preparedSetting.isCrossDomainScope
            );
            preparedSettings.push(_setting);
        });
        preparedSettings = settings.concat(systemSettings);
    }
    else {
        preparedSettings = getLinksSettings( initialSettings, settings, systemSettings)
    }
    return preparedSettings;
};

// Получение настроек создания линков для сохранения
const getLinksSettings = (initialSettings, links, systemLinks) => {
    let _preparedSettings = [];
    let changeLinksSetting = (link, scope) => {
        if(link.isCrossDomainScope !== null) {
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                link.targetParticipantRole,
                link.isCrossDomainScope,
                link.permission,
                'createLinkPermission',
                scope
            );
        }
        else {
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR],
                true,
                link.permission,
                'publicateLinkPermission',
                scope
            );
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                link.targetParticipantRole,
                true,
                link.permission,
                'publicateLinkPermission',
                scope
            );
            _preparedSettings = GET_CHANGED_SETTINGS(
                _preparedSettings,
                initialSettings,
                VIEW_DIRECTION.BY_PARTICIPANT,
                link.participantRole,
                link.targetParticipantRole,
                false,
                link.permission,
                'publicateLinkPermission',
                scope
            );
        }
    };
    links.forEach(link => {
        changeLinksSetting(link, SCOPE.DOMAIN);
    });
    systemLinks.forEach(link => {
        changeLinksSetting(link, SCOPE.SYSTEM);
    });
    return _preparedSettings;
};

