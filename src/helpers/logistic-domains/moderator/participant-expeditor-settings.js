import {
    VIEW_DIRECTION,
    PARTICIPANT_ROLES,
    SCOPE
} from '../../../resources/log_domains_const';
import {
    GET_CHANGED_SETTINGS
} from '../../../helpers/logistic-domains/settings-helper';
import cloneDeep from 'clone-deep'

import {uuid} from "vue-uuid";


// Изменение настроек создания линков
export const GET_CHANGED_EXPEDITOR_LINKS_PARTICIPANT_SETTINGS = (changedSettings, initialSettings, row, access) => {
    let _preparedSettings = cloneDeep(changedSettings);
    let targetParticipantRoles;
    let isCrossDomainScope;
    let _role = row.role;
    let isClient = _role === PARTICIPANT_ROLES.CLIENT;
    switch (access.keyAccess) {
        case 1:
            targetParticipantRoles = row.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.EXPEDITOR] : [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR];
            isCrossDomainScope = true;
            break;
        case 2:
        case 3:
            targetParticipantRoles = row.role === PARTICIPANT_ROLES.EXPEDITOR ? [PARTICIPANT_ROLES.CARRIER, PARTICIPANT_ROLES.CLIENT]
                : isClient ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            isCrossDomainScope = access.keyAccess === 3 ? null : true;
            break;
    }
    if(isCrossDomainScope !== null) {
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            targetParticipantRoles,
            isCrossDomainScope,
            access.access,
            'createLinkPermission'
        );
    }
    else {
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR],
            true,
            access.access,
            'publicateLinkPermission'
        );
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            targetParticipantRoles,
            true,
            access.access,
            'publicateLinkPermission'
        );
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            targetParticipantRoles,
            false,
            access.access,
            'publicateLinkPermission'
        );
    }
    return _preparedSettings;
};

// Изменение уровня для настройки
export const PREPARED_EXPEDITOR_CHANGE_SCOPE = (preparedSettings, participantUuid) => {
    let _preparedSettings =  cloneDeep(preparedSettings);
    _preparedSettings.forEach(setting => {
        if(setting.scope !== SCOPE.DOMAIN) {
            setting.uuid = uuid.v4();
            setting.scope = SCOPE.DOMAIN;
            setting.domainUuid = participantUuid;
        }
    });
    return _preparedSettings;
};