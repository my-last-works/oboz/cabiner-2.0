import {
    VIEW_DIRECTION,
    PARTICIPANT_ROLES,
    SCOPE
} from '../../resources/log_domains_const';
import {
    GET_CHANGED_SETTINGS
} from './settings-helper';
import cloneDeep from 'clone-deep'
import {uuid} from "vue-uuid";

// Изменение настроек видимости
export const GET_CHANGED_PARTICIPANT_SETTINGS = (changedSettings, initialSettings, row, access, participant) => {
    let viewDirection = row.id === 1 ? VIEW_DIRECTION.BY_PARTICIPANT : VIEW_DIRECTION.AT_PARTICIPANT;
    let targetParticipantRoles;
    let isCrossDomainScope;
    switch (access.keyAccess) {
        case 0:
        case 2:
            targetParticipantRoles = [PARTICIPANT_ROLES.EXPEDITOR];
            isCrossDomainScope = access.keyAccess === 2;
            break;
        case 1:
        case 3:
            targetParticipantRoles = participant.role.toUpperCase() === PARTICIPANT_ROLES.CLIENT ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            isCrossDomainScope = access.keyAccess === 3;
            break;
    }
    return GET_CHANGED_SETTINGS(
        changedSettings,
        initialSettings,
        viewDirection,
        participant.role.toUpperCase(),
        targetParticipantRoles,
        isCrossDomainScope,
        access.access,
        'visibilityPermission'
    );
};

// Изменение настроек создания линков
export const GET_CHANGED_LINKS_PARTICIPANT_SETTINGS = (changedSettings, initialSettings, row, access, participant) => {
    let _preparedSettings = cloneDeep(changedSettings);
    let targetParticipantRoles;
    let isCrossDomainScope;
    let _role = participant.role.toUpperCase();
    let isClient = _role === PARTICIPANT_ROLES.CLIENT;
    switch (access.keyAccess) {
        case 0:
        case 2:
            targetParticipantRoles = isClient ? [PARTICIPANT_ROLES.CARRIER] : [PARTICIPANT_ROLES.CLIENT];
            isCrossDomainScope = access.keyAccess === 2;
            break;
        case 1:
            targetParticipantRoles = [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR];
            isCrossDomainScope = true;
            break;
        case 3:
            targetParticipantRoles = isClient ? [PARTICIPANT_ROLES.CARRIER] :
                [PARTICIPANT_ROLES.CLIENT];
            isCrossDomainScope = null;
            break;
    }
    if(isCrossDomainScope !== null) {
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            targetParticipantRoles,
            isCrossDomainScope,
            access.access,
            'createLinkPermission'
        );
    }
    else {
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            [PARTICIPANT_ROLES.EXPEDITOR, PARTICIPANT_ROLES.MODERATOR],
            true,
            access.access,
            'publicateLinkPermission'
        );
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            targetParticipantRoles,
            true,
            access.access,
            'publicateLinkPermission'
        );
        _preparedSettings = GET_CHANGED_SETTINGS(
            _preparedSettings,
            initialSettings,
            VIEW_DIRECTION.BY_PARTICIPANT,
            _role,
            targetParticipantRoles,
            false,
            access.access,
            'publicateLinkPermission'
        );
    }
    return _preparedSettings;
};

// Изменение уровня для настройки
export const PREPARED_CHANGE_SCOPE = (preparedSettings, participantUuid, contractorUuid) => {
    let _preparedSettings =  cloneDeep(preparedSettings);
    _preparedSettings.forEach(setting => {
        if(setting.scope !== SCOPE.PARTICIPANT) {
            setting.uuid = uuid.v4();
            setting.scope = SCOPE.PARTICIPANT;
            setting.domainUuid = contractorUuid;
            setting.participantUuid = participantUuid;
        }
    });
    return _preparedSettings;
};
