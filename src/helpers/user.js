export const iconByRole = (role) => {

  const iconMap = {
    administrator: 'administrator',
    moderator: 'system-moderator',
    expeditor: 'domain-owner',
    client: 'role-client',
    carrier: 'carrier',
    agent: 'agent',
    anonymous: 'profile',
    clicar: 'domain-owner'
  }

  return iconMap[role] || iconMap.anonymous
}

export const iconByRolev2 = (role) => {

  const iconMap = {
    administrator: 'role-administrator',
    moderator: 'role-system-moderator',
    expeditor: 'role-domain-owner',
    client: 'role-client',
    carrier: 'carrier',
    agent: 'role-agent',
    anonymous: 'role-profile',
    clicar: 'role-domain-owner'
  }

  return iconMap[role] || iconMap.anonymous
}

export const iconByRoleV3 = (role) => {
  const iconMap = {
    administrator: 'role-administrator',
    moderator: 'role-system-moderator',
    expeditor: 'role-domain-owner',
    client: 'role-client',
    carrier: 'role-carrier',
    agent: 'role-agent',
    anonymous: 'role-profile',
    clicar: 'role-domain-owner'
  }

  return iconMap[role] || iconMap.anonymous
}