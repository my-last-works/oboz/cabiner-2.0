import RestClient from './RestClient';

let Client = new RestClient({ isErrorThrow: true });

export default {
  getParticipants() {
    return Client.get('/oboz2-security-participants-crud/v1/participants/')
  },
  getParticipantInfo(uuid) {
    return Client.get(`/oboz2-security-participants-crud/v1/participants/info/${uuid}`)
  },
  async saveParticipantCard (payload) {
    await Client.put('/oboz2-security-participants-crud/v1/participant/manual_field_value', payload)
  },
  getParticipantByUuid(uuid) {
    return Client.get(`/oboz2-security-participants-crud/v1/participants/${uuid}`)
  },
  getParticipantsbyRole(role) {
    return Client.get(`/oboz2-security-participants-crud/v1/participants_by_roles/role/${role}`)
  },
  getParticipantsbyRoleWithColor(role) {
    return Client.get(`/oboz2-order-po-draft-viewer/v1/participants/?role_code=${role}`)
  },
  deleteParticipant (uuid) {
    return Client.delete(`/oboz2-security-participants-crud/v1/delete_participant/${uuid}`)
  },
  getFileByUuid(fileUuid) {
    return Client.get(`/oboz2-security-file-storage/v1/${fileUuid}`, { responseType: 'blob' })
  },
  async getHistory (uuid) {
    const res = await Client.get(`/oboz2-security-carrier-risk-assessments-crud/v1/hist_risk/${uuid}`)
    return Promise.resolve(res)
  },
  async downloadReport (uuid) {
    const { data } = await Client.get(`/oboz2-security-recognition-processing/v1/report/participant/${uuid}`, {
      responseType: 'arraybuffer'
    })

    return data
  },
  getParticipantRiskGroup(uuid) {
    return Client.get(`/oboz2-security-participants-crud/v1/participants/${uuid}/risk-group`)
  },
  updateParticipantRisk(uuid, payload) {
    return Client.put(`/oboz2-security-participants-crud/v1/participant/manual_estimation/${uuid}`, payload)
  },
  async getDocuments({ uuid }) {
    try {
      const { data } = await Client.get('/oboz2-security-participants-crud/v1/documents', {
        params: {
          participant_uuid: uuid
        }
      })

      return data
    } catch (e) {
      throw e
    }
  },
  async saveDocument(documentUuid, participantUuid, payload) {
    try {
      await Client.post(`/oboz2-security-participants-crud/v1/documents/${documentUuid}`, payload, {
        params: {
          participant_uuid: participantUuid
        }
      })
    } catch (e) {
      throw e
    }
  },
  async deleteDocument(documentUuid) {
    try {
      await Client.delete(`/oboz2-security-participants-crud/v1/documents/${documentUuid}`)
    } catch (e) {
      throw e
    }
  },
  async loadDocumentsTypes() {
    try {
      const { data } = await Client.get('/oboz2-dictionary-doc-types-crud/v1/dropdown')

      return data
    } catch (e) {
      throw e
    }
  },
  async getInvoices() {
    const { data } = await Client.get(`/oboz2-billing-invoices-crud/v1/accounts`)
    return data
  },
  async addInvoice(payload) {
    return await Client.post(`/oboz2-billing-invoices-crud/v1/account`, payload)
  },
  async setInvoiceAsMain(uuid) {
    return await Client.put(`/oboz2-billing-invoices-crud/v1/main-account/${uuid}`)
  },
  async getInvoicesModerator(data) {
    console.log('daata',data)
    const res = await Client.get(`/oboz2-billing-invoices-crud/v1/accounts?participant_uuid=${data.participantUuid}`)
    return res.data
  },
  async addInvoiceModerator(data) {
    console.log('data',data)
    return await Client.post(`/oboz2-billing-invoices-crud/v1/account?participant_uuid=${data.participantUuid}`, data)
  },
  async setInvoiceAsMainModerator(data) {
    return await Client.put(`/oboz2-billing-invoices-crud/v1/main-account/${data.uuid}?participant_uuid=${data.participantUuid}`)
  }
}
