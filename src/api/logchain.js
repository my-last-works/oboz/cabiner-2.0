import RestClient from './RestClient'

let Client = new RestClient({ isErrorThrow: true })

export default {
  async getlogchainsList ({ resource_class_uuid, status, page_number = 0, page_size = 20, order_field = 'UPDATED_AT', order_direction = 'DESC', query }) {
    try {
      let res = await Client.get(`/oboz2-logchain-logchains-crud/v1/list/`, {
        params: { resource_class_uuid,  status, page_number, page_size, order_field, order_direction, q: query}
      });
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getLogchain(uuid) {
    try {
      const res = await Client.get(`/oboz2-logchain-logchains-crud/v1/logchain/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getLogchainServices(uuid) {
    try {
      let res = await Client.get(`/oboz2-logchain-logchains-crud/v1/services/by_class/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getLogchainContainerServices(uuid) {
    try {
      let res = await Client.get(`/oboz2-logchain-logchains-crud/v1/services/by_class/${uuid}/container`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getLogchainPointServices() {
    try {
      const res = await Client.get(`/oboz2-logchain-logchains-crud/v1/services/on_point/`)
      return res.data
    } catch (e) {
      throw(e)
    }
  },

  async saveLogchain(uuid, payload) {
    try {
      await Client.post(`/oboz2-logchain-logchains-crud/v1/logchain/${uuid}`, payload)
    } catch (e) {
      throw e
    }
  },

  async publishLogchain(uuid) {
    try {
      const res = await Client.post(`/oboz2-logchain-logchains-crud/v1/${uuid}/activate`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async deleteLogchain(uuid) {
    try {
      await Client.delete(`/oboz2-logchain-logchains-crud/v1/logchain/${uuid}`)
    } catch (e) {
      throw e
    }
  },

  async revokeLogchain(uuid) {
    try {
      await Client.post(`/oboz2-logchain-logchains-crud/v1/logchain/${uuid}/set_to_draft`)
    } catch (e) {
      throw e
    }
  },

  async getLocations(payload) {
    try {
      const res = await Client.post('/oboz2-order-address-suggester/v2/locations/filter', payload)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getAllLogchainServices() {
    try {
      const res = await Client.get(`/oboz2-logchain-logchains-crud/v1/services/container_logchain/`)
      return res.data
    } catch (e) {
      throw e
    }
  }
}