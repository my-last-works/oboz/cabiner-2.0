import RestClient from './RestClient'

let Client = new RestClient()

export default {
  getSettings (clicarUuid, type) {
    return Client.get(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/?type=${type}`)
  },
  getDocsSettings (uuid, category) {
    return Client.get(`/oboz2-clicar-account-settings-crud/v1/settings/${uuid}/docs_settings?category=${category}`)
  },
  saveDocsSettings (clicarUuid, params, category) {
    return Client.post(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/docs_settings?category=${category}`, params)
  },
  getListCustomers ({ page, size }, clicarUuid) {
    return Client.get(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/doc_settings/clients?page=${page}&limit=${size}`)
  },
  getDocsSettingsByCustomer (clicarUuid, clientUuid) {
    return Client.get(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/docs_settings/${clientUuid}`)
  },
  savePersonalSetting (clicarUuid, clientUuid, params) {
    return Client.post(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/docs_settings/${clientUuid}`, params)
  },
  deleteAllPersonalSetting (clicarUuid, clientUuid) {
    return Client.delete(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/docs_settings/${clientUuid}`)
  },
  deletePersonalSetting (clicarUuid, direction) {
    return Client.delete(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/docs_settings/per_location`, {
      data: {
        ...direction
      }
    })
  },
  getPersonalDirection ({ clicarUuid, client, start, end }) {
    return Client.post(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}/docs_settings/per_location`, {
      clientUuid: client,
      startLocationUuid: start,
      endLocationUuid: end,
    })
  }
}