import RestClient from './RestClient'
import store from '@/store'

let Client = new RestClient();

export default {
  async fetch() {
    try {
      let res = await Client.get('/oboz2-user-access-menu-viewer/v1/menu_elements')
      // console.log(res.data.menu_elements.find(key => key.title === 'Заказ'))
      // console.log('!!!!', res.data.menu_elements)
      // const add_menu = {
      //   action: null,
      //   class: null,
      //   code: "order",
      //   icon: null,
      //   level: 3,
      //   need_show_parent_only: false,
      //   parent_uuid: "08e5587c-b92b-48ff-91e0-b8734a06ea10",
      //   position: 1,
      //   title: "Заказы 2 - для 3-шника",
      //   uuid: "dfb67309-82cc-46ca-b663-ce683ec88bdx"
      // }
      //
      // const o = {
      //   ...res.data.menu_elements,
      //   ...add_menu
      // }

      // return o

      const isClicar = store.getters['user/isClicar']
      const menu = res.data.menu_elements
      let addMenu = []

      const createContractButton = menu.find(item => item.code === 'contracts__new')
      const isExistCreateContractDropdownItems = createContractButton && menu.find(item => item.parent_uuid === createContractButton.uuid)

      if (createContractButton && isExistCreateContractDropdownItems) {
        if (!menu.find(item => item.code === 'contracts__new__transportation-parent-contract')) {
          addMenu.push({
            uuid: "437bbf07-010f-4628-b462-dd4921d7df5g",
            parent_uuid: createContractButton.uuid,
            code: "contracts__new__transportation_parent_contract",
            title: "Титульный договор",
            position: 3,
            level: 4,
            action: "transportation_parent_contract",
            class: null,
            icon: null,
            need_show_parent_only: true,
            workerRoles: ["client"]
          })
        }
      }

      const profileItem = menu.find(item => item.title?.toLowerCase() === 'профиль')

      if (profileItem && isClicar) {
        addMenu.push({
          uuid: "437bbf07-010f-4628-b462-dd4924d7df5g",
          parent_uuid: profileItem.uuid,
          code: "commercial__policy",
          title: "Коммерческая политика",
          position: 10,
          level: 2,
          class: null,
          icon: null,
          workerRoles: ["clicar"]
        })
      }

      return [
        ...menu,
        ...addMenu
      ]
    } catch (e) {
      throw e
    }
  }
};
