import RestClient from './RestClient';

let Client = new RestClient();

export default {
  async getListApplications({ page, size, query, status }) {
    try {
      const res = await Client.post(`/oboz2-billing-advances-crud/v1/advances/?page=${page}&size=${size}&status=${status}`, {
        page,
        size,
        query,
        status: status
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getApplication(uuid) {
    try {
      const res = Client.get(`oboz2-suborder-orders-crud/v1/suborders/${uuid}/advances/?status_filter=ALLOCATABLE`);
      return res;
    } catch (e) {
      throw e
    }
  },
  async getStatusDriver(suborderUuid) {
    try {
      const res = await Client.get(`oboz2-suborder-orders-crud/v1/suborder/${suborderUuid}`);
      return res.data
    } catch (e) {
      throw e
    }
  },
  async changeStatusApplication(uuid, params) {
    try {
      const res = Client.post(`oboz2-billing-advances-crud/v1/advances/${uuid}/handle_status`, params);
      return res;
    } catch (e) {
      throw e
    }
  }
}