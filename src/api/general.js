import RestClient from '@/api/RestClient';
import { formationParameters } from '@/helpers';

const Client = new RestClient();

export default {
  getCountriesList(withDeleted, page, size, q, sort, notInUnion) {
    let _notInUnion = notInUnion ? ('&not-in-union=' + notInUnion.value) : '';
    return Client.get('/oboz2-dictionary-countries-viewer/v1' + formationParameters(withDeleted, page, size, q, sort) + _notInUnion);
  },
  getCountriesList1(page, size, q, sort) {
    let _notInUnion = '&not-in-union=true';
    return Client.get('/oboz2-dictionary-countries-viewer/v1' + formationParameters(page, size, q, sort) + _notInUnion);
  },
  getCountriesList2(page, size, q, sort) {
    return Client.get('/oboz2-dictionary-countries-viewer/v1' + formationParameters(page, size, q, sort));
  },
  getCountry(uuid) {
    return Client.get('/oboz2-dictionary-countries-viewer/v1/' + uuid);
  },
  deleteCountry(uuid) {
    return Client.delete('/oboz2-dictionary-countries-viewer/v1/' + uuid);
  },
  getCountryImage(uuid) {
    return Client.get('/oboz2-dictionary-countries-viewer/v1/' + uuid + '/image');
  },
  getCurrencies() {
    return Client.get('/oboz2-dictionary-currencies-viewer/v1/');
  },
  getCurrenciesRates(code, date) {
    return Client.get('/oboz2-dictionary-currencies-viewer/v1/rates?target_currency=' + code + '&date=' + date);
  },
  getCurrenciesDynamics({ currency, startDate, endDate, target }) {
    return Client.get('/oboz2-dictionary-currencies-viewer/v1/rates_interval', { params: { currency, target_currency: target, start_date: startDate, end_date: endDate } });
  },
  getCurrenciesDetail(uuid) {
    return Client.get('/oboz2-dictionary-currencies-viewer/v1/' + uuid);
  },
  getDocuments(page, size, q, sort) {
    return Client.get('/oboz2-dictionary-doc-types-crud/v1' + formationParameters(page, size, q, sort));
  },
  getDocument(uuid) {
    return Client.get('/oboz2-dictionary-doc-types-crud/v1/' + uuid);
  },
  createDocument(data) {
    return Client.post('/oboz2-dictionary-doc-types-crud/v1/', data);
  },
  editDocument(uuid, data) {
    return Client.put('/oboz2-dictionary-doc-types-crud/v1/' + uuid, data);
  },
  deleteDocument(uuid) {
    return Client.delete('/oboz2-dictionary-doc-types-crud/v1/' + uuid);
  },
  getActivities({ page, size, query, sort }) {
    return Client.get(
      '/oboz2-dictionary-activities-viewer/v1',
      { params: { page, size, sort, q: query } }
    );
  },
  getTrackingMethods() {
    return Client.get('/oboz2-dictionary-tracking-methods-viewer/v1/')
  },
  getTrackingMethodsItem(uuid) {
    return Client.get('/oboz2-dictionary-tracking-methods-viewer/v1/' + uuid)
  },
}
