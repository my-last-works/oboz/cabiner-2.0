import RestClient from '@/api/RestClient';

let Client = new RestClient({
  isErrorThrow: true
});

export default {
  async getNotices () {
    try {
      let notices = await Client.get('/oboz2-notificator-settings-crud/v1/notification_types');
      return notices.data;
    } catch (e) {
      throw e;
    }
  },
  async getSettings () {
    try {
      let settings = await Client.get('/oboz2-notificator-settings-crud/v1/settings');
      return settings.data
    } catch (e) {
      throw e;
    }

  },
  async setSettings (data) {
    try {
      let settings = await Client.post('/oboz2-notificator-settings-crud/v1/settings', [data]);
      return settings
    } catch (e) {
      throw e
    }
  },
  async removeSetting (uuid) {
    try {
      let res = await Client.delete(`/oboz2-notificator-settings-crud/v1/settings/${uuid}`);
      return res;
    } catch (e) {
      throw e;
    }
  }
}
