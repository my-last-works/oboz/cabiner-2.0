import RestClient from './RestClient'
import { riskGroupsProcessor } from '@/helpers/risks'

const Client = new RestClient()

export default {
  async getRisk(uuid) {
    try {
      const res = await Client.get(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/admin/participant_field/${uuid}`)
      return res.data && res.data.participant_loss_cargo_field_constraints || [];
    } catch (e) {
      throw e
    }
  },
  async getRiskParticipant (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/participant_field/${uuid}`)
      return riskGroupsProcessor({ data: res.data && res.data.participant_loss_cargo_field_constraints || [] })
    } catch (e) {
      throw e
    }
  },
  async setRiskParticipant ({ risks, uuid }) {
    try {
      let res = await Client.put(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/participant_field/${uuid}`, {
        participant_loss_cargo_field_constraints: risks
      })
      return res
    } catch (e) {
      throw e
    }
  },
  async getRiskResource(uuid) {
    try {
      const res = await Client.get(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/resource_field/${uuid}`)
      return riskGroupsProcessor({ data: res.data && res.data.resource_loss_cargo_field_constraints || [] })
    } catch (e) {
      throw e
    }
  },
  async getCarrierRiskGroups() {
    try {
      const res = await Client.get(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/carrier/risk/groups/`)
      return riskGroupsProcessor({ data: res.data && res.data.risk_groups ? res.data.risk_groups : [], role: 'CARRIER' })
    } catch (e) {
      throw e
    }
  },
  async getClientRiskGroups() {
    try {
      const res = await Client.get(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/client/risk/groups/`)
      return riskGroupsProcessor({ data: res.data && res.data.risk_groups ? res.data.risk_groups : [], role: 'CLIENT' })
    } catch (e) {
      throw e
    }
  },
  async getRiskGroups() {
    let clientRisks = await this.getClientRiskGroups()
    let carrierRisks = await this.getCarrierRiskGroups()
    let uniqueFields = new Set([ ...clientRisks.map(item => item.color), ...carrierRisks.map(item => item.color) ])
    let result = []

    uniqueFields.forEach(color => {
      let clientGroup = clientRisks.find(item => item.color === color) || {}
      let carrierGroup = carrierRisks.find(item => item.color === color) || {}
      let cargo_loss_risk_group_uuid = carrierGroup.cargo_loss_risk_group_uuid
      let participant_payment_risk_group_uuid = clientGroup.participant_payment_risk_group_uuid
      let roles = [ clientGroup.role, carrierGroup.role ]

      result.push({ ...clientGroup, ...carrierGroup, cargo_loss_risk_group_uuid, participant_payment_risk_group_uuid, roles })
    })

    return result
  },
  async setRiskGroups(payload) {
    try {
      const res = await Client.put(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/admin/participant_field/${payload.uuid}`, {participant_loss_cargo_field_constraints: payload.riskValues})
      return res.data
    } catch (e) {
      throw e
    }
  },
  async setRiskGroupsResource(payload) {
    try {
      const res = await Client.put(`/oboz2-security-part-and-res-assessment-criteria-crud/v1/resource_field/${payload.uuid}`, {resource_loss_cargo_field_constraints: payload.riskValues});
      return res.data
    } catch (e) {
      throw e
    }
  },
  async fetch () {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/domain/participant/fields`)
      return res.data && res.data.ParticipantDomainFields || []
    } catch (e) {
      throw e
    }
  },
  async getParticipantDetail (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/domain/participant/fields/${uuid}`)
      return res.data || {}
    } catch (e) {
      throw e
    }
  },
  async updateParticipantDetail ({ uuid, data }) {
    try {
      const res = await Client.put(`/oboz2-security-card-fields-crud/v1/domain/participant/fields/${uuid}`, data)
      return res.data || {}
    } catch (e) {
      throw e
    }
  },
  async fetchResource () {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/domain/resource/fields`)
      return res.data && res.data.ResourceDomainFields || []
    } catch (e) {
      throw e
    }
  },
  async getResourceDetail (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/domain/resource/fields/${uuid}`)
      return res && res.data || {}
    } catch (e) {
      throw e
    }
  },
  async updateResourceDetail ({ uuid, data }) {
    try {
      const res = await Client.put(`/oboz2-security-card-fields-crud/v1/domain/resource/fields/${uuid}`, data)
      return res && res.data || {}
    } catch (e) {
      throw e
    }
  },
  async availableFields () {
    try {
      const res = await Client.get(`/oboz2-security-domain-cards-crud/v1/all_participant_fields`)
      return res.data && res.data.fields || []
    } catch (e) {
      throw e
    }
  },
  async availableFieldsResource () {
    try {
      const res = await Client.get(`/oboz2-security-domain-cards-crud/v1/all_resource_fields`)
      return res.data && res.data.fields || []
    } catch (e) {
      throw e
    }
  },
  async update(payload) {
    try {
      const res = await Client.post(`/oboz2-security-domain-cards-crud/v1/participant_fields`, payload)
      return res.data || {}
    } catch (e) {
      throw e
    }
  },
  async updateResource(payload) {
    try {
      const res = await Client.post(`/oboz2-security-domain-cards-crud/v1/resource_fields`, payload)
      return res.data || {}
    } catch (e) {
      throw e
    }
  },
  async remove(uuid) {
    try {
      const res = await Client.delete(`oboz2-security-domain-cards-crud/v1/participant_fields/${uuid}`)
      return res
    } catch (e) {
      throw e
    }
  },
  async removeResource(uuid) {
    try {
      const res = await Client.delete(`oboz2-security-domain-cards-crud/v1/resource_fields/${uuid}`)
      return res
    } catch (e) {
      throw e
    }
  },
  async activitiesDropdown () {
    try {
      const res = await Client.get(`/oboz2-dictionary-activities-viewer/v1/`)
      return res && res.data
    } catch (e) {
      throw e
    }
  },
  async setAdminRisks ({ uuid, role_code, activity_uuid }) {
    try {
      let url = `/oboz2-security-part-and-res-assessment-criteria-crud/v1/risk/field/${uuid}/admin_settings/${role_code.toLowerCase()}`
      if (activity_uuid) {
        url += `/activities/${activity_uuid}`
      }
      const res = await Client.post(url)
      return res.data || {}
    } catch (e) {
      throw e
    }
  }
}
