import RestClient from '../RestClient';

const Client = new RestClient({baseURL: ''});

export default {
    async fetchUserInfo() {
        try {
            let res = await Client.get(`/oboz2-domains-user-info/v1/userInfo`);
            return res.data;
        } catch (e) {
            throw new Error(e);
        }
    },
    async fetchBonusMethodologies() {
        try {
            let res = await Client.get(`/oboz2-bonus-methodology-crud/v1`)
            return res.data;
        } catch (e) {
            throw new Error(e);
        }
    },
    async createBonusMethodology(data) {
        try {
            let res = await Client.post(`/oboz2-bonus-methodology-crud/v1`, data)
            return res.data;
        } catch (e) {
            throw new Error(e);
        }
    }
}