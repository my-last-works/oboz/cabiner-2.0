import RestClient from '../RestClient'

const Client = new RestClient({baseURL: ''});

export default {
    async fetchBonusMethodologyByUuid(methodology_uuid) {
        try {
            let res = await Client.get(`/oboz2-bonus-methodology-crud/v1/methodologies/${methodology_uuid}`)
            return res.data;
        } catch (e) {
            throw new Error(e)
        }
    },
    async updateBonusMethodologyByUuid(methodology_uuid, data) {
        try {
            let res = await Client.put(`/oboz2-bonus-methodology-crud/v1/methodologies/${methodology_uuid}`, data);
            return res;
        } catch (e) {
            throw new Error(e);
        }
    },
    async deleteBonusMethodologyByUuid(methodology_uuid) {
        try {
            let res = await Client.delete(`/oboz2-bonus-methodology-crud/v1/methodologies/${methodology_uuid}`);
            return res.data;
        } catch (e) {
            throw new Error(e)
        }
    }
}