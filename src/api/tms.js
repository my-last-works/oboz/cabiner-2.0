import RestClient from "./RestClient";

let Client = new RestClient({ isErrorThrow: true });

export default {
  async getTms({
    page = 0,
    size = 20,
    sort = "createdAt",
    direction = "DESC",
    query
  }) {
    try {
      const res = await Client.get('/oboz2-tms-distribution-policies-crud/v1/allocations_policies',
      {
        params: { page, size, sort, direction, q: query }
      })
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getServices() {
    try {
      return Promise.resolve([]);
    } catch (err) {
      throw err;
    }
  },

  async getServicesList({ page = 0, size = 20, type = "TRANSPORTATION" } = {}) {
    try {
      const res = await Client.get(`/oboz2-dictionary-service-viewer/v1/list`, {
        params: {
          page,
          size,
          type,
        },
      });
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  getPolicyNumber() {
    let data = {
      bodyGenerator: {
        entityType: "Distribution_policies",
      },
    };
    return Client.post("/oboz2-code-generator/v1/generate", data);
  },

  async getPolicy(uuid) {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/allocations_policies/${uuid}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getPolicyServices(uuid, { page = 0, size = 20, sort="createdAt", direction="DESC", query }) {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/distribution_services/by_policy/${uuid}`, {
        params: { page, size, sort, direction, q: query }
      });
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getServiceInfo(uuid) {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/distribution_services/${uuid}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async savePolicyServices(uuid, payload) {
    try {
      await Client.put(`/oboz2-tms-distribution-policies-crud/v1/distribution_services/${uuid}`, payload);
    } catch (err) {
      throw err;
    }
  },

  async getPolicyContractors() {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/participants/?role_code=CLICAR&role_code=CARRIER`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async saveTitul(uuid, payload) {
    try {
      await Client.put(`/oboz2-tms-distribution-policies-crud/v1/allocations_policies/${uuid}`, payload);
    } catch (err) {
      throw err;
    }
  },

  async changeStatus(uuid, status) {
    try {
      await Client.put(`/oboz2-tms-distribution-policies-crud/v1/allocations_policies/${uuid}/status/${status}      `)
    } catch (err) {
      throw err;
    }
  },

  async getDirections(uuid) {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/directions/with_volumes/${uuid}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getDirectionsByContractors(uuid) {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/directions/with_participants/${uuid}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getDirectionContractors(uuid) {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/participants/by_distribution_service/${uuid}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getDirectionDetails(uuid) {
    try {
      const res = await Client.get(`/oboz2-tms-distribution-policies-crud/v1/directions/${uuid}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async saveDirections(uuid, payload) {
    try {
      await Client.put(`/oboz2-tms-distribution-policies-crud/v1/directions/${uuid}`, payload);
    } catch (err) {
      throw err;
    }
  },

  async getServicesAll(params) {
    try {
      const { data } = await Client.post(`/oboz2-dictionary-service-viewer/v1/all`, params);
      return data;
    } catch (err) {
      throw err;
    }
  }
};
