import adminPanelAccess from './access'
import adminPanelRoles from './roles'
import users from './users'

export default {
  adminPanelAccess,
  adminPanelRoles,
  users
}
