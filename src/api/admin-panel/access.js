import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  async getFunctions() {
    try {
      const res = await new RestClient().get('/oboz2-user-access-functions-crud/v1/functions')
      return res.data && res.data.functions || []
    } catch (e) {
      throw e
    }
  },
  async removeContractorRoles(funcUuid, data) {
    try {
      const res = await Client.post(`/oboz2-user-access-contractor-role-function-links-crud/v1/remove_link/${funcUuid}`, data)
      return res.data && res.data || {}
    } catch (e) {
      throw e
    }
  },
  async addContractorRoles(funcUuid, data) {
    try {
      const res = await Client.post(`/oboz2-user-access-contractor-role-function-links-crud/v1/add_link/${funcUuid}`, data)
      return res.data && res.data || {}
    } catch (e) {
      throw e
    }
  },
  async setFunctionStatus(funcUuid, data) {
    try {
      const res = await Client.put(`/oboz2-user-access-functions-crud/v1/${funcUuid}`, data)
      return res.data && res.data || {}
    } catch (e) {
      throw e
    }
  }
}
