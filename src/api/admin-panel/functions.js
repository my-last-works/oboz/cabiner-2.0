import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  async getFunctionsAcceptByWorkerRole(workerRoleUuid) {
    try {
      const res = await Client.get(`/oboz2-user-access-worker-role-function-links-crud/v1/${workerRoleUuid}`)
      return res.data && res.data.functions || []
    } catch (e) {
      throw e
    }
  },
  async getFunctionListByWorkerRole(uuid) {
    try {
      const res = await Client.get(`/oboz2-user-access-functions-crud/v1/list_of_functions/${uuid}`)
      return res.data && res.data.functions || []
    } catch (e) {
      throw e
    }
  },
  async getFunctionsByContractorRole(uuid) {
    try {
      const res = await Client.get(`/oboz2-user-access-functions-crud/v1/excluding_worker_list_of_functions/${uuid}`)
      return res.data && res.data.functions || []
    } catch (e) {
      throw e
    }
  }
}
