import RestClient from '@/api/RestClient'

const Client = new RestClient({ isErrorThrow: true })

export default {
  async getAllUsers({ page, size, query, sort }) {
    try {
      const res = await Client.get('/oboz2-user-access-users-crud/v1/all', { params: { page, size, q: query, sort } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getUsers() {
    try {
      const res = await Client.get('/oboz2-user-access-users-crud/v1')
      return res.data.users
    } catch (e) {
      throw e
    }
  },
  async createUser(data) {
    let {
      contractorUuid,
      email,
      first_name,
      last_name,
      patronymic,
      phone,
    } = data

    try {
      await Client.post(`/oboz2-user-access-users-crud/v1/${data.uuid}`, {
        contractorUuid,
        email,
        firstName: first_name,
        lastName: last_name,
        patronymic,
        phone,
      })
    } catch (e) {
      throw e
    }
  },
  async updateUser(data) {
    let {
      first_name,
      last_name,
      patronymic,
      phone,
    } = data

    try {
      await Client.put(`/oboz2-user-access-users-crud/v1/${data.uuid}`, {
        firstName: first_name,
        lastName: last_name,
        patronymic,
        phone,
      })
    } catch (e) {
      throw e
    }
  },
  async deleteUser ({ uuid }) {
    try {
      await Client.delete(`/oboz2-user-access-users-crud/v1/${uuid}`)
    } catch (e) {
      throw e
    }
  },
  async getUserRoles() {
    try {
      const { data } = await Client.get('/oboz2-user-access-users-crud/v1/user/roles')

      return data
    } catch (e) {
      throw e
    }
  },
  async updateUserRoles(data) {
    try {
      await Client.post(`/oboz2-user-access-user-worker-role-links-crud/v1/${data.uuid}/update_role`, { worker_role_uuids: data.worker_roles })
    } catch (e) {
      throw e
    }
  },
  async updateUserStatus(data) {
    try {
      await Client.put(`/oboz2-user-access-users-crud/v1/${data.uuid}/user_status`, { is_active: data.is_active })
    } catch (e) {
      throw e
    }
  },
  async getUserInfo(uuid) {
    try {
      const res = await Client.get(`/oboz2-user-access-users-crud/v1/user_info/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getUsersInfo(uuids) {
    try {
      const res = await Client.post(`/oboz2-user-access-users-crud/v1/user_info/list`, { uuids: uuids })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getUserOrgs(uuid, { page = 0, size = 20, query, sort } = {}) {
    try {
      const { data } = await Client.get(`/oboz2-user-access-users-crud/v1/organization_list/${uuid}`, {
        params: { page, size, q: query, sort }
      })

      return data
    } catch (e) {
      throw e
    }
  },
  async updateUserOrgs(uuid, payload) {
    try {
      await Client.post(`/oboz2-user-access-users-crud/v1/organization/${uuid}`, payload)
    } catch (e) {
      throw e
    }
  }
}
