import RestClient from '@/api/RestClient'

const Client = new RestClient()
import VueCookie from 'vue-cookie'

export default {
  async getUserFunctionalRoles() {
    try {
      const token = VueCookie.get('auth_token')
      const res = await Client.post('/oboz2-user-access-access-checker/v1/all_functions_access/details', { token })

      return Promise.resolve(res && res.data && res.data.functions || [])
    } catch (e) {
      throw e
    }
  },
  async getUserRoles() {
    try {
      const res = await Client.get('/oboz2-user-access-worker-roles-crud/v1/')
      return res.data && res.data.worker_roles || []
    } catch (e) {
      throw e
    }
  },
  async getUserRolesNew() {
    try {
      const { data } = await Client.get('/oboz2-user-access-worker-roles-crud/v1/')

      return data
    } catch (e) {
      throw e
    }
  },
  async addUserRole (data) {
    try {
      const res = await Client.post('/oboz2-user-access-worker-roles-crud/v1/', data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async updateUserRole (uuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-user-access-worker-roles-crud/v1/${uuid}/update_worker_role`, payload)

      return data
    } catch (e) {
      throw e
    }
  },
  async deleteUserRole (uuid) {
    try {
      await Client.delete(`/oboz2-user-access-worker-roles-crud/v1/${uuid}`)
    } catch (e) {
      throw e
    }
  },
  async setUserRoleStatus (uuid, status = false) {
    try {
      const { data } = await Client.put(`/oboz2-user-access-worker-roles-crud/v1/${uuid}/worker_role_status`, {
        is_active: status
      })

      return data
    } catch (e) {
      throw new Error(e)
    }
  },
  async getUserRoleLogs (workerRoleUuid) {
    try {
      const { data } = await Client.get(`/oboz2-user-access-worker-roles-crud/v1/${workerRoleUuid}/logs`)

      return data
    } catch (e) {
      throw new Error(e)
    }
  },
  async getKeycloakRoles ({ query, contractorRole }) {
    try {
      const { data } = await Client.get(`/oboz2-user-access-worker-roles-crud/v1/keycloak/get_by_contractor/${contractorRole}`, {
        params: { q: query }
      })

      return data
    } catch (e) {
      throw new Error(e)
    }
  },
  async getWorkerRoleKeycloakRoles (workerRoleUuid) {
    try {
      const { data } = await Client.get(`/oboz2-user-access-worker-roles-crud/v1/keycloak/${workerRoleUuid}`)
      return data
    } catch (e) {
      throw new Error(e)
    }
  },
  async getKeycloakRolesByContractor (contractorRole) {
    try {
      const { data } = await Client.get(`/oboz2-user-access-contractors-crud/v1/keycloak_roles/${contractorRole}`)

      return data
    } catch (e) {
      throw new Error(e)
    }
  },
  async addWorkerRoleFunctionsLink (data) {
    try {
      const res = await Client.post(`/oboz2-user-access-worker-role-function-links-crud/v1/add_link/${data.uuid}`, data.obj)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteWorkerRoleFunctionsLink (data) {
    try {
      const res = await Client.delete(`/oboz2-user-access-worker-role-function-links-crud/v1/delete_link/${data.uuid}/${data.funcUuid}`)
      return Promise.resolve(res.data)
    } catch (e) {
      throw e
    }
  },
  async getContractorRoles () {
    try {
      const res = await Client.get(`/oboz2-user-access-contractor-roles-viewer/v1/contractor_roles`)
      return res.data && res.data.contractor_roles || []
    } catch (e) {
      throw new Error(e)
    }
  }
}


