import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  async getContractorRoles() {
    try {
      const res = await Client.get('/oboz2-user-access-contractor-roles-viewer/v1/contractor_roles')
      return res.data && res.data.contractor_roles || []
    } catch (e) {
      throw e
    }
  },
}
