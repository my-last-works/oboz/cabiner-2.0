import RestClient from './RestClient'
import { uuid } from 'vue-uuid'

let Client = new RestClient({ isErrorThrow: true })

const iconMap = {
  administrator: 'administrator',
  moderator: 'system-moderator',
  expeditor: 'domain-owner',
  client: 'client',
  carrier: 'carrier',
  agent: 'agent',
  anonymous: 'profile',
  clicar: 'domain-owner'
};

const locationIcon = ({name} = {}) => {
  switch (name) {
    case 'Аэропорт':
      return 'airplane-sm';
    case 'ЖД станция':
      return 'train-sm';
    case 'Контейнерный терминал':
      return 'container-sm';
    case 'Морской Порт':
      return 'ship-sm';
    case 'Опорная точка':
      return 'city-sm';
    case 'Пограничный переход':
      return 'restrict-sm';
    case 'Пункт таможенного оформления/СВХ':
      return 'police-sm';
    default:
      return '';
  }
};

export default {
  async getContractsDraftList ({ page, size, query, sort }) {
    try {
      const res = await Client.get('/oboz2-contract-contracts-viewer/v1/contracts/drafts', {
        params: { page, size, q: query, sort }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getContractsList ({ page, size, query, sort, status, direction }) {
    try {
      let res = await Client.get('/oboz2-contract-contracts-viewer/v1/contracts', {
        params: { page, size, q: query, sort, status, direction }
      })

      let content = []

      res.data.content.forEach((item) => {
        content.push({
          ...item,
          isTariffAgreement: item.contract_type_title?.toLowerCase() === 'тарифное соглашение'
        })
        if (item.tariff_agreements?.length) {
          content = content.concat(item.tariff_agreements.map((item) => {
            return {
              ...item,
              isTariffAgreement: true,
              isChild: true
            }
          }))
        }
      })

      res.data.content = content

      return res.data
    } catch (e) {
      throw e
    }
  },
  async createContract ({ contractTypeUuid } = {}) {
    try {
      const res = await Client.post('/oboz2-contract-contracts-crud/v1/contract', { contractTypeUuid })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async saveTariffAgreement ({ uuid, parentContractUuid, fromDate, toDate, isTimeless } = {}) {
    try {
      const res = await Client.put(`/oboz2-contract-contracts-crud/v1/contracts/tariff_agreement/${uuid}`, { parentContractUuid, fromDate, toDate, isTimeless })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getTariffAgreements ({ uuid } = {}) {
    try {
      const res = await Client.get(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}/tariff_agreements`)
      return res.data.tariffAgreements
    } catch (e) {
      throw e
    }
  },
  async copyContract ({ uuid }) {
    try {
      let res = await Client.post(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}/copy`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async copyContractV2 ({ uuid, isChildInclude = false }) {
    try {
      let res = await Client.post(`/oboz2-contract-contracts-crud/v2/contracts/${uuid}/copy`, null, {
        params: {
          is_child_include: isChildInclude
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getContractDraft ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async saveContractDraft ({ data, type }) {
    try {
      let sendData = {}
      let fields
      let url = `/oboz2-contract-contracts-crud/v1/contracts/${data.uuid}`

      if (type === 'TARIFF_AGREEMENT') {
        url = `/oboz2-contract-contracts-crud/v1/contracts/tariff_agreement/${data.uuid}`
        sendData.parentContractUuid = data.parentContractData?.parentContractUuid

        fields = [
          'fromDate',
          'toDate',
          'isTimeless'
        ]
      } else {
        fields = [
          'clientUuid',
          'contractTypeUuid',
          'fromDate',
          'toDate',
          'originalNumber',
          'originalSignedDate',
          'clientSignerFio',
          'clientCuratorUuid',
          'doerSignerFio',
          'doerCuratorUuid',
          'clientAuthoritySource',
          'doerAuthoritySource',
          'clientEmail',
          'clientPhone',
          'clientProcuratoryFileUuid',
          'doerProcuratoryFileUuid',
          'clientTerminationUnilateralPeriod',
          'needNotifyClientByMail',
          'needNotifyDoerByMail',
          'needNotifyDoerByJurMail',
          'needNotifyClientByJurMail',
          'needNotifyDoerByEmail',
          'needNotifyClientByEmail',
          'needNotifyDoerByPhone',
          'needNotifyClientByPhone',
          'isTimeless',
          'doerEmail',
          'doerPhone'
        ]

        if (type !== 'TRACKING') {
          fields = fields.concat([
            'doerUuid',
            'doerTerminationUnilateralPeriod',
          ])
        }
      }

      fields.forEach((item) => {
        if (type !== 'TARIFF_AGREEMENT' || data[item]) {
          sendData[item] = data[item]
        }
      });

      const res = await Client.put(url, sendData)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteContractDraft ({ uuid }) {
    try {
      const res = await Client.delete(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async signContractDraft ({ uuid }) {
    try {
      const res = await Client.put(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}/signing`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async signContract ({ uuid }) {
    try {
      const res = await Client.put(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}/sign`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async withdrawContract ({ uuid }) {
    try {
      const res = await Client.put(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}/signing/remove`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async closingContract ({ uuid, closingDate }) {
    try {
      const res = await Client.put(`/oboz2-contract-contracts-crud/v1/contracts/${uuid}/closing`, {
        closingDate
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getContractTypes () {
    try {
      const res = await Client.get(`/oboz2-dictionary-contract-types-viewer/v1/`);
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getParticipantList ({ query, page, size }) {
    try {
      const res = await Client.get('/oboz2-security-participants-crud/v1/participants/by_roles', {
        params: { title: query, page, size }
      });

      let content = res.data.participants || res.data.content
      content = content.map(item => {
        return {
          ...item,
          icon: `role-${iconMap[item.roleCode] || iconMap.anonymous}`
        }
      })

      if (res.data.page !== undefined) {
        return {
          ...res.data,
          content
        }
      } else {
        return content
      }
    } catch (e) {
      throw e
    }
  },
  async getUserListByParticipant ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-user-access-users-crud/v1/contractors/${uuid}/workers`)
      let data = res.data.users;
      return data.map(item => {
        return {
          ...item,
          title: item.first_name + ' ' + item.last_name
        }
      })
    } catch (e) {
      throw e
    }
  },
  async getPaticipantInfo ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-security-participants-crud/v1/participants/info/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getContractServices ({ uuid, page, size, query, sort, caste }) {
    try {
      const res = await Client.get(`/oboz2-contract-services-viewer/v1/contracts/${uuid}/services`, { params: { page, size, query, sort, caste } })
      res.data.content = res.data.content.map((item) => {
        const data = item.trackingService || item.service
        return { ...data }
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getServiceDetail ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-contract-services-viewer/v1/services/${uuid}`)
      return res.data.trackingService || res.data.service
    } catch (e) {
      throw e
    }
  },
  async createService ({ data, serviceCaste }) {
    try {
      const res = await Client.post(`/oboz2-contract-services-crud/v2/services/${uuid.v4()}`, {
        serviceDictUuid: data.serviceDictUuid,
        contractUuid: data.contractUuid,
        serviceUnitUuid: data.serviceUnitUuid,
        resourceTypeUuid: data.resourceTypeUuid,
        isVatInclude: data.isVatInclude,
        tariff: data.tariff || {},
        extraMileageTariff: data.extraMileageTariff,
        extraPointTariff: data.extraPointTariff,
        vatType: data.vatType,
        vatRate: data.vatRate,
        serviceCaste,
        geography: data.geography
      })
      return res.data.serviceUuid
    } catch (e) {
      throw e
    }
  },
  async copyService ({ data, sourceUuid }) {
    try {
      const res = await Client.post(`/oboz2-contract-services-crud/v2/services/copy/${sourceUuid}`, {
        uuid: uuid.v4(),
        serviceDictUuid: data.serviceDictUuid,
        contractUuid: data.contractUuid,
        serviceUnitUuid: data.serviceUnitUuid,
        resourceTypeUuid: data.resourceTypeUuid,
        tariff: data.tariff || {},
        extraMileageTariff: data.extraMileageTariff || {},
        extraPointTariff: data.extraPointTariff || {}
      })
      return res.data.serviceUuid
    } catch (e) {
      throw e
    }
  },
  async updateService ({ data, uuid }) {
    let priceObject = data.serviceCaste === 'SIMPLE'
      ? { extraMileageTariff: data.extraMileageTariff, extraPointTariff: data.extraPointTariff }
      : { tariff: data.tariff }

    try {
      const res = await Client.put(`/oboz2-contract-services-crud/v2/services/${uuid}`, {
        serviceCaste: data.serviceCaste,
        isVatInclude: data.isVatInclude,
        ...priceObject,
        gsmTariff: data.gsmTariff,
        onboardDeviceTariff: data.onboardDeviceTariff,
        mobileAppTariff: data.mobileAppTariff,
        vatType: data.vatType,
        vatRate: data.vatRate,
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createServiceOld ({ data }) {
    try {
      const res = await Client.post(`/oboz2-contract-services-crud/v1/services`, data)
      return res.data.serviceUuid
    } catch (e) {
      throw e
    }
  },
  async updateServiceOld ({ data, uuid }) {
    try {
      const res = await Client.put(`/oboz2-contract-services-crud/v1/services/${uuid}`, {
        ...data
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createServiceTracking ({ data }) {
    try {
      const res = await Client.post(`/oboz2-contract-services-crud/v1/services/tracking`, data)
      return res.data.serviceUuid
    } catch (e) {
      throw e
    }
  },
  async updateServiceTracking ({ data, uuid }) {
    try {
      const res = await Client.put(`/oboz2-contract-services-crud/v1/services/${uuid}/tracking`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteService ({ uuid, serviceCaste }) {
    try {
      const res = await Client.delete(`/oboz2-contract-services-crud/v1/services/${uuid}`, {
        data: {
          serviceCaste
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteServiceTracking ({ uuid, serviceCaste }) {
    try {
      const res = await Client.delete(`/oboz2-contract-services-crud/v1/services/${uuid}/tracking`, {
        data: {
          serviceCaste
        }
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getServiceDirections ({ uuid, page, size = 1000, query, sort } = {}) {
    try {
      const res = await Client.get(`/oboz2-contract-services-viewer/v1/services/${uuid}/directions`, { params: { page, size, query, sort } })
      let result = []

      res.data.content.forEach((item) => {
        let group = item.group
        if (group) {
          let from = group.locationTitle;
          let locations = group.directions;
          locations.forEach((to) => {
            let { directionUuid, locationTitle, tariffs, vatType, vatRate, transboundary, routeCategory, locationFromUuid } = to;
            result.push({
              directionUuid,
              locationFromUuid,
              from,
              from_icon: 'city-sm',
              to: locationTitle,
              to_icon: 'city-sm',
              ...tariffs,
              vatType,
              vatRate,
              transboundary,
              routeCategory
            })
          })
        } else {
          let { directionUuid, locationTitleFrom, locationTitleTo, tariffs, vatType, vatRate, transboundary, routeCategory, locationFromUuid } = item;
          result.push({
            directionUuid,
            locationFromUuid,
            locationTitleFrom,
            locationTitleTo,
            ...tariffs,
            tariffTotal: tariffs?.tariff?.priceWithVat || 0,
            extraPointTariffTotal: tariffs?.extraPointTariff?.priceWithVat || 0,
            extraMileageTariffTotal: tariffs?.extraMileageTariff?.priceWithVat || 0,
            vatType,
            vatRate,
            transboundary,
            routeCategory
          })
        }
      });
      res.data.content = result;

      return res.data
    } catch (e) {
      throw e
    }
  },
  async getServiceDirectionsMultistop ({ uuid, page, size, query, sort }) {
    try {
      const res = await Client.get(`/oboz2-contract-services-viewer/v1/services/${uuid}/multistops`, { params: { page, size, query, sort } })

      res.data.content = res.data.content.map(item => {
        return {
          ...item,
          ...item.tariffs
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createDirection ({ data }) {
    try {
      let saveData = {
        serviceUuid: data.serviceUuid,
        locationFromUuid: data.locationFromUuid,
        locationToUuid: data.locationToUuid,
        tariff: data.tariff,
        extraMileageTariff: data.extraMileageTariff,
        extraPointTariff: data.extraPointTariff,
        vatType: data.vatType,
        vatRate: data.vatRate,
        routeCategory: data.routeCategory,
        transboundary: data.transboundary,
      }

      const res = await Client.post(`/oboz2-contract-services-crud/v2/directions/${uuid.v4()}`, saveData)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getRouteParams ({ points = [] } = {}) {
    try {
      let postData = {
        points
      }

      const res = await Client.post(`/oboz2-order-route-calculator/v1/get_route_params`, postData)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async createDirectionOld ({ data }) {
    try {
      const res = await Client.post(`/oboz2-contract-services-crud/v1/directions`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createDirectionMultistop ({ data }) {
    let saveData = {
      serviceUuid: data.serviceUuid,
      points: data.points,
      tariff: data.tariff,
      extraMileageTariff: data.extraMileageTariff,
      extraPointTariff: data.extraPointTariff,
      vatType: data.vatType,
      vatRate: data.vatRate
    }

    try {
      const res = await Client.post(`/oboz2-contract-services-crud/v2/multistops`, saveData)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async updateDirection ({ data, uuid }) {
    let saveData = {
      tariff: data.tariff,
      extraMileageTariff: data.extraMileageTariff,
      extraPointTariff: data.extraPointTariff,
      vatType: data.vatType,
      vatRate: data.vatRate,
    };

    try {
      const res = await Client.put(`/oboz2-contract-services-crud/v2/directions/${uuid}`, saveData)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async updateDirectionOld ({ data, uuid }) {
    let saveData = {
      tariff: data.tariff,
      extraMileageTariff: data.extraMileageTariff,
      extraPointTariff: data.extraPointTariff
    }

    try {
      const res = await Client.put(`/oboz2-contract-services-crud/v1/directions/${uuid}`, saveData)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getDirectionDetail ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-contract-services-viewer/v1/directions/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteDirection ({ uuid }) {
    try {
      const res = await Client.delete(`/oboz2-contract-services-crud/v1/directions/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteDirectionMultistop ({ uuid }) {
    try {
      const res = await Client.delete(`/oboz2-contract-services-crud/v1/multistops/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getServiceLocations ({ uuid, page, size, query, sort }) {
    try {
      const res = await Client.get(`/oboz2-contract-services-viewer/v1/services/${uuid}/locations`, { params: { page, size, query, sort } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createLocation ({ data }) {
    try {
      let saveData = {
        tariff: data.tariff,
        serviceUuid: data.serviceUuid,
        locationUuid: data.locationUuid,
        vatType: data.vatType,
        vatRate: data.vatRate
      }

      const res = await Client.post(`/oboz2-contract-services-crud/v2/locations/${uuid.v4()}`, saveData)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createLocationOld ({ data }) {
    try {
      const res = await Client.post(`/oboz2-contract-services-crud/v1/locations`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async updateLocation ({ data, uuid }) {
    try {
      let saveData = {
        tariff: data.tariff,
        vatType: data.vatType,
        vatRate: data.vatRate
      }

      const res = await Client.put(`/oboz2-contract-services-crud/v2/locations/${uuid}`, saveData)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async updateLocationOld ({ data, uuid }) {
    try {
      const res = await Client.put(`/oboz2-contract-services-crud/v1/locations/${uuid}`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getLocationDetail ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-contract-services-viewer/v1/locations/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getDictionaryLocationDetail ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-dictionary-locations-crud/v1/${uuid}`)
      const icons = await Client.get('/oboz2-dictionary-location-types-viewer/v1/')

      res.data.icon = locationIcon(icons.data.content.find(key => key.uuid === res.data.location_type_uuid))

      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteLocation ({ uuid }) {
    try {
      const res = await Client.delete(`/oboz2-contract-services-crud/v1/locations/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getServiceMeasurementUnit({ q, page, size, uuid }) {
    try {
      return await Client.get('/oboz2-dictionary-service-measurement-units-crud/v1/' + uuid, { params: { q, page, size } } )
    } catch (e) {
      throw e
    }
  },
  async getServices ({ uuid, caste, query }) {
    try {
      let res = await Client.get(`/oboz2-contract-services-viewer/v1/contracts/${uuid}/services/available`, {
        params: { caste }
      })
      res = res.data.services.map((item) => {
        const unitTitle = item.serviceUnits[0].title;
        const unitUuid = item.serviceUnits[0].uuid;
        const title = item.resourceSpeciesTitle
          ? `${item.serviceTypeTitle}. ${item.resourceSpeciesTitle}. ${item.resourceSubtypeTitle} ${item.resourcePatternParameter}`
          : `${item.serviceTypeTitle}`;
        return {
          ...item,
          title,
          subtitle: unitTitle,
          serviceUnitUuid: unitUuid,
          uuid: item.serviceDictUuid
        }
      });
      if (query) {
        return res.filter(key => key.title.toLowerCase().includes(query.toLowerCase()))
      }
      return res
    } catch (e) {
      throw e
    }
  },
  async getLocations ({ page = null, size = null, query = null, sort = null }) {
    try {
      let res = await Client.get('/oboz2-dictionary-locations-crud/v1/dropdown', { params: { q: query, page, size, sort }})
      let icons = await Client.get('/oboz2-dictionary-location-types-viewer/v1/');
      res.data.content = res.data.content.map((item) => {
        item.icon = locationIcon(icons.data.content.find(key => key.uuid === item.location_type_uuid))
        return {
          ...item
        }
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getFilesName ({ uuids }) {
    try {
      let res = await Client.post(`/oboz2-security-file-storage/v1/names`, uuids)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async uploadFile ({ uuid, file }, contract_uuid) {
    try {
      const form = new FormData();
      const jsonData = {
        filename: file.name,
        contract_uuid
      };
      form.append('jsonData', JSON.stringify(jsonData));
      form.append('file', file);
      await Client.post(`/oboz2-contract-contracts-crud/v1/contract/file/${uuid}`, form)
      return uuid
    } catch (e) {
      throw e
    }
  },
  async deleteFile ({ uuid }) {
    try {
      await Client.delete(`/oboz2-contract-contracts-crud/v1/contract/file/${uuid}`)
      return uuid
    } catch (e) {
      throw e
    }
  },
  getFileByUuid(fileUuid) {
    return Client.get(`/oboz2-security-file-storage/v1/${fileUuid}`, { responseType: 'blob' })
  }
}


