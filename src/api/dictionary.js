import RestClient from './RestClient'

const Client = new RestClient()

export default {
  async getLegalEntities() {
    try {
      const res = await Client.get(`/oboz2-dictionary-legal-entities-crud/v1`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getRequisiteGroups() {
    try {
      const res = await Client.get(`/oboz2-dictionary-requisite-groups-crud/v1/`)
      return res.data && res.data.requisite_groups
    } catch (e) {
      throw e
    }
  },
  async dictionariesDropdown() {
    try {
      const res = await Client.get(`/oboz2-dictionary-dictionaries-viewer/v1/dropdown`)
      return res && res.data
    } catch (e) {
      throw e
    }
  },
  async dictionaryDropdownByType ({ code, page, size } = {}) {
    try {
      const res = await Client.get(`/oboz2-dictionary-dictionaries-viewer/v1/dropdown/${code}`, {
        params: { page, size }
      })
      return res && res.data
    } catch (e) {
      throw e
    }
  },
  async dictionaryInfo ({ code } = {}) {
    try {
      const res = await Client.get(`/oboz2-dictionary-dictionaries-viewer/v1/${code}`)
      return res && res.data
    } catch (e) {
      throw e
    }
  },
}
