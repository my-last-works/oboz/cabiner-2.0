import RestClient from "./RestClient";
import { uuid as vue_uuid } from "vue-uuid";

let Client = new RestClient();

export default {
  getRoute(points) {
    const data = {
      type: "route.request",
      route_uuid: vue_uuid.v4(),
      base_points: points,
    };
    return Client.post(`/oboz2-geo-routing-proxy/v1`, data);
  },
  async getAddressByCoordinates({ coordinates = [], lang = "RU" }) {
    try {
      // /oboz2-order-address-suggester/v1/get_place_by_coordinates/
      const res = await Client.get(
        `/oboz2-order-address-suggester/v1/get_place_by_coordinates/`,
        {
          params: { coordinates, lang },
        }
      );
      return (res && res.data) || null;
    } catch (e) {
      throw e;
    }
  },
  async searchAddress({ search_address = "", lang = "RU" }) {
    try {
      let res = await Client.get(
        `/oboz2-geo-geocoding-proxy/v1/search_address`,
        {
          params: { search_address, lang },
        }
      );
      return (res && res.data && res.data.results) || [];
    } catch (e) {
      throw e;
    }
  },
  async searchAddressSuggester({ search_address = "", lang = "RU" }) {
    try {
      // /oboz2-order-address-suggester/v2/search_address
      let res = await Client.get(
        `/oboz2-order-address-suggester/v1/search_address`,
        {
          params: { search_address, lang },
        }
      );
      return (res && res.data) || [];
    } catch (e) {
      throw e;
    }
  },

  async searchLocationsAddressSuggester({ search_address = "", lang = "RU" }) {
    try {
      let res = await Client.get(
        `/oboz2-geo-geocoding-proxy/v1/search_address/`,
        {
          params: { search_address, lang },
        }
      );
      return (res && res.data) || [];
    } catch (e) {
      throw e;
    }
  },

  async getLocationAddressByCoordinates({ coordinates = [], lang = "RU" }) {
    try {
      // /oboz2-order-address-suggester/v1/get_place_by_coordinates/
      const res = await Client.get(
        `/oboz2-geo-geocoding-proxy/v1/get_place_by_coordinates/`,
        {
          params: { coordinates, lang },
        }
      );
      return (res && res.data) || null;
    } catch (e) {
      throw e;
    }
  },

  async getLocations({ name }) {
    try {
      const res = await Client.post(
        "/oboz2-order-address-suggester/v2/locations/filter",
        { name }
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getLocationDescription(uuid) {
    try {
      const res = await Client.get(
        `/oboz2-dictionary-locations-crud/v1/${uuid}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getBaseByCoordinates(lat, lon) {
    try {
      const res = await Client.post(
        "/oboz2-logchain-base-location-by-address-detector/v1/",
        {
          bodyBaseLocation: [
            {
              coordinate: {
                lat,
                lon,
              },
            },
          ],
        }
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },
};
