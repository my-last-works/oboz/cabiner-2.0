import RestClient from '@/api/RestClient';

const Client = new RestClient();

export default {
  async getRequisiteGroups({ page, size, query, sort }) {
    try {
      const res = await Client.get(
        '/oboz2-security-requisite-groups-crud/v1/',
        { params: { page, size, sort, q: query } }
      )
      console.warn(res);
      return res.data && res.data.requisite_groups
        ? { data: { content: res.data.requisite_groups, total_elements: res.data.requisite_groups.length } }
        : { data: { error: true, content: [], total_elements: 0 } }
    } catch (e) {
      throw e
    }
  },
  async getRequisiteGroup(uuid) {
    try {
      const res = await Client.get(`/oboz2-security-requisite-groups-crud/v1/${uuid}`)
      return res.data && res.data || {}
    } catch (e) {
      throw e
    }
  },
  async createRequisiteGroup(data) {
    try {
      data.title.label = data.uuid;
      const res = await Client.post(`/oboz2-security-requisite-groups-crud/v1/`, data)
      return res.data && res.data || {}
    } catch (e) {
      throw e
    }
  },
  async editRequisiteGroup(uuid, data) {
    try {
      const res = await Client.put(`/oboz2-security-requisite-groups-crud/v1/${uuid}`, data)
      return res.data && res.data || {}
    } catch (e) {
      throw e
    }
  },
  async deleteRequisiteGroup(uuid) {
    try {
      const res = await Client.delete(`/oboz2-security-requisite-groups-crud/v1/${uuid}`)
      return res.data && res.data || {}
    } catch (e) {
      throw e
    }
  }
}
