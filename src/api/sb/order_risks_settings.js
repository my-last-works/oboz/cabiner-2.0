
import RestClient from '@/api/RestClient';

const Client = new RestClient();

export default {

  async getRisksSettings() {
    try {
      const { data } = await Client.get('/oboz2-security-cargo-risk-manager/v1/cost-and-lost-probability-to-color/all')
      return data
    } catch (e) {
      throw e
    }
  },

  async putRisksSettings(payload) {
    try {
      await Client.put('/oboz2-security-cargo-risk-manager/v1/cost-and-lost-probability-to-color/all', payload)
    } catch (e) {
      throw e
    }
  },

  async getRiskSettingsOptions() {
    try {
      const { data } = await Client.get('/oboz2-security-cargo-risk-manager/v1/cargo_cost_options');
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRiskCarrierSettings() {
    try {
      const { data } = await Client.get('/oboz2-security-cargo-risk-manager/v1/cargo-risk-to-carrier/all')
      return data
    } catch (e) {
      throw e
    }
  },

  async putRiskCarrierSettings(payload) {
    try {
      await Client.put('/oboz2-security-cargo-risk-manager/v1/cargo-risk-to-carrier/all', payload)
    } catch (e) {
      throw e
    }
  }
};