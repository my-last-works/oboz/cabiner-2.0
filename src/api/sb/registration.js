import RestClient from '@/api/RestClient';

const Client = new RestClient();

export default {
  async getInfoByInn (inn, uuid) {
    try {
      const { data } = await Client.get(`/oboz2-security-registration-form-viewer/v1/data/inn/${inn}/code/${uuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  }
}
