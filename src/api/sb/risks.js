import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  getWeights() {
    return Client.get('/oboz2-security-participant-assessment-weights-crud/v1/participant/fields/weights/')
  },
  getNumbers() {
    return Client.get('/oboz2-security-participant-group-weights-crud/v1/participants/groups/weights/')
  },
  getDeals() {
    return Client.get('/oboz2-security-contract-terms-risk-groups-crud/v1/participant/')
  },
  async getCargoLostRisk() {
    try {
      const response = await Client.get('/oboz2-security-cargo-risk-manager/v1/cargo_loss_risk');
      return response;
    } catch (e) {
      throw e;
    }
  },
  async putCargoLostRisk(uuid, payload) {
    try {
      await Client.put(`/oboz2-security-cargo-risk-manager/v1/cargo_loss_risk/${uuid}`, payload);
    } catch (e) {
      throw e;
    }
  }
}
