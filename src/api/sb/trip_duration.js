import RestClient from '@/api/RestClient';

const Client = new RestClient({ isErrorThrow: true });

export default {
  async getTripDurationOptions() {
    try {
      const { data } = await Client.get('/oboz2-security-cargo-risk-manager/v1/cargo-duration-options')
      return data
    } catch (e) {
      throw e
    }
  },

  async putTripDurationOptions(payload) {
    try {
      await Client.put('/oboz2-security-cargo-risk-manager/v1/cargo-duration-options', payload)
    } catch (e) {
      throw e
    }
  },
};
