import RestClient from './RestClient'

const Client = new RestClient({ isErrorThrow: true })

export default {
  async getClientsList ({ page, query }) {
    try {
      let res = await Client.get(`/oboz2-security-participants-crud/v1/participants/by_roles`, {
        params: {
          size: 100,
          page,
          q: query
        }
      })
      res.data.content = res.data.content.filter(item => item.roleCode === 'client')
      return res.data
    } catch (e) {
      throw e
    }
  },
  // Наценка
  async getMarkupSettings ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-account-clicar-settings-crud/v3/settings/markup`, {
        params: {
          clicar_uuid: uuid
        }
      })
      return res.data.markups
    } catch (e) {
      throw e
    }
  },
  async saveMarkupSettings ({ data }) {
    try {
      let res = await Client.post(`/oboz2-account-clicar-settings-crud/v3/settings/markup`, {
        markups: {
          ...data
        }
      })
      return res.data.markups
    } catch (e) {
      throw e
    }
  },
  async getMarkupClientSettings ({ page = 0, query = '', size = 20 } = {}) {
    try {
      let res = await Client.get(`/oboz2-account-clicar-settings-crud/v3/settings/markup/clients`, {
        params: {
          page, q: query, size
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async setMarkupClientSettings ({ clientUuid, markups }) {
    try {
      let res = await Client.post(`/oboz2-account-clicar-settings-crud/v3/settings/markup/${clientUuid}`, {
        markups
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteMarkupClientSettings ({ clientUuid }) {
    try {
      let res = await Client.delete(`/oboz2-account-clicar-settings-crud/v3/settings/markup/${clientUuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  // Лимит
  async getCostReductionSettings ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-account-clicar-settings-crud/v3/settings/cost_reduction`, {
        params: {
          clicar_uuid: uuid
        }
      })
      return res.data.costReductions
    } catch (e) {
      // throw e
      return {}
    }
  },
  async saveCostReductionSettings ({ data }) {
    try {
      let res = await Client.post(`/oboz2-account-clicar-settings-crud/v3/settings/cost_reduction`, {
        costReductions: {
          ...data
        }
      })
      return res.data.costReductions
    } catch (e) {
      throw e
    }
  },
  async getCostReductionClientSettings ({ page = 0, query = '', size = 20 } = {}) {
    try {
      let res = await Client.get(`/oboz2-account-clicar-settings-crud/v3/settings/cost_reduction/clients`, {
        params: {
          page, q: query, size
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async setCostReductionClientSettings ({ clientUuid, costReductions }) {
    try {
      let res = await Client.post(`/oboz2-account-clicar-settings-crud/v3/settings/cost_reduction/${clientUuid}`, {
        costReductions
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteCostReductionClientSettings ({ clientUuid }) {
    try {
      let res = await Client.delete(`/oboz2-account-clicar-settings-crud/v3/settings/cost_reduction/${clientUuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  // Автопоиск
  async getAutosearchSettings () {
    try {
      let res = await Client.get(`/oboz2-account-clicar-settings-crud/v3/settings/autosearch`)
      return res.data
    } catch (e) {
      // throw e
      return {}
    }
  },
  async saveAutosearchSettings ({ data }) {
    try {
      let res = await Client.post(`/oboz2-account-clicar-settings-crud/v3/settings/autosearch`, data)
      return res.data.costReductions
    } catch (e) {
      throw e
    }
  },
  // Личный кабинет
  async getPersonalAccountSettings () {
    try {
      const { data } = await Client.get(`/oboz2-account-clicar-settings-crud/v3/settings/personal_account`)
      return data
    } catch (e) {
      // throw e
      return {}
    }
  },
  async savePersonalAccountSettings ({ data }) {
    try {
      const res = await Client.post(`/oboz2-account-clicar-settings-crud/v3/settings/personal_account`, { generalSettings : data })
      return res.data.generalSettings
    } catch (e) {
      throw e
    }
  },
}
