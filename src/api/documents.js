import RestClient from './RestClient';

const Client = new RestClient();

export default {
  async getListDocuments() {
    try {
      const res = await Client.get('/oboz2-security-participants-crud/v1/documents');
      return res.data;
    } catch (e) {
      throw e;
    }
  },
  async loadTypeDocuments() {
    try {
      const res = await Client.get('/oboz2-dictionary-doc-types-crud/v1/dropdown');
      return res.data;
    } catch (e) {
      throw e;
    }
  },
  async saveNewDocument(document_uuid, params) {
    try {
      const res = await Client.post(`/oboz2-security-participants-crud/v1/documents/${document_uuid}`, params);
      return res.data;
    } catch (e) {
      throw e;
    }
  },
  async sendScanFiles(uuid, scan) {
      try {
      const res = await Client.post(`/oboz2-security-file-storage/v1/${uuid}`, scan);
      return res.data;
    } catch (e) {
      throw e;
    }
  },
  async deleteDocument(document_uuid) {
    try {
      const res = await Client.delete(`/oboz2-security-participants-crud/v1/documents/${document_uuid}`);
      return res.data;
    } catch (e) {
      throw e;
    }
  }
}