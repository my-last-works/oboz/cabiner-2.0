import RestClient from './RestClient'
import { formationParameters } from '@/helpers';


const Client = new RestClient()

export default {
    getCustomsUnions(page, size, q, sort) {
        return Client.get('/oboz2-dictionary-customs-unions-crud/v1' + formationParameters(page, size, q, sort));
    },
    getCustomsUnionsItem(uuid) {
        return Client.get('/oboz2-dictionary-customs-unions-crud/v1/' + uuid)
    },
    createCustomsUnion(data) {
        return Client.post('/oboz2-dictionary-customs-unions-crud/v1/', data)
    },
    deleteCustomsUnion(uuid) {
        return Client.delete('/oboz2-dictionary-customs-unions-crud/v1/' + uuid)
    },
    editCustomsUnion(uuid, data) {
        return Client.put('/oboz2-dictionary-customs-unions-crud/v1/' + uuid, data)
    }
}
