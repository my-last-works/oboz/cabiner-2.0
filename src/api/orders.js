import RestClient from "./RestClient";
import { orderItem } from "@/resources/order";
// import { compareValues } from "@/helpers"

let Client = new RestClient({ isErrorThrow: true });

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const requestRetry = async ({ url, n }) => {
  try {
    return await Client.get(url);
  } catch (e) {
    if (e.status == 500 || e.status == 404) throw e;
    if (n <= 1) throw e;
    await sleep(500);
    return await requestRetry({ url, n: n - 1 });
  }
};

export default {
  async getDrafts({
    page = 0,
    size,
    field = "created_at",
    direction = "desc",
    query,
  }) {
    try {
      const res = await Client.get(
        "/oboz2-order-po-draft-list-viewer/v1/list/",
        {
          params: { page, size, field, direction, filter: query },
        }
      );
      // res.data.content = res.data.content.sort(compareValues('createdAt', 'desc'));
      return res.data;
    } catch (e) {
      throw e;
    }
  },
  getOrders({ page = 0, size = 100 } = {}) {
    return Client.get(
      "/oboz2-order-po-draft-viewer/v1/filtered/status/FORMED/list",
      {
        params: { page, size },
      }
    );
  },
  saveOrder(uuid, data) {
    return Client.put("/oboz2-order-po-draft-saver/v2/drafts/" + uuid, data);
  },
  saveOrderByTariff(tariffUUID, id) {
    return Client.put(
      `/oboz2-order-po-draft-saver/v2/drafts/${id}/tariff/${tariffUUID}`
    );
  },
  getOrder(uuid) {
    return Client.get("/oboz2-order-po-draft-viewer/v2/" + uuid);
  },
  getOrderMock() {
    return Promise.resolve(orderItem);
  },
  changeOrderStatus(uuid, status) {
    return Client.put(
      `/oboz2-order-po-draft-saver/v2/change_status/order/${uuid}/status/${status}`
    );
  },
  getOrderId(type) {
    let data = {
      bodyGenerator: {
        entityType: type,
      },
    };
    return Client.post("/oboz2-code-generator/v1/generate", data);
  },

  async findLogchains(payload) {
    const { transactionUuid } = payload;
    await Client.post(`/oboz2-order-logchains-crud/v1/list`, payload);
    try {
      const verify = await requestRetry({
        url: `/oboz2-common-transaction-manager/v1/transaction/${transactionUuid}`,
        n: 18,
      });

      return verify.data;
    } catch (e) {
      throw e;
    }
  },

  async getLogchainErrors(uuid) {
    try {
      const res = await Client.get(`/oboz2-order-po-draft-viewer/v1/order/${uuid}/calculation_errors`);
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async saveLogchain(uuid) {
    try {
      await Client.post(`/oboz2-order-logchains-crud/v1/${uuid}/choose`);
    } catch (e) {
      throw e;
    }
  },

  async createGoGp(uuid) {
    try {
      await Client.post(
        `/oboz2-order-loading-and-unloading-points-saver/v1/create/${uuid}`
      );
    } catch (e) {
      throw e;
    }
  },

  async getTransport(code) {
    try {
      const res = await Client.get(
        `/oboz2-order-po-draft-viewer/v1/service_type_by_code/${code}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getTransportTypes(draftUuid, uuid) {
    try {
      const res = await Client.get(
        `/oboz2-order-po-draft-viewer/v1/dropdown/${draftUuid}/by_service_type/${uuid}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  getOrderTariff(orderUuid, data) {
    return Client.post(
      `/oboz2-order-contracts-viewer/v1/fetch_tariffs/${orderUuid}/`,
      data
    );
  },

  async setOrderManualTariff(uuid, amount) {
    try {
      const res = await Client.post(
        `/oboz2-order-contracts-viewer/v1/manual_tariff/draft/${uuid}/price/${amount}/currency/RUR`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async fetchContracts(payload) {
    try {
      const res = await Client.post(`/oboz2-order-contracts-viewer/v1/fetch_actual_contracts`, payload);
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async saveContract(uuid, payload) {
    try {
      await Client.post(`/oboz2-order-contracts-viewer/v1/save_actual_contract/order/${uuid}`, payload);

    } catch (e) {
      throw e;
    }
  },

  getOrderLogist(contractorUuid, clientUuid) {
    return Client.get(
      `/oboz2-order-responsible-worker-selector/v1/responsible_worker/contractor/${contractorUuid}/client/${clientUuid}`
    );
  },

  createLogChain(data) {
    return Client.post(`/oboz2-logchain-chain-creator/v1/`, data);
  },

  getOrderRisks(type, cost) {
    return Client.get(
      `/oboz2-order-risk-calculator/v1/risk_group/cargo_type/${type}/cost/${cost}`
    );
  },
  getVisitPoint({ uuid }) {
    return Client.get(
      `/oboz2-order-loading-and-unloading-points-saver/v1/point/${uuid}`
    );
  },

  updateVisitPoint(payload) {
    return Client.post(
      `/oboz2-order-loading-and-unloading-points-saver/v1/update_visit_points`,
      payload
    );
  },

  getLol(lol) {
    //@remove
    return Client.get("/dictionary-lol-types/" + lol);
  },

  async uploadOrderFromFile(file) {
    const form = new FormData();
    const jsonData = {
      filename: file.file.name,
    };
    form.append("jsonData", JSON.stringify(jsonData));
    form.append("file", file.file);
    try {
      await Client.post(`/oboz2-common-file-storage/v1/${file.uuid}`, form);
    } catch (e) {
      throw e;
    }
  },

  async confirmUploadOrderFromFile(uuid) {
    try {
      await Client.post(`/oboz2-order-po-draft-file-uploader/v1/${uuid}`);
    } catch (e) {
      throw e;
    }
  },

  async downloadTemplate(uuid) {
    try {
      const res = await Client.get(`/oboz2-common-file-storage/v1/${uuid}`, {
        responseType: "arraybuffer",
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  deleteOrder(uuid) {
    return Client.delete(`/oboz2-order-po-draft-saver/v1/${uuid}/parameters`);
  },

  async getOrderContacts(uuid, query = "") {
    try {
      const res = await Client.get(
        `/oboz2-order-client-info-crud/v1/${uuid}/search_contact_fios`,
        query
      );
      return res;
    } catch (e) {
      throw e;
    }
  },

  async getOrderContactPhones(uuid, fio) {
    try {
      const res = await Client.get(
        `/oboz2-order-client-info-crud/v1/${uuid}/search_contact_phones`,
        { params: { fio: fio } }
      );
      return res;
    } catch (e) {
      throw e;
    }
  },

  async getOrderOnlinePrice(uuid) {
    try {
      const res = await Client.post(
        `/oboz2-order-contracts-viewer/v1/online_tariff/draft/${uuid}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async validatePoints(payload) {
    try {
      const res = await Client.post(
        `/oboz2-order-loading-and-unloading-points-saver/v1/validation/is_save/false`,
        payload
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async checkContinueFirst(uuid) {
    try {
      const res = await Client.get(
        `/oboz2-order-po-draft-saver/v1/${uuid}/checkContinueFirst`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async chooseContractor(uuid) {
    try {
      const { data } = await Client.put(
        `/oboz2-order-po-draft-saver/v1/tms/choose_contractor/${uuid}`
      );

      return data;
    } catch (e) {
      throw e;
    }
  },

  async updateParameters(uuid, service) {
    try {
      await Client.post(`/oboz2-order-po-draft-saver/v1/${uuid}/parameters`, {
        services: service,
      });
    } catch (e) {
      throw e;
    }
  },

  async getTemperatureRegim(uuid) {
    try {
      const res = await Client.get(
        `/oboz2-order-po-draft-viewer/v2/dropdown/temperatureRegim/${uuid}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async initTrackingOrder() {
    try {
      const res = await Client.get(`/oboz2-order-trackings-crud/v1/empty_form`);
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getTrackingOrder(uuid) {
    try {
      const res = await Client.get(
        `/oboz2-order-trackings-crud/v2/order/${uuid}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getTrackingOrderDetails(uuid) {
    try {
      const res = await Client.get(
        `/oboz2-order-trackings-crud/v1/order_details/${uuid}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getRoute() {
    try {
      const res = await Client.get(
        `https://router.project-osrm.org/route/v1/driving/37.617635,55.755814;30.315868,59.939095?overview=false&alternatives=true&steps=true`
      );
      return res;
    } catch (e) {
      throw e;
    }
  },

  async saveTrackingOrder(uuid, payload) {
    try {
      await Client.post(
        `/oboz2-order-trackings-crud/v2/tracking_saver/${uuid}`,
        payload
      );
    } catch (e) {
      throw e;
    }
  },

  async getServiceCost(payload) {
    try {
      const res = await Client.post(
        `/oboz2-order-trackings-crud/v1/cost_calculation`,
        payload
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async fetchTrack(id) {
    try {
      const res = await Client.get(
        `/oboz2-order-trackings-crud/v1/fetch_track/${id}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getSuborders(uuid) {
    try {
      const res = await requestRetry({
        url: `/oboz2-order-po-crud/v1/orders/${uuid}/suborders`,
        n: 1000,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getCargoCodes({ page = 0, size = 20, query }) {
    try {
      const res = await Client.get(
        `oboz2-dictionary-etsng-viewer/v1/dropdown`,
        {
          params: { page, size, q: query },
        }
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getCargoCode(uuids) {
    try {
      const res = await Client.post(
        `oboz2-dictionary-etsng-viewer/v1/internal/get_by_uuids`,
        uuids
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getLogistsList(contractor, client) {
    try {
      const res = await Client.get(
        `oboz2-order-responsible-worker-selector/v1/list_responsible_workers/contractor/${contractor}/client/${client}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getDeviceStatus(uuid) {
    try {
      const res = await Client.get(
        `oboz2-order-trackings-crud/v1/device_status/${uuid}`
      );
      return res;
    } catch (e) {
      throw e;
    }
  },

  async trackingOrderAction(uuid, action) {
    try {
      const res = await Client.post(
        `oboz2-order-trackings-crud/v1/tracking_order/${uuid}/action/${action}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getSimpleOrders(
    {
      page = 0,
      size,
      field = "created_at",
      direction = "desc",
      query,
    }
  ) {
    try {
      const res = await Client.get(
        "/oboz2-draft-order-simple-order-crud/v1",
        {
          params: { page, size, field, direction, filter: query },
        }
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getOrderLink(uuid) {
    try {
      const res = await Client.get(
        `/oboz2-order-trackings-crud/v1/order_link/${uuid}`
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getServicesForSimpleOrders(code) {
    try {
      const res = await Client.get(`/oboz2-order-po-draft-viewer/v1/services_and_types_by_code/${code}`);
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async createSimpleOrder(uuid, payload) {
    try {
      const res = await Client.put(`/oboz2-draft-order-simple-order-crud/v1/${uuid}`, payload);
      return res;
    } catch (err) {
      throw err
    }
  },

  async RelocationFromAnonymous(uuid) {
    try {
      await Client.put(`/oboz2-order-po-draft-saver/v1/relocation_from_anonymous/${uuid}?authorization_type=LOGIN`)
    } catch (e) {
      throw e
    }
  },

  async publishSimpleOrder(uuid) {
    try {
      const res = await Client.post(`/oboz2-draft-order-simple-order-crud/v1/${uuid}/FORMED`)
      return res;
    } catch (e) {
      throw e
    }
  },

  async deleteSimpleOrder(uuid) {
    try {
      const res = await Client.delete(`/oboz2-draft-order-simple-order-crud/v1/${uuid}`)
      return res;
    } catch (e) {
      throw e
    }
  },

  async getParticipantForAnonymous() {
    try {
      const res = await Client.get(`oboz2-order-po-draft-viewer/v1/participant/for_not_authorized`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getAnonymousSettings(clicarUuid, typeRequest) {
    try {
      const res = await Client.get(`/oboz2-clicar-account-settings-crud/v1/settings/${clicarUuid}?type=${typeRequest}`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getUnauthorizedTrackingOrder(uuid) {
    try {
      const res = await Client.get(`/oboz2-order-trackings-crud/v1/unauthorized/order/${uuid}`, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },

  async resetSnapshots(uuid) {
    try {
      const res = await Client.post(`/oboz2-order-contracts-viewer/v1/reset_snapshots/${uuid}`)
      return res.data
    } catch (e) {
      throw(e)
    }
  },

  async saveRoute(uuid, route) {
    try {
      const { data } = await Client.put(`/oboz2-order-po-draft-saver/v1/${uuid}/route`, route)
      return data
    } catch (e) {
      throw e
    }
  },

  async getPublicLink(uuid) {
    try {
      const { data } = await Client.post(`/oboz2-order-trackings-crud/v1/${uuid}/create_and_get_public_link`)
      return data
    } catch (e) {
      throw e
    }
  },

  async getRisks(uuid) {
    try {
      const { data } = await Client.get(`/oboz2-order-po-draft-viewer/v1/order/${uuid}/risks`)
      return data
    } catch (e) {
      throw e
    }
  },

  async getInvoicePrintForm(uuid) {
    try {
      const res = await Client.get(`oboz2-order-po-draft-viewer/v1/invoice/print-form/PDF/?po_draft_uuid=${uuid}`, { responseType: 'blob' })
      return res
    } catch (e) {
      throw e
    }
  },

  async getreplaceableServices(uuid) {
    try {
      const { data } = await Client.get('oboz2-order-logchains-crud/v1/replaceable_services',
        {
          params: {
            draft_uuid: uuid
          }
        }
      )
      return data
    } catch (e) {
      throw e
    }
  },

  async replaceService(uuid) {
    try {
      const res = await Client.post(`oboz2-order-logchains-crud/v1/replaceable_services?logchain_point_service_uuid=${uuid}`)
      return res
    } catch (e) {
      throw e
    }
  },

  async copyVisitPoint(newUuid, donorUuid) {
    try {
      const { data } = await Client.post(`oboz2-order-po-draft-saver/v1/route_point/${newUuid}?default_route_point_uuid=${donorUuid}`)
      return data
    } catch (e) {
      throw e
    }
  },

  async deleteVisitPoint(uuid) {
    try {
      await Client.delete(`oboz2-order-po-draft-saver/v1/route_point/${uuid}`)
    } catch (e) {
      throw e
    }
  },

  async recalculationDistancePrice(pointUuid, orderUuid) {
    try {
      await Client.post(`oboz2-order-loading-and-unloading-points-saver/v1/recalculation_distance_price/${pointUuid}?order_uuid=${orderUuid}`)
    } catch (e) {
      throw e
    }
  },

  async fetchPaymentAccount(orderUuid) {
    try {
      return await Client.get(`/oboz2-order-po-draft-saver/v1/${orderUuid}/payment_account`);
    } catch (e) {
      throw e
    }
  },

  async savePaymentAccount(orderUuid, data) {
    try {
      return await Client.post(`/oboz2-order-po-draft-saver/v1/${orderUuid}/payment_account`, data);
    } catch (e) {
      throw e
    }
  },
};
