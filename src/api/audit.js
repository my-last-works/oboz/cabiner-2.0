import RestClient from './RestClient'

let Client = new RestClient()

export default {
  async getHistory({schema = 'dictionaries', type, uuid}, page, size) {
    try {
      const res = await Client.get('/oboz2-dictionary-entity-audit-service/v1/' + schema + '.' + type + '/' + uuid, {
        params: {number: page, size}
      });
      return res
    } catch (e) {
      throw e
    }
  }
}
