import RestClient from './RestClient';
// import { formationParameters } from '@/helpers';

let Client = new RestClient();

export default {
  getVersions() {
    return Client.get('/oboz2-scoring-version-crud/v1/nn/versions/')
  },
  getSingleVersion(uuid) {
    return Client.get('/oboz2-scoring-version-crud/v1/nn/versions/' + uuid + '/')
  },
  setSingleVersion(uuid, data) {
    return Client.put('/oboz2-scoring-version-crud/v1/nn/versions/' + uuid + '/', data)
  },
  activateVersion(uuid){
    return Client.put(`/oboz2-scoring-version-crud/v1/nn/versions/${uuid}/active/`, {})
  },
  getSettings() {
    return Client.get('/oboz2-scoring-retraining-schedule-crud/v1/settings/')
  },
  setSettings(data) {
    return Client.patch('/oboz2-scoring-retraining-schedule-crud/v1/settings/', data)
  },
  updateSettings(data) {
    return Client.patch('/oboz2-scoring-retraining-schedule-crud/v1/settings/', data)
  },
}
