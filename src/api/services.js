import RestClient from './RestClient';
import { formationParameters } from '@/helpers';

let Client = new RestClient();

export default {
    getServices(page, size, q, sort) {
        return Client.get('/oboz2-dictionary-services-crud/v1' + formationParameters(page, size, q, sort))
    },
    getService(uuid) {
        return Client.get('/oboz2-dictionary-services-crud/v1/' + uuid)
    },
    createService(data) {
        return Client.post('/oboz2-dictionary-services-crud/v1/', data)
    },
    editService(data, groupUuid) {
        return Client.put('/oboz2-dictionary-services-crud/v1/' + groupUuid, data)
    },
    deleteService(uuid) {
        return Client.put('/oboz2-dictionary-services-crud/v1/' + uuid + '/true')
    },
    getServiceTypesList(page, size, q, sort) {
        return Client.get('/oboz2-dictionary-service-types-crud/v1/' + formationParameters(page, size, q, sort))
    },
    getServiceTypes(page, size, q, sort) {
        return Client.get('oboz2-dictionary-service-viewer/v1/service_types/list/' + formationParameters(page, size, q, sort))
    },
    getServiceType(uuid) {
        return Client.get('/oboz2-dictionary-service-types-crud/v1/' + uuid)
    },
    async createServiceType(uuid, data) {
        try {
            await Client.post(`/oboz2-dictionary-service-types-crud/v2/${uuid}`, data)
        } catch (e) {
            throw e
        }
    },
    deleteServiceType(uuid) {
        return Client.delete('/oboz2-dictionary-service-types-crud/v1/' + uuid)
    },
    getServiceMeasurementUnits(page, size, q, sort) {
        return Client.get('/oboz2-dictionary-service-measurement-units-crud/v1' + formationParameters(page, size, q, sort))
    },
    getServiceMeasurementUnit(uuid) {
        return Client.get('/oboz2-dictionary-service-measurement-units-crud/v1/' + uuid)
    },
    createServiceMeasurementUnit(data) {
        return Client.post('/oboz2-dictionary-service-measurement-units-crud/v1/', data)
    },
    editServiceMeasurementUnit(uuid, data) {
        return Client.put('/oboz2-dictionary-service-measurement-units-crud/v1/' + uuid, data)
    },
    deleteServiceMeasurementUnit(uuid) {
        return Client.put('/oboz2-dictionary-service-measurement-units-crud/v1/' + uuid + '/true')
    },
    async getServiceRequirements() {
        try {
            const res = await Client.get('/oboz2-dictionary-service-requirements-crud/v1/list/');
            return res.data;
        } catch (e) {
            throw e;
        }
    },
    async createServiceRequirements(payload) {
        try {
            await Client.post('/oboz2-dictionary-service-requirements-crud/v1/', payload);
        } catch (e) {
            throw e;
        }
    },
    async editServiceRequirements(uuid) {
        try {
            await Client.put(`/oboz2-dictionary-service-requirements-crud/v1/${uuid}`);
        } catch (e) {
            throw e;
        }
    },
    async deleteServiceRequirements(uuid) {
        try {
            await Client.delete(`/oboz2-dictionary-service-requirements-crud/v1/${uuid}`);
        } catch (e) {
            throw e;
        }
    }
}
