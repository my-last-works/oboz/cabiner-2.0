import RestClient from './RestClient'

const Client = new RestClient({ isErrorThrow: true })

export default {
  getByRole (uuid) {
    return Client.get(`/oboz2-user-access-contractors-crud/v1/${uuid}`);
  },

  createContractor (uuid, data) {
    return Client.post(`/oboz2-dictionary-consignors-and-consignees-crud/v1/${uuid}`, data);
  },

  getContractorByTitle ({ ownerUuid, title }) {
    return Client.get(`/oboz2-dictionary-consignors-and-consignees-crud/v1/contractor_uuid_by_name/${title}/owner/${ownerUuid}`);
  },

  addTypeToContractor({ contractorUuid, ownerUuid, type }) {
    return Client.put(`/oboz2-dictionary-consignors-and-consignees-crud/v1/contractor/${contractorUuid}/owner/${ownerUuid}/type/${type}`);
  },

  getContractorsByDistance ({ uuid, type, coordinates, limit = 5 } = {}) {
    const payload = { ownerUuid: uuid, type, coordinates };
    return Client.post(`/oboz2-order-infographs-crud/v1/fetch_contractors/limit/${limit}`, payload);
  },

  getContractorsByType ({ ownerUuid, type, list =  [] } = {}) {
    const payload = {};
    if (list.length) payload.list = list;

    return Client.post(
      `/oboz2-dictionary-consignors-and-consignees-crud/v1/fetch_list_exclude_uuid/type/${type}/owner/${ownerUuid}`,
      payload
    );
  }
}
