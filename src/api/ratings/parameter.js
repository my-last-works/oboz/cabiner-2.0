import RestClient from '../RestClient'

const Client = new RestClient()

export default {
  async fetchParametersOfMethodology(methodology_uuid) {
    try {
      let res = await Client.get(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}/parameters`)
      return res.data;
    } catch (e) {
        throw new Error(e)
    }
  },
  async createValueParameterOfMethodology(methodology_uuid, data) {
    try {
      let res = await Client.post(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}/parameters`, data)
      return res
    } catch (e) {
      throw new Error(e)
    }
  },
  async updateValueParameterOfMethodology(methodology_uuid, parameter_value_uuid, data) {
    try {
      return await Client.put(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}/parameters/${parameter_value_uuid}`, data)
    } catch (e) {
      throw new Error(e)
    }
  },
  async deleteValueParameterOfMethodology(methodology_uuid, parameter_uuid) {
    try {
      let res = await Client.delete(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}/parameters/${parameter_uuid}`)
      return res
    } catch (e) {
      throw new Error(e)
    }
  }
}