import RestClient from '../RestClient'

const Client = new RestClient()

export default {
  async fetchParticipantActivities() {
    try {
      let res = await Client.get(`/oboz2-rating-methodology-crud/v1/activities`)
      return res.data;
    } catch (e) {
        throw new Error(e)
    }
  }
}