import RestClient from '../RestClient'

const Client = new RestClient({baseURL:''})

export default {
  async fetchMethodologyByUuid(methodology_uuid) {
    try {
      // if (window.isMock) {
      //     return Promise.resolve(_data)
      // } else {
      let res = await Client.get(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}`)
      return res.data;
      // }
    } catch (e) {
        throw new Error(e)
    }
  },
  async updateMethodologyByUuid(methodology_uuid, data) {
    // console.log(data, methodology_uuid)
    try {
      let res = await Client.put(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}`, data)
      return res.data;
    } catch (e) {
        throw new Error(e)
    }
  },
  async deleteMethodologyByUuid(methodology_uuid) {
    // console.log(methodology_uuid)
    try {
      let res = await Client.delete(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}`)
      return res.data;
    } catch (e) {
        throw new Error(e)
    }
  }
}