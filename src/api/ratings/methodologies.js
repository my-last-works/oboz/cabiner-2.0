import RestClient from '../RestClient'

const Client = new RestClient({baseURL:''})

export default {
  async fetchMethodologies() {
    try {
      // if (window.isMock) {
      //     return Promise.resolve(_data)
      // } else {
      let res = await Client.get(`/oboz2-rating-methodology-crud/v1`)
      return res.data;
      // }
    } catch (e) {
        throw new Error(e)
    }
  },
  async createMethodology(data) {
    // console.log(data)
    try {
      let res = await Client.post(`/oboz2-rating-methodology-crud/v1`, data)
      return res.data;
    } catch (e) {
        throw new Error(e)
    }
  }
}