import RestClient from '../RestClient'

const Client = new RestClient()

export default {
  async fetchParameters(methodology_uuid) {
    try {
      // if (window.isMock) {
      //     return Promise.resolve(_data)
      // } else {
      let res = await Client.get(`/oboz2-rating-methodology-crud/v1/parameters?methodology_uuid=${methodology_uuid}`)
      return res.data;
      // }
    } catch (e) {
        throw new Error(e)
    }
  }
}