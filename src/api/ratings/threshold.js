import RestClient from '../RestClient'

const Client = new RestClient()

export default {
  async fetchRagingThresholdOfMethodology(methodology_uuid) {
    try {
      // if (window.isMock) {
      //     return Promise.resolve(_data)
      // } else {
      let res = await Client.get(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}/threshold`)
      return res.data;
      // }
    } catch (e) {
        throw new Error(e)
    }
  },
  async updateRagingThresholdOfMethodology(methodology_uuid, data) {
    // console.log(data, methodology_uuid)
    try {
      let res = await Client.put(`/oboz2-rating-methodology-crud/v1/methodologies/${methodology_uuid}/threshold`, data)
      return res.data;
    } catch (e) {
        throw new Error(e)
    }
  },
}