import RestClient from './RestClient';
import {formationParameters} from '@/helpers';

let Client = new RestClient({ isErrorThrow: true })

export default {
  async getForwardingUnitList(page, size, q, sort) {
    try {
      return await Client.get('/oboz2-dictionary-forwarding-units-crud/v1' + formationParameters(page, size, q, sort))
    } catch (e) {
      throw e
    }
  },
  getForwardingUnit(uuid) {
    return Client.get('/oboz2-dictionary-forwarding-units-crud/v1/' + uuid)
  },
  createForwardingUnit(data) {
    return Client.post('/oboz2-dictionary-forwarding-units-crud/v1/', data)
  },
  editForwardingUnit(uuid, data) {
    return Client.put('/oboz2-dictionary-forwarding-units-crud/v1/' + uuid, data)
  },
  deleteForwardingUnit(uuid) {
    return Client.put('/oboz2-dictionary-forwarding-units-crud/v1/' + uuid + '/true')
  },
  getPatterns(page, size, q, sort) {
    return Client.get('/oboz2-dictionary-resource-patterns-crud/v1' + formationParameters(page, size, q, sort))
  },
  getPattern(uuid) {
    return Client.get('/oboz2-dictionary-resource-patterns-crud/v1/' + uuid)
  },
  createPattern(data) {
    return Client.post('/oboz2-dictionary-resource-patterns-crud/v1/', data)
  },
  deletePattern(uuid) {
    return Client.delete('/oboz2-dictionary-resource-patterns-crud/v1/' + uuid)
  },
  async getPatternMasks(page, size, q, sort) {
    try {
      const res = await Client.get('/oboz2-dictionary-resource-pattern-masks-crud/v1' + formationParameters(page, size, q, sort))
      return res
    } catch (e) {
      throw e
    }
  },
  async getResourceSpecies({page, size, query, sort}) {
    try {
      const { data } = await Client.get('/oboz2-dictionary-resource-species-crud/v1' + formationParameters(page, size, query, sort))
      return data || []
    } catch (e) {
      throw e
    }
  },
  getResourceTypes(page, size, q, sort) {
    return Client.get('/oboz2-dictionary-resource-types-crud/v1', { params: { page, size, q, sort }})
  },
  editResourceTypes(data) {
    return Client.put('/oboz2-dictionary-resource-types-crud/v1/' + data.uuid, data)
  },
  getResourceSubTypesBySpecies(speciesUuid) {
    return Client.get(`/oboz2-dictionary-resource-subtypes-crud/v1/dropdown/by_species/${speciesUuid}`)
  },
  createResourceTypes(data) {
    return Client.post('/oboz2-dictionary-resource-types-crud/v1/', data)
  },
  getResourceType(uuid) {
    return Client.get(`/oboz2-dictionary-resource-types-crud/v1/${uuid}`)
  },
  getPatternsByMask(uuid) {
    return Client.get(`/oboz2-dictionary-resource-patterns-crud/v1/by_mask/${uuid}`)
  },
  getFilteredPatterns(subTypeUuid) {
    return Client.get(`/oboz2-dictionary-resource-types-crud/v1/edit?type_uuid=${subTypeUuid}`)
  },

  async getParticipantDetail ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-security-resources-crud/v1/resource_card/${uuid}`)
      return res.data && res.data.domains || []
    } catch (e) {
      throw e
    }
  },
  async getResourceList () {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-crud/v1/resource_list`)
      return data ? data.expeditors : []
    } catch (e) {
      throw e
    }
  },
  async getResourceCard (uuid) {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-crud/v1/resource_card/${uuid}`)
      return data || []
    } catch (e) {
      throw e
    }
  },
  async saveResourceCard (payload) {
    try {
      await Client.put(`/oboz2-security-resources-crud/v1/resource_card/manual_field_value`, payload)
    } catch (e) {
      throw e
    }
  },
  addResource (uuid, payload) {
    return Client.post(`/oboz2-security-resources-crud/v1/resource/${uuid}`, payload);
  },
  async deleteResource (uuid) {
    try {
      await Client.delete(`/oboz2-security-resources-crud/v1/delete_resource/${uuid}`)
    } catch (e) {
      throw e
    }
  },
  async getDoc (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-file-storage/v1/${uuid}`, { responseType: 'blob' })
      return res
    } catch (e) {
      throw e
    }
  },
  async getHistory (uuid) {
    let res = await Client.get(`/oboz2-security-resources-crud/v1/hist_risk/${uuid}`)
    return Promise.resolve(res)
  },
  async getNewResourceInputs(payload) {
    return Client.post(`/oboz2-security-resources-crud/v1/resource_fields/`, payload);
  },

  updateResourceRisk(uuid, payload) {
    return Client.put(`/oboz2-security-resources-crud/v1/resource_card/manual_estimation/${uuid}`, payload)
  },


  // New resources list

  async getTractorsList({ carrier_uuid, filter, page, size, query, sort }) {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-viewer/v1/tractors`, { params: { carrier_uuid, filter, page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },
  async getWagonsList({ carrier_uuid, filter, page, size, query, sort }) {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-viewer/v1/vans`, { params: { carrier_uuid, filter, page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },
  async getSemitrailersList({ carrier_uuid, filter, page, size, query, sort, semitrailer_type }) {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-viewer/v1/semitrailers`, {
        params: { carrier_uuid, filter, page, size, q: query, sort, semitrailer_type }
      })

      return data
    } catch (e) {
      throw e
    }
  },
  async getDriversList({ carrier_uuid, filter, page, size, query, sort }) {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-viewer/v1/drivers`, { params: { carrier_uuid, filter, page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },
  async getTrailersList({ carrier_uuid, filter, page, size, query, sort }) {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-viewer/v1/trailers`, { params: { carrier_uuid, filter, page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },
  async getContainerTrucksList({ carrier_uuid, filter, page, size, query, sort }) {
    try {
      const { data } = await Client.get(`/oboz2-security-resources-viewer/v1/container_trucks`, { params: { carrier_uuid, filter, page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },
  async getOwnershipForms () {
    try {
      const { data } = await Client.get('/oboz2-dictionary-ownership-forms-viewer/v1/dropdown')
      return data
    } catch (e) {
      throw e
    }
  },
  async registerResource (uuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-security-customized-resources-crud/v1/registration/${uuid}`, payload)
      return data
    } catch (e) {
      throw e
    }
  },
  async getResourceDocuments({ uuid }) {
    try {
      const { data } = await Client.get(`/oboz2-security-customized-resources-crud/v1/documents/${uuid}`)
      return data
    } catch (e) {
      throw e
    }
  },
  async saveDocument(documentUuid, resourceUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-security-customized-resources-crud/v1/documents/${resourceUuid}/${documentUuid}`, payload)
      return data
    } catch (e) {
      throw e
    }
  },
  async deleteDocument(documentUuid) {
    try {
      await Client.delete(`/oboz2-security-customized-resources-crud/v1/documents/${documentUuid}`)
    } catch (e) {
      throw e
    }
  },
  async getLoadingMethods () {
    try {
      const { data } = await Client.get('/oboz2-dictionary-loading-methods-viewer/v1/dropdown/')
      return data
    } catch (e) {
      throw e
    }
  },
  async getResourceSpeciesDropdown () {
    try {
      const { data } = await Client.get('/oboz2-dictionary-resource-species-crud/v1/dropdown/')
      return data
    } catch (e) {
      throw e
    }
  },
  async getResourceTypesDropdownBySpecies(uuid) {
    try {
      const { data } = await Client.get(`/oboz2-dictionary-resources-classification-viewer/v1/dropdown/by_species/${uuid}`)
      return data
    } catch (e) {
      throw e
    }
  },
  async getCarriers ({ page, size, query }) {
    try {
      const { data } = await Client.get('/oboz2-security-participants-crud/v1/dropdown/carriers', {
        params: { page, size, q: query }
      })

      return data
    } catch (e) {
      throw e
    }
  },
  async getResourceItem(uuids) {
    try {
      const { data } = await Client.post(`oboz2-dictionary-resource-species-crud/v1/internal/list`, {uuids: uuids})
      return data
    } catch (e) {
      throw e
    }
  }
}
