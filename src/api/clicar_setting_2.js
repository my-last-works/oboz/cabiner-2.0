import RestClient from './RestClient'

let Client = new RestClient();

export default {
  getSettings(clicarUuid, type) {
    return Client.get(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/?type=${type}`);
  },
  putSettings(data) {
    return Client.put(`/oboz2-account-clicar-settings-crud/v1/settings/`,{
      ...data
    })
  },
  getFacsimile(carrierUuid) {
    return Client.get(`/oboz2-account-carrier-settings-crud/v1/settings/${carrierUuid}`);
  },
  getFacsimileForClicar(uuid) {
    return Client.get(`/oboz2-account-clicar-settings-crud/v1/settings/${uuid}`);
  },
  async getFacsimileV3 () {
    const { data } = await Client.get(`/oboz2-account-clicar-settings-crud/v3/settings/facsimile`)
    return data
  },
  async saveFacsimileV3 ({ data }) {
    const res = await Client.post(`/oboz2-account-clicar-settings-crud/v3/settings/facsimile`, data)
    return res.data
  },
  postFacsimile (fileUuid, data) {
    return Client.post(`/oboz2-common-file-storage/v1/upload/${fileUuid}`, data);
  },
  //Подпись
  putFacsimileSignature(carrierUuid, fileUuid) {
    return Client.put(`/oboz2-account-carrier-settings-crud/v1/settings/${carrierUuid}/facsimile/signature/${fileUuid}`);
  },
  putFacsimileSignatureForClicar(clicarUuid, fileUuid) {
    return Client.put(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/facsimile/signature/${fileUuid}`);
  },
  deleteFacsimileSignature(carrierUuid){
    return Client.delete(`/oboz2-account-carrier-settings-crud/v1/settings/${carrierUuid}/facsimile/signature`);
  },
  deleteFacsimileSignatureForClicar(carrierUuid){
    return Client.delete(`/oboz2-account-clicar-settings-crud/v1/settings/${carrierUuid}/facsimile/signature`);
  },

  //Печать
  putFacsimileStamp(carrierUuid, fileUuid) {
    return Client.put(`/oboz2-account-carrier-settings-crud/v1/settings/${carrierUuid}/facsimile/stamp/${fileUuid}`);
  },
  putFacsimileStampForClicar(clicarUuid, fileUuid) {
    return Client.put(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/facsimile/stamp/${fileUuid}`);
  },
  deleteFacsimileStamp(carrierUuid){
    return Client.delete(`/oboz2-account-carrier-settings-crud/v1/settings/${carrierUuid}/facsimile/stamp`);
  },
  deleteFacsimileStampForClicar(carrierUuid){
    return Client.delete(`/oboz2-account-clicar-settings-crud/v1/settings/${carrierUuid}/facsimile/stamp`);
  },
  //НАЦЕНКА

  getMarkupSettings(clicarUuid) {
    return Client.get(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup`);
  },
  saveMarkupSettings(clicarUuid, data) {
    return Client.post(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup`, data);
  },
  async getMarkupClients({ page, size, sort, query }, clicarUuid) {
    try {
      const res = await Client.get(
          `/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup/clients/`, {
            params: { page, size, sort, q: query } }
      );
      return res.data;
    } catch (e) {
      throw e;
    }
  },
  postClientMarkup(clicarUuid, data) {
    return Client.post(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup/per_location`, data);
  },
  saveClientMarkup(clicarUuid,clientUuid, data) {
    return Client.post(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup/${clientUuid}`, data);
  },
  getClientMarkup(clicarUuid,clientUuid) {
    return Client.post(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup/${clientUuid}/per_location`);
  },
  deleteClientMarkup(clicarUuid, direction) {
    return Client.delete(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup/per_location`, {
      data: {
        ...direction
      }
    });
  },
  deleteMarkup(clicarUuid, clientUuid) {
    return Client.delete(`/oboz2-account-clicar-settings-crud/v1/settings/${clicarUuid}/markup/${clientUuid}`);
  }
}
