import RestClient from './RestClient';
import { formationParameters } from '@/helpers';

let Client = new RestClient();

export default {
    getTemperatureRecorders(page, size, q, sort) {
        return Client.get('/oboz2-dictionary-temperature-recorders-crud/v1' + formationParameters(page, size, q, sort))
    },
    getTemperatureRecorders1(page, size, q, sort) {
        return Client.get('/oboz2-dictionary-temperature-recorders-crud/v1/dropdown' + formationParameters(page, size, q, sort))
    },
    getTemperatureRecorder(uuid) {
        return Client.get('/oboz2-dictionary-temperature-recorders-crud/v1/' + uuid)
    },
    createTemperatureRecorder(data) {
        return Client.post('/oboz2-dictionary-temperature-recorders-crud/v1/', data)
    },
    editTemperatureRecorder(uuid, data) {
        return Client.put('/oboz2-dictionary-temperature-recorders-crud/v1/' + uuid, data)
    },
    deleteTemperatureRecorder(uuid) {
        return Client.delete('/oboz2-dictionary-temperature-recorders-crud/v1/' + uuid)
    },
    getTemperatureModesCrud(page, size, q, sort) {
        return Client.get('/oboz2-dictionary-temperature-modes-crud/v1/internal/dropdown' + formationParameters(page, size, q, sort))
    },
    getTemperatureMode(uuid) {
        return Client.get('/oboz2-dictionary-temperature-modes-crud/v1/' + uuid)
    },
    async getTemperatureModeTypes () {
        try {
            const res = await Client.get('/oboz2-dictionary-temperature-modes-crud/v1/roots')
            return res && res.data && res.data.content || []
        } catch (e) {
            throw e
        }
    },
    createTemperatureMode(data) {
        return Client.post('/oboz2-dictionary-temperature-modes-crud/v1/', data)
    },
    deleteTemperatureMode(uuid) {
        return Client.delete('/oboz2-dictionary-temperature-modes-crud/v1/' + uuid)
    },
}
