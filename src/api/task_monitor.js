import RestClient from './RestClient';
import { formationParameters } from '@/helpers';

let Client = new RestClient();

export default {

  // PARTICIPANT
  getParticipantTaskFields(uuid) {
    return Client.get(`/oboz2-security-participants-crud/v1/tasks/${uuid}`)
  },
  postParticipantTaskFields(payload) {
    return Client.post(`/oboz2-security-card-field-values-crud/v1/participant/`, payload)
  },
  getExistingParticipantFields(uuid) {
    return Client.get(`/oboz2-security-participants-crud/v1/participants/${uuid}`)
  },
  getParticipantConflictFields(uuid) {
    return Client.get(`/oboz2-security-card-field-value-priority-crud/v1/participants/tasks/${uuid}`)
  },
  getParticipantConflictFieldsInfo(uuid, payload) {
    return Client.post(`/oboz2-security-card-field-values-crud/v1/participant/card/values/${uuid}`, payload)
  },
  postParticipantConflictFields(uuid, payload) {
    return Client.post(`/oboz2-security-card-field-value-priority-crud/v1/participants/tasks/${uuid}/priority`, payload)
  },

  getCarrierRiskCard(cardUuid, taskUuid) {
    return Client.get(`/oboz2-security-carrier-risk-assessments-crud/v1/cards/${cardUuid}/tasks/${taskUuid}`)
  },
  postCarrierRiskCard(cardUuid, taskUuid, payload) {
    return Client.post(`/oboz2-security-carrier-risk-assessments-crud/v1/cards/${cardUuid}/tasks/${taskUuid}/assessment`, payload)
  },
  getCarrierRiskCardAssessment(cardUuid, taskUuid) {
    return Client.get(`/oboz2-security-carrier-risk-assessments-crud/v1/assessments/cards/${cardUuid}/tasks/${taskUuid}`)
  },

  // RESOURCE
  getResourceTaskFields(uuid) {
    return Client.get(`/oboz2-security-resources-crud/v1/tasks/${uuid}`)
  },
  postResourceTaskFields(payload) {
    return Client.post(`/oboz2-security-card-field-values-crud/v1/resource`, payload)
  },
  getResourceConflictFields(uuid) {
    return Client.get(`/oboz2-security-card-field-value-priority-crud/v1/resources/tasks/${uuid}`)
  },
  getResourceConflictFieldsInfo(uuid, payload) {
    return Client.post(`/oboz2-security-card-field-values-crud/v1/resource/card/values/${uuid}`, payload)
  },
  postResourceConflictFields(uuid, payload) {
    return Client.post(`/oboz2-security-card-field-value-priority-crud/v1/resources/tasks/${uuid}/priority`, payload)
  },
  getResourceCurrentRiskAssessment(uuid) {
    return Client.get(`/oboz2-security-resources-crud/v1/current_risk_assessment/${uuid}`)
  },
  getResourceRiskGroup() {
    return Client.get(`/oboz2-security-resources-crud/v1/risk_group`)
  },
  postResourceRiskGroup(uuid, payload) {
    return Client.post(`/oboz2-security-resources-crud/v1/risk_group/${uuid}`, payload)
  },

  // COMMON

  async getTasks({ page, size, query, sort, archive }) {
    try {
      const res = await Client.get('/oboz2-security-monitor-task-crud/v1/list', { params: { page, size, q: query, sort, archive } })
      return res.data
    } catch (e) {
      throw e
    }
  },

  postTaskExecuted(uuid, worker_uuid) {
    return Client.post(`/oboz2-security-monitor-task-crud/v1/${uuid}/executed/`, {
      worker_uuid
    })
  },
  postConflictTaskExecuted(uuid, payload) {
    return Client.post(`/oboz2-security-monitor-task-crud/v1/${uuid}/conflict_task_executed/`, payload)
  },
  // seems getTaskSystemRisk is unused
  getTaskSystemRisk(uuid) {
    return Client.get(`/oboz2-security-carrier-risk-assessments-crud/v1/carrier/${uuid}/activities`)
  },
  postTaskFile(fileUuid, payload) {
    return Client.post(`/oboz2-security-file-storage/v1/${fileUuid}`, payload);
  },

  // OPTIONS
  async getOptions(dictionaryName, page, size, q, sort) {
    const data = await Client.get(`/oboz2-dictionary-dictionaries-viewer/v1/dropdown/${dictionaryName}` + formationParameters(page, size, q, sort));
    const modified = {
      ...data,
      values:()=>{
        return data.values.map(el => {
          return {
            ...el,
            title: `${(el && el.article_number ? el.article_number + ' – ' : '')}${(el.title || el.name)}`
          }
        })
      }
    }

    return modified;
  }
}
