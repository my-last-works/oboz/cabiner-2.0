import accountOrder            from './accountOrder'
import adminPanel              from './admin-panel/index'
import audit                   from './audit'
import card                    from './card'
import cardField               from './card_field'
import cargo                   from './dictionaries/cargo'
import contract                from './contract'
import contractors             from './contractors'
import customs                 from './customs'
import dictionaries            from './dictionaries/index'
import dictionary              from './dictionary'
import domainSettingsExpeditor from './domain-settings/expeditor'
import domainSettingsModerator from './domain-settings/moderator'
import files                   from './files'
import functions               from './admin-panel/functions'
import general                 from './general'
import geocoder                from './geocoder'
import linkBrowser             from './link_browser'
import locations               from './locations'
import logchain                from './logchain'
import menu                    from './menu'
import notice                  from './notice'
import orderProcess            from './order_process'
import orderRisksSettings      from './sb/order_risks_settings'
import orders                  from './orders'
import organisations           from './organisations'
import registration            from './registration'
import registrationSb          from './sb/registration'
import resources               from './resources'
import risks                   from './sb/risks'
import sbRequisites            from './sb/requisites'
import services                from './dictionaries/services'
import taskMonitor             from './task_monitor'
import temperature             from './temperature'
import tenders                 from './tenders/index'
import tendersExcels           from './tenders/excels'
import tendersContractors      from './tenders/contractors'
import tendersScenarios        from './tenders/scenarios'
import tripDuration            from './sb/trip_duration'
import user_roles              from './admin-panel/user_roles'
import users                   from './admin-panel/users'
import scoring                 from './scoring'
import index_settings          from './ratings/index_settings'
import activities              from './ratings/activities'
import parameters              from './ratings/parameters'
import parameter               from './ratings/parameter'
import methodologies           from './ratings/methodologies'
import methodology             from './ratings/methodology'
import bonus_methodologies     from './bonuses/bonus_methodologies'
import bonus_activities        from './bonuses/bonus_activities'
import bonus_methodology       from './bonuses/bonus_methodology'
import threshold               from './ratings/threshold'
import freshness               from './ratings/freshness'
import base_prices             from './base_prices'
import base_prices_data_import from './base_prices/base_prices_data_import'
import base_prices_clustering  from './base_prices/base_prices_clustering'
import base_prices_intracity   from './base_prices/base_prices_intracity'
import spotAuction             from './spotAuction'
import advances_payment        from './advances_payment'
import tms                     from './tms'
import clicar_setting          from './clicar_setting'
import clicar_setting_2        from './clicar_setting_2'
import clicar_setting_3        from './clicar_setting_3'
import documents               from './documents'
import tracking                from './tracking'
import subscriptions           from './subscriptions'

export default {
    accountOrder,
    adminPanel,
    audit,
    card,
    cardField,
    cargo,
    contract,
    contractors,
    customs,
    dictionaries,
    dictionary,
    domainSettingsExpeditor,
    domainSettingsModerator,
    files,
    functions,
    general,
    geocoder,
    linkBrowser,
    locations,
    logchain,
    menu,
    notice,
    orderProcess,
    orderRisksSettings,
    orders,
    organisations,
    registration,
    registrationSb,
    resources,
    risks,
    sbRequisites,
    services,
    taskMonitor,
    temperature,
    tenders,
    tendersExcels,
    tendersContractors,
    tendersScenarios,
    tripDuration,
    scoring,
    index_settings,
    activities,
    parameters,
    parameter,
    methodologies,
    methodology,
    bonus_methodologies,
    bonus_methodology,
    bonus_activities,
    threshold,
    freshness,
    users,
    user_roles,
    base_prices,
    base_prices_data_import,
    base_prices_clustering,
    base_prices_intracity,
    spotAuction,
    advances_payment,
    clicar_setting,
    clicar_setting_2,
    clicar_setting_3,
    tms,
    documents,
    tracking,
    subscriptions
}
