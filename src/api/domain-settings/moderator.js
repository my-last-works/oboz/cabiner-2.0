import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  getDomainSettings () {
    return Client.get('/oboz2-domains-domain-settings-crud/v1/settings/')
  },
  putDomainSettings (data) {
    return Client.put('/oboz2-domains-domain-settings-crud/v1/settings/', data)
  },
  getTimeSettings () {
    return Client.get('/oboz2-domains-domain-settings-crud/v1/time_settings')
  },
  putTimeSettings (data) {
    return Client.put('/oboz2-domains-domain-settings-crud/v1/time_settings', data)
  },
  getParticipantsWithPersonalSettings() {
    return Client.get('oboz2-domains-domain-participant-settings-crud/v1/participants_with_personal_settings/');
  },
  getParticipantsWithoutPersonalSettings() {
    return Client.get('oboz2-domains-domain-participant-settings-crud/v1/participants_without_personal_settings/');
  },
  getParticipantSettings (participantUuid) {
    return Client.get('/oboz2-domains-domain-participant-settings-crud/v1/settings/' + participantUuid)
  },
  deleteParticipantSettings (participantUuid) {
    return Client.delete('/oboz2-domains-domain-participant-settings-crud/v1/settings/' + participantUuid)
  },
  saveParticipantSettings (participantUuid, settings) {
    return Client.put('/oboz2-domains-domain-participant-settings-crud/v1/settings/' + participantUuid, settings)
  },
  saveExpeditorParticipantSettings (participantUuid, settings) {
    return Client.put('/oboz2-domains-domain-participant-settings-crud/v1/settings/expeditor/' + participantUuid, settings)
  },
  deleteExpeditorParticipantSettings (participantUuid) {
    return Client.delete('/oboz2-domains-domain-participant-settings-crud/v1/settings/expeditor/' + participantUuid)
  }
}
