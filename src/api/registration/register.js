import RestClient from '@/api/RestClient'

const Client = new RestClient({ isErrorThrow: true })

export default {
  async getCountriesDropdown () {
    try {
      const res = await Client.get('/oboz2-security-registration-form-viewer/v1', { params: { withoutToken: true } })
      return res.data.countries
    } catch (e) {
      throw e
    }
  },
  async checkEmail ({ email }) {
    try {
      const res = await Client.post('/oboz2-security-registration-request-processing/v1/registration/email', { email }, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async checkPhone ({ phone }) {
    try {
      const res = await Client.post('/oboz2-security-registration-request-processing/v1/registration/phone', { phone }, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async checkPhoneCode ({ phone, code }) {
    try {
      const res = await Client.post('/oboz2-security-registration-request-processing/v1/registration/phone/code', { phone, code }, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async checkKpp (inn, kpp) {
    try {
      const { data } = await Client.get(`/oboz2-security-registration-form-viewer/v1/inn/${inn}/check_kpp_exist/${kpp}`, { params: { withoutToken: true } })

      return data
    } catch (e) {
      throw e
    }
  },
  async getInfoByInn ({ inn, uuid, countrySymbolCode }) {
    try {
      const res = await Client.get(`/oboz2-security-registration-form-viewer/v1/data/inn/${inn}/code/${uuid}?countrySymbolCode=${countrySymbolCode}`, {
        params: { withoutToken: true }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async regReplay ({ email }) {
    try {
      const res = await Client.post(`/oboz2-security-registration-request-processing/v1/registration/replay`, { email }, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async submitRegistration ({ data }) {
    try {
      const res = await Client.post('/oboz2-security-registration-request-processing/v1/registration', data, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async verifyToken (token) {
    try {
      const res = await Client.get(`/oboz2-security-registration-request-processing/v1/verification/email/${token}`, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getRegistrarCountryCode (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-participants-crud/v1/participants/${uuid}/country`)
      return res.data
    } catch (e) {
      throw e
    }
  },
}
