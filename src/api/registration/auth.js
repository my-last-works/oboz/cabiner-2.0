import RestClient from '@/api/RestClient'

const Client = new RestClient({ isErrorThrow: true })

export default {
  async getRestorePasswordLink ({ email }) {
    try {
      const res = await Client.post(`/oboz2-security-auth-tokenizer/v1/${email}/password_recovery`, false, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async startRestorePassword ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-security-auth-tokenizer/v1/password_recovery/${uuid}`, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async saveNewPassword ({ uuid, password }) {
    try {
      const res = await Client.post(`/oboz2-security-auth-tokenizer/v1/password_recovery/${uuid}`, { password }, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async login ({ username, password }) {
    try {
      const res = await Client.post('/oboz2-security-auth-tokenizer/v1/login', {
        email: username,
        password: password
      }, {
        params: {
          withoutToken: true
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async relocateAnonymous (uuid) {
    try {
      const res = await Client.put(`/oboz2-order-po-draft-saver/v1/relocation_from_anonymous/${uuid}?authorization_type=LOGIN`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async refresh ({ token }) {
    try {
      const res = await Client.post('/oboz2-security-registration-request-processing/v1/refresh', { token })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async status () {
    try {
      const res = await Client.get('/oboz2-security-participants-crud/v1/participant/status')
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getUserInfo () {
    try {
      const res = await Client.get('/oboz2-domains-user-info/v1/userInfo')
      return res.data
    } catch (e) {
      throw e
    }
  },
  async anonLogin() {
    try {
      const res = await Client.post('/oboz2-security-auth-tokenizer/v1/login-guests', false, { params: { withoutToken: true } })
      return res.data
    } catch (e) {
      throw e
    }
  }
}
