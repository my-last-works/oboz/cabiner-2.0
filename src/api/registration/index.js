import register from './register'
import auth from './auth'
import accreditation from './accreditation'

export default {
  register,
  auth,
  accreditation
}
