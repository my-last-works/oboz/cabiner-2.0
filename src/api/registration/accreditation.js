import RestClient from '@/api/RestClient'

const Client = new RestClient({ isErrorThrow: true })

export default {
  async getActivities () {
    try {
      const res = await Client.get('/oboz2-security-participant-to-activities-crud/v1/activities')
      return res.data.activities
    } catch (e) {
      throw e
    }
  },

  async setUserActivitiesAndPersonalData (data) {
    try {
      let res = await Client.post('/oboz2-security-participant-to-activities-crud/v2/activities/accreditation', data)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getUserType () {
    try {
      let res = await Client.get(`/oboz2-security-registration-request-processing/v1/participant/before_accreditation_info`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async getBikInfo (bikNumber) {
    try {
      let res = await Client.get(`/oboz2-security-bik-info-proxy/v1/info?bik=${bikNumber}`)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async setUserAccreditation (payload) {
    try {
      let res = await Client.post(`/oboz2-security-participants-crud/v3/participants/accreditation`, payload)
      return res.data
    } catch (e) {
      throw e
    }
  },
}
