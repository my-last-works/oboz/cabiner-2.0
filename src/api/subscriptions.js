import RestClient from "./RestClient";

let Client = new RestClient({ isErrorThrow: true });

export default {
  /**
   * Подгрузка подписки и ее дополнительных элементов
   */
  async showSubscriptions (uuid) {
    let res;
    try {
      if (uuid) {
        res = await Client.get('/oboz2-account-carrier-settings-crud/v1/signups?participantUuid=' + uuid);
      } else {
        res = await Client.get('/oboz2-account-carrier-settings-crud/v1/signups');
      }
      return res.data;
    } catch (err) {
      if (err.status === 404) return []
      throw err;
    }
  },
  async getLocations () {
    try {
      const res = await Client.get('/oboz2-dictionary-locations-crud/v1/fetch_base_locations/');
      return res.data;
    } catch (err) {
      throw err;
    }
  },
  async showListClients (sort, direction, page, size, q) {
    try {
      const res = await Client.get('/oboz2-domains-participant-groups-crud/v1/search/members',{ params: { page, size, direction, sort, q }})
      return res.data.content.filter(value => value.roleCode === 'CLIENT');
    } catch (err) {
      throw err;
    }
  },
  /**
   * Общие методы
   */
  async getServiceTypeByCode (code = 'FTL') {
    try {
      const res = await Client.get(`/oboz2-dictionary-service-viewer/v1/service_type_by_code/${code}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  },
  async getAdditionalInfoByServices (data) {
    // add Accept-Language равный RU
    try {
      const res = await Client.post('/oboz2-dictionary-service-viewer/v1/all', data);
      return res.data;
    } catch (err) {
      throw err;
    }
  },
  /**
   * Создание подписки и подтягивание данных в модалку
   */
  async createSubscription (signup, uuid) {
    try {
      if(uuid) {
        const res = await Client.post('/oboz2-account-carrier-settings-crud/v1/signups?participantUuid='+ uuid, signup);
        return res.data;
      }else {
        const res = await Client.post('/oboz2-account-carrier-settings-crud/v1/signups', signup);
        return res.data;
      }
    } catch (err) {
      throw err;
    }
  },
  async selectIndividuallyClient () {
    console.log('selectIndividuallyClient');
    try {
      const res = await Client.get('/oboz2-security-participants-crud/v1/dropdown/clients?size=20&page=0');
      return res.data;
    } catch (err) {
      throw err;
    }
  },
  /**
   * Удаление подписки
   */
  async deleteSubscriptions (uuid) {
    try {
      const res = await Client.delete(`/oboz2-account-carrier-settings-crud/v1/signups/${uuid}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  }
};