import RestClient from "./RestClient";

let Client = new RestClient({ isErrorThrow: true });

export default {
  async getBlocksList() {
    try {
      const { data } = await Client.get('/oboz2-tracking-devices-crud/v1/onboard_devices/list/')
      return data
    } catch (e) {
      throw e
    }
  },

  async getBlockCarriers() {
    try {
      const { data } = await Client.get('/oboz2-tracking-devices-crud/v1/carriers')
      return data
    } catch (e) {
      throw e
    }
  },

  async validateBlock(vehicleNumber, imei) {
    try {
      const { data } = await Client.post(`/oboz2-tracking-devices-crud/v1/validation`, {vehicleNumber, imei})
      return data
    } catch (e) {
      throw e
    }
  },

  async addBlock(uuid, payload) {
    try {
      const res = await Client.post(`/oboz2-tracking-devices-crud/v1/devices/${uuid}`, payload)
      return res
    } catch (e) {
      throw e
    }
  },

  async addBlockConnection(imei) {
    try {
      const res = await Client.post(`/oboz2-tracking-devices-crud/v1/${imei}/add_to_carrier`)
      return res
    } catch (e) {
      throw e
    }
  },

  async addBlockCarriers(imei, payload) {
    try {
      const res = await Client.post(`/oboz2-tracking-devices-crud/v1/${imei}/link_to_carriers`, payload)
      return res
    } catch (e) {
      throw e
    }
  }
}