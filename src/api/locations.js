import RestClient from './RestClient'

let Client = new RestClient({isErrorThrow : true})

export default {
  async getLocations(page, size, q, sort, filters) {
    try {
      const res = await Client.get('/oboz2-dictionary-locations-crud/v1/' + formationParameters(page, size, q, sort, filters))
      return res
    } catch (e) {
      throw e
    }
  },
  async getLocation(uuid) {
    try {
      const res = await Client.get('/oboz2-dictionary-locations-crud/v1/' + uuid)
      return res
    } catch (e) {
      throw e
    }
  },
  createLocation(data) {
    return Client.post('/oboz2-dictionary-locations-crud/v1/', data)
  },
  editLocation(uuid, data) {
    return Client.put('/oboz2-dictionary-locations-crud/v1/' + uuid, data)
  },
  deleteLocation(uuid) {
    return Client.put('/oboz2-dictionary-locations-crud/v1/' + uuid + '/true')
  },
  async getLocationTypes(page, size, q, sort) {
    try {
      const res = await Client.get('/oboz2-dictionary-location-types-viewer/v1/' + formationParameters(page, size, q, sort))
      return res
    } catch (e) {
      throw e
    }
  },
  async getLocationTypesCount() {
    try {
      const res = await Client.get('/oboz2-dictionary-locations-crud/v1/counts')
      return res && res.data
    } catch (e) {
      throw e
    }
  },
  async getLocationType(uuid) {
    try {
      const res = await Client.get('/oboz2-dictionary-location-types-viewer/v1/' + uuid)
      return res
    } catch (e) {
      throw e
    }
  },
  async getRaylwayAgglomerations() {
    try {
      const { data } = await Client.get('oboz2-dictionary-locations-crud/v1/agglomerations')
      return data
    } catch (e) {
      throw e
    }
  },
  async getLocationsInAgglomeration(uuid) {
    try {
      const {data} = await Client.get(`oboz2-dictionary-locations-crud/v1/agglomeration/${uuid}/locations`)
      return data
    } catch (e) {
      throw e
    }
  },
  async getLocationsAvailableForAgglomeration({uuid, page = 0, size = 20, query}) {
    try {
      const {data} = await Client.get(`oboz2-dictionary-locations-crud/v1/agglomeration/locations` + formationParameters(page, size, query, null, {
        uuid
      }))
      return data
    } catch (e) {
      throw e
    }
  },
  async createAgglomeration(uuid, title) {
    try {
      return await Client.post(`oboz2-dictionary-locations-crud/v1/agglomeration/${uuid}`, { title });
    } catch (e) {
      throw e
    }
  },
  async updateAgglomeration(uuid, title) {
    try {
      return await Client.put(`oboz2-dictionary-locations-crud/v1/agglomeration/${uuid}`, { title });
    } catch (e) {
      throw e
    }
  },
  async deleteAgglomeration(uuid) {
    try {
      return await Client.delete(`oboz2-dictionary-locations-crud/v1/agglomeration/${uuid}`);
    } catch (e) {
      throw e
    }
  },
  async saveLocationsInAgglomeration(uuid, params) {
    try {
      return await Client.put(`oboz2-dictionary-locations-crud/v1/agglomeration/${uuid}/locations`, params);
    } catch (e) {
      throw e
    }
  },
  async deleteLocationFromAgglomeration(uuid, locationUuid) {
    try {
      return await Client.delete(`oboz2-dictionary-locations-crud/v1/agglomeration/${uuid}/location/${locationUuid}`);
    } catch (e) {
      throw e
    }
  },
  async getCompatibleLocations() {
    try {
      const { data } = await Client.get('oboz2-dictionary-locations-crud/v1/container_routes');
      return data;
    } catch (e) {
      throw e
    }
  },
  async getContainerRouteLocations(uuid) {
    try {
      const { data } = await Client.get(`oboz2-dictionary-locations-crud/v1/container_route/${uuid}/locations`)
      return data
    } catch (e) {
      throw e
    }
  },
  async getDirectionsList({ page = 0, size = 20, query }) {
    try {
      const { data } = await Client.get(`oboz2-dictionary-locations-crud/v1/container_route/geography`,
        {
          params: { page, size, q: query },
        }
      )
      return data
    } catch (e) {
      throw e
    }
  },
  async getDirectionLocations(uuid, type, page = 0, size = 20, query) {
    try {
      const { data } = await Client.get(`oboz2-dictionary-locations-crud/v1/container_route/dropdown_locations/?uuid=${uuid}&georaphy_type=${type}`,
        {
          params: { page, size, q: query },
        }
      )
      return data
    } catch (e) {
      throw e
    }
  },
  async deleteDirectionLocation(directionUuid, locationUuid) {
    try {
      await Client.delete(`oboz2-dictionary-locations-crud/v1/container_route/${directionUuid}/location/${locationUuid}`)
    } catch (e) {
      throw e
    }
  },
  async deleteDirection(uuid) {
    try {
      await Client.delete(`oboz2-dictionary-locations-crud/v1/container_route/${uuid}`)
    } catch (e) {
      throw e
    }
  },
  async addNewDirection(uuid, payload) {
    try {
      const res = await Client.post(`oboz2-dictionary-locations-crud/v1/container_route/${uuid}`, payload)
      return res
    } catch (e) {
      throw e
    }
  },
  async addLocationsToDirection(uuid, payload) {
    try {
      await Client.put(`oboz2-dictionary-locations-crud/v1/container_route/${uuid}/locations`, payload)
    } catch (e) {
      throw e
    }
  },
  async getRailStations({ page = 0, size = 20, query }) {
    try {
      const { data } = await Client.get('oboz2-dictionary-locations-crud/v1/dropdown',
        {
          params: { page, size, q: query },
        }
      )
      return data
    } catch (e) {
      throw e
    }
  }
}

const formationParameters = (page, size, q, sort, filters) => {
  let parameters = ''
  if (page !== undefined) parameters += '&page=' + page
  if (size !== undefined) parameters += '&size=' + size
  if (sort && sort.length > 0) {
    sort.forEach(i => {
      let _sort = i.desc ? 'desc' : 'asc'
      parameters += '&sort=' + i.selector + ',' + _sort
    });
  }
  if (filters) {
    for (let prop in filters) {
      if (filters[prop] !== undefined && filters[prop] !== null && filters[prop].length > 0) {
        parameters += '&' + prop + '=' + filters[prop]
      }
    }
  }
  if (q !== undefined && q.length > 0) parameters += '&q=' + q
  if (parameters.length > 0) {
    parameters = '?' + parameters;
  }
  return parameters;
}
