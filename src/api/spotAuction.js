import RestClient from '@/api/RestClient';
import { createDownloadLink } from '@/helpers/files';

let Client = new RestClient({
  isErrorThrow: true
});

export default {
  getSpotId () {
    const data = {
      'bodyGenerator': {
        'entityType': 'Spot Auction'
      }
    }
    return Client.post('/oboz2-code-generator/v1/generate', data)
  },
  async getSpotItem (uuid) {
    const res = await Client.get('/oboz2-spot-order-orders-crud/v1/draft/' + uuid)
    return res.data
  },
  async getSpotsByCarrier (filters) {
    const res = await Client.post('/oboz2-spot-order-auctions-crud/v1/list/carrier/', filters)
    return res.data
  },
  async getSpotOrderByCarrier (uuid) {
    const res = await Client.get('/oboz2-spot-order-orders-crud/v1/actual/' + uuid)
    return res.data
  },
  async getSpotAuctionByCarrier (uuid) {
    const res = await Client.get('/oboz2-spot-order-auctions-crud/v1/auction_for_carrier/' + uuid)
    return res.data
  },
  makeBidByCarrier (bid_uuid, data) {
    return Client.post('/oboz2-auction-bids-crud/v1/' + bid_uuid, data)
  },
  async getSpotListByTab ({ postData }) {
    try {
      const res = await Client.post('/oboz2-spot-order-orders-crud/v1/list/', postData)
      return res.data
    } catch (e) {
      throw e;
    }
  },
  async getAuctionInfo (filters) {
    try {
      const res = await Client.post('/oboz2-spot-order-auctions-crud/v1/list/', filters)
      return res.data
    } catch (e) {
      throw e;
    }
  },
  async getAuctionInfoSimple (list) {
    try {
      const res = await Client.post('/oboz2-spot-order-auctions-crud/v1/simple_list/', list)
      return res.data
    } catch (e) {
      throw e;
    }
  },
  saveSpotItemDraft (uuid, data) {
    return Client.post('/oboz2-spot-order-orders-crud/v1/draft/' + uuid, data)
  },
  async validationSpotDraft (uuid) {
    const res = await Client.get('/oboz2-spot-order-orders-crud/v1/draftValidation/' + uuid)
    return res.data
  },
  publicSpotItemDraft (uuid) {
    return Client.post('/oboz2-spot-order-orders-crud/v1/public_draft/' + uuid)
  },
  publicSpotItemDrafts (uuids = []) {
    return Client.post('/oboz2-spot-order-orders-crud/v1/public_draft_list/', uuids)
  },
  getActualSpotItem (uuid) {
    return Client.get('/oboz2-spot-order-orders-crud/v1/actual/' + uuid)
  },
  getActualAuction (auctionUuid, orderUuid) {
    return Client.get('/oboz2-spot-order-auctions-crud/v1/actual_auction/' + auctionUuid + '/order/' + orderUuid)
  },
  getParticipantList (uuid) {
    return Client.get('/oboz2-auction-lots-reports/v1/parent_entities/' + uuid + '/history?distinct=true')
  },
  setWinningCarrier (uuid, bid_uuid) {
    return Client.post('/oboz2-spot-order-orders-crud/v1/set_carrier/' + uuid + '/bid/' + bid_uuid)
  },
  cancelSpotOrder (uuid) {
    return Client.post('/oboz2-spot-order-orders-crud/v1/cancel/' + uuid)
  },
  getParticipantsGroups ({ page, size, uuids = [], exclude = true, roleCode = 'CARRIER', ownerUuid }) {
    return Client.post('/oboz2-domains-participant-groups-crud/v1/participants/list_by_groups', {
        uuids,
        exclude,
        roleCode,
        ownerUuid
      }, {
        params: { page, size }
      }
    )
  },
  copySpotItem (old_order_uuid, new_order_uuid) {
    return Client.post(`/oboz2-spot-order-orders-crud/v1/copy_spot/${new_order_uuid}/old_order/${old_order_uuid}`)
  },
  getSpotEvents (uuid) {
    return Client.get('/oboz2-spot-order-orders-crud/v1/order_event/' + uuid)
  },
  assignWinnerClient (uuid, limit) {
    return Client.get(`/oboz2-auction-lots-reports/v1/parent_entities/${uuid}/winner?limit=${limit}`)
  },
  async getListTemplates () {
    return await Client.get('/oboz2-spot-order-file-uploader/v1/templates')
  },
  async downloadTemplate ({uuid, isDownload = true}) {
    try {
      const res = await Client.get(`/oboz2-common-file-storage/v1/${uuid}`, { responseType: 'blob' });
      const file = { data: res.data, name: 'Таблица спот-аукционов.xlsx'};

      if (isDownload) {
        createDownloadLink(file);
        return true
      } else {
        return file
      }
    } catch (e) {
      throw e;
    }
  },
  postExcelFile (fileUuid, data) {
    return Client.post(`/oboz2-common-file-storage/v1/upload/${fileUuid}`, data);
  },
  saveSpotAuction (file_uuid) {
    return Client.post(`/oboz2-spot-order-file-uploader/v1/files/${file_uuid}`)
  },
  removeSpotByUuid (uuid) {
    return Client.delete(`/oboz2-spot-order-orders-crud/v1/draft/${uuid}`)
  }
}
