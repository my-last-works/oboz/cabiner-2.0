import RestClient from './RestClient';
import { formationParameters } from '@/helpers';

let Client = new RestClient();

export default {
    async getCargoTypes(withDeleted, page, size, q, sort) {
        try {
            const res = await Client.get('/oboz2-dictionary-cargo-types-crud/v1' + formationParameters(withDeleted, page, size, q, sort));
            return res.data;
        } catch (e) {
            throw e;
        }
    },
    getCargoType(uuid) {
        return Client.get('/oboz2-dictionary-cargo-types-crud/v1/' + uuid);
    },
    createCargoType(data) {
        return Client.post('/oboz2-dictionary-cargo-types-crud/v1/', data);
    },
    editCargoType(uuid, data) {
        return Client.put('/oboz2-dictionary-cargo-types-crud/v1/' + uuid, data);
    },
    deleteCargoType(uuid) {
        return Client.put('/oboz2-dictionary-cargo-types-crud/v1/' + uuid + '/true');
    },
    getDangerousGoodsClasses(withDeleted, page, size, q, sort) {
        return Client.get('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + formationParameters(withDeleted, page, size, q, sort))
    },
    getDangerousGoodsClass(uuid) {
        return Client.get('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + uuid)
    },
    getDangerousGoodsClassImage(uuid) {
        return Client.get('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + uuid + '/image')
    },
    createDangerousGoodsClassImage(uuid, data) {
        return Client.post('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + uuid + '/image', data)
    },
    getEtsngList(withDeleted, page, size, q, sort) {
        return Client.get('/oboz2-dictionary-etsng-viewer/v1/' + formationParameters(withDeleted, page, size, q, sort))
    },
    getEtsng(uuid) {
        return Client.get('/oboz2-dictionary-etsng-viewer/v1/' + uuid)
    },
    getTnvedCount() {
        return Client.get('/oboz2-dictionary-tn-ved-viewer/v1/roots/count')
    },
    getTnvedList(page, size, q) {
        return Client.get('/oboz2-dictionary-tn-ved-viewer/v1/roots' + formationParameters(undefined, page, size, q))
    },
    getTnvedItem(uuid) {
        return Client.get('/oboz2-dictionary-tn-ved-viewer/v1/' + uuid + '/cargo-cards')
    },
}
