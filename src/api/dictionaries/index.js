import countries from './countries'
import resources from './resources'
import temperature from './temperature'
import services from './services'
import events from './events'
export default {
  countries,
  resources,
  temperature,
  services,
  events
}
