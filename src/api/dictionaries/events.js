import RestClient from "../RestClient";

let Client = new RestClient({ isErrorThrow: true });

export default {
  async getEvents() {
    try {
      const { data: { eventTypes } } = await Client.get('/oboz2-dictionary-event-types-crud/v1/list')
      return eventTypes
    } catch (e) {
      throw e
    }
  },

  async addReasons(uuid, payload) {
    try {
      const res = await Client.put(`oboz2-dictionary-event-types-crud/v1/event/${uuid}/reasons`, payload)
      return res
    } catch (e) {
      throw e
    }
  }
}