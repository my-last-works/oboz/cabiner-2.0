import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  async getCountriesDropdown({ query = null, page = null, size = null }) {
    try {
      const res = await Client.get('/oboz2-dictionary-countries-viewer/v1/dropdown', { params: { q: query, page, size } })
      return res.data && res.data || []
    } catch (e) {
      throw e
    }
  },

  async getCountryByCode(code) {
    try {
      const res = await Client.get(`/oboz2-dictionary-countries-viewer/v1/by_short_code/${code}`)
      return res.data
    } catch (e) {
      throw e
    }
  }
}
