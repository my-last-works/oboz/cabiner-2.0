import RestClient from '@/api/RestClient'
import { formationParameters } from '@/helpers'

const Client = new RestClient({isErrorThrow : true})

export default {
  getServices({ page = null, size = null, q = null, sort = null }) {
    let _sort
    if (sort) {
      sort.forEach(field => {
        _sort = field.selector + ',' + (field.desc ? 'desc' : 'asc')
      })
    }
    try {
      return Client.get('/oboz2-dictionary-service-viewer/v1/list', { params: { q, page, size, sort: _sort } })
    } catch (e) {
      throw e
    }
  },
  getServiceMeasurementUnits(page = null, size = null, q = null, sort = null) {
    let _sort
    if (sort) {
      sort.forEach(field => {
        _sort = field.selector + ',' + (field.desc ? 'desc' : 'asc')
      })
    }
    try {
      const res = Client.get('/oboz2-dictionary-service-measurement-units-crud/v1', { params: { q, page, size, sort: _sort } })
      return res && res || []
    } catch (e) {
      throw e
    }
  },
  getService(uuid) {
    return Client.get('/oboz2-dictionary-services-crud/v1/' + uuid)
  },
  createService(data) {
    return Client.post('/oboz2-dictionary-services-crud/v1/', data)
  },
  editService(data, groupUuid) {
    return Client.put('/oboz2-dictionary-services-crud/v1/' + groupUuid, data)
  },
  deleteService(uuid) {
    return Client.put('/oboz2-dictionary-services-crud/v1/' + uuid + '/true')
  },
  getServiceTypesList({ page = 0, size = 20, query }) {
    return Client.get('/oboz2-dictionary-service-types-crud/v2/',
      {
        params: { page, size, q: query },
      }
    )
  },
  getServiceTypes(page, size, q, sort) {
    return Client.get('/oboz2-dictionary-service-viewer/v1/service_types/list' + formationParameters(page, size, q, sort))
  },
  getServiceType(uuid) {
    return Client.get('/oboz2-dictionary-service-types-crud/v2/' + uuid)
  },
  createServiceType(uuid, data) {
    return Client.put(`/oboz2-dictionary-service-types-crud/v2/${uuid}`, data)
  },
  deleteServiceType(uuid) {
    return Client.delete('/oboz2-dictionary-service-types-crud/v1/' + uuid)
  },
  getServiceMeasurementUnit(uuid) {
    return Client.get('/oboz2-dictionary-service-measurement-units-crud/v1/' + uuid)
  },
  createServiceMeasurementUnit(data) {
    return Client.post('/oboz2-dictionary-service-measurement-units-crud/v1/', data)
  },
  editServiceMeasurementUnit(uuid, data) {
    return Client.put('/oboz2-dictionary-service-measurement-units-crud/v1/' + uuid, data)
  },
  async changeServiceMeasurementUnitStatus(uuid, payload) {
    try {
      await Client.put(`/oboz2-dictionary-service-measurement-units-crud/v1/change_status/${uuid}`, payload)
    } catch (e) {
      throw e
    }
  },
  deleteServiceMeasurementUnit(uuid) { 
    return Client.put('/oboz2-dictionary-service-measurement-units-crud/v1/' + uuid + '/true')
  },
  getMeasurementDropdown() {
    return Client.get('/oboz2-dictionary-service-measurement-units-crud/v1/dropdown')
  },
  async getServiceRequirements() {
    try {
      const res = await Client.get('/oboz2-dictionary-service-requirements-crud/v1/list/');
      return res.data;
    } catch (e) {
      throw e;
    }
  },
  async createServiceRequirements(payload) {
    try {
      await Client.post('/oboz2-dictionary-service-requirements-crud/v1/', payload);
    } catch (e) {
      throw e;
    }
  },
  async editServiceRequirements(uuid, payload) {
    try {
      await Client.put(`/oboz2-dictionary-service-requirements-crud/v1/${uuid}`, payload);
    } catch (e) {
      throw e;
    }
  },
  async deleteServiceRequirements(uuid) {
    try {
      await Client.delete(`/oboz2-dictionary-service-requirements-crud/v1/${uuid}`);
    } catch (e) {
      throw e;
    }
  },
  async getActiveServiceRequirements() {
    try {
      const res = await Client.get('/oboz2-dictionary-service-requirements-crud/v1/active_items_list/');
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async getReplaceableServices() {
    try {
        const { data } = await Client.get(`/oboz2-dictionary-services-crud/v1/replaceable_services`)
        return data
    } catch (e) {
        throw e
    }
  },

  async addReplaceableServices(uuid, uuids) {
    try {
        await Client.post(`/oboz2-dictionary-services-crud/v1/replaceable_services?uuid=${uuid}`, uuids);
    } catch (e) {
        throw e
    }
  },

  async deleteReplaceableServices(uuid, replaceableUuid) {
    try {
        await Client.delete(`/oboz2-dictionary-services-crud/v1/replaceable_services?uuid=${uuid}&replaceable_uuid=${replaceableUuid || ''}`)
    } catch (e) {
        throw e
    }
  },

  async getServicesAll(params) {
    try {
      const { data } = await Client.post(`/oboz2-dictionary-service-viewer/v1/all`, params);
      return data;
    } catch (err) {
      throw err;
    }
  },
}
