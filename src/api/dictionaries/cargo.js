import RestClient from '@/api/RestClient';
import { formationParameters } from '@/helpers';

const Client = new RestClient();

export default {
  async getCargoTypes({ page, size, q, sort } = {}, delegate) {
    const url = '/oboz2-dictionary-cargo-types-crud/v1/'
    if (delegate) return url

    try {
      const res = await Client.get(url + formationParameters(page, size, q, sort))
      return res.data
    } catch (e) {
      throw e
    }
  },
  getOrderCargoTypes() {
    return Client.get('/oboz2-dictionary-cargo-types-crud/v1/');
  },
  getCargoType(uuid) {
    return Client.get('/oboz2-dictionary-cargo-types-crud/v1/' + uuid);
  },
  createCargoType(data) {
    return Client.post('/oboz2-dictionary-cargo-types-crud/v1/', data);
  },
  editCargoType(uuid, data) {
    return Client.put('/oboz2-dictionary-cargo-types-crud/v1/' + uuid, data);
  },
  deleteCargoType(uuid) {
    return Client.delete('/oboz2-dictionary-cargo-types-crud/v1/' + uuid);
  },
  getDangerousGoodsClasses({ page, size, q, sort }) {
    return Client.get('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + formationParameters(page, size, q, sort))
  },
  getDangerousGoodsClass(uuid) {
    return Client.get('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + uuid)
  },
  getDangerousGoodsClassImage(uuid) {
    return Client.get('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + uuid + '/image')
  },
  createDangerousGoodsClassImage(uuid, data) {
    return Client.post('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + uuid + '/image', data)
  },
  getEtsngList(page, size, q, sort) {
    return Client.get('/oboz2-dictionary-etsng-viewer/v1/' + formationParameters(page, size, q, sort))
  },
  getEtsng(uuid) {
    return Client.get('/oboz2-dictionary-etsng-viewer/v1/' + uuid)
  },
  getTnvedCount() {
    return Client.get('/oboz2-dictionary-tn-ved-viewer/v1/roots/count')
  },
  getTnvedList(page, size, q) {
    return Client.get('/oboz2-dictionary-tn-ved-viewer/v1/roots' + formationParameters(page, size, q))
  },
  getTnvedItem(uuid) {
    return Client.get('/oboz2-dictionary-tn-ved-viewer/v1/' + uuid + '/cargo-cards')
  },
  async getCargoCostOptions() {
    try {
      const response = await Client.get('/oboz2-security-cargo-risk-manager/v1/cargo_cost_options');
      return response;
    } catch (e) {
      throw (e);
    }
  },
  async setCargoCostOptions(payload) {
    try {
      await Client.put('/oboz2-security-cargo-risk-manager/v1/cargo_cost_options', payload);
    } catch (e) {
      throw (e);
    }
  },
  async deleteDangerousGoodsClass(uuid) {
    try {
      return await Client.delete('/oboz2-dictionary-dangerous-goods-classes-viewer/v1/' + uuid);
    }
    catch (e) {
      throw e
    }

  },
  async changeEtsngStatus(uuid, status) {
    try {
      return await Client.put(`/oboz2-dictionary-etsng-viewer/v1/change_status/${uuid}`, {isActive: status})
    } catch (e) {
      throw e
    }
  }
}
