import RestClient from '@/api/RestClient'

const Client = new RestClient();

export default {
  async getTempConditions() {
    try {
      const res = await Client.get('/oboz2-dictionary-temperature-modes-crud/v1/internal/dropdown')
      return res.data && res.data || []
    } catch (e) {
      throw e
    }
  },
  async getTempConditionsV2() {
    try {
      // /oboz2-dictionary-temperature-modes-crud/v2/internal/dropdown
      const res = await Client.get('/oboz2-dictionary-temperature-modes-crud/v2/internal/dropdown')
      return res.data && res.data || []
    } catch (e) {
      throw e
    }
  }
}
