import RestClient from '@/api/RestClient'

const Client = new RestClient();

export default {
  async getResourceType({ page, size }) {
    try {
      const res = await Client.get('/oboz2-dictionary-resource-types-crud/v1/dropdown', { params: { page, size } })
      return res.data && res.data || []
    } catch (e) {
      throw e
    }
  },
  async getResourceSubtype() {
    try {
      const res = await Client.get('/oboz2-dictionary-resource-subtypes-crud/v1/dropdown');
      return res.data && res.data || []
    } catch (e) {
      throw new Error(e);
    }
  },
  async getResourceClasses({ page, size }) {
    try {
      const res = await Client.get('/oboz2-dictionary-resource-classes-crud/v1/', { params: { page, size } })
      return res.data && res.data || []
    } catch (e) {
      throw e
    }
  },
  async getResourceSpeciesByClass(classUuid) {
    try {
      const res = await Client.get(`/oboz2-dictionary-resource-species-crud/v1/dropdown/byClass/${classUuid}`)
      return res.data && res.data.resourceSpecies || []
    } catch (e) {
      throw e
    }
  },
  async getResourceSubTypesBySpecies(speciesUuid) {
    try {
      const res = await Client.get(`/oboz2-dictionary-resource-subtypes-crud/v1/dropdown/by_species/${speciesUuid}`)
      return res.data && res.data.resourceSubtypes || []
    } catch (e) {
      throw e
    }
  },
  async getResourceTypesBySpecies(subTypes) {
    try {
      const res = await Client.post(`/oboz2-dictionary-resource-types-crud/v1/dropdown/by_subtype`, subTypes)
      return res.data && res.data || []
    } catch (e) {
      throw e
    }
  },
  async getForwardingUnitList({ query = null, page = null, size = null }) {
    try {
      const res = await Client.get(`/oboz2-dictionary-forwarding-units-crud/v1/dropdown`, { params: { q: query, page, size } })
      return res.data && res.data || []
    } catch (e) {
      throw e
    }
  },
  async getOrderResourcesTypesBySpecies(uuid) {
    try {
      const res = await Client.get(`/oboz2-order-po-draft-viewer/v1/dropdown/by_species/${uuid}`);
      return res.data;
    } catch (e) {
      throw e;
    }
  }
}
