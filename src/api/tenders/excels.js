import RestClient from '../RestClient';

const Client = new RestClient({ isErrorThrow: true });

export default {

  async RFQParticipants(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tender-xls-creator/v1/tenders/${tenderUuid}/participants`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async RFIParticipants(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tender-xls-creator/v1/tenders/${tenderUuid}/participants`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async RFQOffers(tenderUuid, requestUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tender-xls-creator/v1/tenders/${tenderUuid}/request/${requestUuid}/offers`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async scenarios(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-xls-creator/v1/tenders/${tenderUuid}/scenarios`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async scenario(tenderUuid, scenarioUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-xls-creator/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async RFQLots(tenderUuid, requestUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tender-xls-creator/v1/tenders/${tenderUuid}/request/${requestUuid}/lots`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async RFQSummary(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-xls-creator/v1/tenders/${tenderUuid}/winner`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async RFIQuestionnaire(questionnaireUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tender-xls-creator/v1/questionnaire/${questionnaireUuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async RFIOffers(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tender-xls-creator/v1/tenders/${tenderUuid}/offers`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async RFISummary(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tender-xls-creator/v1/tenders/${tenderUuid}/offers/winners`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async downloadExcelTemplateForClient(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tender-xls-creator/v1/tenders/${tenderUuid}/pattern`, {
        headers: { 'Accept': '*/*' },
        responseType: 'arraybuffer'
      })

      return data
    } catch (e) {
      throw e
    }
  },

  async downloadExcelTemplateForCarrier(tenderUuid, requestUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tender-xls-creator//v1/tenders/${tenderUuid}/request/${requestUuid}/lots/carrier_pattern`, {
        headers: { 'Accept': '*/*' },
        responseType: 'arraybuffer'
      })

      return data
    } catch (e) {
      throw e
    }
  },

  async uploadExcelTemplateForClient(tenderUuid, requestUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/request/${requestUuid}/excel`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async uploadExcelTemplateForCarrier(tenderUuid, requestUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/request/${requestUuid}/carrier_excel`, payload)

      return data
    } catch (e) {
      throw e
    }
  }
}