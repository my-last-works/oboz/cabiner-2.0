import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  async getTenderScenarios({ tenderUuid, page, size, query, sort }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenarios-crud/v1/tenders/${tenderUuid}`, { params: { page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },

  async setTenderScenarioWinner(tenderUuid, scenarioUuid) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfq-scenarios-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}`)
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderWinnerScenarioCarriers(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenarios-crud/v1/internal/tender/${tenderUuid}/winner_scenario/carriers`)
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioByUuids(tenderUuid, scenarioUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenarios-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}`)
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioResultsByUuids({ page, size, query, sort, tenderUuid, scenarioUuid, localityFrom, localityTo, carrierTitle }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}`, { params: { page, size, q: query, sort, localityFrom, localityTo, carrierTitle } })
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioResultsByUuidsAndLots({ page, size, query, sort, tenderUuid, scenarioUuid, localityFrom, localityTo }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/by_lots`, { params: { page, size, q: query, sort, localityFrom, localityTo } })
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioOffersByUuidsAndLot({ page, size, query, sort, tenderUuid, scenarioUuid, lotUuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/lots/${lotUuid}`, { params: { page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioResultsByUuidsAndCarriers({ page, size, query, sort, tenderUuid, scenarioUuid, carrierTitle }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/by_carriers`, { params: { page, size, q: query, sort, carrierTitle } })
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioOffersByUuidsAndCarrier({ page, size, query, sort, tenderUuid, scenarioUuid, carrierUuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/carriers/${carrierUuid}`, { params: { page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioPostedOffersCarriers({ page, size, query, sort, tenderUuid, scenarioUuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenarios-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/carriers/filter`, { params: { page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },

  async postTenderScenarioBoundaryValues({ page, size, query, sort, tenderUuid, scenarioUuid, payload }) {
    try {
      const { data } = await Client.post(`/oboz2-tender-rfq-scenarios-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/boundary_values`, payload, { params: { page, size, q: query, sort } })
      return data
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioIncludedDeviations({ tenderUuid, scenarioUuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenarios-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/included_deviation`)
      return data
    } catch (e) {
      throw e
    }
  },

  async saveTenderScenarioByUuids(tenderUuid, scenarioUuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfq-scenarios-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/activation`, payload)
      return data
    } catch (e) {
      throw e
    }
  },

  async recalculateTenderScenario(payload) {
    try {
      const response = await Client.post(`/oboz2-tender-rfq-scenarios-crud/v1/recalculation`, payload)
      return response
    } catch (e) {
      throw e
    }
  },

  async getTenderScenarioResultByValues({ tenderUuid, scenarioUuid, sort, size, page, query, localityFrom, localityTo, carrierTitle }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}`, { params: { page, size, q: query, sort, localityFrom, localityTo, carrierTitle } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderScenarioResultByLots({ tenderUuid, scenarioUuid, sort, size, page, query, localityFrom, localityTo }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/by_lots`, { params: { page, size, q: query, sort, localityFrom, localityTo } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderScenarioResultByCarriers({ tenderUuid, scenarioUuid, sort, size, page, query, carrierTitle }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-scenario-results-crud/v1/tenders/${tenderUuid}/scenario/${scenarioUuid}/by_carriers`, { params: { page, size, q: query, sort, carrierTitle } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTmsPolicyInfo (tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tms-policy-creator/v1/${tenderUuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async createTmsPolicy (payload, params) {
    try {
      const { data } = await Client.post('/oboz2-tender-tms-policy-creator/v1/', payload, params);
      return data;
    } catch (e) {
      throw e;
    }
  }
}
