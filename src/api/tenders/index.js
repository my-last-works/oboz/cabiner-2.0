import RestClient from '@/api/RestClient';

const Client = new RestClient();

export default {

  async getTenders({ page, size, query, sort, statuses, type, archived }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders`, { params: { page, size, q: query, sort, statuses, type, archived } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderTypeByUuid(uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tender/${uuid}/type`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderByTypeAndUuid(type, uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async createTenderByType(type, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/draft`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async updateTenderByTypeAndUuid(type, uuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}/draft`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async deleteTenderByTypeAndUuid(type, uuid) {
    try {
      const { data } = await Client.delete(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async cancelTenderByTypeAndUuid(type, uuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}/cancel`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async manualFinishTenderByTypeAndUuid(type, uuid) {
    try {
      const { data } = await Client.put(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}/termination`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async publishTenderByTypeAndUuid(type, uuid) {
    try {
      const response = await Client.get(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}/publication`);
      return response;
    } catch (e) {
      throw e;
    }
  },

  async finishTenderByUuid(uuid) {
    try {
      const { data } = await Client.put(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/completed`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async acceptTenderParticipationByTypeAndUuid(type, uuid) {
    try {
      const { data } = await Client.post(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}/accept`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async rejectTenderParticipationByTypeAndUuid(type, uuid) {
    try {
      const { data } = await Client.post(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${uuid}/reject`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async sendTenderOfferByUuid(uuid) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfq-tenders/v1/offer/${uuid}/publish`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async sendTenderFilledFormByUuids(tenderUuid, filledFormUuid) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfi-tenders/v1/tenders/${tenderUuid}/filled_form/${filledFormUuid}/publish`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async recallTenderOfferByUuid(offerUuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfq-tenders/v1/offer/${offerUuid}/recall`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRFQTenderOffersByUuids({ page, size, query, sort, tenderUuid, requestUuid, filter, carrierTitle }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/request/${requestUuid}`, { params: { page, size, q: query, sort, filter, carrierTitle } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRFQTenderOffersByUuidsAndLots({ page, size, query, sort, tenderUuid, requestUuid, filter }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/request/${requestUuid}/by_lots`, { params: { page, size, q: query, sort, filter } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRFQTenderParticipantsByUuidsAndOffers({ page, size, query, sort, tenderUuid, lotUuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/lots/${lotUuid}`, { params: { page, size, q: query, sort } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRFQTenderOffersByUuidsAndCarriers({ page, size, query, sort, tenderUuid, requestUuid, carrierTitle }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/request/${requestUuid}/by_carriers`, { params: { page, size, q: query, sort, carrierTitle } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRFQTenderCarrierOffersByUuids({ page, size, query, sort, tenderUuid, requestUuid, carrierUuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/request/${requestUuid}/carriers/${carrierUuid}`, { params: { page, size, q: query, sort } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRFITenderOffersByQuestions(uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tenders/v1/tenders/${uuid}/offers/by_questions`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getRFITenderOffersByCompanies(uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tenders/v1/tenders/${uuid}/offers/by_companies`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderContractorStatistics(uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-participants-crud/v1/participants/${uuid}/statistic`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderContractorsByUuid({ page, size, query, sort, uuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants`, { params: { page, size, q: query, sort } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderContractorsByUuidByGroups({ page, size, query, sort, uuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants/by_groups`, { params: { page, size, q: query, sort } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getUnappliedTenderContractorsByUuid({ page, size, query, sort, uuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants/list`, { params: { page, size, q: query, sort } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getUnappliedTenderContractorsByUuidByGroups({ page, size, query, sort, uuid }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants/list_by_groups`, { params: { page, size, q: query, sort } });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getAllUnappliedTenderContractorsInGroup(tender_uuid, group_uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${tender_uuid}/participants/group/${group_uuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getAllUnappliedTenderContractorsWithoutGroup(tender_uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${tender_uuid}/participants/without_group`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getContractors({ page, size, sort, query }) {
    try {
      const { data } = await Client.get(
        '/oboz2-tender-participants-crud/v1/participants',
        { params: { page, size, sort, q: query } }
      );
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getContractorByUuid(uuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-participants-crud/v1/participants/${uuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async addContractorsToTenderByUuid(uuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants/add`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async deleteContractorsFromTenderByUuid(uuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants/delete`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async addParticipantsToTenderSummary(uuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants/winners/add`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async deleteParticipantsFromTenderSummary(uuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-tenders-crud/v1/tenders/${uuid}/participants/winners/delete`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getUsersList(payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-users-viewer/v1/users/info`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTypesOfCarriage() {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/types/transportation/dropdown`);
      return data.transportationTypes;
    } catch (e) {
      throw e;
    }
  },

  async getTaxTreatment() {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/vats/dropdown`);
      return data.vats;
    } catch (e) {
      throw e;
    }
  },

  async getLots(tenderUuid, requestUuid, params) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/request/${requestUuid}/lots`, params);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async createLot(uuid, data) {
    try {
      const res = await Client.post(`/oboz2-tender-rfq-tenders/v1/lots/${uuid}`, data);
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async updateLot(lotUuid, data) {
    try {
      const res = await Client.put(`/oboz2-tender-rfq-tenders/v1/lots/${lotUuid}`, data);
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async deleteLot(tenderUuid, lotUuid) {
    try {
      const { data } = await Client.delete(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/lots/${lotUuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getLotsChecklists(requestUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/request/${requestUuid}/options`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getLotForm(requestUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfq-tenders/v1/request/${requestUuid}/form/lot`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getLotsTableSettings(requestUuid) {
    try {
      const res = await Client.get(`/oboz2-tender-rfq-tenders/v1/request/${requestUuid}/settings`);
      return res.data;
    } catch (e) {
      throw e;
    }
  },

  async updateLotsTableSettings(requestUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-rfq-tenders/v1/request/${requestUuid}/settings`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async saveDraftByCarrier(offerUuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfq-tenders/v1/offer/${offerUuid}/draft`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getQuestionnaire(questionnaireUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getQuestionnaireSettings(questionnaireUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}/settings`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async updateQuestionnaireSettings(questionnaireUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}/settings`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async updateQuestionnaireWeights(questionnaireUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}/weights`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async addQuestionnaireUserQuestions(questionnaireUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}/question`, payload)
      return data
    } catch (e) {
      throw e
    }
  },

  async modifyQuestionnaireUserQuestions(questionnaireUuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}/question`, payload)
      return data
    } catch (e) {
      throw e
    }
  },

  async deleteQuestionnaireUserQuestions(questionnaireUuid, payload) {
    try {
      const { data } = await Client.delete(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}/question`, {
        data: { ...payload }
      })
      return data
    } catch (e) {
      throw e
    }
  },

  async getQuestionnaireByCarrier(questionnaireUuid, carrierUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tenders/v1/questionnaire/${questionnaireUuid}/carriers/${carrierUuid}`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async saveQuestionnaireDraft(filledFormUuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-rfi-tenders/v1/filled_form/${filledFormUuid}/draft`, payload);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async getTenderOffersWinners(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-rfi-tenders/v1/tenders/${tenderUuid}/offers/winners`);
      return data;
    } catch (e) {
      throw e;
    }
  },

  async duplicateTender(type, tenderUuid, payload) {
    try {
      const response = await Client.post(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${tenderUuid}/duplication`, payload);
      if (response && response.error) throw `Ошибка при дублировании тендера: ${response.message}`;
      return response.data;
    } catch (e) {
      throw e;
    }
  },

  async archiveTender(type, tenderUuid) {
    try {
      await Client.put(`/oboz2-tender-${type.toLowerCase()}-tenders/v1/tenders/${tenderUuid}/archive`);
    } catch (e) {
      throw e;
    }
  },

  async createNewStage(tenderUuid, newTenderUuid) {
    try {
      await Client.post(`/oboz2-tender-rfq-tenders/v1/tenders/${tenderUuid}/new_stage`, {
        newTenderUuid
      });
    } catch (e) {
      throw e;
    }
  },

  async getTopicsAndReplies(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${tenderUuid}/topics-and-replies`);

      return data;
    } catch (e) {
      throw e;
    }
  },

  async addTopic(tenderUuid, payload) {
    try {
      await Client.post(`/oboz2-tender-tenders-crud/v1/tenders/${tenderUuid}/topic`, payload);
    } catch (e) {
      throw e;
    }
  },

  async addComment(tenderUuid, payload) {
    try {
      await Client.post(`/oboz2-tender-tenders-crud/v1/tenders/${tenderUuid}/topic/reply`, payload);
    } catch (e) {
      throw e;
    }
  },

  async resendInvitation(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${tenderUuid}/resend_invitation`);

      return data;
    } catch (e) {
      throw e;
    }
  },

  async getStages(tenderUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-tenders-crud/v1/tenders/${tenderUuid}/stages`);

      return data;
    } catch (e) {
      throw e;
    }
  },

  async getReports({ page, size, query, sort }) {
    try {
      const { data } = await Client.get('/oboz2-tender-reports-crud/v1/reports', {
        params: { page, size, sort, q: query }
      });

      return data;
    } catch (e) {
      throw e;
    }
  }
}
