import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {
  async getParticipantGroups ({ page, size, query: q, sort }) {
    try {
      const { data } = await Client.get(
        '/oboz2-tender-participants-crud/v1/groups',
        { params: { page, size, sort, q } }
      )
      return data
    } catch (e) {
      throw e
    }
  },

  async checkParticipantGroupExisting (payload) {
    try {
      return await Client.post('/oboz2-tender-participants-crud/v1/participant/exist', payload)
    } catch (e) {
      throw e
    }
  },

  async createParticipantGroup (payload) {
    try {
      const res = await Client.post('/oboz2-tender-participants-crud/v1/participant/group', payload)
      return res
    } catch (e) {
      throw e
    }
  },

  async getParticipantGroup (participantGroupUuid) {
    try {
      const { data } = await Client.get(`/oboz2-tender-participants-crud/v1/groups/${participantGroupUuid}`)
      return data
    } catch (e) {
      throw e
    }
  },

  async deleteParticipantGroup (participantGroupUuid) {
    try {
      const { data } = await Client.delete(`/oboz2-tender-participants-crud/v1/participant/group/${participantGroupUuid}`)
      return data
    } catch (e) {
      throw e
    }
  },

  async getParticipants ({ page, size, query }) {
    try {
      const { data } = await Client.get(`/oboz2-tender-participants-crud/v1/participants`, { params: { page, size, q: query } })
      return data
    } catch (e) {
      throw e
    }
  },

  async getParticipantsOfParticipantsGroup (participantGroupUuid, { page, size, sort, query: q }) {
    try {
      const { data, status } = await Client.get(
        `/oboz2-tender-participants-crud/v1/groups/${participantGroupUuid}/participants`,
        { params: { page, size, q, sort } }
      )

      return { data, status }
    } catch (e) {
      throw e
    }
  },

  async updateParticipantGroup (participantGroupUuid, payload) {
    try {
      const { data } = await Client.put(`/oboz2-tender-participants-crud/v1/group/${participantGroupUuid}`, payload)
      return data
    } catch (e) {
      throw e
    }
  },

  async addParticipantsToGroup (participantGroupUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-participants-crud/v1/groups/${participantGroupUuid}/participants/add`, payload)
      return data
    } catch (e) {
      throw e
    }
  },

  async deleteParticipantsFromGroup (participantGroupUuid, payload) {
    try {
      const { data } = await Client.post(`/oboz2-tender-participants-crud/v1/groups/${participantGroupUuid}/participants/delete`, payload)
      return data
    } catch (e) {
      throw e
    }
  },

  async getParticipantsShortCard (participantUuid) {
    try {
      const { data, status } = await Client.get(`/oboz2-tender-participants-crud/v1/participants/${participantUuid}/short_card`)
      return { data, status }
    } catch (e) {
      throw e
    }
  },

  async getParticipantUsers (participantUuid) {
    try {
      const { data, status } = await Client.get(`/oboz2-tender-users-viewer/v1/participants/${participantUuid}`)
      return { data, status }
    } catch (e) {
      throw e
    }
  },

  async getParticipantGroupOutside (participantGroupUuid, { page, size, query }) {
    try {
      const { data, status } = await Client.get(
        `/oboz2-tender-participants-crud/v1/groups/${participantGroupUuid}/participants/outside`,
        { params: { page, size, q: query } }
      )
      return { data, status }
    } catch (e) {
      throw e
    }
  }
}