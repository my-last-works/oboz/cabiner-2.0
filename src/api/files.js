import RestClient from './RestClient'
import { createDownloadLink } from '@/helpers/files'

const Client = new RestClient({ isErrorThrow: true })

export default {
  async getFile (uuid, isDownload = false) {
    try {
      const res = await Client.get(`/oboz2-common-file-storage/v1/${uuid}`, { responseType: 'blob' })
      const file = { data: res.data, name: decodeURIComponent(res.headers['content-disposition'].split("''")[1])}
      if (isDownload) {
        createDownloadLink(file)
        return true
      } else {
        return file
      }
    } catch (e) {
      throw e
    }
  },

  async getFileForDowload (uuid) {
    try {
      const res = await Client.get(`/oboz2-common-file-storage/v1/${uuid}`, { responseType: 'blob' })
      return { data: res.data }
    } catch (e) {
      console.error(e)
    }
  },

  async postFile ({ uuid = null, file = {}, is_personal = false, participant_uuid = null, options = {} } = {}) {
    try {
      const jsonData = {
        filename: file.name,
        ...(participant_uuid && { participant_uuid }),
        ...(is_personal && { is_personal_info: is_personal })
      }

      const formData = new FormData()
      formData.append('jsonData', JSON.stringify(jsonData))
      formData.append('file', file)

      const res = await Client.post(`/oboz2-common-file-storage/v1/${uuid}`, formData, options)
      return res.data
    } catch (e) {
      throw e
    }
  },

  async postFileUpload (file) {
    const form = new FormData();
    const jsonData = {
      filename: file.file.name,
    };
    form.append("jsonData", JSON.stringify(jsonData));
    form.append("file", file.file);
    try {
      await Client.post(`/oboz2-common-file-storage/v1/upload/${file.uuid}`, form);
    } catch (e) {
      console.error(e)
    }
  },

  async deleteFile (fileUuid) {
    try {
      await Client.delete(`/oboz2-common-file-storage/v1/${fileUuid}`);
    } catch (e) {
      console.error(e)
    }
  },

  async getSecurityFile (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-file-storage/v1/${uuid}`, { responseType: 'arraybuffer' })
      return { data: res.data, name: decodeURIComponent(res.headers['content-disposition'].split("''")[1])}
    } catch (e) {
      throw e
    }
  },

  async postSecurityFile ({ uuid, participant_uuid, domain_uuid = null, file = {}, options = {} } = {}) {
    try {
      const jsonData = {
        filename: file.name,
        is_personal_info: true,
        participant_uuid,
        ...(domain_uuid && { domain_uuid })
        // ...(participant_uuid && { participant_uuid }),

      }

      const formData = new FormData()
      formData.append('jsonData', JSON.stringify(jsonData))
      formData.append('file', file)

      const res = await Client.post(`/oboz2-security-file-storage/v1/${uuid}`, formData, options)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async postSecurityFileV2 ({ uuid, participant_uuid, is_personal_info = false, file_side = 'GENERAL', domain_uuid = null, file = {}, options = {} } = {}) {
    try {
      const jsonData = {
        filename: file.name,
        is_personal_info,
        participant_uuid,
        file_side,
        ...(domain_uuid && { domain_uuid })
      }

      const formData = new FormData()

      formData.append('jsonData', JSON.stringify(jsonData))
      formData.append('file', file)

      const { data } = await Client.post(`/oboz2-security-file-storage/v2/${uuid}`, formData, options)

      return data
    } catch (e) {
      throw e
    }
  },
  async getFilesName ({ uuids }) {
    try {
      let res = await Client.post(`/oboz2-common-file-storage/v1/names`, uuids)
      return res.data
    } catch (e) {
      throw e
    }
  },
}
