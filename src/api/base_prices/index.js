import RestClient from '../RestClient';

let Client = new RestClient({ isErrorThrow: true });

export default {
  async getMathModel() {
    try {
      const { data } = await Client.get('/oboz2-baseprice-ui-api/v1/mathmodel');
      return data;
    } catch (e) {
      throw e;
    }
  },
  async updateMathModel(model) {
    try {
      const { data } = await Client.put(
        '/oboz2-baseprice-ui-api/v1/mathmodel',
        model
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getTariffs(departure_uuid, arrival_uuid, resource_type_uuid) {
    try {
      let {
        data: { content, contentWRFromTo, contentWRWithoutGeography },
      } = await Client.post('/oboz2-baseprice-ui-api/v1/tariffs', {
        filter: {
          departure_uuid,
          arrival_uuid,
          resource_type_uuid,
        },
      });

      return { content, contentWRFromTo, contentWRWithoutGeography};
    } catch (e) {
      throw e;
    }
  },
  async getRoutes(
    departure_uuid,
    arrival_uuid,
    resource_type_uuid,
    page_num,
    page_size
  ) {
    try {
      const {
        data,
      } = await Client.post('/oboz2-baseprice-ui-api/v1/routes', {
        filter: {
          departure_uuid,
          arrival_uuid,
          resource_type_uuid,
        },
        page_num,
        page_size,
      });
      return data;
    } catch (e) {
      console.log(e);
      throw e;
    }
  },
  async getFixedPrice(departure_uuid, arrival_uuid, resource_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/prices/fixed/departure/${departure_uuid}/arrival/${arrival_uuid}/resource/${resource_uuid}`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async updateFixedPrice(departure_uuid, arrival_uuid, resource_uuid, body) {
    try {
      const { data } = await Client.put(
        `/oboz2-baseprice-ui-api/v1/prices/fixed/departure/${departure_uuid}/arrival/${arrival_uuid}/resource/${resource_uuid}`,
        body
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async deleteFixedPrice(departure_uuid, arrival_uuid, resource_uuid) {
    try {
      const { data } = await Client.delete(
        `/oboz2-baseprice-ui-api/v1/prices/fixed/departure/${departure_uuid}/arrival/${arrival_uuid}/resource/${resource_uuid}`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getExpertPrice(departure_uuid, arrival_uuid, resource_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/prices/expert/departure/${departure_uuid}/arrival/${arrival_uuid}/resource/${resource_uuid}`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async updateExpertPrice(departure_uuid, arrival_uuid, resource_uuid, body) {
    try {
      const { data } = await Client.put(
        `/oboz2-baseprice-ui-api/v1/prices/expert/departure/${departure_uuid}/arrival/${arrival_uuid}/resource/${resource_uuid}`,
        body
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getExtrapolationDistance(departure_uuid, arrival_uuid, resource_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/extrapolation/departure/${departure_uuid}/arrival/${arrival_uuid}/resource/${resource_uuid}/distance`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getBasePriceFormula(base_price_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/prices/base/${base_price_uuid}/stat/formula`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getAveragedBasePriceChart(tariff_class_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/prices/averaged/orders/facts/${tariff_class_uuid}/stat/chart`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getBasePriceChart(tariff_class_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/prices/base/${tariff_class_uuid}/stat/chart`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getAveragePriceFormula(averaged_price_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/prices/averaged/${averaged_price_uuid}/stat/formula`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getAveragePriceChart(tariff_class_uuid) {
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-ui-api/v1/prices/averaged/${tariff_class_uuid}/stat/chart`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getRecalculateStatus() {
    try {
      const { data } = await Client.get(
        '/oboz2-baseprice-recalculate-api/v1/recalculate/report/status'
      );
      return data
    } catch (e) {
      throw e;
    }
  },
  async recalculateTariffBasePrice(tariff_class_uuid) {
    try {
      const { data } = await Client.post(
        `/oboz2-baseprice-recalculate-api/v1/recalculate/tariff/${tariff_class_uuid}`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async getCurrentPlan(){
    try {
      const { data } = await Client.get(
        `/oboz2-baseprice-recalculate-api/v1/recalculate/scheduling`
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async updateScheduling(body){
    try{
      const { data } = await Client.put(
        `/oboz2-baseprice-recalculate-api/v1/recalculate/scheduling`,
        body
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
  async startGlobalRecalculate(){
    try{
      const { data } = await Client.post(
        `/oboz2-baseprice-recalculate-api/v1/recalculate/report/run`
      );
      return data;
    } catch (e) {
      throw e;
    }
  }
};
