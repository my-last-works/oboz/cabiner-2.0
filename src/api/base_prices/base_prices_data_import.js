import RestClient from '../RestClient'

let Client = new RestClient({ isErrorThrow: true })

export default {
    async getListFiles ({ author_uuid = null, type = null, page_number = 0, page_size = 20, order_field = 'UPDATED_AT', order_direction = 'DESC', query }) {
        try {
            let params = { page_number, page_size, order_field, order_direction, q: query};
            if ( author_uuid ) {
                params.author = author_uuid;
            }
            if ( type ) {
                params.type = type;
            }
            let res = await Client.get(`/oboz2-baseprice-data-loader/v1/listfiles`, {
                params: params
            });
            return res.data;
        } catch (e) {
            throw e
        }
    },

    async loadFile ({ type, file_uuid, summary, confidence_koef, type_locations }) {
        try {
            let params = {};
            if( type_locations ) {
                params.type_locations = type_locations;
            }
            let res = await Client.get(`/oboz2-baseprice-data-loader/v1/loadfile/type/${type}/file/${file_uuid}/summary/${summary}/confidence_koef/${confidence_koef}`, {
                params: params
            });
            return res.data
        } catch (e) {
            throw e
        }
    },

    async activation (uuid) {
        try {
            let res = await Client.put(`/oboz2-baseprice-data-loader/v1/activation/${uuid}` );
            return res;
        } catch (e) {
            throw e
        }
    },

    async deActivation (uuid) {
        try {
            let res = await Client.put(`/oboz2-baseprice-data-loader/v1/deactivation/${uuid}` );
            return res;
        } catch (e) {
            throw e
        }
    },

}