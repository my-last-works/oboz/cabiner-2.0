import RestClient from '../RestClient'

let Client = new RestClient({ isErrorThrow: true })

export default {
    // Получение всех групп транспорта
    async getTransportGroups ({ page_size = 20, page_num = 0 }) {
        try {
            let res = await Client.get(`/oboz2-baseprice-ui-api/v1/clusters/resources/groups?page_size=${page_size}&page_num=${page_num}`);
            return res.data;
        } catch (e) {
            throw e
        }
    },

    // Создание группы
    async createTransportGroup ( payload, author = null ) {
        try {
            let authorParam = author ? '?author='+author : '';
            let res = await Client.post(`/oboz2-baseprice-ui-api/v1/clusters/resources/groups${authorParam}`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },

    // Редактирование списка типов транспорта
    async updateTransportGroupServices ( uuid, payload ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-ui-api/v1/clusters/resources/groups/${uuid}/services`, payload);
            return res;
        } catch (e) {
            throw e
        }
    },

    // Редактирование группы
    async updateTransportGroup ( uuid, title ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-ui-api/v1/clusters/resources/groups/${uuid}?title=${title}`);
            return res;
        } catch (e) {
            throw e
        }
    },

    // Удаление группы
    async deleteTransportGroup ( uuid ) {
        try {
            let res = await Client.delete(`/oboz2-baseprice-ui-api/v1/clusters/resources/groups/${uuid}`);
            return res
        } catch (e) {
            throw e
        }
    },

    // Удаление из списка типов транспорта
    async deleteTransportGroupService ( group_uuid, service_uuid ) {
        try {
            let res = await Client.delete(`/oboz2-baseprice-ui-api/v1/clusters/resources/groups/${group_uuid}/services/${service_uuid}`);
            return res
        } catch (e) {
            throw e
        }
    },

    // Получение групп направлений
    async getDirectionsGroups ({ page_size = 20, page_num = 0 }) {
        try {
            let res = await Client.get(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups?page_size=${page_size}&page_num=${page_num}`);
            return res.data;
        } catch (e) {
            throw e
        }
    },

    // Создание группы направлений
    async createDirectionGroup ( payload ) {
        try {
            let res = await Client.post(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },

    // Удаление группы направлений
    async deleteDirectionGroup ( uuid ) {
        try {
            let res = await Client.delete(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups/${uuid}`);
            return res
        } catch (e) {
            throw e
        }
    },

    // Редактирование группы направлений
    async updateDirectionGroup ( uuid, title ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups/${uuid}?title=${title}`);
            return res;
        } catch (e) {
            throw e
        }
    },

    // Создание направления
    async createDirection (group_uuid, payload ) {
        try {
            let res = await Client.post(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups/${group_uuid}/directions`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },

    // Удаление направления
    async deleteDirection ( group_uuid, direction_uuid ) {
        try {
            let res = await Client.delete(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups/${group_uuid}/directions/${direction_uuid}`);
            return res
        } catch (e) {
            throw e
        }
    },

    // Редактирование списка локаций в направлении
    async updateDirection (group_uuid, direction_uuid, payload ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups/${group_uuid}/directions/${direction_uuid}`, payload);
            return res;
        } catch (e) {
            throw e
        }
    },

    // Удаление локации
    async deleteLocation ( location_uuid, direction_uuid, group_uuid ) {
        try {
            let res = await Client.delete(`/oboz2-baseprice-ui-api/v1/clusters/ways/groups/${group_uuid}/directions/${direction_uuid}/location/${location_uuid}`);
            return res
        } catch (e) {
            throw e
        }
    },

    // получение кластеров для монитора базовых цен
    async getClusterExpertPrice ({direction_group_uuid, location_from_uuid = null, location_to_uuid = null, page_size = 20, page_num = 0 }) {
        try {
            let res = await Client.get(`/oboz2-baseprice-ui-api/v1/clusters/list/direction_group/${direction_group_uuid}`,{
                params: {
                    location_from_uuid: location_from_uuid,
                    location_to_uuid: location_to_uuid,
                    page_size: page_size,
                    page_num: page_num,
                }
            });
            return res.data;
        } catch (e) {
            throw e
        }
    },

    // Назначение экспертной цены для кластеров
    async setExpertPrice ( payload ) {
        try {
            let res = await Client.post(`/oboz2-baseprice-ui-api/v1/clusters/expert_price/`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },

    // получение кластеров контейнеров для монитора базовых цен
    async getClusterContainers ({ clusterFromUuid = null, clusterToUuid = null, pageSize = 20, pageNum = 0 }) {
        try {
            let res = await Client.get(`/oboz2-baseprice-ui-api/v1/containers`,{
                params: {
                    cluster_from_uuid: clusterFromUuid,
                    cluster_to_uuid: clusterToUuid,
                    page_size: pageSize,
                    page_num: pageNum,
                }
            });
            return res.data;
        } catch (e) {
            throw e
        }
    },

    // получение связок агломераций для фильтра
    async getContainerAgglomerations () {
        try {
            let res = await Client.get(`/oboz2-baseprice-ui-api/v1/containers/list_directions`);
            return res.data;
        } catch (e) {
            throw e
        }
    },

    // Назначение фиксированной цены для контейнерных кластеров
    async setContainerFixedPrice ( payload ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-ui-api/v1/containers/fixed_prices/`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },

    // Отмена фиксированной цены для контейнерных перевозок
    async cancelContainerFixedPrice ( payload ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-ui-api/v1/containers/fixed_prices/cancellation`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },
}