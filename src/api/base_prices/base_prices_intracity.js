import RestClient from '../RestClient'

let Client = new RestClient({ isErrorThrow: true })

export default {
    // Получение групп для внутригородских перевозок.
    async getIntracityClusters ({pageSize = 20, pageNum = 0 }) {
        try {
            let res = await Client.get(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_clusters/`,{
                params: {
                    page_size: pageSize,
                    page_num: pageNum,
                }
            });
            return res.data;
        } catch (e) {
            throw e
        }
    },

    // Создание группы
    async createIntracityGroup ( payload ) {
        try {
            let res = await Client.post(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_clusters/`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },

    // Редактирование группы
    async updateIntracityGroup ( uuid, payload ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_clusters/group/${uuid}`, payload);
            return res;
        } catch (e) {
            throw e
        }
    },

    // Удаление группы
    async deleteIntracityGroup ( uuid ) {
        try {
            let res = await Client.delete(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_clusters/group/${uuid}`);
            return res
        } catch (e) {
            throw e
        }
    },


    // Назначение цены для кластера внутригородских перевозок.
    async createIntracityPrice ( payload ) {
        try {
            let res = await Client.post(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_prices/`, payload);
            return res.data
        } catch (e) {
            throw e
        }
    },

    // Изменение цены для кластера внутригородских перевозок.
    async updateIntracityPrice ( uuid, payload ) {
        try {
            let res = await Client.put(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_prices/group_price/${uuid}`, payload);
            return res;
        } catch (e) {
            throw e
        }
    },

    // Удаление цены для кластера внутригородских перевозок.
    async deleteIntracityPrice ( uuid ) {
        try {
            let res = await Client.delete(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_prices/group_price/${uuid}`);
            return res
        } catch (e) {
            throw e
        }
    },

    // Получение списка кластеров и услуг для задания цены на внутригородские перевозки.
    async getIntracityServices () {
        try {
            let res = await Client.get(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_prices/services`);
            return res.data;
        } catch (e) {
            throw e
        }
    },

    // Получение данных для монитора внутригородских перевозок
    async getIntracityPrices (locationUuid) {
        try {
            let res = await Client.get(`/oboz2-baseprice-intracity-clusters-crud/v1/intracity_prices/location/${locationUuid}`);
            return res.data;
        } catch (e) {
            throw e
        }
    },

}