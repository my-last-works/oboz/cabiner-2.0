import SockJS from 'sockjs-client'
import Stomp from 'webstomp-client'
import { getCookie } from '@/utils/cookie'

const defaultApiUrl = '/oboz2-notificator-websocket-sender/notificator-ws-sender'

export default class RestClient {
	constructor(props = {}) {
		this.token = getCookie('auth_token')
		this.baseURL = props.baseURL || window.OBOZ_CONFIG.OBOZ_API_HOST
		this.apiUrl = props.apiURL || defaultApiUrl
		this.connected = false
		this.messageTypeOptions = props.messageTypes || {}
		this.sessionId = null
		this.frame = null
		this.socket = null
		this.stompClient = null
		this.wsMessageHandler = props.handler

		// this.connect()
	}
	async connect () {
		this.socket = new SockJS(this.baseURL + this.apiUrl)
		this.stompClient = Stomp.over(this.socket)
		this.stompClient.connect(
			{ token: getCookie('auth_token') },
			frame => {
				console.log(this, '2222222')
				this.connected = true

				this.frame = frame
				this.sessionId = /\/([^/]+)\/websocket/.exec(this.socket._transport.url)[1]

				console.log("session id: " + this.sessionId)
				console.log('Connected: ' + this.frame)

				this.subscribe()
			},
			error => {
				console.log(error)
				this.connected = false
			}
		);
	}
	subscribe () {
		this.stompClient.subscribe('/user/queue/reply', resMessage => {
			const { type, message } = JSON.parse(resMessage.body)

			if (this.wsMessageHandler && typeof this.wsMessageHandler === 'function') {
				this.wsMessageHandler({ type, message: JSON.parse(message) })
			}
		})
	}
	unsubscribe () {
		this.stompClient.unsubscribe('/user/queue/reply')
	}
	disconnect () {
		this.stompClient.disconnect(() => {
			this.connected = false
		})
	}
}
