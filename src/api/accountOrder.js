import RestClient from './RestClient';
import {uuid as vue_uuid} from 'vue-uuid';
import {createDownloadLink} from "@/helpers/files";

let Client = new RestClient();

export default {
  getUser () {
    return Client.get('/oboz2-user-access-users-crud/v1/user/')
  },
  getAccountOrders (filters) {
    return Client.post('/oboz2-suborder-carrier-searchers-crud/v1/internal/suborders', {
      ...filters
    })
  },
  getAccountOrdersTwo (filters) {
    return Client.get(`/oboz2-suborder-orders-crud/v1/suborders${filters}`);
  },
  getFacsimile (suborder_uuid) {
    return Client.get(`/oboz2-suborder-order-print-form-viewer/v1/print-form/can-be-signed/${suborder_uuid}`);
  },
  postFacsimile (suborder_uuid, doc_type) {
    return Client.get(`/oboz2-suborder-order-print-form-viewer/v1/print-form/upload-signed/${doc_type}/${suborder_uuid}`);
  },
  async getFacsimileFile (suborder_uuid, fileName, isDownload = true){
    try {
      const res = await Client.get(`/oboz2-suborder-order-print-form-viewer/v1/print-form/signed/pdf/${suborder_uuid}`, { responseType: 'blob' })
      const file = { data: res.data, name: 'Шаблон заявки заказа.pdf'}

      if (isDownload) {
        createDownloadLink(file)
        return true
      } else {
        return file
      }
    } catch (e) {
      throw e
    }
  },
/*
  getFacsimileFile(suborder_uuid, doc_type) {
    return Client.get(`/oboz2-suborder-order-print-form-viewer/v1/print-form/signed/${doc_type}/${suborder_uuid}`);
  },*/
  cancelTheOrder (type, uuid) {
    return Client.put(`/oboz2-suborder-${type}-crud/v1/suborders/${uuid}/reject`);
  },
  getParamsAuction (uuid, status) {
    return Client.post('/oboz2-auction-lots-reports/v1/personal/lots', {suborders: uuid, status: status});
  },
  getParamsBooking (uuid, status) {
    return Client.post('/oboz2-booking-lots-reports/v1/personal/lots', {suborders: uuid, status: status});
  },
  listHelperLogistic (suborder_uuid) {
    return Client.post('/oboz2-suborder-released-resources-predictor/v1', {suborderUuids: suborder_uuid});
  },
  getAccountOrder (suborder_uuid) {
    return Client.get(`/oboz2-suborder-orders-crud/v1/suborder/${suborder_uuid}`);
  },
  getHelperLogisticAccount (uuid) {
    return Client.get(`/oboz2-suborder-released-resources-predictor/v1?suborder_uuid=${uuid}`);
  },
  getAdditionalInfoOrder (uuid, type) {
    return Client.get(`/oboz2-${type}-lots-reports/v1/personal/lots/${uuid}`);
  },
  async getPersonalOfferInfo (uuid) {
    try {
      let res = await Client.get(`/oboz2-suborder-carrier-assortmenter/v1/offer/${uuid}`)
      res.data.uuid = uuid

      return res
    } catch (e) {
      throw e;
    }
  },
  async changePersonalOfferStatus ({ uuid, status }) {
    try {
      let res = await Client.post(`/oboz2-suborder-carrier-assortmenter/v1/offer/${uuid}`, { status })

      return res.data
    } catch (e) {
      throw e
    }
  },
  acceptOrder (uuid) {
    return Client.put(`/oboz2-booking-requests-crud/v1/${uuid}`, {
        "status": "ACCEPTED"
      }
    )
  },
  makeBid (bid, lotUuid) {
    let uuid = vue_uuid.v4();
    return Client.post(`/oboz2-auction-bids-crud/v1/${uuid}`, {
        "bid": bid,
        "lotUuid": lotUuid
      }
    )
  },
  getAccountAdvance (suborder_uuid, status_filter) {
    return Client.get(`/oboz2-suborder-orders-crud/v1/suborders/${suborder_uuid}/advances?status_filter=${status_filter}`)
  },
  newAccountAdvance (suborder_uuid, advanceWithNds, status_filter ) {
    return Client.post(`/oboz2-suborder-orders-crud/v1/suborders/${suborder_uuid}/new_advance/?status_filter=${status_filter}`, {amount: advanceWithNds });
  },
  async advanceConditions (suborder_uuid, isDownload = true){
    try {
      const res = await Client.get(`/oboz2-suborder-order-print-form-viewer/v1/advance_conditions/pdf/${suborder_uuid}`, { responseType: 'blob' })
      const file = { data: res.data, name: 'Условия авансирования.pdf'}

      if (isDownload) {
        createDownloadLink(file)
        return true
      } else {
        return file
      }
    } catch (e) {
      throw e
    }
  },
  async getAutoSignedFile ({ suborderUuid } = {}){
    // {
    //   "fileUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "suborderUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // }
    try {
      const res = await Client.get(`/oboz2-suborder-order-print-form-viewer/v1/print-form/upload-signed/pdf/${suborderUuid}` )

      return res.data
    } catch (e) {
      throw e
    }
  },
  async getExecutorsOrders ({ q, filters = {}, page = 0, size = 50, searchStatus =  'ACTIVE', carrierSearchType = 'ALL', sort = '' } = {}) {
    try {
      const { resourceSpeciesUuid, resourceTypeUuid, resourceClassUuid, afterDt, fromLocationUuid, toLocationUuid } = filters
      const sortArray = sort.split(',')

      const res = await Client.get(`/oboz2-suborder-carrier-searchers-crud/v1/suborders`, {
        params: {
          q
        },
        headers: {
          searchScope: JSON.stringify({
            searchStatus,
            carrierSearchType
          }),
          filters: JSON.stringify({
            resourceSpeciesUuid,
            resourceTypeUuid,
            resourceClassUuid,
            afterDt,
            fromLocationUuid,
            toLocationUuid
          }),
          sorting: JSON.stringify({
            sortField: sortArray[0] || 'searchStartAt',
            sortDirection: sortArray[1] || 'desc'
          }),
          pagination: JSON.stringify( {
            page,
            size
          })
        }
      })

      res.data.content = res.data.suborderList

      return res.data
    } catch (e) {
      throw e
    }
  },
  async getExtraRunAndPoint (uuid) {
    try {
      const res = await Client.get(`/oboz2-suborder-orders-crud/v1/suborder/carrier/${uuid}` );

      return res.data
    } catch (e) {
      throw e;
    }
  }
}
