import RestClient from './RestClient'

const Client = new RestClient()

export default {
  async fetch () {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/participant/fields`)
      return res.data.participant_fields
    } catch (e) {
      throw e
    }
  },
  async fetchResource () {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/resource/fields`)
      return res.data && res.data.resource_fields || []
    } catch (e) {
      throw e
    }
  },
  async create (payload) {
    try {
      const res = await Client.post(`/oboz2-security-card-fields-crud/v1/participant/fields/${payload.uuid}`, payload)
      return res
    } catch (e) {
      throw e
    }
  },
  async createResource (payload) {
    try {
      const res = await Client.post(`/oboz2-security-card-fields-crud/v1/resource/fields/${payload.uuid}`, payload)
      return res
    } catch (e) {
      throw e
    }
  },
  async update (payload) {
    try {
      const res = await Client.put(`/oboz2-security-card-fields-crud/v1/participant/fields/${payload.uuid}`, payload)
      return res
    } catch (e) {
      throw e
    }
  },
  async updateResource (payload) {
    try {
      const res = await Client.put(`/oboz2-security-card-fields-crud/v1/resource/fields/${payload.uuid}`, payload)
      return res
    } catch (e) {
      throw e
    }
  },
  async get (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/participant/fields/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getResource (uuid) {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/resource/fields/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getRequisiteGroups () {
    try {
      const res = await Client.get(`/oboz2-security-requisite-groups-crud/v1/`)
      return res.data && res.data.requisite_groups || []
    } catch (e) {
      throw e
    }
  },
  async getRoles () {
    try {
      const res = await Client.get(`/oboz2-dictionary-roles-viewer/v1/`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getRolesParticipant ({ uuid } = {}) {
    try {
      const res = await Client.get(`/oboz2-security-card-fields-crud/v1/field/${uuid}/participant/roles`)
      return res.data && res.data.roles || []
    } catch (e) {
      throw e
    }
  },
  async getLegalEntities () {
    try {
      const res = await Client.get(`/oboz2-dictionary-legal-entities-crud/v1/`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getServices () {
    try {
      const res = await Client.get(`/oboz2-security-ext-services-viewer/v1/`)
      return res.data && res.data.services
    } catch (e) {
      throw e
    }
  },
  async getDocs ({ type } = {}) {
    try {
      const res = await Client.get(`/oboz2-security-reg-doc-types-viewer/v1/${type}/dropdown`)
      return res.data && res.data.reg_doc_types || []
    } catch (e) {
      throw e
    }
  },
  async getCountries ({ page, size, query } = {}) {
    try {
      const res = await Client.get(`/oboz2-dictionary-countries-viewer/v1/`, {
        params: {
          page, size, q: query
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  }
}
