import RestClient from "./RestClient";
import { uuid as vue_uuid } from "vue-uuid";

let Client = new RestClient({ isErrorThrow: true });

export default {
  getDomains() {
    return Client.get("/oboz2-domains-domain-viewer/v1/stats_by_domain");
  },
  getLinks(uuid) {
    return Client.get(`/oboz2-domains-domain-viewer/v1/by_domain/${uuid}`);
  },
  getRestriction(uuid) {
    return Client.get(
      `/oboz2-domains-domain-viewer/v1/can_create_link/${uuid}`
    );
  },
  getPublicRestriction(uuid) {
    return Client.get(
      `/oboz2-domains-domain-viewer/v1/can_publicate_link/${uuid}`
    );
  },
  createPublicLink(data) {
    return Client.post(
      `/oboz2-domains-link-requests-crud/v1/${vue_uuid.v4()}`,
      data
    );
  },
  removePublicLink(uuid, data) {
    return Client.put(`/oboz2-domains-links-crud/v1/${uuid}`, data);
  },
  publicateLink(data) {
    return Client.post(
      `/oboz2-domains-link-publication-requests-crud/v1/public_link/${vue_uuid.v4()}`,
      data
    );
  },
  checkIfPendingPublic(contractor_uuid, participant_uuid) {
    return Client.get(
      `oboz2-domains-link-publication-requests-crud/v1/${contractor_uuid}/${participant_uuid}`
    );
  },
  checkIfPending(contractor_uuid, participant_uuid) {
    return Client.get(
      `oboz2-domains-link-requests-crud/v1/${contractor_uuid}/${participant_uuid}`
    );
  },

  // version 2.0
  async getAllAgents({ page = 0, size, sort, direction, query }) {
    try {
      const res = await Client.get(
        `/oboz2-domains-participant-groups-crud/v1/search/members`,
        { params: { page, size, sort, direction, q: query } }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getAllGroups({
    page = 0,
    size = 1000,
    sort = "createdAt",
    direction = "desc",
    query,
  }) {
    try {
      const res = await Client.get(
        `/oboz2-domains-participant-groups-crud/v1/search/groups`,
        { params: { page, size, sort, direction, q: query } }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async addGroup(uuid, group) {
    try {
      const res = await Client.put(
        `/oboz2-domains-participant-groups-crud/v1/groups/${uuid}`,
        group
      );
      return res;
    } catch (err) {
      throw err;
    }
  },

  async deleteGroup(uuid) {
    try {
      return await Client.delete(
        `/oboz2-domains-participant-groups-crud/v1/groups/${uuid}`
      );
    } catch (err) {
      throw err;
    }
  },

  async getGroupAgents(
    uuid,
    { page = 0, size = 1000, sort = "title", direction = "desc", query }
  ) {
    try {
      const res = await Client.get(
        `/oboz2-domains-participant-groups-crud/v1/search/members`,
        { params: { group_uuid: uuid, page, sort, size, direction, q: query } }
      );

      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getAgentsToAdd({
    page = 0,
    size = 20,
    sort = "title",
    direction = "desc",
    query,
  }) {
    try {
      const res = await Client.get(
        `/oboz2-domains-participant-groups-crud/v1/search/members`,
        { params: { page, size, sort, direction, q: query } }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async addGroupAgent(uuid, payload) {
    try {
      const res = await Client.patch(
        `/oboz2-domains-participant-groups-crud/v1/groups/${uuid}/members`,
        { action: "ADD", participants: payload }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async deleteGroupAgent(uuid, payload) {
    try {
      const res = await Client.patch(
        `/oboz2-domains-participant-groups-crud/v1/groups/${uuid}/members`,
        { action: "REMOVE", participants: payload }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getAllBlockedAgents({
    page = 0,
    size = 1000,
    sort,
    direction,
    query,
    group_type = "BLACKLIST",
  }) {
    try {
      const res = await Client.get(
        `/oboz2-domains-participant-groups-crud/v1/search/members`,
        { params: { page, size, sort, direction, q: query, group_type } }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getAgent(uuid) {
    try {
      const res = await Client.get(
        `/oboz2-domains-participant-groups-crud/v1/participants/${uuid}`
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getAllAgentInfo(uuid) {
    try {
      const res = await Client.get(
        `oboz2-security-participants-crud/v1/participants/${uuid}`
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async getAgentOrders() {
    try {
      const res = [
        {
          date: "2020-10-10T10:42:00.000Z",
          route: {
            points: [
              {
                title: "Москва",
              },
              {
                title: "Санкт-Петербург",
              },
            ],
          },
          modality: "truck",
          resource: "Полуприцеп. Изотерм. 22 т / 90 м3 / 33 пал",
        },
      ];

      return Promise.resolve(res);
    } catch (err) {
      throw err;
    }
  },

  async getAgentResource({ type, uuid, page, size } = {}) {
    try {
      const res = await Client.get(
        `/oboz2-security-resources-viewer/v1/${type}`,
        { params: { carrier_uuid: uuid, filter: "ACTIVE", page, size } }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },

  async toggleMembershipPeriod({ group_uuid, uuid, membershipPeriodTo }) {
    try {
      const res = await Client.post(
        `/oboz2-domains-participant-groups-crud/v1/group/${group_uuid}/participant/${uuid}`,
        { membershipPeriodTo }
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  }
};
