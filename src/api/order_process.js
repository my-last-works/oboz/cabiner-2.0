import RestClient from './RestClient'
import { getRiskClass } from '@/helpers/risks'
import { parseNumber } from '@/helpers/order'
import { createDownloadLink } from '@/helpers/files'
import { uuid } from 'vue-uuid'
import cloneDeep from 'clone-deep'

const Client = new RestClient({ isErrorThrow: true })

export default {
  async getOrdersList ({ page, size, query, sort, filter, filters = {}, url }, props) {
    try {
      let res = await Client.get(`/oboz2-order-po-viewer/v1/orders`, {
        params: {
          page,
          size,
          q: query,
          sort,
          filter:filter,
          ...filters,
          group: url,
          transportationWay: props.transport ? props.transport : 'FTL',
          fromLocationTitle: props.from,
          toLocationTitle: props.to,
          arriveOrDepartureDt: props.date,
          services: props.services,
          resourceTypeTitle: props.resource,
          statusTitle: props.status,
          logistUuid: props.logist,
          authorUuid: props.author,
        }
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getOrder ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}`)
      let result = res.data.cliCarResponse || res.data.clientResponse
      let executor
      if (result.responseOrder.contractorUuid) {
        executor = {
          uuid: result.responseOrder.contractorUuid,
          title: result.responseOrder.contractorTitle,
          icon: getRiskClass(result.responseOrder.riskColor)
        }
      } else {
        executor = null
      }
      return { ...result, executor: executor }
    } catch (e) {
      throw e
    }
  },
  async getOrderEvents ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}/events`)
      return res.data.orderEvents
    } catch (e) {
      throw e
    }
  },
  async orderCancellation ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}/cancel`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getSubOrders ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-suborder-orders-crud/v1/orders/${uuid}/suborders`)
      let result = []

      for (let key in res.data) {
        result = result.concat(res.data[key])
      }
      return result
    } catch (e) {
      throw e
    }
  },
  async getTrips ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}/suborders`)

      let result = []

      for (let key in res.data.suborders) {
        result = result.concat(res.data.suborders[key])
      }

      return result.map((item) => {
        let numberObj = item.resourceNumber ? parseNumber(item.resourceNumber) : {}
        return {
          ...item,
          ...numberObj
        }
      })
    } catch (e) {
      throw e
    }
  },
  async getTripEvents ({ uuid, suborderUuid }) {
    try {
      let res = await Client.get(`/oboz2-order-trips-crud/v1/trips/${uuid}/suborders/${suborderUuid}/events`)
      return res.data.tripEvents
    } catch (e) {
      throw e
    }
  },
  async getTripPointsInfo ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-order-trips-crud/v1/trips/${uuid}/visit_points`)
      let result = {}

      res.data.tripPoints.forEach((point) => {
        result[point.visitPointUuid] = { ...point }
      })

      return result
    } catch (e) {
      throw e
    }
  },
  async getSubOrder ({ data, orderUuid, isClient = false, isManual = false }) {
    try {
      let res;

      if (isClient) {
        res = await Client.get(`/oboz2-order-po-crud/v1/orders/${orderUuid}/suborders/${data.uuid}`, {
          params: {
            is_manual: isManual
          }
        })
      } else {
        res = await Client.get(`/oboz2-suborder-orders-crud/v1/suborder/${data.uuid}`, {
          params: {
            is_manual: isManual
          }
        })
      }

      let result = res.data.clicarResponse || res.data.clientResponse || res.data.carrierResponse || res.data;

      const advance = {
        advanceAvailable: result.advanceAvailable,
        advanceServicePrice: result.advanceServicePrice
      };

      const baseData = result.baseOrder || result.legOrder || result.logchainOrder || result.shipmentPointOrder || result;

      result = {
        ...baseData,
        ...advance
      };

      let resources = result.resources && result.resources.assignedResources ? result.resources.assignedResources.map((item) => {
        return {
          ...item,
          icon: getRiskClass(item.riskColor),
          ...parseNumber(item.number, item.superclass)
        }
      }) : [];
      const requiredResources = result.resources && result.resources.requiredResources;
      return { ...result, resources, requiredResources }
    } catch (e) {
      throw e
    }
  },
  async getSubOrderEvents ({ subOrder }) {
    try {
      let res = await Client.get(`/oboz2-suborder-orders-crud/v1/suborders/${subOrder.uuid}/events`)
      return res.data.suborderEvents
    } catch (e) {
      throw e
    }
  },
  async getSubOrderTime ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/status/time`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getExecutorsList ({ page, size, query, uuid }) {
    try {
      let res = await Client.get(`/oboz2-suborder-participant-list-viewer/v1/suborder/${uuid}/carriers`, {
        params: { page, size, q: query }
      })

      return res.data.participants.map((item) => {
        return {
          isDisabled: !item.isEnabled,
          icon: getRiskClass(item.riskColor),
          ...item
        }
      })
    } catch (e) {
      throw e
    }
  },
  async getExecutorsListAll ({ page, size, query, uuid, species }) {
    try {
      let res
      if (['AUTO', 'TRAIN'].includes(species)) {
        res = await Client.get(`/oboz2-suborder-participant-list-viewer/v1/suborder/${uuid}/carriers/all`, {
          params: { page, size, q: query }
        })
      } else {
        res = await Client.get(`/oboz2-suborder-participant-list-viewer/v1/suborder/service/${uuid}/carriers/all`, {
            params: { page, size, q: query, uuid }
          })
      }

      return res.data.participants.map((item) => {
        return {
          isDisabled: !item.isEnabled,
          icon: getRiskClass(item.riskColor),
          ...item
        }
      })
    } catch (e) {
      throw e
    }
  },
  async selectExecutor ({ uuid, doerUuid, tariff, isVatInclude, assignedTariff, contractUuid, fileUuids }) {
    try {
      let res = await Client.put(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/carrier`, {
        doerUuid,
        source: 'MANUAL',
        assignedAt: new Date(),
        tripTariff: tariff,
        isVatInclude,
        assignedTariff,
        contractUuid,
        fileUuids
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async selectExecutorNotAuto ({ subOrder, contractorUuid, tripTariff, fileUuids, isVatInclude, assignedTariff, variantId, preorderId }) {
    try {
      let res = await Client.put(`/oboz2-suborder-orders-crud/v1/suborder/${subOrder.uuid}/carrier`, {
        contractorUuid,
        tripTariff,
        fileUuids,
        isVatInclude,
        assignedTariff,
        variantId,
        preorderId
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async isMultimodalOrder ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}/is_multimodal`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async changeTariff ({ uuid, tariff, isVatInclude, fileUuids, comment, assignedTariff }) {
    try {
      let res = await Client.put(`/oboz2-suborder-orders-crud//v1/suborders/${uuid}/tariff`, {
          tariff,
          isVatInclude,
          fileUuids,
          comment,
          assignedTariff
        })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async setCarriageNumber ({ subOrder, carriageNumber }) {
    try {
      let res = await Client.post(`/oboz2-suborder-orders-crud/v1/suborders/${subOrder.uuid}/carriage`, {
        carriageNumber
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async setWaybillNumber ({ subOrder, waybillNumber }) {
    try {
      let res = await Client.put(`/oboz2-suborder-orders-crud/v1/suborder/${subOrder.uuid}/waybill`, {
        waybillNumber
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getOrderContainersList ({ uuid }) {
    try {
      let res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}/containers`)
      return res.data.containers
    } catch (e) {
      throw e
    }
  },
  async addOrderContainer ({ order, number }) {
    try {
      let res = await Client.post(`/oboz2-order-po-crud/v1/orders/${order.uuid}/containers`, {
        number
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteOrderContainer ({ order, resourceUuid }) {
    try {
      let res = await Client.delete(`/oboz2-order-po-crud/v1/orders/${order.uuid}/containers`, {
        data: { resourceUuid }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getSubOrderContainers ({ subOrder }) {
    try {
      let res = await Client.get(`/oboz2-order-po-crud/v1/suborders/${subOrder.uuid}/containers`)
      return res.data.containers
    } catch (e) {
      throw e
    }
  },
  async getSelectedSubOrderContainers ({ subOrder }) {
    try {
      let res = await Client.get(`/oboz2-suborder-orders-crud/v1/suborder/${subOrder.uuid}/container`)
      return res.data.containers
    } catch (e) {
      throw e
    }
  },
  async addSubOrderContainer ({ subOrder, resourceUuid, resourceNumber }) {
    try {
      let res = await Client.post(`/oboz2-suborder-orders-crud/v1/suborder/${subOrder.uuid}/container/${resourceUuid}`, {
        containerUuid: resourceUuid,
        number: resourceNumber
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteSubOrderContainer ({ subOrder, resourceUuid }) {
    try {
      let res = await Client.delete(`/oboz2-suborder-orders-crud/v1/suborder/${subOrder.uuid}/container/${resourceUuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getResourcesList ({ page, size, query, superclass, orderUuid }) {
    /**
     * arclass
     * DRIVER - Выдача списка водителей по перевозчику (по страницам)
     * TRACTOR - Выдача списка тягачей по перевозчику (по страницам)
     * WAGON - Выдача списка фур по перевозчику (по страницам)
    */

    try {
      let res = await Client.get(`/oboz2-suborder-participant-list-viewer/v1/suborder/${orderUuid}/resources`,{
        params: { page, size, q: query, superclass }
      })
      let content = res.data.content.resources.map((item) => {
        let number = { number }
        if (!['driver'].includes(superclass)) {
          number = parseNumber(item.number, superclass)
        }
        return {
          ...item,
          ...number,
          isDisabled: !item.isEnabled,
          icon: getRiskClass(item.risk.riskGroupColor)
        }
      })
      return {
        content,
        ...res.data.pagination
      }
    } catch (e) {
      throw e
    }
  },
  async selectResource ({ uuid, data }) {
    // example object
    //     // {
    //     //   "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     //   "superclass": "DRIVER"
    //     // }
    try {
      let res = await Client.post(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/resources`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async selectResourcesList ({ uuid, data }) {
    // example array
    // [
    //   {ы
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "superclass": "DRIVER"
    //   }
    // ]

    try {
      let res = await Client.post(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/resources_by_list`, {
        resources: data
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async requestExecution ({ uuid, data }) {
    try {
      const { signerInitiator, signingReason, signerUserUuid } = data
      let res = await Client.post(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/statement/${data.statement_uuid}`, {
        signerInitiator,
        signingReason,
        signerUserUuid
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async requestExecutionNotAuto ({ subOrder, data }) {
    try {
      const { signingReason } = data
      let res = await Client.post(`/oboz2-suborder-orders-crud/v1/suborder/${subOrder.uuid}/statement`, {
        signingReason
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getParticipantsConfirmedBooking ({ uuid, limit }) {
    try {
      const res = await Client.get(`/oboz2-booking-lots-reports/v1/parent_entities/${uuid}/participants/accepted`, {
        params: { limit }
      })
      return res.data.carriers.map((item) => {
        return {
          ...item,
          tripTariff: item.tariff,
          icon: getRiskClass(item.riskColor)
        }
      })
    } catch (e) {
      throw e
    }
  },
  async getParticipantsAuctionWinner ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-auction-lots-reports/v1/parent_entities/${uuid}/winner`)
      const item = res.data
      return {
        ...item,
        tripTariff: item.tariff,
        icon: getRiskClass(item.riskColor)
      }
    } catch (e) {
      throw e
    }
  },
  async changePvpDate ({ uuid, fileUuid, pvp }) {
    try {
      const res = await Client.put(`/oboz2-order-po-crud/v1/orders/${uuid}/pvp`, { pvp, fileUuid })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async changeTariffLimit ({ uuid, fileUuid, newTariffLimit }) {
    try {
      const res = await Client.post(`/oboz2-suborder-carrier-searchers-crud/v1/suborders/${uuid}/change_tariff_limit`, { fileUuid, newTariffLimit })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async confirmOrder ({ uuid, data }) {
    const { fileUuid, acceptedCount, receivingDate } = data
    try {
      const res = await Client.put(`/oboz2-order-po-crud/v1/orders/${uuid}/execution/confirmed`, {
        fileUuid,
        receivingDate,
        acceptedCount
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async confirmSubOrder ({ subOrder, data }) {
    const { fileUuid, acceptedCount, receivingDate } = data
    try {
      const res = await Client.put(`/oboz2-suborder-orders-crud/v1/suborders/${subOrder.uuid}/execution/confirmed`, {
        fileUuid,
        receivingDate,
        acceptedCount
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async cancelOrder ({ uuid, fileUuid, comment, cancelInitiator }) {
    try {
      const res = await Client.put(`/oboz2-order-po-crud/v1/orders/${uuid}/cancel`, { cancelInitiator, comment, fileUuid })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async cancelExecutor ({ uuid, fileUuid }) {
    try {
      const res = await Client.put(`/oboz2-suborder-orders-crud/v1/orders/${uuid}/cancel/carrier`, { fileUuid })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async cancelResources ({ uuid, fileUuid, resources }) {
    try {
      const res = await Client.delete(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/resources`, { data: { fileUuid, resources } })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getBillingDocs ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-billing-invoices-crud/v1/billing/${uuid}`)
      return res.data || {}
    } catch (e) {
      throw e
    }
  },
  async getPaticipantInfo ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-security-participants-crud/v1/participants/info/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createShipment ({ uuid, number }) {
    try {
      const res = await Client.post(`/oboz2-order-po-crud/v1/orders/${uuid}/shipment_numbers`, {
        number
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteShipment ({ shipmentUuid , uuid }) {
    try {
      const res = await Client.delete(`/oboz2-order-po-crud/v1/orders/${uuid}/shipment_numbers`, {
        data: {
          shipmentUuid
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getShipmentsList ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}/shipment_numbers`)
      return res.data.shipmentNumbers || []
    } catch (e) {
      throw e
    }
  },
  async createSuborderShipment ({ number, uuid }) {
    try {
      const res = await Client.post(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/shipment_numbers`, {
        number
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async deleteSuborderShipment ({ shipmentUuid, uuid }) {
    try {
      const res = await Client.delete(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/shipment_numbers`, {
        data: {
          shipmentUuid
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getSuborderShipmentsList ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/shipment_numbers/list`)
      return res.data.shipmentNumbers || []
    } catch (e) {
      throw e
    }
  },
  async getOrderRequestTemplate ({ uuid, isDownload = true }) {
    try {
      const res = await Client.get(`/oboz2-suborder-order-print-form-viewer/v1/pdf/${uuid}`, { responseType: 'blob' })
      const file = { data: res.data, name: 'Шаблон заявки заказа.pdf'}

      if (isDownload) {
        createDownloadLink(file)
        return true
      } else {
        return file
      }
    } catch (e) {
      throw e
    }
  },
  async getLotHistory ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-auction-lots-reports/v1/parent_entities/${uuid}/history`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getAdditionalServicesByOrder ({ uuid }) {
    try {
      const res = await Client.post(`/oboz2-add-service-services-crud/v1/additional_services/order/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getAdditionalServices () {
    try {
      const res = await Client.get(`/oboz2-add-service-services-crud/v1/list_of_additional_services`)
      return res.data.map((item) => {
        return {
          ...item,
          fullTitle: item.title + '. ' + item.serviceUnitTitle,
          isVatTitle: item.isVatCharged ? 'НДС' : ''
        }
      })
    } catch (e) {
      throw e
    }
  },
  async getAdditionalServicesInfo ({ uuid, isSuborder = false }) {
    try {
      const url = isSuborder ? `/oboz2-suborder-orders-crud/v1/data_for_services/${uuid}` : `/oboz2-order-po-crud/v1/data_for_services/${uuid}`
      const res = await Client.get(url)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getAdditionalServiceTariff ({ clientUuid, contractorUuid, serviceUuid, geography, locationFromUuid = null, locationToUuid = null, locationUuid = null, pvpDate, visitPointUuid }) {
    try {
      const res = await Client.post('/oboz2-add-service-services-crud/v1/additional_service/tariff', {
        clientUuid,
        contractorUuid,
        serviceUuid,
        geography,
        locationFromUuid,
        locationToUuid,
        locationUuid,
        visitPointUuid,
        pvpDate
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async addAdditionalService ({ data }) {
    try {
      const res = await Client.put(`/oboz2-add-service-services-crud/v1/additional_service/${uuid.v4()}`, data);
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getProcessOrderInfo ({ uuid, isSuborder = false }) {
    try {
      const url = isSuborder ? `/oboz2-suborder-orders-crud/v1/suborders/${uuid}/process_info` : `/oboz2-order-po-crud/v1/orders/${uuid}/process_info`
      const res = await Client.get(url)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async cancelAdditionalService ({ uuid, status }) {
    try {
      const res = await Client.post(`/oboz2-add-service-services-crud/v1/additional_service/${uuid}/status/${status}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async reExposeAdditionalService ({ parentServiceUuid, orderUuid, pricePerService, vat, isVatIncluded, vatType }) {
    try {
      const res = await Client.put(`/oboz2-add-service-services-crud/v1/additional_service/${uuid.v4()}/cross_charge`, {
        parentServiceUuid,
        orderUuid,
        pricePerService,
        vat,
        isVatIncluded,
        vatType
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async addNonFinancialAdjustment ({ data }) {
    // {
    //   "orderUuid": "46e32f02-c2c9-4a85-8391-f40bd7ba9974",
    //   "orderKind": "ORDER",
    //   "typeUuid": "24f006d2-7bbd-408c-a729-47646cd12c65",
    //   "code": "COMPANY",
    //   "oldValue": {
    //   "uuid": "469e6f7e-b186-4251-9d70-490a35af48ca",
    //     "value": "ООО Нестле Россия"
    // },
    //   "newValue": {
    //   "uuid": "519b6516-32ed-4144-b915-c266f4072bf2",
    //     "value": "ООО ЛЕРУА МЕРЛЕН ВОСТОК"
    // },
    //   "comments": "Просто потому что клиент был другой. Пруф в прикрепленном файле",
    //   "fileUuids": [
    //   "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // ]
    // }
    // Пример тела запроса для изменения маршрута
    // {
    //   "orderUuid": "46e32f02-c2c9-4a85-8391-f40bd7ba9974",
    //   "orderKind": "ORDER",
    //   "typeUuid": "24f006d2-7bbd-408c-a729-47646cd12c65",
    //   "code": "ROUTE",
    //   "oldValue": {
    //   "route": [
    //     {
    //       "ordinal": 0,
    //       "title": "ТЦ  METRO Cash and Carry",
    //       "addressTitle": "Шоссейная ул. д.2",
    //       "direction": "FROM",
    //       "slotStartAt": "2021-04-28T12:29:05.647Z",
    //       "slotEndAt": "2021-04-28T12:29:05.647Z"
    //     }
    //   ]
    // },
    //   "newValue": {
    //   "route": [
    //     {
    //       "ordinal": 0,
    //       "title": "ТЦ  METRO Cash and Carry - 19",
    //       "addressTitle": "Шоссейная ул. д.2Б",
    //       "direction": "FROM",
    //       "slotStartAt": "2021-04-28T12:29:05.647Z",
    //       "slotEndAt": "2021-04-28T12:29:05.647Z"
    //     }
    //   ]
    // },
    //   "comments": "Адрес ТЦ Метро указа неправильно",
    //   "fileUuids": [
    //   "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // ]
    // }

    try {
      const res = await Client.post(`/oboz2-add-service-services-crud/v1/non_financial_adjustment/${uuid.v4()}`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async orderNonFinancialAdjustmentsList ({ uuid }) {
    // orderKind
    // ORDER - первичный
    // SUBORDER - вторичный

    try {
      const res = await Client.post(`/oboz2-add-service-services-crud/v1/non_financial_adjustments/order/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async listOfNonFinancialAdjustments ({ orderKind }) {
    // orderKind
    // ORDER - первичный
    // SUBORDER - вторичный

    try {
      const res = await Client.get(`/oboz2-add-service-services-crud/v1/list_of_non_financial_adjustments/${orderKind}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getDataOfNonFinancialAdjustments ({ typeUuid, orderUuid, orderKind }) {
    // orderKind
    // ORDER - первичный
    // SUBORDER - вторичный

    try {
      const res = await Client.get(`/oboz2-add-service-services-crud/v1/non_financial_adjustments/data/${typeUuid}`, {
        params: {
          order_uuid: orderUuid,
          order_kind: orderKind
        }
      })

      if (res.data.currentValue.assignedResources) {
        res.data.currentValue.assignedResources = res.data.currentValue.assignedResources.map(item => {
          return {
            ...item,
            icon: getRiskClass(item.riskColor),
            ...parseNumber(item.number, item.superclass)
          }
        })
      }

      return res.data
    } catch (e) {
      throw e
    }
  },
  async contractsSignedList ({ page, size, query, sort, clientTitle, contractorTitle }) {
    try {
      const res = await Client.get('/oboz2-contract-contracts-viewer/v1/contracts/signed', {
        params: { page, size, q: query, sort, client_title: clientTitle, doer_title: contractorTitle }
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getExternalNumberList ({ page, size, query, sort, clientUuid, doerUuid }) {
    try {
      const res = await Client.get(`/oboz2-contract-contracts-viewer/v1/contracts/${clientUuid}/${doerUuid}/external_number_list`, {
        params: { page, size, q: query, sort }
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async participantsByRoles ({ page, size, query, sort, code }) {
    try {
      const res = await Client.get(`/oboz2-security-participants-crud/v1/participants_by_roles/role/${code}`, {
        params: { page, size, q: query, sort }
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async nonFinancialAdjustmentsDropdown ({ page, size, query, typeUuid, orderUuid, orderKind }) {
    try {
      const res = await Client.get(`/oboz2-add-service-services-crud/v1/non_financial_adjustments/dropdown/${typeUuid}`, {
        params: { page, size, q: query, order_uuid: orderUuid, order_kind: orderKind}
      });
      return res.data
    } catch (e) {
      throw e
    }
  },
  async nonFinancialAdjustmentsDropdown2 ({ code, orderKind, superclass, orderUuid }) {
    try {
      const res = await Client.post(`/oboz2-add-service-services-crud/v1/non_financial_adjustments/dropdown`, {
        code,
        orderKind,
        superclass,
        orderUuid
      })
      return res.data.map(item => {
        if (item.assignedResources) {
          return {
            ...item.assignedResources,
            ...parseNumber(item.assignedResources.number, item.assignedResources.superclass)
          }
        } else {
          return item
        }
      })
    } catch (e) {
      throw e
    }
  },
  async resourcesByParticipant ({ page, size, query, sort, carrierUuid, code }) {
    const urlMap = {
      DRIVER: 'drivers',
      TRACTOR: 'tractors',
      SEMITRAILER: 'semitrailers',
      TRAILER: 'trailers'
    }
    try {
      let res = await Client.get(`/oboz2-security-resources-viewer/v1/${urlMap[code]}`, {
        params: { page, size, q: query, sort, carrier_uuid: carrierUuid, filter: 'active' }
      })

      res.data.content = res.data.content[urlMap[code]]

      return res.data
    } catch (e) {
      throw e
    }
  },
  async getRequiredResources ({ uuid }) {
    try {
      let res = await Client.get(`oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/required_resources`)
      return res.data.serviceTypes.map(item => {
        let requiredResources = item.requiredResources.map(item => {
          return {
            ...item,
            superclass: item.resourceSpeciesCode
          }
        })

        return {
          ...item,
          requiredResources
        }
      })
    } catch (e) {
      throw e
    }
  },

  async responsibleWorkers ({ contractorUuid, clientUuid }) {
    try {
      const res = await Client.get(`/oboz2-user-access-users-viewer/v2/responsible_workers/contractor/${contractorUuid}/client/${clientUuid}`)
      return res.data.map((item) => {
        return {
          ...item,
          title: `${item.firstname} ${item.lastname}`
        }
      })
    } catch (e) {
      throw e
    }
  },
  async getPartricipantUsers ({ contractorUuid }) {
    try {
      const res = await Client.get(`/oboz2-user-access-users-crud/v1/internal/contractors/${contractorUuid}`)
      return res.data.list.map((item) => {
        return {
          ...item,
          title: `${item.firstname} ${item.lastname}`
        }
      })
    } catch (e) {
      throw e
    }
  },
  async changeSlot ({ uuid, point_uuid, slotStart, slotEnd, fileUuid }) {
    try {
      const res = await Client.put(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/points/${point_uuid}/timeslot`, {
        slotStart,
        slotEnd,
        fileUuid
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getAdvanceInfo ({ uuid }) {
    const status = 'EXECUTABLE';
    try {
      const res = await Client.get(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/advances`, {
        params: {
          status_filter: status
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getAdvancePrices ({ client_uuid, price }) {
    try {
      const res = await Client.get(`/oboz2-billing-advances-crud/v1/advances/${client_uuid}/advance_max/${price}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async newAdvance ({ uuid, amount }) {
    const status = 'EXECUTABLE'
    try {
      const res = await Client.post(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/new_advance`, { amount }, {
        params: {
          status_filter: status
        }
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getSecondaryListByClicar({page, size, query, sort, url}, params) {
    return await Client.get(`/oboz2-suborder-orders-crud/v1/suborders/list_for_clicar?group=${url}`, {
      params: {
        page,
        size,
        q: query,
        sort,
        transportationWay: params.transport ? params.transport : 'FTL',
        startAddress: params.from,
        endAddress: params.to,
        pvpDate: params.date,
        serviceUuid: params.services,
        resourceTypeUuid: params.resource
      }
    });
  },
  async getPodList ({ uuid }) {
    // {
    //   "main": {
    //   "sendPlanD": "2020-05-19",
    //     "sentD": "2020-05-19",
    //     "sentCount": 0,
    //     "podComment": " Все документы приняты. Дополнительных документов не требуется",
    //     "podStatus": "NEW"
    // },
    //   "documents": [
    //   {
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "typeUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "typeTitle": "Товарная накладная",
    //     "typeDescription": "Товарная накладная с отметками о получении товара получателем",
    //     "count": 0,
    //     "uploadedAt": "2020-05-19T21:00:00.000Z",
    //     "validatedAt": "2020-05-19T21:00:00.000Z",
    //     "scans": [
    //       {
    //         "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //         "files": [
    //           {
    //             "fileUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //             "fileName": "nakladnaya_18_09_2020.xls"
    //           }
    //         ],
    //         "comment": "string",
    //         "scanStatus": "NEW"
    //       }
    //     ],
    //     "validatorTitle": "Глебов П.А.",
    //     "documentStatus": "NEW"
    //   }
    // ]
    // }
    //
    try {
      const res = await Client.get(`/oboz2-docflow-pods-crud/v1/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async savePodScans ({ uuid, fileUuids }) {
    // {
    //   "fileUuids": [
    //   "15d074a7-b853-4084-b062-31ef331cfe66",
    //   "15d074a7-b553-4084-b062-31ef331cfe66"
    // ]
    // }
    try {
      const res = await Client.post(`/oboz2-docflow-pods-crud/v1/scan/${uuid}`, { fileUuids })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async changePodStatus ({ uuid, comment, scanStatus }) {
    // {
    //   "comment": "На ТТН отсутсвует подпись водителя. Позиция 2 не читаема.",
    //   "scanStatus": "INVALID"
    // }
    try {
      const res = await Client.post(`/oboz2-docflow-pods-crud/v1/verification/${uuid}`, { comment, scanStatus })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async confirmationDispatchPod ({ uuid, sentD, fileUuid, sentCount }) {
    // {
    //   "sentD": "2020-05-19",
    //   "fileUuid": "15d074a7-b853-4084-b062-31ef331cfe66",
    //   "sentCount": 7
    // }
    try {
      const res = await Client.post(`/oboz2-docflow-pods-crud/v1/dispatch/${uuid}`, { sentD, sentCount, fileUuid })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async setCommentPod ({ uuid, comment }) {
    // {
    //   "comment": "Комплект документов принят с замечаниями."
    // }
    try {
      const res = await Client.post(`/oboz2-docflow-pods-crud/v1/comment_pod/${uuid}`, {
        comment
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async redirectPodDocument ({ sourcePodUuid, recipientPodUuid, files }) {
    // {
    //   "sourcePodUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "recipientPodUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "files": [
    //    {
    //      "fileUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    //    }
    //   ]
    // }
    try {
      const res = await Client.post(`/oboz2-docflow-pods-crud/v1/redirect_pod_document/`, {
        sourcePodUuid,
        recipientPodUuid,
        files
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async autosearchRestart ({ uuid, fileUuid }) {
    try {
      const res = await Client.post(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/carrier/autosearch`, {
        fileUuid
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getFirstOrderXls ({ uuid, number, isDownload = true }) {
    try {
      const res = await Client.get(`/oboz2-order-reports/v1/orders/${uuid}/suborders_xls_download`, { responseType: 'blob' })
      const file = { data: res.data, name: `Заказ ${number}.xls`}

      if (isDownload) {
        createDownloadLink(file)
        return true
      } else {
        return file
      }
    } catch (e) {
      throw e
    }
  },
  async getOrderDraftCard ({ po_draft_uuid }) {
    try {
      const res = await Client.get(`/oboz2-order-po-draft-viewer/v2/${po_draft_uuid}/no_check`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getNewOrderNumber ({ entityType = 'Order' } = {}) {
    try {
      const data = {
        'bodyGenerator': {
          entityType
        }
      }
      const res = await Client.post('/oboz2-code-generator/v1/generate', data)
      return res.data.code
    } catch (e) {
      throw e
    }
  },
  async saveOrderDraft ({ uuid, data }) {
    try {
      const res = await Client.put(`/oboz2-order-po-draft-saver/v2/drafts/${uuid}`, data)
      return res.data.code
    } catch (e) {
      throw e
    }
  },
  async saveRoutes ({ uuid, data }) {
    try {
      const res = await Client.put(`/oboz2-order-po-draft-saver/v1/${uuid}/route`, data)
      return res.data.code
    } catch (e) {
      throw e
    }
  },
  async getOrderLogisticians ({ contractorUuid, clientUuid }) {
    // [
    //   {
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "firstname": "Кирилл",
    //     "lastname": "Капустин",
    //     "phone": "+79991112233"
    //   },
    //   {
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "firstname": "Кирилл",
    //     "lastname": "Капустин",
    //     "phone": "+79991112233"
    //   }
    // ]
    try {
      const res = await Client.get(`/oboz2-order-responsible-worker-selector/v1/list_responsible_workers/contractor/${contractorUuid}/client/${clientUuid}`)
      return res.data.map((item) => {
        const { firstname, lastname } = item

        return {
          ...item,
          title: `${lastname} ${firstname}`
        }
      })
    } catch (e) {
      throw e
    }
  },
  async getSuborderLogisticians ({ uuid }) {
    // [
    //   {
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "firstname": "Кирилл",
    //     "lastname": "Капустин",
    //     "phone": "+79991112233"
    //   },
    //   {
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "firstname": "Кирилл",
    //     "lastname": "Капустин",
    //     "phone": "+79991112233"
    //   }
    // ]
    try {
      const res = await Client.get(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/list_responsible_workers`)
      return res.data.map((item) => {
        const { firstname, lastname } = item

        return {
          ...item,
          title: `${lastname} ${firstname}`
        }
      })
    } catch (e) {
      throw e
    }
  },
  async setOrderLogist ({ uuid, data }) {
    const { logistUuid, comment, fileUuids } = data
    // {
    //   "logistUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "comment": "string",
    //   "fileUuids": [
    //   "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // ]
    // }
    try {
      const res = await Client.put(`/oboz2-order-po-crud/v1/orders/${uuid}/clicar_logist`, {
        logistUuid,
        comment,
        fileUuids
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async setSuborderLogist ({ uuid, data }) {
    const { logistUuid, comment, fileUuids } = data
    // {
    //   "logistUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "comment": "Сотрудник в отпуске",
    //   "fileUuids": [
    //   "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // ]
    // }
    try {
      const res = await Client.put(`/oboz2-suborder-orders-crud/v1/suborders/${uuid}/clicar_logist`, {
        logistUuid,
        comment,
        fileUuids
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createEvent ({ data }) {
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/event`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getEventInfo ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-business-event-events-crud/v1/event/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getDictionaryEventInfo ({ entityType, eventTypeCode }) {
    try {
      let res = await Client.get(`/oboz2-dictionary-event-types-crud/v1/entityType/${entityType}/eventTypeCode/${eventTypeCode}`)
      res.data.reasons = res.data.reasons || []
      return res.data
    } catch (e) {
      throw e
    }
  },
  async cancelCheck ({ uuid }) {
    try {
      const res = await Client.get(`/oboz2-order-po-crud/v1/orders/${uuid}/cancel_check`)
      return res.data.isCancelable
    } catch (e) {
      throw e
    }
  },
  async updateEvent ({ uuid, data }) {
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/event/${uuid}`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async changeExternalNumber ({ data }) {
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/order_external_number_change`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async cancelOrderEvent ({ data }) {
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/order_cancellation_event`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async changeConsignorOrConsigneeEvent ({ data }) {
    // {
    //   "comment": "Исправили опечатку в наименовании ГО",
    //   "fileUuids": [
    //   "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // ],
    //   "suborderUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "visitPointUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "consigneeOrConsignor": "CONSIGNEE",
    //   "consigneeOrConsignorUuidOld": "21682980-a6bd-4669-86bb-a600f36d8f56",
    //   "consigneeOrConsignorUuidNew": "38382c8e-dc87-4af4-a613-44a2c8ae24bc",
    //   "tsdAddress": "2-й Сетуньский проезд, 25с3, Москва, Россия"
    // }
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/change-consignor-or-consignee-event`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async forwardingEvent ({ data }) {
    // {
    //   "comment": "Водитель уточнил время прибытия по телефону",
    //   "fileUuids": [
    //     "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    //   ],
    //   "visitPointUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "newVisitPointData": {
    //     "addressTitle": "string",
    //     "direction": "FROM",
    //     "coordinates": {
    //       "lat": 0,
    //         "lon": 0
    //     },
    //     "consigneeOrConsignor": "CONSIGNEE",
    //     "consigneeOrConsignorUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    //   },
    //   "newTimeSlot": {
    //     "slotStartAt": "2021-03-03T22:35:11.934Z",
    //     "slotEndAt": "2021-03-03T22:35:11.934Z"
    //   },
    //   "newClientTariff": {
    //   "priceWithoutVat": 9251,
    //     "vatPrice": 11101,
    //     "vatRate": 20,
    //     "priceWithVat": 11101
    //   },
    //   "newCarrierTariff": {
    //   "priceWithoutVat": 9251,
    //     "vatPrice": 11101,
    //     "vatRate": 20,
    //     "priceWithVat": 11101
    //   },
    //   "orderUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // }
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/order_point_forward_event`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async addPointEvent ({ data }) {
    // {
    //   "orderUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "comment": "Водитель уточнил время прибытия по телефону",
    //   "fileUuids": [
    //   "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // ],
    //   "visitPointUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "newVisitPointData": {
    //   "addressTitle": "Москва, Вернадского 6",
    //     "direction": "FROM",
    //     "coordinates": {
    //     "lat": 0,
    //       "lon": 0
    //   },
    //   "consigneeOrConsignor": "CONSIGNEE",
    //     "consigneeOrConsignorUuid": "21682980-a6bd-4669-86bb-a600f36d8f56"
    // },
    //   "newTimeSlot": {
    //   "slot_start_at": "2021-02-25T08:48:23.084Z",
    //     "slot_end_at": "2021-02-25T08:48:23.084Z"
    // },
    //   "newClientTariff": {
    //   "priceWithoutVat": 9251,
    //     "vatPrice": 11101,
    //     "vatRate": 20,
    //     "priceWithVat": 11101
    // },
    //   "newCarrierTariff": {
    //   "priceWithoutVat": 9251,
    //     "vatPrice": 11101,
    //     "vatRate": 20,
    //     "priceWithVat": 11101
    // }
    // }
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/order_add_point_event`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async removePointEvent ({ data }) {
    // {
    //   "comment": "Водитель уточнил время прибытия по телефону",
    //   "fileUuids": [
    //   "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // ],
    //   "visitPointUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //   "newClientTariff": {
    //   "priceWithoutVat": 9251,
    //     "vatPrice": 11101,
    //     "vatRate": 20,
    //     "priceWithVat": 11101
    // },
    //   "oldCarrierTariff": {
    //   "priceWithoutVat": 9251,
    //     "vatPrice": 11101,
    //     "vatRate": 20,
    //     "priceWithVat": 11101
    // },
    //   "newCarrierTariff": {
    //   "priceWithoutVat": 9251,
    //     "vatPrice": 11101,
    //     "vatRate": 20,
    //     "priceWithVat": 11101
    // },
    //   "orderUuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    // }
    try {
      const res = await Client.post(`/oboz2-business-event-events-crud/v1/order_remove_point_event`, data)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getConsignorsConsigneesInfo ({ uuid, ownerUuid }) {
    try {
      const res = await Client.get(`/oboz2-dictionary-consignors-and-consignees-crud/v1/contractor/owner/${ownerUuid}/contractor/${uuid}`)
      return res.data
    } catch (e) {
      throw e
    }
  },
  async getConsignorsConsigneesList ({ ownerUuid, type, coordinates, limit = 5 } = {}) {
    try {
      const res = await Client.post(`/oboz2-order-infographs-crud/v1/fetch_contractors/limit/${limit}`, {
        ownerUuid,
        coordinates,
        type
      })
      return res.data.list
    } catch (e) {
      throw e
    }
  },
  async getConsignorsConsigneesListExcludedUuids ({ ownerUuid, type, list = [] } = {}) {
    try {
      const res = await Client.post(`/oboz2-dictionary-consignors-and-consignees-crud/v1/fetch_list_exclude_uuid/type/${type}/owner/${ownerUuid}`, {
        list
      })
      return res.data.list
    } catch (e) {
      throw e
    }
  },
  async createConsignorsConsignees ({ contractorUuid, ownerUuid, contractor, isConsignee, isConsignor, inn } = {}) {
    try {
      const res = await Client.post(`/oboz2-dictionary-consignors-and-consignees-crud/v1/${contractorUuid}`, {
        ownerUuid,
        contractor,
        isConsignee,
        isConsignor,
        inn
      })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async createCorrectionDocument ({ orderUuid } = {}) {
    try {
      return await Client.post(`/oboz2-add-service-services-crud/v1/correction_document/order/${orderUuid}`)
    } catch (e) {
      throw e
    }
  },
  async vatCalculation ({ list } = []) {
    try {
      let emptyIndexes = []

      list.forEach((item, index) => {
        const value = item.priceWithVat !== undefined ? item.priceWithVat : item.priceWithoutVat

        if (value === undefined || value === '') {
          emptyIndexes.push(index)
        }
      })
      let res = await Client.post(`/oboz2-common-vat-calculation/v1/vat_calculation`, {
        itemsList: list
      })

      let computedItemsList = cloneDeep(res.data.itemsList)

      emptyIndexes.forEach(index => {
        computedItemsList.splice(index, 0, {
          priceWithVat: '',
          priceWithoutVat: '',
          vatPrice: ''
        })
      })

      res.data.itemsList = computedItemsList

      return res.data
    } catch (e) {
      throw e
    }
  },
  async autosearchCancel ({ uuid, data }) {
    try {
      const res = await Client.post(`/oboz2-suborder-carrier-searchers-crud/v1/suborder/${uuid}/carrier/autosearch_cancel`, data)

      return res.data
    } catch (e) {
      throw e
    }
  },
  async sendOrderInfo ({ data }) {
    try {
      const { status } = await Client.post(`/oboz2-business-event-events-crud/v1/sending_data_event`, data)

      return status === 201
    } catch (e) {
      throw e
    }
  },
  async getAttorneyLettersInfo ({ orderUuid, suborderUuid }) {
    // {
    //   "consignor": {
    //   "uuid": "f137cc91-316f-47f7-9e 22-85fef29d5ea3",
    //     "title": "ООО Ромашка"
    // },
    //   "driver": {
    //   "uuid": "f137cc91-316f-47f7-9e22-85fef29d5ea3",
    //     "fio": "Васильева Дарья",
    //     "phone": "79778015997"
    // },
    //   "materialValues": "Бутилированная вода",
    //   "unitTitle": "Бутылок",
    //   "unitValue": 10,
    //   "comment": "Комментарий",
    //   "files": [
    //   "f137cc91-316f-47f7-9e22-85fef29d5ea3"
    // ]
    // }
    try {
      const { data } = await Client.post(`/oboz2-suborder-attorney-letters-crud/v1/attorney_letters/actual`, {
        orderUuid,
        suborderUuid
      })

      return data
    } catch (e) {
      throw e
    }
  },
  async saveAttorneyLetters ({ data }) {
    // {
    //   "orderUuid": "f137cc91-316f-47f7-9e22-85fef29d5ea3",
    //   "suborderUuid": "f137cc91-316f-47f7-9e22-85fef29d5ea3",
    //   "consignor": {
    //   "uuid": "f137cc91-316f-47f7-9e22-85fef29d5ea3",
    //     "title": "ООО Ромашка"
    // },
    //   "driver": {
    //   "uuid": "f137cc91-316f-47f7-9e22-85fef29d5ea3",
    //     "fio": "Васильева Дарья",
    //     "phone": "79778015997"
    // },
    //   "materialValues": "Бутилированная вода",
    //   "unitTitle": "Бутылок",
    //   "unitValue": 10,
    //   "comment": "Комментарий",
    //   "files": [
    //   "f137cc91-316f-47f7-9e22-85fef29d5ea3"
    // ]
    // }
    try {
      const res = await Client.post(`/oboz2-suborder-attorney-letters-crud/v1/attorney_letters/${uuid.v4()}`, data)

      return res.data
    } catch (e) {
      throw e
    }
  },
  async getApproachingSuborders ({ suborderUuid, exactRoute, page = 0, size = 50 } = {}) {
    // {
    //   "page": 10,
    //   "size": 30,
    //   "numberOfElements": 17,
    //   "totalPages": 10,
    //   "totalElements": 287,
    //   "variantsList": [
    //   {
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "accuracy": 0,
    //     "tractorNumber": "а111аа 199",
    //     "trailerNumber": "АА9999 47",
    //     "carrierTitle": "ООО ОБОЗ ДИДЖИТАЛ",
    //     "phone": "+7 (906) 753-11-05",
    //     "driverFio": "Петр Довгаль",
    //     "departureDt": "2021-09-17T08:08:40.292Z",
    //     "timelag": 0,
    //     "distance": 0
    //   }
    // ]
    // }
    try {
      let res = await Client.get(`/oboz2-suborder-carrier-assortmenter/v1/approaching_suborders/${suborderUuid}`, {
        headers: {
          exactRoute,
          pagination: JSON.stringify( {
            page,
            size
          })
        }
      })

      res.data.content = res.data.approachingVariantsList.map(item => {
        return {
          ...item,
          tractorGosNumber: parseNumber(item.tractorNumber),
          trailerGosNumber: parseNumber(item.trailerNumber),
        }
      })

      return res.data
    } catch (e) {
      throw e
    }
  },
  async getLoopingSuborders ({ suborderUuid, exactRoute, page = 0, size = 50 } = {}) {
    // {
    //   "page": 10,
    //   "size": 30,
    //   "numberOfElements": 17,
    //   "totalPages": 10,
    //   "totalElements": 287,
    //   "variantsList": [
    //   {
    //     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    //     "accuracy": 0,
    //     "tractorNumber": "а111аа 199",
    //     "trailerNumber": "АА9999 47",
    //     "carrierTitle": "ООО ОБОЗ ДИДЖИТАЛ",
    //     "phone": "+7 (906) 753-11-05",
    //     "driverFio": "Петр Довгаль",
    //     "departureDt": "2021-09-17T08:08:40.292Z",
    //     "timelag": 0,
    //     "distance": 0
    //   }
    // ]
    // }
    try {
      let res = await Client.get(`/oboz2-suborder-carrier-assortmenter/v1/looping_suborders/${suborderUuid}`, {
        headers: {
          exactRoute,
          pagination: JSON.stringify( {
            page,
            size
          })
        }
      })

      res.data.content = res.data.loopingSuborderList

      return res.data
    } catch (e) {
      throw e
    }
  },
  async getInfoForOffer ({ toSuborderUuid = null, carrierUuid = null, targetSuborderUuid, fromSuborderUuid = null } = {}) {
    try {
      let res = await Client.get(`/oboz2-suborder-carrier-assortmenter/v1/info_for_offer`, {
        headers: {
          offer: JSON.stringify( {
            toSuborderUuid,
            carrierUuid,
            targetSuborderUuid,
            fromSuborderUuid
          })
        }
      })

      if (res.data.resources) {
        res.data.resources = res.data.resources.map(item => {
          return {
            ...item,
            icon: getRiskClass(item.riskColor),
            ...parseNumber(item.number, item.superclass)
          }
        })
      }

      return res.data
    } catch (e) {
      throw e
    }
  },
  async createOffer ({ data }) {
    try {
      let res = await Client.post(`/oboz2-suborder-carrier-assortmenter/v1/create_offer`, data)

      return res.data
    } catch (e) {
      throw e
    }
  },
}
