import RestClient from './RestClient'

let Client = new RestClient()

export default {
  saveOrder (uuid, data) {
    return Client.put('/oboz2-order-po-draft-saver/v2/drafts/' + uuid, data);
  },
  getOrder (uuid) {
    return Client.get('/oboz2-order-po-viewer/v1/' + uuid)
  },
  getOrderId () {
    let data = {
      "bodyGenerator": {
        "entityType": "Order"
      }
    }
    return Client.post('/oboz2-code-generator/v1/generate', data)
  },
  getOrderTariff (uuid) {
    return Client.post(`/v1/fetch_tariffs/${uuid}`)
  }
}
