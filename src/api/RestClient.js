import axios from 'axios'
import {getCookie} from '@/utils/cookie'
import store from "@/store"
import VueCookie from 'vue-cookie'

let numOpenConnections = 0

function openConnections(col) {
    numOpenConnections = numOpenConnections + col != -1 ? numOpenConnections + col : 0
    sessionStorage.numOpenConnections = numOpenConnections
    sessionStorage.initRest = true
}

function apiStatus(response = {}, {isErrorThrow = false, isWithoutToken = false} = {}) {
    if (!response || typeof response === 'string') {
        throw new Error(response)
    }

    const {data, status, statusText, headers} = response

    const statusError = !status || ((status < 200 || status > 300))
    const serviceError = (data && (data.error || data.details)) || (statusError && data && data.code)
    const serviceErrorText = serviceError || (data && data.message) || 'Unknown error!'
    const errorValue = serviceErrorText || statusText || 'error'
    const initialError = typeof data === 'object' ? {...data} : {}
    setTimeout(() => {
        openConnections(-1)
    }, 1000);
    if (status === 401 && !isWithoutToken) {
        VueCookie.delete('auth_token')
        localStorage.setItem('clear_auth_token', Math.random());
        window.location = '/'
    } else if (statusError || serviceError) {
        if (isErrorThrow) {
            throw {...initialError, error: errorValue, status}
        } else {
            return {...initialError, error: errorValue, status}
        }
    } else {
        return {data, headers, status, statusText}
    }
}

export default class RestClient {
    constructor(props = {}) {
        this.instance = axios.create({
            baseURL: props.baseURL || window.OBOZ_CONFIG.OBOZ_API_HOST
        })

        this.isErrorThrow = Boolean(props.isErrorThrow)
        let statusParams = {
            isErrorThrow: this.isErrorThrow,
            isWithoutToken: false
        }

        this.instance.interceptors.request.use((config = {}) => {
            const withoutToken = Boolean(config.params && config.params.withoutToken)
            statusParams.isWithoutToken = withoutToken
            if (config.params) {
                for (let key in config.params) {
                    if (config.params[key] === null || config.params[key] === undefined || config.params[key] === '' || key === 'withoutToken') {
                        delete config.params[key]
                    }
                }
            }
            const token = getCookie('auth_token')
            if (!token && !withoutToken) {
                window.location.href = '/'
            } else {
                if (!config.headers.common) {
                    config.headers.common = {}
                }

                config.headers.common = {
                    'Accept-Version': 1,
                    Accept: 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    ...config.headers.common
                }

                if (token && !withoutToken) {
                    config.headers.common.Authorization = `Bearer ${token}`
                }

                try {
                    config.headers.common['Accept-Language'] = props.lang || store.getters.getCurrentLocale
                } catch (e) {
                    console.log(e)
                }
            }
            return config
        }, (error) => {
            return Promise.reject(error)
        })
        this.instance.interceptors.response.use((response) => apiStatus(response, statusParams), ({response} = {}) => apiStatus(response, statusParams))
    }

    get(url, options = {}) {
        openConnections(1)
        return this.instance.get(url, {...options})
    }

    post(url, data = {}, options = {}) {
        openConnections(1)
        return this.instance.post(url, data, {...options})
    }

    put(url, data = {}, options = {}) {
        openConnections(1)
        return this.instance.put(url, data, {...options})
    }

    patch(url, data = {}, options = {}) {
        openConnections(1)
        return this.instance.patch(url, data, {...options})
    }

    delete(url, options = {}) {
        openConnections(1)
        return this.instance.delete(url, {...options})
    }
}
