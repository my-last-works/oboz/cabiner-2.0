import { getCookie } from './utils/cookie'
import jwtDecode from 'jwt-decode'

let cookie = () => {
    return getCookie('auth_token')
}

export const tokenData = cookie() ? jwtDecode(cookie()) : {}

export const getTokenData = () => {
    return cookie() ? jwtDecode(cookie()) : {}
}
