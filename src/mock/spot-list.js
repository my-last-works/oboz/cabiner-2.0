export const spotDraftList = [
  {
    "uuid": "8b8f7dd7-917b-4012-91c4-0b03f1d4bda4",
    "number": "PO19 320 3572",
    "createdAt": "2020-08-14T11:31:42.817Z",
    "fromLocation": "Москва",
    "toLocation": "Москва",
    "additionalPointsCount": 3,
    "fromDt": "2020-08-14T15:10:50.839Z",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionType": "DOWNGRADE",
    "currentPrice": 2500000,
    "orderStatus": "DRAFT"
  },
  {
    "uuid": "8b8f7dd7-917b-4012-91c4-0b03f1d4bda4",
    "number": "AO 20 125 77 18",
    "draftLifetime": 2076,
    "createdAt": "2020-08-14T10:14:15.666234Z",
    "fromLocation": "Тверь",
    "toLocation": "Киров",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionType": "FIRST_APPROVE",
    "currentPrice": "1582000",
    "leadPartner": "ООО «Омега»",
    "orderStatus": "DRAFT"
  },
  {
    "uuid": "8b8f7dd7-917b-4012-91c4-0b03f1d4bda4",
    "number": "AO 20 125 77 19",
    "draftLifetime": 860762,
    "createdAt": "2020-07-04T10:14:15.666234Z",
    "fromLocation": "Калининград",
    "toLocation": "Москва",
    "additionalPointsCount": 5,
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionType": "BLIND",
    "leadPartner": "ООО «Солнышко»",
    "currentPrice": "16823500",
    "orderStatus": "DRAFT"
  }
]

export const spotCompletedList = [{
  "uuid": "4a203e4c1-bb91-482f-8159-5dd30c0bd316",
  "number": "PO55 320 3572",
  "createdAt": "2020-08-14T11:31:42.817Z",
  "fromLocation": "Ржев",
  "toLocation": "Москва",
  "additionalPointsCount": 3,
  "fromDt": "2020-08-14T15:10:50.839Z",
  "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
  "auctionType": "DOWNGRADE",
  "currentPrice": 2500000,
  "orderStatus": "COMPLETED"
}, {
  "uuid": '5266c56ec-22bd-4795-bd8e-1353d6b0c988',
  "number": 'AO 66 125 77 18',
  "draftLifetime": 2076,
  "createdAt": '2020-08-14T10:14:15.666234Z',
  "fromLocation": 'Тверь',
  "toLocation": 'Киров',
  // "additionalPointsCount": 3,
  "resourceTypeTitle": 'Полуприцеп изотерм 20 т / 68 м3 / 33 пал',
  "auctionType": 'FIRST_APPROVE',
  "currentPrice": '1582000',
  "leadPartner": 'ООО «Омега»',
  "orderStatus": 'COMPLETED' // Подведение итогов
}, {
  "uuid": "695a2438a-041e-4062-94ad-c2b246e977fc",
  "number": "AO 77 125 77 19",
  "draftLifetime": 860762,
  "createdAt": "2020-07-04T10:14:15.666234Z",
  "fromLocation": "Калининград",
  "toLocation": "Москва",
  "additionalPointsCount": 5,
  "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
  "auctionType": 'BLIND',
  "leadPartner": 'ООО «Солнышко»',
  "currentPrice": '16823500',
  "orderStatus": 'CANCELLED'
}]

export const spotActualList = [
  {
    "uuid": "7fa85f64-5717-4562-b3fc-2c963f66afa5",
    "number": "PO19 320 3572",
    "createdAt": "2020-08-25T06:31:18.978Z",
    "fromLocation": "Москва",
    "toLocation": "Москва",
    "additionalPointsCount": 3,
    "fromDt": "2020-08-25T06:31:18.978Z",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "7fa85f64-5717-4562-b3fc-2c963f66afa6",
    "orderStatus": "AUCTION_ENDED"
  }, {
    "uuid": "1a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "number": "AO 20 320 3572",
    "createdAt": "2020-08-14T11:31:42.817Z",
    "fromLocation": "Москва",
    "toLocation": "Москва",
    "additionalPointsCount": 3,
    "fromDt": "2020-08-14T15:10:50.839Z",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "d2e88df7-4334-4aab-b2c8-951cc9c65323",
    "orderStatus": "DRAFT"
  }, {
    "uuid": '2266c56ec-22bd-4795-bd8e-1353d6b0c988',
    "number": 'AO 22 125 77 18',
    "draftLifetime": 2076,
    "createdAt": '2020-08-14T10:14:15.666234Z',
    "fromLocation": 'Тверь',
    "toLocation": 'Киров',
    // "additionalPointsCount": 3,
    "resourceTypeTitle": 'Полуприцеп изотерм 20 т / 68 м3 / 33 пал',
    "auctionUuid": "7fa85f64-5717-4512-b3fc-2c963f66afa6",
    "orderStatus": 'NEW' // Подведение итогов
  }, {
    "uuid": "395a2438a-041e-4062-94ad-c2b246e977fc",
    "number": "AO 33 125 77 19",
    "draftLifetime": 860762,
    "createdAt": "2020-07-04T10:14:15.666234Z",
    "fromLocation": "Калининград",
    "toLocation": "Москва",
    "additionalPointsCount": 5,
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'AUCTION'
  }, {
    "uuid": "14fa85f64-5717-4562-b3fc-2c963f66afa6",
    "number": "PO 11 320 3572",
    "createdAt": "2020-08-14T11:31:42.817Z",
    "fromLocation": "Москва",
    "toLocation": "Москва",
    "additionalPointsCount": 3,
    "fromDt": "2020-08-14T15:10:50.839Z",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": "AUCTION_ENDED" // Подведение итогов
  }, {
    "uuid": '25266c56ec-22bd-4795-bd8e-1353d6b0c988',
    "number": 'AO 22 125 77 18',
    "draftLifetime": 2076,
    "createdAt": '2020-08-14T10:14:15.666234Z',
    "fromLocation": 'Тверь',
    "toLocation": 'Киров',
    // "additionalPointsCount": 3,
    "resourceTypeTitle": 'Полуприцеп изотерм 20 т / 68 м3 / 33 пал',
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'PROCESSED' // Завершен
  }, {
    "uuid": "365a2438a-041e-4062-94ad-c2b246e977fc",
    "number": "AO 33 125 77 19",
    "draftLifetime": 860762,
    "createdAt": "2020-07-04T10:14:15.666234Z",
    "fromLocation": "Калининград",
    "toLocation": "Москва",
    "additionalPointsCount": 5,
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'CANCELLED' // Отменен
  }, {
    "uuid": "17fa85f64-5717-4562-b3fc-2c963f66afa6",
    "number": "PO 11 320 3572",
    "createdAt": "2020-08-14T11:31:42.817Z",
    "fromLocation": "Москва",
    "toLocation": "Москва",
    "additionalPointsCount": 3,
    "fromDt": "2020-08-14T15:10:50.839Z",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "d2e88df7-4334-4aab-b2c8-951cc9c65323",
    "orderStatus": 'CANCELLED' // Отменен
  }, {
    "uuid": '2866c56ec-22bd-4795-bd8e-1353d6b0c988',
    "number": 'AO 22 125 77 18',
    "draftLifetime": 2076,
    "createdAt": '2020-08-14T10:14:15.666234Z',
    "fromLocation": 'Тверь',
    "toLocation": 'Киров',
    // "additionalPointsCount": 3,
    "resourceTypeTitle": 'Полуприцеп изотерм 20 т / 68 м3 / 33 пал',
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'AUCTION'
  }, {
    "uuid": "399a2438a-041e-4062-94ad-c2b246e977fc",
    "number": "AO 33 125 77 19",
    "draftLifetime": 860762,
    "createdAt": "2020-07-04T10:14:15.666234Z",
    "fromLocation": "Калининград",
    "toLocation": "Москва",
    "additionalPointsCount": 5,
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'AUCTION_ENDED'
  }, {
    "uuid": "130a85f64-5717-4562-b3fc-2c963f66afa6",
    "number": "PO 11 320 3572",
    "createdAt": "2020-08-14T11:31:42.817Z",
    "fromLocation": "Москва",
    "toLocation": "Москва",
    "additionalPointsCount": 3,
    "fromDt": "2020-08-14T15:10:50.839Z",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": "AUCTION_ENDED"
  }, {
    "uuid": '2216c56ec-22bd-4795-bd8e-1353d6b0c988',
    "number": 'AO 22 125 77 18',
    "draftLifetime": 2076,
    "createdAt": '2020-08-14T10:14:15.666234Z',
    "fromLocation": 'Тверь',
    "toLocation": 'Киров',
    // "additionalPointsCount": 3,
    "resourceTypeTitle": 'Полуприцеп изотерм 20 т / 68 м3 / 33 пал',
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'AUCTION'
  }, {
    "uuid": "392a2438a-041e-4062-94ad-c2b246e977fc",
    "number": "AO 33 125 77 19",
    "draftLifetime": 860762,
    "createdAt": "2020-07-04T10:14:15.666234Z",
    "fromLocation": "Калининград",
    "toLocation": "Москва",
    "additionalPointsCount": 5,
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'AUCTION'
  }, {
    "uuid": "133a85f64-5717-4562-b3fc-2c963f66afa6",
    "number": "PO 11 320 3572",
    "createdAt": "2020-08-14T11:31:42.817Z",
    "fromLocation": "Москва",
    "toLocation": "Москва",
    "additionalPointsCount": 3,
    "fromDt": "2020-08-14T15:10:50.839Z",
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": "AUCTION"
  }, {
    "uuid": '2246c56ec-22bd-4795-bd8e-1353d6b0c988',
    "number": 'AO 22 125 77 18',
    "draftLifetime": 2076,
    "createdAt": '2020-08-14T10:14:15.666234Z',
    "fromLocation": 'Тверь',
    "toLocation": 'Киров',
    // "additionalPointsCount": 3,
    "resourceTypeTitle": 'Полуприцеп изотерм 20 т / 68 м3 / 33 пал',
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'AUCTION' // Опубликован
  }, {
    "uuid": "39552438a-041e-4062-94ad-c2b246e977fc",
    "number": "AO 33 125 77 19",
    "draftLifetime": 860762,
    "createdAt": "2020-07-04T10:14:15.666234Z",
    "fromLocation": "Калининград",
    "toLocation": "Москва",
    "additionalPointsCount": 5,
    "resourceTypeTitle": "Полуприцеп изотерм 20 т / 68 м3 / 33 пал",
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "orderStatus": 'NEW'
  }]

export const spotAuctionInfo = [
  {
    "auctionUuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
    "type": "DOWNGRADE",
    "currentPrice": 2500000,
    "actualTime": 600,
    "exclamationMark": true,
    "startPlanDt": "2020-09-19T21:00:00.000Z",
    "endPlanDt": "2020-09-19T21:00:00.000Z",
    "carrier": {
      "uuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
      "title": "ООО Рефлайнер",
      "phone": "+7 495 15 50 00",
      "email": "alog@refliner.com"
    },
    "auctionStatus": "DRAFT"
  },
  {
    "auctionUuid": "d2e88df7-4334-4aab-b2c8-951cc9c65323",
    "type": "FIRST_APPROVE",
    "currentPrice": 3500000,
    "actualTime": 600,
    "exclamationMark": false,
    "startPlanDt": "2020-10-19T21:00:00.000Z",
    "endPlanDt": "2020-10-19T21:00:00.000Z",
    "carrier": {
      "uuid": "a203e4c1-bb91-482f-8159-5dd30c0bd316",
      "title": "ООО Рефлайнер 2",
      "phone": "+7 495 15 50 11",
      "email": "alog@refliner2.com"
    },
    "auctionStatus": "CANCELLED"
  }
]
