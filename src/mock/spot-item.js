export const spotItemMock = {
  'main': {
    'number': 'AO20 555 1201',
    'routeDurationInHr': '33',
    'routeLengthInKm': '999',
    'contractor': {
      'uuid': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
      'roleCode': 'CARRIER',
      'title': 'ООО ОБОЗ ДИДЖИТАЛ'
    }
  },
  'points': [
    {
      'pointType': 'LOADING',
      'ordinal': 1,
      'pointUuid': '15d074a7-b853-4084-b062-31ef331cfe66',
      'addressCoordinates': {
        'lat': 53.7724771811557,
        'lon': 92.3435323860238
      },
      'addressTitle': 'Москва, 4-я линия Хорошёвского Серебряного Бора, 156с2',
      'arrivalDt': '2020-09-25T20:30:00.000Z',
      'locationUuid': '15d074a7-b853-4084-b062-31ef331cfe66',
      'locationTitle': 'Москва',
      'countryCode': 'string'
    },
    {
      'pointType': 'UNLOADING',
      'locationUuid': '',
      'ordinal': 1,
      'pointUuid': '1d0a01f6-85f5-4c0a-8ca9-2c1a1da1bff4',
      'addressCoordinates': { lat: 54.51160812, lon: 36.26047516 },
      'addressTitle': 'Московская улица, 7, Калуга, Россия',
      'arrivalDt': '2020-09-30T03:30:00.000Z'
    },
    {
      'pointType': 'UNLOADING',
      'locationUuid': '',
      'ordinal': 2,
      'pointUuid': '2d9555a4-f371-431e-b67c-31247cca69a5',
      'addressCoordinates': { lat: 54.16453552, lon: 37.62476349 },
      'addressTitle': 'Рязанская улица, Тула, Россия',
      'arrivalDt': '2020-10-01T06:30:00.000Z'
    },
    {
      'pointType': 'UNLOADING',
      'locationUuid': '',
      'ordinal': 3,
      'pointUuid': 'ba6a10fe-4295-44c7-8e45-f8832c2e7cb2',
      'addressCoordinates': { lat: 54.01116943, lon: 38.2901535 },
      'addressTitle': 'Комсомольская улица, Новомосковск, Тульская область, Россия',
      'arrivalDt': '2020-10-02T09:30:00.000Z'
    }
  ],
  'transportation': {
    'numberOfVehicle': 2,
    'needTracking': true,
    'dangerousClassUuid': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
    'resourceClassUuid': 'e7519b46-faf4-11e9-8f0b-362b9e155667',
    'resourceSpeciesUuid': '2f293f68-faf6-11e9-8f0b-362b9e155667',
    'resourceTypeUuid': 'a2e25172-c154-444a-902a-808519625114',
    'temperatureModeUuid': '9b3e1e32-d1bd-412a-8409-2730e6dd9adc'
  },
  //2f293f68-faf6-11e9-8f0b-362b9e155667
  'cargo': {
    'cargoTypes': ['455d3b3e-5e85-4b1b-96ac-eb5fd5255c69', 'ef3015ad-a818-43e1-981f-4a586acd13a5'],
    'cargoWaybillPrice': 2500000
  },
  'parametrs': {
    'comment': 'Номер пломбы 512362',
    'serviceRequirements': ['b9156a24-3526-4d75-8eae-eb8672ee1e9d', '7d2eb006-ae40-470a-a2ce-3b93b66f4f0d']
  },
  'auction': {
    'type': 'DOWNGRADE',
    'step': 3000,
    'startPrice': 2500000,
    'isManualStartPrice': true,
    'duration': 2,
    'startPlanDt': '2020-09-01T21:00:00.000Z',
    'endPlanDt': '2020-09-01T21:16:00.000Z'
  }
}
