import { Server, Model } from "miragejs";
// import { spotCompletedList } from '@/mock/spot-list'
// import { spotItemMock } from 'src/mock/spot-item'

export function makeServer ({ environment = "development" } = {}) {
  let server = new Server({
    models: {
      logchain: Model,
      item: Model
    },

    seeds (server) {
      server.create('logchain', {
        "number": "LOG19 259 7433",
        "modality": ["truck", "train-sm"],
        "route": ["Москва", "Санкт-Петербург", "Челябинск"],
        "date": "2020-03-20",
        "type": "CAR",
        "ready": false
      })

      server.create('logchain', {
        "number": "LOG19 329 5724",
        "modality": ["truck"],
        "route": ["Москва", "Казань"],
        "date": "2020-03-19",
        "type": "CAR",
        "ready": true
      })

      server.create('logchain', {
        "number": "LOG19 304 7835",
        "modality": ["truck"],
        "route": ["Москва", "Ростов-на-Дону"],
        "date": "2020-03-18",
        "type": "CAR",
        "ready": true
      })

      server.create('logchain', {
        "number": "LOG19 304 7835",
        "modality": ["truck"],
        "route": ["Москва", "Ростов-на-Дону"],
        "date": "2020-03-18",
        "type": "CONTAINER",
        "ready": true
      })

      server.create('logchain', {
        "number": "LOG19 304 7835",
        "modality": ["truck"],
        "route": ["Москва", "Ростов-на-Дону"],
        "date": "2020-03-18",
        "type": "WAGON",
        "ready": true
      })

      server.create('item', {
        uuid: 1,
        isPublished: true,
        points: [
          {
            uuid: 33,
            ordinal: 1,
            resource_class_uuid: null,
            isMain: false,
            services: [],
            address: {
              isValid: true,
              label: 'Москва',
              icon: '',
              coordinates: {}
            }
          },
          {
            uuid: 44,
            ordinal: 2,
            resource_class_uuid: null,
            isMain: true,
            services: [
              { "uuid": "cab02c16-57ff-4d75-818c-51f06a512f09", "name": "Грузовик 10 т" },
              { "uuid": "1bcb88bb-57d4-4968-a447-f3f2fbff13ad", "name": "Манипулятор 05 т" },
              { "uuid": "4a27c9c8-d529-44ad-816d-37a7e94f3974", "name": "Эвакуатор грузовой 05 т" }
            ],
            address: {
              isValid: true,
              label: 'Санкт-Петербург',
              icon: '',
              coordinates: {}
            }
          }
        ]
      })

      server.create('item', {
        uuid: 2,
        isPublished: false,
        points: [
          {
            uuid: 33,
            ordinal: 1,
            resource_class_uuid: null,
            isMain: false,
            services: [],
            address: {
              isValid: true,
              label: 'Москва',
              icon: '',
              coordinates: {}
            }
          },
          {
            uuid: 44,
            ordinal: 2,
            resource_class_uuid: null,
            isMain: true,
            services: [
              { "uuid": "cab02c16-57ff-4d75-818c-51f06a512f09", "name": "Грузовик 10 т" },
              { "uuid": "1bcb88bb-57d4-4968-a447-f3f2fbff13ad", "name": "Манипулятор 05 т" },
              { "uuid": "4a27c9c8-d529-44ad-816d-37a7e94f3974", "name": "Эвакуатор грузовой 05 т" }
            ],
            address: {
              isValid: true,
              label: 'Санкт-Петербург',
              icon: '',
              coordinates: {}
            }
          }
        ]
      })
    },

    environment,
    routes () {
      this.urlPrefix = window.OBOZ_CONFIG.OBOZ_API_HOST

      this.get('/oboz2-notificator-settings-crud/v1/notification_types', () => {
        return [{
          uuid: 'f137cc91-316f-47f7-9e22-85fef29d5ea3',
          title: 'Новый клиентский заказ “Спот”'
        }, {
          uuid: 'f137cc91-316f-47f7-9e22-85fef29d5ea4',
          title: 'Новый клиентский заказ “Отправить Груз”'
        }, {
          uuid: 'f137cc91-316f-47f7-9e22-85fef29d5ea5',
          title: 'Новый клиентский заказ “Мультимодалка, контейнеры”'
        }]
      });

      this.post('/oboz2-notificator-settings-crud/v1/settings', (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        console.log('MIRAGE_POST', attrs)
      });
      this.get('/oboz2-notificator-settings-crud/v1/settings', () => {
        return [
            {
          "notificationTypeUuid": "f137cc91-316f-47f7-9e22-85fef29d5ea3",
          "settingType": "USER",
          "recipientInfo": [
            {
              "settingUuid": "2addaf3a-47a4-4e77-8f37-22f15160008e",
              "userUuid": "53eed6e8-e61f-4da5-be3f-818d632a1bc9"
            },
            {
              "settingUuid": "fb21c43f-9311-41e1-ae54-cf9e612d4656",
              "userUuid": "adccbda3-54fa-4a9f-9701-26b78375fd3a"
            }
          ],
          "isActive": true,
          "channels": [
            "EMAIL",
            "SMS"
          ],
          "frequencySending": "ONLINE",
          "periodFrom": "10:00",
          "periodTo": "18:00"
        },
          {
          "notificationTypeUuid": "f137cc91-316f-47f7-9e22-85fef29d5ea3",
          "settingType": "EXTERNAL_RECIPIENT",
          "recipientInfo": {
            "settingUuid": "e3723515-70da-4655-bbe6-63801778879f",
            "externalRecipientTitle": "Иван Иванов",
            "externalRecipientEmail": "test@test.com",
            "externalRecipientSms": "89671234567",
            "externalRecipientWhatsapp": "89671234567",
            "externalRecipientTelegram": "89671234567"
          },
          "isActive": true,
          "channels": [
            "EMAIL",
            "SMS"
          ],
          "frequencySending": "ON_MONDAY",
          "timeAt": "10:00"
        }]
      });
      this.del('/oboz2-notificator-settings-crud/v1/settings/');

      // this.get('/oboz2-spot-order-orders-crud/v1/draft_list/', () => {
      //   return spotDraftList
      // })
      // this.get('/oboz2-spot-order-orders-crud/v1/list', () => {
      //     return spotActualList
      // }, { timing: 2000 })
      // this.post('/oboz2-spot-order-auctions-crud/v1/list', () => {
      //     return spotAuctionInfo
      // })
      // this.get('/oboz2-spot-order-orders-crud/v1/spot-list-completed', () => {
      //     return spotCompletedList
      // })
      // this.get('/oboz2-code-generator/v1/generate', () => {
      //   return {
      //     'code': 'AA-251-958'
      //   }
      // })

      // Чтение карточки spot
      // this.get('/oboz2-spot-order-orders-crud/v1/draft/8b8f7dd7-917b-4012-91c4-0b03f1d4bda4', () => {
      //   return spotItemMock
      // })

      // Получение данных route
      // this.post('/oboz2-geo-routing-proxy/v1', () => {
      //   return {
      //     properties: {
      //       estimated_time: 360,
      //       path_lenght: 134000
      //     }
      //   }
      // })

      //get logchain services
      // this.get("/oboz2-logchain-logchains-crud/v1/services/by_class/:id", () => {
      //   return [
      //     {
      //       uuid: "17a9e89d-ed00-4e79-98f2-dda74e75cdda",
      //       title: 'Перевозки крупнотоннажным автотранспортом - контейнеровозом',
      //       children: [
      //         {
      //           uuid: "17a9e89d-ed00-4e78-97f2-dda74e75cdda",
      //           title: "Длиннобазный 20 т / высокий / 2*20 ’",
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "34cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: "Короткобазный 20 т / низкий / 1*20 ’",
      //           unit: "Рейс"
      //         }
      //       ]
      //     },
      //     {
      //       uuid: "17a9e89d-ed00-4e79-98f2-dda74e85cdda",
      //       title: 'Перевозки крупнотоннажным автотранспортом - фурой',
      //       children: [
      //         {
      //           uuid: "34cd3a81-99a1-4b70-ac56-11fc0c491e2a",
      //           title: 'Полуприцеп Тент 14 т / 55 м3 / 24 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "35cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Изотерм 20 т / 86 м3 / 33 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "36cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Тент 20 т / 86 м3 / 32 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "37cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Изотерм 20 т / 86 м3 / 32 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "38cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Реф 20 т / 86 м3 / 32 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "39cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Изотерм 22 т / 92 м3 / 33 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "40cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Тент 22 т / 92 м3 / 33 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "41cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Реф 22 т / 92 м3 / 33 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "42cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Тент 22 т / 96 м3 / 33 паллет',
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "43cd3a81-99a1-4b70-ac56-11fc0c481e2a",
      //           title: 'Полуприцеп Изотерм 14 т / 55 м3 / 24 паллет',
      //           unit: "Рейс"
      //         }
      //       ]
      //     },
      //     {
      //       uuid: "17a9e99d-ed00-5e79-99f2-dda74e75cdda",
      //       title: 'Перевозки средне- и мелкотоннажным автотранспортом',
      //       children: [
      //         {
      //           uuid: "fdde045c-7df8-416e-af4e-c2e3379d1033",
      //           title: "Автокран 10 т",
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "cd9d7737-b689-43e1-886b-0935fcd2b07a",
      //           title: "Грузовик 02 т",
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "b8f061a9-d004-4a21-a9a1-de3364284a7a",
      //           title: "Грузовик 06 т",
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "a3626b85-2e65-48d8-8988-a842c55054e9",
      //           title: "Грузовик 40 т",
      //           unit: "Рейс"
      //         }
      //       ]
      //     },
      //     {
      //       title: 'Перевозка контейнером',
      //       children: [
      //         {
      //           uuid: "cab02c16-57ff-4d75-818c-51f06a512f09",
      //           title: "Грузовик 10 т",
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "1bcb88bb-57d4-4968-a447-f3f2fbff13ad",
      //           title: "Манипулятор 05 т",
      //           unit: "Рейс"
      //         },
      //         {
      //           uuid: "4a27c9c8-d529-44ad-816d-37a7e94f3974",
      //           title: "Эвакуатор грузовой 05 т",
      //           unit: "Рейс"
      //         }
      //       ]
      //     }
      //   ]
      // });

      // Allow unhandled requests on the current domain to pass through
      this.passthrough()
    }
  })
  return server;
}
