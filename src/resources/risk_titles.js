export const TITLES = {
    BLACK:  'Уровень риска 6',
    RED:    'Уровень риска 5',
    ORANGE: 'Уровень риска 4',
    YELLOW: 'Уровень риска 3',
    GREEN:  'Уровень риска 1',
    BLUE:   'Уровень риска 2'
}
