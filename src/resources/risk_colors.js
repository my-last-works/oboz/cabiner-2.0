export const COLORS = {
    BLACK:  'risk-6-black',
    RED:    'risk-5-red',
    ORANGE: 'risk-4-orange',
    YELLOW: 'risk-3-yellow',
    BLUE:   'risk-2-green',
    GREEN:  'risk-1-blue'
}

export const COLORS_ORDER = {
    BLACK:  6,
    RED:    5,
    ORANGE: 4,
    YELLOW: 3,
    BLUE:   2,
    GREEN:  1
}

export const COLORS_STYLES = {
    BLACK:  'color: #fff; background: #000;',
    RED:    'color: #fff; background: red;',
    ORANGE: 'color: #fff; background: orange;',
    YELLOW: 'color: #000; background: #FFEB48;',
    BLUE:   'color: #fff; background: #8ece4e;',
    GREEN:  'color: #fff; background: #10CF96;'
}
