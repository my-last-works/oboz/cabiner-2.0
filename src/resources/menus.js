const obj = [
  {
    caption: 'Заказы',
    disabled: true,
    href: '#',
  },
  {
    caption: 'Пломба',
    disabled: true,
    href: '#',
  },
  {
    caption: 'Мониторинг',
    disabled: true,
    href: '#',
  },
  {
    caption: 'Документы',
    disabled: true,
    href: '#',
  },
  {
    caption: 'Аналитика',
    disabled: true,
    href: '#',
  },
  {
    caption: 'Оплаты',
    disabled: true,
    href: '#',
  },
  {
    caption: 'Договор',
    disabled: true,
    href: '#',
  },
  {
    caption: 'Справочники',
    members: [
      {
        caption: 'Общие',
        to: { name: 'countries' },
        members: [
          {
            title: 'Общие',
          },
          {
            border: true
          },
          {
            title: 'Страны',
            to: {name: 'countries'}
          },
          {
            title: 'Таможенные союзы',
            to: {name: 'customs-unions'}
          },
          {
            title: 'Валюты',
            to: {name: 'currencies'}
          },
          {
            title: 'Статьи ГК и УК',
            to: {name: 'law-articles'}
          },
        ]
      },
      {
        caption: 'Локации',
        to: { name: 'location-types' },
        members: [
          {
            title: 'Локации',
          },
          {
            border: true
          },
          {
            title: 'Типы локаций',
            to: {name: 'location-types'}
          },
          {
            title: 'Локации',
            to: {name: 'locations'}
          },
        ]
      },
      {
        caption: 'Температура',
        to: { name: 'temperature-recorders' },
        members: [
          {
            title: 'Температура',
          },
          {
            border: true
          },
          {
            title: 'Температурные регистраторы',
            to: {name: 'temperature-recorders'}
          },
          {
            title: 'Температурные режимы транспортировки',
            to: {name: 'temperature-transportation-modes'}
          },
        ]
      },
      {
        caption: 'Грузы',
        to: { name: 'cargo-types' },
        members: [
          {
            title: 'Грузы',
          },
          {
            border: true
          },
          {
            title: 'Типы грузов',
            to: {name: 'cargo-types'}
          },
          {
            title: 'Классы опасных грузов',
            to: {name: 'dangerous-goods-classes'}
          },
          {
            title: 'Коды ЕТСНГ',
            to: {name: 'etsng'}
          },
          {
            title: 'Коды ТН ВЭД',
            to: {name: 'tnved'}
          },
        ]
      },
      {
        caption: 'Ресурсы',
        to: { name: 'resource-types' },
        members: [
          {
            title: 'Ресурсы',
          },
          {
            border: true
          },
          {
            title: 'Типы ресурсов',
            to: {name: 'resource-types'}
          },
          {
            title: 'Виды ресурсов',
            to: {name: 'resource-species'}
          },
          {
            title: 'Шаблоны характеристик ресурсов',
            to: {name: 'patterns'}
          },
          {
            title: 'Маски ресурсов',
            to: {name: 'pattern-masks'}
          },
          {
            title: 'Единицы экспедиторского учета',
            to: {name: 'forwarding-unit'}
          },
          {
            title: 'Методы трекинга',
            to: {name: 'tracking-methods'}
          },
        ]
      },
      {
        caption: 'Услуги',
        to: { name: 'services' },
        members: [
          {
            title: 'Услуги',
          },
          {
            border: true
          },
          {
            title: 'Услуги',
            to: {name: 'services'}
          },
          {
            title: 'Типы услуг',
            to: {name: 'service-types'}
          },
          {
            title: 'Единицы измерения',
            to: {name: 'service-measurement-units'}
          },
          {
            title: 'Типы документов',
            to: {name: 'document-types'}
          },
          {
            title: 'Направления деятельности',
            to: {name: 'activities'}
          },
        ]
      },
    ],
  },
  {
    caption: 'Настройки',
    members: [
      {
        caption: 'Администрирование',
        href: '/oboz2-front-admin-panel/',
        only: ['moderator']
      },
      {
        caption: 'Логистические домены экспедиторов',
        href: '/oboz2-front-logistic-domain-panel/link-browser-expeditor',
        only: ['expeditor']
      },
      {
        caption: 'Логистические домены клиентов/поставщиков',
        href: '/oboz2-front-logistic-domain-panel/link-browser',
        only: ['carrier', 'client']
      },
      {
        caption: 'Логистические домены модератора',
        href: '/oboz2-front-logistic-domain-panel/link-browser-moderator',
        only: ['moderator']
      }
    ]
  }
];

const authorize = (obj) => {

  return obj;
}

export const topLevelMembers = authorize(obj);
