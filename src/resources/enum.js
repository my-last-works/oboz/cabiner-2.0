export const DOCUMENT_CLASS = [{
  value: 'Логистический документ'
}, {
  value: 'Закрывающий финансовый документ'
}, {
  value: 'Счет'
}
];

export const STATUS_LIST = [
  {
    value: true,
    text: 'Активен'
  },
  {
    value: false,
    text: 'Неактивен'
  }
];

export const TEMPERATURE_MODE_ICONS = Object.freeze({
  FREEZING            : 'freezing',
  COOLING             : 'cooling',
  ROOM_TEMPERATURE    : 'room-temperature',
  WITHOUT_SPECIAL_MODE: null
});

export const TEMPERATURE_MODE = Object.freeze({
  COOLING             : 'Охлаждение',
  FREEZING            : 'Заморозка',
  ROOM_TEMPERATURE    : 'Комнатная температура',
  WITHOUT_SPECIAL_MODE: 'Не нужен специальный режим'
});

export const TEMPERATURE_MODE_ICONS_BY_NAME = Object.freeze({
  'охлаждение'           : 'freezing',
  'заморозка'            : 'cooling',
  'комнатная температура': 'room-temperature'
});

export const TEMPERATURE_MODE_SELECT_OPTIONS = [
  Object.freeze({
    value: 'COOLING',
    label: TEMPERATURE_MODE.COOLING,
    icon: TEMPERATURE_MODE_ICONS.COOLING
  }),
  Object.freeze({
    value: 'FREEZING',
    label: TEMPERATURE_MODE.FREEZING,
    icon: TEMPERATURE_MODE_ICONS.FREEZING
  }),
  Object.freeze({
    value: 'ROOM_TEMPERATURE',
    label: TEMPERATURE_MODE.ROOM_TEMPERATURE,
    icon: TEMPERATURE_MODE_ICONS.ROOM_TEMPERATURE
  })
];

export const RADIUS_LABEL = {
  zero_interrogation_radius: 'Нулевой допробег',
  hard_connect_radius      : 'Радиус жесткой привязки',
  vehicle_arrival_radius   : 'Зачет прибытия ТС',
  vehicle_departure_radius : 'Зачет убытия ТС',
  spot_listing             : 'Попадание в спотовый список',
  futures                  : 'Фьючерсирование',
  hard_binding_radius      : 'Радиус жесткой привязки'
};

export const SERVICE_GEOGRAPHY = {
  FROM_TO                  : 'Откуда-Куда',
  WHERE                    : 'Где',
  WITHOUT_GEOGRAPHY_BINDING: 'Без привязки к географии'
};

export const SERVICE_GEOGRAPHY_OPTIONS = [
  {
    value: 'FROM_TO',
    text: 'Откуда-Куда'
  },
  {
    value: 'WHERE',
    text: 'Где'
  },
  {
    value: 'WITHOUT_GEOGRAPHY_BINDING',
    text: 'Без привязки к географии'
  }
];

export const STATUS_MODEL = {
  EXTENDED: 'Расширенная',
  REDUCED: 'Сокращенная',
};

export const STATUS_MODEL_OPTIONS = [

  {
    value: 'REDUCED',
    text: 'Сокращенная',
  },
  {
    value: 'EXTENDED',
    text: 'Расширенная',
  }
];

export const SERVICE_CATEGORY = [
  {
    value: 'COMMERCIAL',
    text: 'Коммерческая услуга',
  },
  {
    value: 'COMPENSATION_FOR_LOSS_OR_DAMAGE',
    text: 'Возмещение убытка/ущерба',
  },
  {
    value: 'SURCHARGE',
    text: 'Штраф',
  },
]

export const RESOURCES_MODEL = {
  NEED_RESOURCES    : 'Нужны ресурсы',
  NO_NEED_RESOURCES : 'Не требует ресурсов'
}

export const RESOURCES_OPTIONS = [
  {
    value: true,
    text: 'Нужны ресурсы',
  },
  {
    value: false,
    text: 'Не требует ресурсов',
  },
]

export const INCLUSION_OPTIONS = [
  {
    value: 'REQUIRED_AND_ON',
    text: 'Обязательна и включена по умолчанию'
  },
  {
    value: 'NOT_REQUIRED_AND_ON',
    text: 'Не обязательна и включена по умолчанию'
  },
  {
    value: 'NOT_REQUIRED_AND_OFF',
    text: 'Не обязательна и не включена по умолчанию'
  },
  {
    value: 'NOT_INCLUDED',
    text: 'Отсутствует в черновике'
  }
]

export const CODEX_NAME = (type = 'default') => {
  const map = {
    criminal: {
      short: 'УК',
      full: 'Уголовный кодекс'
    },
    civil: {
      short: 'ГК',
      full: 'Гражданский кодекс'
    },
    default: {
      short: '-',
      full: '-'
    }
  }

  return map[type.toLowerCase()];
};

export const COUNTRY_CODE = (type = 'default') => {
  const map = {
    rus: {
      short: 'РФ',
      full: 'Россия'
    },
    default: {
      short: '-',
      full: '-'
    }
  };

  return map[type.toLowerCase()];
};

export const CREATED_BY = ['Клиент', 'Поставщик', 'Экспедитор'];

export const WORKER_ROLE = {
  TENDER_PURCHASER       : 'Закупщик',
  TENDER_COMISSION_HEAD  : 'Участник тендерной комиссии',
  TENDER_SUPPLIER        : 'Поставщик',
  TENDER_SUPPLIER_MANAGER: 'Менеджер поставщика'
};

export const LOADED_STATE = Object.freeze({
  'LOADING': 'LOADING', // : 'Загрузка',
  'READY': 'READY', // : 'Загружено',
  'FAIL': 'FAIL' //: 'Ошибка'
});

export const ORDER_STATUS = Object.freeze({
  DRAFT: 'Черновик',
  NEW: 'Ожидает публикации',
  AUCTION: 'Опубликован',
  AUCTION_ENDED: 'Подведение итогов',
  PROCESSED: 'Завершен',
  CANCELLED: 'Отменен'
});

export const AUCTION_STATUS = Object.freeze({
  DRAFT: 'Черновик',
  NEW: 'Ожидает публикации',
  IN_PROCCESS: 'Идет аукцион',
  AUCTION_ENDED: 'Подведение итогов',
  PROCESSED: 'Аукцион завершен',
  CANCELLED: 'Аукцион отменен'
});

export const AUCTION_TYPES = Object.freeze({
  DOWNGRADE: {
    value: 'DOWNGRADE',
    titleShort: 'Торги идут до завершения аукциона. Победителем является тот, кто предложил наименьшую ставку',
    title: 'Аукцион на понижение',
    icon: 'auction'
  },
  FIRST_APPROVE: {
    value: 'FIRST_APPROVE',
    titleShort: 'Победителем аукциона является тот, кто первым согласился на предложенные вами условия',
    title: 'Аукцион первого согласия',
    icon: 'first-bid'
  },
  BLIND: {
    value: 'BLIND',
    titleShort: 'Аукцион-опрос, в котором участники самостоятельно определяют ставку только один раз. Побеждает минимальная ставка',
    title: 'Закрытый аукцион',
    icon: 'blind-auction'
  }
});