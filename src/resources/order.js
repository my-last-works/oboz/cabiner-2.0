export const orderInitData = {
  bodySaveDraft: {
    number: '',
    contractorUuid: '',
    orderType: '',
    clientUuid: '',
    logistUuid: '',
    orderSpecies: null,
    logist: {
      email: null,
      firstname: null,
      lastname: null,
      ptronymic: null,
      uuid: null
    },
    transportationWay: 'FTL',
    temperatureRegimeUuid: '',
    tariffUuid: '',
    isPrivateContainer: false,
    route: {
      departureDt: '',
      arriveDt: '',
      isCrossborder: false,
      transboundary: '',
      points: [
        {
          ordinal: 1,
          direction: 'FROM',
          visitPointCount: 1,
          type: 'ADDRESS',
          childPoints: [],
          params: {
            uuid: '',
            coordinates: null,
            countryCode: 'RUS',
            locationUuid: '',
            locationTypeUuid: '',
            addressTitle: ''
          }
        },
        {
          ordinal: 2,
          direction: 'TO',
          visitPointCount: 1,
          type: 'ADDRESS',
          childPoints: [],
          params: {
            uuid: '',
            coordinates: null,
            countryCode: 'RUS',
            locationUuid: '',
            locationTypeUuid: '',
            addressTitle: ''
          }
        }
      ],
    },
    ftlParams: {
      speciesUuids: '',
      typeUuids: '',
      count: '1'
    },
    serviceParams: {
      serviceUuid: '',
      serviceTypeUuid: '',
      count: '1'
    },
    isDangerous: false,
    needTracking: false
  },
  creators: {
    userUuid: '',
    contractorUuid: ''
  },
  cargo: {
    weightInKg: null,
    volumeInCubicMeters: null,
    palletCount: null,
    accountingType: 'BY_PACKAGE',
    freightForwardingUnitUuid: '',
    etsngs: []
  },
  logchain: {},
  services: {},
  parameters: {
    cargoTypeUuid: '',
    serviceRequirements: [],
    insuranceCargo: {
      cost: null,
      currency: 'RUR'
    },
    contactPerson: {
      fio: '',
      phone: ''
    },
    comments: ''
  },
  primeCost: {
    primeCost: null,
    vatValue: null,
    profit: null,
    profitPercent: null,
    currency: null
  }
};

export const draftFactory = (number, kind) => {
  return {
    bodySaveDraft: {
      number: number,
      orderKind: kind,
      contractorUuid: '',
      orderType: 'STANDARD',
      clientUuid: '',
      logistUuid: '',
      logist: {
        email: null,
        firstname: null,
        lastname: null,
        ptronymic: null,
        uuid: null
      },
      transportationWay: 'FTL',
      orderSpecies: null,
      temperatureRegimeUuid: '',
      tariffUuid: '',
      isPrivateContainer: false,
      route: {
        departureDt: '',
        arriveDt: '',
        isCrossborder: false,
        transboundary: '',
        points: [
          {
            ordinal: 1,
            direction: 'FROM',
            visitPointCount: 1,
            type: 'ADDRESS',
            childPoints: [],
            params: {
              uuid: '',
              coordinates: null,
              countryCode: 'RUS',
              locationUuid: '',
              locationTypeUuid: '',
              addressTitle: ''
            }
          },
          {
            ordinal: 2,
            direction: 'TO',
            visitPointCount: 1,
            type: 'ADDRESS',
            childPoints: [],
            params: {
              uuid: '',
              coordinates: null,
              countryCode: 'RUS',
              locationUuid: '',
              locationTypeUuid: '',
              addressTitle: ''
            }
          }
        ],
      },
      ftlParams: {
        speciesUuids: '',
        typeUuids: '',
        count: '1'
      },
      serviceParams: {
        serviceUuid: '',
        serviceTypeUuid: '',
        count: '1'
      },
      isDangerous: false,
      needTracking: false
    },
    creators: {
      userUuid: '',
      contractorUuid: ''
    },
    cargo: {
      weightInKg: '',
      volumeInCubicMeters: '',
      palletCount: '',
      accountingType: 'BY_PACKAGE',
      freightForwardingUnitUuid: '',
      etsngs: []
    },
    logchain: {},
    services: {},
    parameters: {
      cargoTypeUuid: '',
      serviceRequirements: [],
      insuranceCargo: {
        cost: null,
        currency: 'RUR'
      },
      contactPerson: {
        fio: '',
        phone: ''
      },
      comments: ''
    },
    primeCost: {
      primeCost: null,
      vatValue: null,
      profit: null,
      profitPercent: null,
      currency: null
    }
  }
}

export const orderItem = {
  "bodySaveDraft": {
    "number": "PO20 230 7717",
    "externalNumber": "PO20 230 7717",
    "clientUuid": "519b6516-32ed-4144-b915-c266f4072bf2",
    "contractorUuid": "811c9200-cf93-46ce-b9d8-da60386eb6a0",
    "logistUuid": "0d561db8-8afc-463c-845f-a1c5ea2445d4",
    "orderType": "STANDARD",
    "orderSpecies": null,
    "riskColor": "GREEN",
    "needTracking": false,
    "isDangerous": false,
    "temperatureRegimeUuid": "e1fdc890-c81b-4d96-b1b4-3524597c679c",
    "page": "PARAMETERS",
    "transportationWay": null,
    "tariffUuid": "04560ac1-13b6-45eb-94ca-bbd3f776e961",
    "route": {
      "departureDt": null,
      "arriveDt": "2020-02-20T21:00:00Z",
      "isCrossborder": false,
      "points": [{
        "direction": "FROM",
        "ordinal": 1,
        "visitPointCount": 1,
        "type": "ADDRESS",
        "params": {
          "uuid": "ddbdc5a0-623b-46de-b77b-334d34c080f1",
          "coordinates": {"lat": 56.33303452, "lon": 38.14474869},
          "countryCode": "RUS",
          "locationUuid": null,
          "locationTypeUuid": null,
          "addressTitle": "Северный-5, микрорайон, Сергиев Посад, Московская область, Россия"
        }
      }, {
        "direction": "TO",
        "ordinal": 2,
        "visitPointCount": 1,
        "type": "ADDRESS",
        "params": {
          "uuid": "37b7dc94-2dcb-4087-9208-96e39a1a100b",
          "coordinates": {"lat": 55.50671768, "lon": 36.01736069},
          "countryCode": "RUS",
          "locationUuid": null,
          "locationTypeUuid": null,
          "addressTitle": "Можайск, Московская область, Россия"
        }
      }]
    },
    "ftlParams": {
      "speciesUuids": "2f293a4a-faf6-11e9-8f0b-362b9e155667",
      "typeUuids": "8939390a-fb04-11e9-aad5-362b9e155667",
      "count": 1
    }
  },
  "creators": {
    "userUuid": "3ab75ce4-8d30-4dc8-bc46-08bb20a77316",
    "contractorUuid": "519b6516-32ed-4144-b915-c266f4072bf2"
  },
  "brif": {
    "visitPoints": [{
      "uuid": "56a80c6f-6a7e-40af-8196-009a3ff7d62c",
      "ordinal": 1,
      "infographUuid": "df19d07c-7e7b-42bc-a732-833573d461ab",
      "title": "МегаГрузОтпр",
      "type": "FROM",
      "timeSlot": {"date": "2020-02-20", "startT": "21:00", "endT": "22:00"},
      "address": "Северный-5, микрорайон, Сергиев Посад, Московская область, Россия",
      "comments": null,
      "extraDistanceInKm": 0
    }, {
      "uuid": "68137ad5-03dd-4e11-998c-4f28da39afcb",
      "ordinal": 2,
      "infographUuid": "a89b782d-95f8-44b1-98c2-e3302a77a2d1",
      "title": "ООО КПД Карго",
      "type": "TO",
      "timeSlot": {"date": "2020-02-20", "startT": "21:00", "endT": "22:00"},
      "address": "Можайск, Московская область, Россия",
      "comments": "3",
      "extraDistanceInKm": 0
    }]
  },
  "cargo": {
    "weightInKg": 15,
    "volumeInCubicMeters": 0,
    "palletCount": 0,
    "accountingType": "BY_PACKAGE",
    "freightForwardingUnitUuid": "95720096-cda7-405c-ac46-036ef7d26170"
  },
  "parameters": {
    "cargoTypeUuid": "ee855e1b-2225-4ce2-817b-917b0df19b1f",
    "serviceRequirements": [],
    "insuranceCargo": {"cost": 3520, "currency": "RUR"},
    "contactPerson": {"contactUuid": null, "phoneUuid": null, "phone": null},
    "comments": null
  },
  "logchain": {
    "baseLocations": [{
      "uuid": "8bd1390a-ea82-4030-887a-3af0b25a2625",
      "direction": "FROM",
      "date": "2020-02-19T09:35:39.035Z"
    }], "routeLength": 0, "routeDuration": 0, "tariff": {"value": 2950000, "currency": null}
  },
  "actualTariff": {
    "totalPrice": 2950000,
    "baseServicePrice": 2950000,
    "extraDistancePrice": 0,
    "currency": "RUB",
    "vatInPercents": 20,
    "vatValue": 590000
  },
  "services": {
    "contractNumber": "CTR19 345 0971",
    "contractEffectiveD": "2019-12-13",
    "totalPrice": 2950000,
    "titleGeoraphy": "Северный-5, микрорайон, Сергиев Посад, Московская область, Россия -> Можайск, Московская область, Россия",
    "baseServices": [{
      "baseServiceClientName": "Изотерм. 20 т / 68 м3 / 33 пал",
      "geography": "null -> null",
      "count": 1,
      "price": null
    }]
  }
}

export const subTypeItems = [
  {
    title: 'ЗАО «ТрансКорж»',
    icon: 'risk-1-blue',
    grzNum: 'к 324 бб',
    grzReg: '199',
    name: 'Алексеев Вадим А.',
    description: 'Камаз / +7 (823) 589-34-15 / 2015 г. / В собственности',
    tariff: '49 000',
    reg: '2019-12-03T13:15',
    count: 2,
    pravo: 'В собственности',
    year: '2008',
    status: 'В рейсе',
    modelAuto: 'DAF XF105'
  },
  {
    title: 'ООО «Трансастана»',
    icon: 'risk-2-blue',
    grzNum: 'ав 3241',
    grzReg: '199',
    name: 'Авдеев Вадим А.',
    description: 'Полуприцеп. Изотерм. / 20т / 86м³/ 33 пал / Гидроборт, задняя загрузка  / 2009 г. / ....',
    tariff: '48 200',
    reg: '2019-12-01T22:12',
    count: 6,
    pravo: 'Лизинг',
    year: '2001',
    status: 'Нет данных',
    modelAuto: 'Камаз'
  },
  {
    title: 'ООО «Транспортные линии»',
    icon: 'risk-1-blue',
    grzNum: 'к 787 шх',
    grzReg: '63',
    name: 'Сергеев Вадим А.',
    description: '+7 (823) 589-34-15',
    tariff: '45 000',
    reg: '2019-12-01T21:15',
    count: null,
    pravo: 'Аренда',
    year: '2016',
    status: 'Доступен',
    modelAuto: 'Volvo FH'
  }
]
