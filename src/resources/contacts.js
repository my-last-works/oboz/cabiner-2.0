export const contacts = {
  gruzi: {
    email: "support@gruzi.ru",
    linkphone: "+74952601778",
    phone: "+7 (495) 260-17-78",
    title: "Грузи.Ру",
  },
  oboz: {
    email: "support@oboz.app",
    linkphone: "+74956460603",
    phone: "+7 (495) 646-06-03",
    title: "Обоз",
  }
}