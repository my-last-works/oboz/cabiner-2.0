import Vue from "vue"

export const events = new Vue()

export default {
  install(Vue) {
    const notify = params => {
      if(!params.event) {
        if (!params.type || !params.message) {
          throw (`Notification params not valid`)
        }

        (params.type === "snackbars" || params.type === "system_notification_in_load" || params.type === "system_notification_success" || params.type === "system_notification_error") ? params.position = "left" : params.position = "right";

        if (!params.duration) {
          params.duration = 5000
        }
        params.id = params.id || Math.ceil(Math.random() * 99999 * 2e4)
        params.show = true

        events.$emit("ob_add_notification", params)
      }else{
        events.$emit("ob_close_notification", params)
      }
    }
    Vue.prototype.$notification = notify
  }
}
