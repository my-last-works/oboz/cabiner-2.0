import { requiredIf } from 'vuelidate/lib/validators'
import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      reasonsList: 'order/reasonsList'
    }),
    currentFormModel() {
      const isExistReasons = Boolean(this.currentReasonsList?.length)
      let formModel = this.formModel || {}
      let reasonsModel = {
        reasonUuid: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return isExistReasons
            })
          }
        }
      }

      return { ...formModel, ...reasonsModel }
    }
  },
  methods: {

  }
}