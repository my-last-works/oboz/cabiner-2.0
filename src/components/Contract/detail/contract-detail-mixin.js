import { mapGetters, mapMutations, mapActions } from 'vuex'

const CLIENT_CODE = 'client'
const DOER_CODE = 'doer'

export default {
  props: {
    validator: {
      type: Object,
      default: () => {}
    },
    activeTab: {
      type: String,
      default: 'SIMPLE'
    },
    caste: {
      type: String,
      default: 'SIMPLE'
    }
  },
  data () {
    return {
      NOT_SELECT_USER_PLACEHOLDERS: {
        [CLIENT_CODE]: 'Сначала выберите Заказчика',
        [DOER_CODE]: 'Сначала выберите Исполнителя'
      }
    }
  },
  computed: {
    ...mapGetters({
      VIEW_STATES: 'contract/VIEW_STATES',
      CLIENT_CODE: 'contract/CLIENT_CODE',
      DOER_CODE: 'contract/DOER_CODE',
      contract: 'contract/contract',
      currentRole: 'contract/currentRole',
      isDraft: 'contract/isDraft',
      isSigning: 'contract/isSigning',
      isSigned: 'contract/isSigned',
      isStatus: 'contract/isStatus',
      signedDate: 'contract/signedDate',
      isNotCreator: 'contract/isNotCreator',
      isClientAccountUser: 'contract/isClientAccountUser',
      isDoerAccountUser: 'contract/isDoerAccountUser',
      isClientAuthority: 'contract/isClientAuthority',
      isDoerAuthority: 'contract/isDoerAuthority',
      isTimeless: 'contract/isTimeless',
      isNotSelectClientUser: 'contract/isNotSelectClientUser',
      isNotSelectDoerUser: 'contract/isNotSelectDoerUser',
      isExistServices: 'contract/isExistServices',
      isTracking: 'contract/isTracking',
      isTransportation: 'contract/isTransportation',
      isTitul: 'contract/isTitul',
      isTariffAgreement: 'contract/isTariffAgreement',
      isVatPayer: 'contract/isDoerVatPayer',
      isExistClicar: 'contract/isExistClicar',
      participantUuid: 'user/participantUuid',
      isManualCreated: 'contract/isManualCreated',
      isExistTariffAgreements: 'contract/isExistTariffAgreements'
    }),
    isUserClient () {
      return this.contract.clientUuid === this.participantUuid
    },
    isUserDoer () {
      return this.contract.doerUuid === this.participantUuid
    },
    contractId () {
      return this.$route && this.$route.params.id
    },
    rolesItems () {
      return [
        {
          label: 'Я Заказчик',
          value: this.CLIENT_CODE
        },
        {
          label: 'Я Исполнитель',
          value: this.DOER_CODE
        }
      ]
    },
    isClientNotification () {
      const isNotification = [
        this.contract.needNotifyClientByPhone,
        this.contract.needNotifyClientByEmail,
        this.contract.needNotifyClientByMail,
        this.contract.needNotifyClientByJurMail
      ].includes(true)

      return isNotification && Boolean(
  (!this.contract.needNotifyClientByPhone || this.contract.needNotifyClientByPhone && this.contract.clientPhone && this.isValidClientPhone) &&
        (!this.contract.clientEmail || this.contract.clientEmail && this.isValidClientEmail) &&
        (!this.contract.needNotifyClientByMail || this.contract.needNotifyClientByMail && this.contract.clientPostAddress) &&
        (!this.contract.needNotifyClientByJurMail || this.contract.needNotifyClientByJurMail && this.contract.clientLegalAddress)
      )
    },
    isOneNotificationClient () {
      const clientNotifications = [
        this.contract.needNotifyClientByPhone,
        this.contract.needNotifyClientByEmail,
        this.contract.needNotifyClientByMail,
        this.contract.needNotifyClientByJurMail
      ]

      return clientNotifications.filter(item => item === true).length === 1
    },
    isOnlyClientMailNotification () {
      return this.contract.needNotifyClientByEmail && this.isOneNotificationClient
    },
    isDoerNotification () {
      const isNotification = [
        this.contract.needNotifyDoerByPhone,
        this.contract.needNotifyDoerByEmail,
        this.contract.needNotifyDoerByMail,
        this.contract.needNotifyDoerByJurMail
      ].includes(true)

      return isNotification && Boolean(
  (!this.contract.needNotifyDoerByPhone || this.contract.needNotifyDoerByPhone && this.contract.doerPhone && this.isValidDoerPhone) &&
        (!this.contract.doerEmail || this.contract.doerEmail && this.isValidDoerEmail) &&
        (!this.contract.needNotifyDoerByMail || this.contract.needNotifyDoerByMail && this.contract.doerPostAddress) &&
        (!this.contract.needNotifyDoerByJurMail || this.contract.needNotifyDoerByJurMail && this.contract.doerLegalAddress)
      )
    },
    isOneNotificationDoer () {
      const doerNotifications = [
        this.contract.needNotifyDoerByPhone,
        this.contract.needNotifyDoerByEmail,
        this.contract.needNotifyDoerByMail,
        this.contract.needNotifyDoerByJurMail
      ]

      return doerNotifications.filter(item => item === true).length === 1
    },
    isOnlyDoerMailNotification () {
      return this.contract.needNotifyDoerByEmail && this.isOneNotificationDoer
    },
    isValidNumberAndDates () {
      return this.isValidDate && this.isValidOriginalSignedDate && this.isValidOriginalNumber
    },
    isValidDate () {
      return this.formValidator.fromDate && (this.formValidator.toDate || this.contract.isTimeless) && !this.formValidator.fromDate.$invalid && !this.formValidator.toDate.$invalid
    },
    isValidOriginalSignedDate () {
      return this.isExistClicar && this.isTitul ? Boolean(this.contract.originalSignedDate) : true
    },
    isValidOriginalNumber () {
      return this.isExistClicar && this.isTitul ? Boolean(this.contract.originalNumber) : true
    },
    isValidClientEmail () {
      return this.formValidator.clientEmail && !this.formValidator.clientEmail.$invalid
    },
    isValidDoerEmail () {
      return this.formValidator.doerEmail && !this.formValidator.doerEmail.$invalid
    },
    isValidClientPhone () {
      return this.formValidator.clientPhone && !this.formValidator.clientPhone.$invalid
    },
    isValidDoerPhone () {
      return this.formValidator.doerPhone && !this.formValidator.doerPhone.$invalid
    },
    formValidator () {
      return this.validator && this.validator.formData || {}
    },
    revertRole () {
      return this.currentRole === this.CLIENT_CODE ? this.DOER_CODE : this.CLIENT_CODE
    },
    revertUserTitle () {
      return this.currentRole === this.CLIENT_CODE ? 'Исполнителя' : 'Заказчика'
    },
    validateFormState () {
      let validatorByType = {}
      let originalContractValidator = {}

      if (this.isExistClicar && this.isTitul) {
        originalContractValidator = {
					originalNumber: {
            isValid: Boolean(this.contract.originalNumber),
            label: `Укажите внешний номер договора`
          },
          originalSignedDate: {
            isValid: Boolean(this.contract.originalSignedDate),
            label: `Укажите дату подписания`
          }
        }
      }

      if (this.isTracking || this.isTransportation || this.isTariffAgreement) {
        validatorByType.tarification = {
          services: {
            isValid: this.isExistServices,
            label: `Добавьте услуги, которые будут оказаны в рамках ${this.isTariffAgreement ? 'тарифного соглашения' : 'договора'}`
          }
        }
      }

      if (this.isTariffAgreement) {
        validatorByType.frontPage = {
          ...originalContractValidator,
          validDate: {
            isValid: this.isValidDate,
            label: 'Укажите период действия тарифного соглашения'
          },
          statusContract: {
            isValid: this.contract.parentContractData?.status !== 'DRAFT',
            isHidden: true
          }
        }
      } else {
        validatorByType.frontPage = {
          // clientAuthority: {
          //   isValid: this.isClientAuthority,
          //   label: 'Укажите подписанта со стороны Заказчика'
          // },
          // clientAccountUser: {
          //   isValid: this.isClientAccountUser,
          //   label: 'Выберите ответственного сотрудника со стороны Заказчика'
          // },
          ...originalContractValidator,
          clientNotification: {
            isValid: this.isClientNotification,
            label: 'Настройте отправку уведомлений Заказчику'
          },
          contractor: {
            isValid: !this['isNotSelect' + this.firstLetterUp(this.revertRole) + 'User'],
            label: 'Выберите ' + this.revertUserTitle
          },
          // doerAuthority: {
          //   isValid: this.isDoerAuthority,
          //   label: 'Укажите подписанта со стороны Исполнителя'
          // },
          // doerAccountUser: {
          //   isValid: this.isDoerAccountUser,
          //   label: 'Выберите ответственного сотрудника со стороны Исполнителя'
          // },
          doerNotification: {
            isValid: this.isDoerNotification,
            label: 'Настройте отправку уведомлений для Исполнителя'
          },
          validDate: {
            isValid: this.isValidDate,
            label: 'Укажите период действия электронного договора'
          }
        }
      }
      return {
        ...validatorByType
      }
    },
    isAllValidate () {
      return Object.values(this.validateFormState).every((item) => {
        return Object.values(item).every((item) => item.isValid)
      })
    },
    currentValidate () {
      return Object.values(this.validateFormState[this.activeTab] || []).filter(item => !item.disable)
    },
    currentValidateAll () {
      return !this.currentValidate.length || this.currentValidate.every((item) => item.isValid || item.isHidden)
    }
  },
  methods: {
    ...mapActions({
      saveContract: 'contract/saveContract',
      setContractField: 'contract/setContractField',
      calcExistServices: 'contract/calcExistServices'
    }),
    ...mapMutations({
      setContractData: 'contract/setContractData',
      setViewState: 'contract/setViewState',
      setEditedService: 'contract/setEditedService',
      setExistServices: 'contract/setExistServices'
    }),
    conditionPlaceholder (placeholder, userTypeCode) {
      let mapKeys = {
        [this.CLIENT_CODE]: 'clientUuid',
        [this.DOER_CODE]: 'doerUuid',
      }
      return this.currentRole === userTypeCode || Boolean(this.contract[mapKeys[userTypeCode]]) ? placeholder : this.NOT_SELECT_USER_PLACEHOLDERS[userTypeCode]
    },
    firstLetterUp (str) {
      str = str || this.CLIENT_CODE
      return str[0].toUpperCase() + str.slice(1)
    },
    changeFieldHandler (field, value, save = false) {
      this.setContractField({ field, value, save })
    },
    saveContractField (field) {
      this.saveContract({ [field]: this.contract[field] })
    }
  }
}
