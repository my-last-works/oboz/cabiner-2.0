import Router from 'vue-router'
// import i18n          from '@/translations/lang'
import store         from '@/store/index'
import { getCookie } from '@/utils/cookie'

const Home                                  = () => import('@/pages/Home/index')
const AccessPage                            = () => import('@/pages/accessPage/index')
const AccountPage                           = () => import('@/pages/account/index')
const ActivitiesPage                        = () => import('@/pages/activitiesPage/index')
const AllComponents                         = () => import('@/pages/allComponentsPage/index')
const AllUsersPage                          = () => import('@/pages/allUsersPage/index')
const ArchiveOrders                         = () => import('@/pages/account/tabs/archiveOrders')
const CargoTypesPage                        = () => import('@/pages/cargoTypesPage/index')
const ContractDetail                        = () => import('@/pages/contractPage/ContractDetail')
const ContractorRolesPage                   = () => import('@/pages/contractorRolesPage/index')
const ContractsDraft                        = () => import('@/pages/contractPage/DraftList')
const ContractsList                         = () => import('@/pages/contractPage/List')

const CostCategoriesPage                    = () => import('@/pages/costCategoriesPage/index')
const CountriesPage                         = () => import('@/pages/countriesPage/index')
const CurrenciesPage                        = () => import('@/pages/currenciesPage/index')
const CustomsUnionsPage                     = () => import('@/pages/customsUnionsPage/index')
const DangerousGoodsClassesPage             = () => import('@/pages/dangerousGoodsClassesPage/index')
const Deals                                 = () => import('@/pages/risksPages/tabs/deals')
const DevelopingPage                        = () => import('@/pages/developingPage/index')
const DictionariesServicesPage              = () => import('@/pages/dictionariesServicePage/index')
const DocumentTypesPage                     = () => import('@/pages/documentTypesPage/index')
const DomainSettingsExpeditor               = () => import('@/pages/logisticDomainsSettings/DomainSettingsExpeditor')
const DomainSettingsModerator               = () => import('@/pages/logisticDomainsSettings/DomainSettingsModerator')

const EMSCodePage                           = () => import('@/pages/emsCodePage/index')
const ExecutedOrders                        = () => import('@/pages/account/tabs/executedOrders')
const ForwardingUnitPage                    = () => import('@/pages/forwardingUnitPage/index')
const GroupsRequisitesPage                  = () => import('@/pages/groupsRequisitesPage/index')
const HSCodesPage                           = () => import('@/pages/hsCodesPage/index')
const IndividualDomainSettingsExpeditor     = () => import('@/pages/logisticDomainsSettings/IndividualDomainSettingsExpeditor')
const IndividualDomainSettingsModeratorPage = () => import('@/pages/logisticDomainsSettings/IndividualDomainSettings')
const LawArticlesPage                       = () => import('@/pages/lawArticlesPage/index')
const LinkBrowserPage                       = () => import('@/pages/linkBrowser/index')
const LocationsPage                         = () => import('@/pages/locationsPage/index')
const LocationTypesPage                     = () => import('@/pages/locationTypesPage/index')
const LkSubscriptions                       = () => import('@/pages/LkSubscriptions/index')

const NestedRoutesMain                      = () => import('@/pages/nestedRoutesMain')

const Numbers                               = () => import('@/pages/risksPages/tabs/numbers')
const orderAll                              = () => import('@/pages/orderPage/orderAll')
const orderDraftsUpload                     = () => import('@/pages/orderPage/orderDraftsUpload')
const orderItem                             = () => import('@/pages/orderPage/orderItem')
const orderTracking                         = () => import('@/pages/orderPage/orderTracking')
const orderDraft                            = () => import('@/pages/orderPage/orderDraft')
const orderDraftNonRegular                 = () => import('@/pages/orderPage/orderDraftNonRegualar')
// const orderItemGuest                        = () => import('@/pages/guest/index')
const orderItemGuest                        = () => import('@/pages/guest/index2')
const orderItemGuestPromo                   = () => import('@/pages/guest/promo')
const OrderPerformer                        = () => import('@/pages/orderPage/orderList/performer/index')
const OrderConfirm                          = () => import('@/pages/orderPage/orderList/confirm/index')
const OrderPayment                          = () => import('@/pages/orderPage/orderList/payment/index')
const OrderArchive                          = () => import('@/pages/orderPage/orderList/archive/index')
const OrderRisksSettingsPage                = () => import('@/pages/orderRisksSettingsPage/index')
const OrganizationPage                      = () => import('@/pages/organizationPage/organization/index')
const CardParticipant                       = () => import('@/pages/CardParticipant/index')
const OrganizationsCardConfiguratorPage     = () => import('@/pages/organizationsCardConfiguratorPage/index')
const OrganizationsCardConstructorPage      = () => import('@/pages/organizationsCardConstructorPage/index')
const OrganizationsPage                     = () => import('@/pages/organizationPage/index')
const OrderTrackingItem                     = () => import('@/pages/orderPage/orderTrackingItem')
const orderAllSimple                        = () => import("@/pages/orderPage/orderAllSimple")

const AdvancePayment                        = () => import('@/pages/fintech/advancePayment/index')

const AllAgents                             = () => import('@/pages/LogisticDomains/allAgents')
const AgentGroups                           = () => import('@/pages/LogisticDomains/groups')
const BlockedAgents                         = () => import('@/pages/LogisticDomains/blocked')

const RaylwayAgglomerations                 = () => import('@/pages/raylwayAgglomeration/raylwayAgglomeration')
const ResourceCardConstructorPage           = () => import('@/pages/resourceCardConstructorPage/index')
const ResourceMasksPage                     = () => import('@/pages/resourceMasksPage/index')
const ResourcePage                          = () => import('@/pages/resourcesPage/resource')
const ResourcesPage                         = () => import('@/pages/resourcesPage/index')
const ResourcePatternsPage                  = () => import('@/pages/resourcePatternsPage/index')
const ResourcesCardConfiguratorPage         = () => import('@/pages/resourcesCardConfiguratorPage/index')
const ResourceSpeciesPage                   = () => import('@/pages/resourceSpeciesPage/index')
const ResourceTypesPage                     = () => import('@/pages/resourceTypesPage/index')
const RisksLossCargoPage                    = () => import('@/pages/risksLossCargoPage/index')
const RisksPages                            = () => import('@/pages/risksPages/index')
const RisksWeight                           = () => import('@/pages/risksPages/tabs/weights')

const ScenarioPage                          = () => import('@/pages/tendersPage/tender/scenario/index')
const ServiceAndSecurity                    = () => import('@/pages/serviceAndSecurity')
const ServiceMeasurementUnitsPage           = () => import('@/pages/serviceMeasurementUnitsPage/index')
const ServicesPage                          = () => import('@/pages/servicesPage/index')
const ServiceTypesPage                      = () => import('@/pages/serviceTypesPage/index')

const SpotListActual                        = () => import('@/pages/spotPage/spotListActual')
const SpotListCompleted                     = () => import('@/pages/spotPage/spotListCompleted')
const SpotDraftItem                         = () => import('@/pages/spotPage/spotDraftItem/index')
const SpotDraftNewItem                      = () => import('@/pages/spotPage/spotDraftItem/spotDraftNewItem')
const SpotItem                              = () => import('@/pages/spotPage/spotItem/index')

const TaskMonitorPage                       = () => import('@/pages/taskMonitorPage/index')
const TemperatureRecordersPage              = () => import('@/pages/temperatureRecordersPage/index')
const TemperatureTransportationPage         = () => import('@/pages/temperatureTransportationPage/index')

const TenderPage                            = () => import('@/pages/tendersPage/tender/index')
const TendersContractor                     = () => import('@/pages/tendersContractorsPage/contractor')
const TendersContractors                    = () => import('@/pages/tendersContractorsPage/index')
const TendersGroup                          = () => import('@/pages/tendersGroupsPage/group')
const TendersGroups                         = () => import('@/pages/tendersGroupsPage/index')
const TendersGroupsMain                     = () => import('@/pages/tendersGroupsPage/main')
const TendersPage                           = () => import('@/pages/tendersPage/index')

const TrackingMethodsPage                   = () => import('@/pages/trackingMethodsPage/index')
const TripDuration                          = () => import('@/pages/tripDuration/index')
const TypeRegistrationsDocsPage             = () => import('@/pages/typeRegistrationsDocsPage/index')
const UserRolesPage                         = () => import('@/pages/userRolesPage/index')
const UserRolesPageNew                      = () => import('@/pages/userRolesPageNew/index')
const UsersPage                             = () => import('@/pages/usersPage/index')
const Rating                                = () => import('@/pages/rating/index')
const VerificationPage                      = () => import('@/pages/verificationPage/index')

const pageNotFound                          = () => import('@/pages/statusPages/pageNotFound')
const PageInWorking                         = () => import('@/pages/PageInWorking')
const accessDenied                          = () => import('@/pages/statusPages/accessDenied')
const serverError                           = () => import('@/pages/statusPages/serverError')
const logchainList                          = () => import('@/pages/logchainPage/logchainList')
const logchainDraftList                     = () => import('@/pages/logchainPage/logchainDraftList')
const logchainItem                          = () => import('@/pages/logchainPage/logchainItem')

const Entry                                 = () => import('@/pages/Entry')
const Login                                 = () => import('@/pages/Login')
const RegistrationPage                      = () => import('@/pages/Registration')
const ConfirmEmail                          = () => import('@/pages/ConfirmEmail')
const Restore                               = () => import('@/pages/Restore')
const ChangePassword                        = () => import('@/pages/ChangePassword')
const Settings                              = () => import('@/pages/settings')
const CommercialPolicy                      = () => import('@/pages/CommercialPolicy')
const PersonalAccountSettings               = () => import('@/pages/PersonalAccountSettings')
const SettingsDirection                     = () => import('@/pages/settings/Documents/SettingDirection/index')
const Subscriptions                         = () => import('@/pages/Subscriptions/index')
const NoticePage                            = () => import('@/pages/noticePage/index')
// const CardParticipant                       = () => import('@/pages/profile/cardParticipant')
const DownloadStorageFile                   = () => import('@/pages/DownloadStorageFile')

//test1 pages:
const RatingsPage                           = () => import('@/pages/ratingsPage/index')
const BonusesPage                           = () => import('@/pages/bonusesPage/index')
const RatingsIndexPage                      = () => import('@/pages/ratingsIndexPage/index')
const basePrices                            = () => import('@/pages/basePricesPage')
const basePricesDataImport                  = () => import('@/pages/basePricesDataImport')
const basePricesClustering                  = () => import('@/pages/basePricesClustering')
const basePricesSettings                    = () => import('@/pages/basePricesSettings')
const tracking                              = () => import('@/pages/trackingPage')
const trackingDevices                       = () => import('@/pages/trackingPage/trackingDevices')
const trackingSettings                      = () => import('@/pages/trackingPage/trackingSettings')
const DictionaryServiceReplacement          = () => import('@/pages/DictionaryServiceReplacement')
// const ScoringPage                           = () => import('@/pages/scoringPage/index')

const logDmainsAll                      = () => import('@/pages/LogisticDomains/allAgents')
const logDmainsGroups                   = () => import('@/pages/LogisticDomains/groups')
const logDmainsBlocked                  = () => import('@/pages/LogisticDomains/blocked')
const AgentCard                         = () => import('@/pages/LogisticDomains/agentCard')
const GroupPage                         = () => import('@/pages/LogisticDomains/groupPage')
const websocketsTest                    = () => import('@/pages/websocketsTest')
const TmsPage                           = () => import('@/pages/Tms/index.vue')
const NewTmsPage                        = () => import('@/pages/Tms/new.vue')
const TmsDirections                     = () => import('@/pages/Tms/components/directions.vue')
const UnauthorizedTrackingMap           = () => import('@/pages/UnauthorizedTrackingMap/index.vue')
const DictionaryContainersCompatibility = () => import('@/pages/DictionaryContainersCompatibility')
const DictionaryEventsTypes             = () => import('@/pages/DictionaryEventsTypes')

const TestPage                          = () => import('@/pages/testPage')

let routes = [
  // Registration module
  {
    path: '/entry',
    name: 'entry',
    component: Entry,
    meta: {
      title: 'Вход',
      localPage: true
    }
  },
  {
    path: '/test',
    name: 'test',
    component: TestPage,
    meta: {
      title: 'Test',
      localPage: true
    }
  },
  {
    path: '/signup',
    name: 'registration',
    component: RegistrationPage,
    meta: {
      title: 'Регистрация',
      localPage: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      title: 'Логин',
      localPage: true
    }
  },
  {
    path: '/confirm-email/:id',
    name: 'confirm_email',
    component: ConfirmEmail,
    meta: {
      title: 'Подтверждение адреса электронной почты',
      localPage: true
    }
  },
  {
    path: '/restore',
    name: 'restore',
    component: Restore,
    meta: {
      title: 'Восстановление пароля',
      localPage: true
    }
  },
  {
    path: '/password-recovery/:id',
    name: 'change-password',
    component: ChangePassword,
    meta: {
      title: 'Смена пароля',
      localPage: true
    }
  },

  {
    path: '/download-from-storage/:id',
    name: 'download-from-storage',
    component: DownloadStorageFile,
    meta: {
      title: 'Скачать файл',
      localPage: true
    }
  },
  {
    path: '/base-prices',
    name: 'base-prices',
    component: basePrices,
    meta: {
      title: 'Базовые цены',
      localPage: true
    }
  },
  {
    path: '/base-prices',
    name: 'base-prices__monitor',
    component: basePrices,
    meta: {
      title: 'Базовые цены',
      localPage: true
    }
  },
  {
    path: '/base-prices-data-import',
    name: 'base-prices__data-import',
    component: basePricesDataImport,
    meta: {
      title: 'Базовые цены - загрузка данных',
      localPage: true
    }
  },
  {
    path: '/base-prices-clustering',
    name: 'base-prices__clustering',
    component: basePricesClustering,
    meta: {
      title: 'Базовые цены - Кластеризация',
      localPage: true
    }
  },
  {
    path: '/base-prices-settings',
    name: 'base-prices__settings',
    component: basePricesSettings,
    meta: {
      title: 'Базовые цены - настройки',
      localPage: true
    }
  },
  {
    path: '/ratings',
    name: 'RatingsPage',
    component: RatingsPage,
    meta: {
      title: 'Bедение справочника Методики рейтингования',
      localPage: true
    }
  },
  {
    path: '/ratingsIndex',
    name: 'RatingIndexPage',
    component: RatingsIndexPage,
    meta: {
      title: 'Индекс Методики рейтингования',
      localPage: true
    }
  },
  {
    path: '/bonuses',
    name: 'BonusesPage',
    component: BonusesPage,
    meta: {
      title: 'Bедение справочника Методики бонусных программ',
      localPage: true
    }
  },
  {
    path: '/tenders',
    component: NestedRoutesMain,
    children: [
      {
        path: '',
        component: TendersPage,
        name: 'tenders__tenders',
        meta: {
          title: 'Тендеры',
          localPage: true
        }
      },
      {
        path: ':uuid',
        component: NestedRoutesMain,
        children: [
          {
            path: ':tab?',
            name: 'tenders__tenders__tender',
            component: TenderPage,
            meta: {
              title: 'Тендер',
              localPage: true
            }
          },
          {
            path: 'scenario/:scenarioUuid',
            name: 'tenders__tenders__scenario',
            component: ScenarioPage,
            meta: {
              title: 'Cценарий',
              localPage: true
            }
          }
        ]
      }
    ]
  },
  {
    path: '/tenders-contractors',
    component: NestedRoutesMain,
    children: [
      {
        path: '',
        component: TendersContractors,
        name: 'tenders__contractors',
        meta: {
          title: 'Контрагенты тендеров',
          localPage: true
        }
      },
      {
        path: ':uuid',
        name: 'tenders__contractors__contractor',
        component: TendersContractor,
        meta: {
          title: 'Контрагент тендеров - :uuid',
          localPage: true
        }
      }
    ]
  },
  {
    path: '/tenders-contractor-groups',
    component: TendersGroupsMain,
    children: [
      {
        path: '',
        component: TendersGroups,
        name: 'tenders__contractor_groups',
        meta: {
          title: 'Группы контрагентов',
          localPage: true
        }
      },
      {
        path: ':uuid',
        name: 'tenders__contractor_groups__group',
        component: TendersGroup,
        meta: {
          title: 'Группа контрагентов - :uuid',
          localPage: true
        }
      }
    ]
  },
  {
    path: '/risks',
    name: 'risksAnalysis',
    component: RisksPages,
    meta: {
      title: 'Настройка модели оценки риска клиентов',
      localPage: true
    },
    redirect: () => ({
      name: 'risksWeight'
    }),
    children: [
      {
        path: '/risks/weights',
        name: 'risksWeight',
        component: RisksWeight
      },
      {
        path: '/risks/numbers',
        name: 'risksNumbers',
        component: Numbers
      },
      {
        path: '/risks/deals',
        name: 'risksDeals',
        component: Deals
      }
    ]
  },
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: 'Oboz логистический цифровой оператор'
    }
  },
  {
    path: '/link-browser',
    name: 'settings__log_domains__link_browser',
    component: LinkBrowserPage,
    meta: {
      title: 'Браузер линков'
    }
  },
  {
    path: '/domain-settings',
    name: 'settings__log_domains__domain_settings',
    component: DomainSettingsModerator,
    meta: {
      title: 'Доменные настройки модератора'
    }
  },
  {
    path: '/domain-settings-exp',
    name: 'settings__log_domains__domain_settings_exp',
    component: DomainSettingsExpeditor,
    meta: {
      title: 'Доменные настройки экспедитора'
    }
  },
  {
    path: '/individual-domain-settings',
    name: 'settings__log_domains__individual_domain_settings',
    component: IndividualDomainSettingsModeratorPage,
    meta: {
      title: 'Индивидуальные настройки'
    }
  },
  {
    path: '/individual-domain-settings-exp',
    name: 'settings__log_domains__individual_domain_settings_exp',
    component: IndividualDomainSettingsExpeditor,
    meta: {
      title: 'Индивидуальные настройки экспедитора'
    }
  },
  {
    path: '/countries',
    name: 'dictionaries__common__countries',
    component: CountriesPage,
    meta: {
      title: 'Страны '
    }
  },
  {
    path: '/law-articles',
    name: 'dictionaries__sb_dictionaries__law_articles',
    component: LawArticlesPage,
    meta: {
      title: 'Статьи ГК и УК '
    }
  },
  {
    path: '/resource-species',
    name: 'dictionaries__resources__resource_species',
    component: ResourceSpeciesPage,
    meta: {
      title: 'Виды ресурсов '
    }
  },
  {
    path: '/document-types',
    name: 'dictionaries__services__document_types',
    component: DocumentTypesPage,
    meta: {
      title: 'Типы документов '
    }
  },
  {
    path: '/contractor-roles',
    name: 'settings__admin__contractor_roles',
    component: ContractorRolesPage,
    meta: {
      title: 'Роли участников '
    }
  }, {
    path: '/access',
    name: 'settings__admin__access',
    component: AccessPage,
    meta: {
      title: 'Доступы '
    }
  },
  {
    path: '/users',
    name: 'settings__admin__users',
    component: UsersPage,
    meta: {
      title: 'Пользователи '
    }
  },
  {
    path: '/all-users',
    name: 'settings__admin__all_users',
    component: AllUsersPage,
    meta: {
      title: 'Все пользователи '
    }
  },
  {
    path: '/subscriptions',
    name: 'subscriptions',
    component : LkSubscriptions,
    meta: {
      title: 'Подписки'
    }
  },
  {
    path: '/settings__admin__rating',
    name: 'settings__admin__rating',
    component: Rating,
    meta: {
      title: 'Рейтинг '
    }
  },
  {
    path: '/developing',
    name: 'DevelopingPage',
    component: DevelopingPage,
    meta: {
      title: 'В процессе разработки '
    }
  },
  {
    path: '/task-monitor',
    name: 'settings__sb_settings__task_monitor',
    component: TaskMonitorPage,
    meta: {
      title: 'Монитор задач ',
      localPage: true
    }
  },
  {
    path: '/requisite-groups',
    name: 'dictionaries__sb_dictionaries__requisite_groups',
    component: GroupsRequisitesPage,
    meta: {
      title: 'Группы реквизитов'
    }
  },
  {
    path: '/dictionaries_services',
    name: 'dictionaries__services__services_list',
    component: DictionariesServicesPage,
    meta: {
      title: 'Услуги'
    }
  },
  {
    path: '/services',
    name: 'services__services_list',
    component: ServicesPage,
    meta: {
      title: 'Сервисы '
    }
  }, {
    path: '/reg-docs',
    name: 'dictionaries__sb_dictionaries__registration_docs',
    component: TypeRegistrationsDocsPage,
    meta: {
      title: 'Типы регистрационных документов '
    }
  },
  {
    path: '/verification-date',
    name: 'dictionaries__sb_dictionaries__verification_date',
    component: VerificationPage,
    meta: {
      title: 'Сроки действия проверки '
    }
  },
  {
    path: '/organizations',
    component: NestedRoutesMain,
    children: [
      {
        path: '',
        component: OrganizationsPage,
        name: 'settings__sb_settings__organizations',
        meta: {
          title: 'Участники'
        },
      },
      {
        path: ':uuid',
        name: 'settings__sb_settings__organizations',
        component: OrganizationPage,
        meta: {
          title: 'Участник'
        }
      },
    ]
  },
  {
    path: '/participant_cards',
    name: 'settings__sb_settings__participant_cards',
    component: OrganizationsCardConfiguratorPage,
    meta: {
      title: 'Конфигуратор карточки участника '
    }
  },
  {
    path: '/resource_cards',
    name: 'settings__sb_settings__resource_cards',
    component: ResourcesCardConfiguratorPage,
    meta: {
      title: 'Конфигуратор карточки ресурса '
    }
  },
  {
    path: '/order-risks-settings',
    name: 'settings__morz__order_risks',
    component: OrderRisksSettingsPage,
    meta: {
      title: 'Матрица риска',
      localPage: true
    }
  },
  {
    path: '/trip-duration',
    name: 'settings__morz__cargo_trip_duration',
    component: TripDuration,
    meta: {
      title: 'Длительность рейса',
      localPage: true
    }
  },
  // TripDuration
  {
    path: '/order-risks-compare',
    name: 'settings__morz__compare_risks',
    component: DevelopingPage,
    meta: {
      title: 'Сопоставление рисков',
      localPage: true
    }
  },
  {
    path: '/service-measurement-units',
    name: 'dictionaries__services__service_measurement_units',
    component: ServiceMeasurementUnitsPage,
    meta: {
      title: 'Единицы измерения услуг '
    }
  },
  {
    path: '/service-types',
    name: 'dictionaries__services__service_types',
    component: ServiceTypesPage,
    meta: {
      title: 'Конфигуратор типов услуг '
    }
  },
  {
    path: '/temperature-recorders',
    name: 'dictionaries__temperature__temperature_recorders',
    component: TemperatureRecordersPage,
    meta: {
      title: 'Температурные регистраторы'
    },
  },
  {
    path: '/currencies',
    name: 'dictionaries__common__currencies',
    component: CurrenciesPage,
    meta: {
      title: 'Валюты'
    },
  },
  {

    path: '/loss-risks',
    name: 'settings__morz__loss_risks',
    component: RisksLossCargoPage,
    meta: {
      title: 'Риски утери груза '
    }
  },
  {
    path: '/cargo-types',
    name: 'dictionaries__cargo__cargo_types',
    component: CargoTypesPage,
    meta: {
      title: 'Типы грузов '
    }
  },
  {
    path: '/cargo-types',
    name: 'dictionaries__locations__cargo_types',
    component: CargoTypesPage,
    meta: {
      title: 'Типы грузов '
    }
  },
  {
    path: '/customs-unions',
    name: 'dictionaries__common__customs_unions',
    component: CustomsUnionsPage,
    meta: {
      title: 'Таможенные союзы '
    }
  },
  {
    path: '/location-types',
    name: 'dictionaries__locations__location_types',
    component: LocationTypesPage,
    meta: {
      title: 'Типы локаций '
    }
  },
  {
    path: '/locations',
    name: 'dictionaries__locations__locations_list',
    component: LocationsPage,
    meta: {
      title: 'Локации '
    }
  },
  {
    path: '/temperature-transportation-modes',
    name: 'dictionaries__temperature__temperature_transportation_modes',
    component: TemperatureTransportationPage,
    meta: {
      title: 'Температурные режимы транспортировки '
    }
  },
  {
    path: '/dangerous-goods-classes',
    name: 'dictionaries__cargo__dangerous_goods_classes',
    component: DangerousGoodsClassesPage,
    meta: {
      title: 'Классы опасных грузов '
    }
  },
  {
    path: '/etsng',
    name: 'dictionaries__cargo__etsng',
    component: EMSCodePage,
    meta: {
      title: 'Коды ЕТСНГ '
    }
  },
  {
    path: '/tnved',
    name: 'dictionaries__cargo__tnved',
    component: HSCodesPage,
    meta: {
      title: 'Коды ТН ВЭД '
    }
  },
  {
    path: '/resource-types',
    name: 'dictionaries__resources__resource_types',
    component: ResourceTypesPage,
    meta: {
      title: 'Типы ресурсов '
    }
  },
  {
    path: '/patterns',
    name: 'dictionaries__resources__patterns',
    component: ResourcePatternsPage,
    meta: {
      title: 'Шаблоны характеристик ресурсов '
    }
  },
  {
    path: '/pattern-masks',
    name: 'dictionaries__resources__pattern_masks',
    component: ResourceMasksPage,
    meta: {
      title: 'Маски ресурсов '
    }
  },
  {
    path: '/forwarding-unit',
    name: 'dictionaries__resources__forwarding_unit',
    component: ForwardingUnitPage,
    meta: {
      title: 'Единицы экспедиторского учёта '
    }
  },
  {
    path: '/tracking-methods',
    name: 'dictionaries__resources__tracking_methods',
    component: TrackingMethodsPage,
    meta: {
      title: 'Методы трекинга '
    }
  },
  {
    path: '/activities',
    name: 'dictionaries__services__activities',
    component: ActivitiesPage,
    meta: {
      title: 'Направления деятельности '
    }
  },
  {
    path: '/ext-sources',
    name: 'dictionaries__sb_dictionaries__external_sources',
    component: ServicesPage,
    meta: {
      title: 'Сервисы '
    }
  },
  {
    path: '/price-categories',
    name: 'settings__morz__price_categories',
    component: CostCategoriesPage,
    meta: {
      title: 'Категории стоимости '
    }
  },
  {
    path: '/organization-forms',
    name: 'dictionaries__sb_dictionaries__organization_forms',
    component: DevelopingPage,
    meta: {
      title: 'Формы организации'
    }
  },
  {
    path: '/okved',
    name: 'dictionaries__sb_dictionaries__okved',
    component: DevelopingPage,
    meta: {
      title: 'ОКВЭД '
    }
  },
  {
    path: '/user_roles',
    name: 'settings__admin__user_roles',
    component: UserRolesPage,
    meta: {
      title: 'Роли пользователей '
    }
  },
  {
    path: '/user_roles_new',
    name: 'settings__admin__user_roles_new',
    component: UserRolesPageNew,
    meta: {
      title: 'Роли пользователей new'
    }
  },
  {
    path: '/participant_fields',
    name: 'settings__sb_settings__participant_fields',
    component: OrganizationsCardConstructorPage,
    meta: {
      title: 'Состав карточки участника '
    }
  },
  {
    path: '/resource_fields',
    name: 'settings__sb_settings__resource_fields',
    component: ResourceCardConstructorPage,
    meta: {
      title: 'Состав карточки ресурса '
    }
  },
  {
    path: '/developing',
    name: 'settings__sb_settings__developing',
    component: DevelopingPage,
    meta: {
      title: 'В процессе разработки ',
      localPage: true
    }
  },
  {
    path: '/order',
    name: 'order',
    redirect: {
      name: 'order__performerer'
    }
  },
  {
    path: '/order__drafts',
    name: 'order__drafts',
    component: orderAll,
    meta: {
      title: 'Черновики',
      guestOnly: true,
      localPage: true
    },

    children: [
      {
        path: 'upload',
        name: 'order__drafts_upload',
        component: orderDraftsUpload
      }
    ]
  },
  {
    path: '/order-list-from',
    name: 'order__list_from',
    component: OrderPerformer,
    meta: {
      title: 'Входящие заказы',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order__performerer',
    name: 'order__performerer',
    component: OrderPerformer,
    meta: {
      title: 'Исполнение',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-all',
    name: 'orders__all__orders',
    component: OrderPerformer,
    meta: {
      title: 'Все заказы',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order__confirm_performer',
    name: 'order__confirm_performer',
    component: OrderConfirm,
    meta: {
      title: 'Подтверждение исполнения',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order__payment',
    name: 'order__payment',
    component: OrderPayment,
    meta: {
      title: 'Оплата',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order__archive',
    name: 'order__archive',
    component: OrderArchive,
    meta: {
      title: 'Архив',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-list-to',
    name: 'order__list_to',
    component: OrderPerformer,
    meta: {
      title: 'Исходящие заказы',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-list-boo',
    name: 'order__list_to',
    component: OrderPerformer,
    meta: {
      title: 'Заказы в работе',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-item/:id',
    name: 'order__item',
    component: orderItem,
    meta: {
      title: 'Заказ',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-tracking/new-order',
    name: 'order-tracking',
    component: orderTracking,
    meta: {
      title: 'Трекиновый заказ',
      localPage: true
    }
  },
  {
    path: '/order-tracking-item/:id',
    name: 'order-tracking-item',
    component: OrderTrackingItem,
    meta: {
      title: 'Карточка трекингового заказа',
      localPage: true
    }
  },
  {
    path: '/order-drafts-item/:id',
    name: 'order__drafts__item',
    component: orderDraft,
    meta: {
      title: 'Заказ',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-drafts/new-nonregular-order',
    name: 'new__nonregular__order',
    component: orderDraftNonRegular
  },
  {
    path: '/order-item-guest/:id',
    name: 'order-item-guest ',
    component: orderItemGuest,
    meta: {
      title: 'Заказ',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-item-guest-promo',
    name: 'order-item-guest-promo ',
    component: orderItemGuestPromo,
    meta: {
      title: 'Заказ',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/order-drafts-item/transportation',
    name: 'order__new_order__transportation',
    redirect: '/order-drafts-item/new-order'
  },
  {
    path: 'order-drafts-item/nonregular_order',
    name: 'order__new_order__nonregular_order',
    redirect: '/order-drafts/new-nonregular-order'
  },
  {
    path: '/order-drafts-item/tracking',
    name: 'order__new_order__tracking',
    redirect: '/order-tracking/new-order'
  },
  {
    path: '/order-new-order/:id',
    name: 'order__new_order',
    component: orderDraft,
    meta: {
      title: 'Заказ',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/contracts',
    name: 'contracts',
    redirect: {
      name: 'contracts__draft'
    }
  },
  {
    path: '/contracts__list-signing-from',
    name: 'contracts__list-signing-from',
    component: ContractsList,
    meta: {
      title: 'Договор - входящие на подпись'
    }
  },
  {
    path: '/contracts__list-signing-to',
    name: 'contracts__list-signing-to',
    component: ContractsList,
    meta: {
      title: 'Договор - отправленные на подпись'
    }
  },
  {
    path: '/contracts__list-signed',
    name: 'contracts__list-signed',
    component: ContractsList,
    meta: {
      title: 'Договор - подписанные'
    }
  },
  {
    path: '/contracts-draft',
    name: 'contracts__draft',
    component: ContractsDraft,
    meta: {
      title: 'Черновики',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/contract/:id',
    name: 'contracts__detail',
    component: ContractDetail,
    meta: {
      title: 'Договор',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/contracts-draft/new',
    name: 'contracts__new',
    redirect: {
      name: 'contracts__new__transportation'
    }
  },
  {
    path: '/contracts-draft/transportation',
    name: 'contracts__new__transportation',
    component: ContractsDraft,
    meta: {
      title: 'Договор',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/contracts-draft/tracking',
    name: 'contracts__new__tracking',
    component: ContractsDraft,
    meta: {
      title: 'Договор',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/contracts-draft/transportation_parent_contract',
    name: 'contracts__new__transportation_parent_contract',
    component: ContractsDraft,
    meta: {
      title: 'Договор',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/all-components',
    name: 'all_components',
    component: AllComponents,
    meta: {
      title: 'Все компоненты',
      localPage: true
    }
  },
  {
    path: '/requests',
    name: 'requests',
    redirect: {
      name: 'requests__search-orders'
    },
    component: AccountPage,
    meta: {
      title: 'ЛК перевозчика',
      localPage: true
    }
  },
  {
    path: '/requests__search-orders',
    name: 'requests__search-orders',
    component: AccountPage,
    meta: {
      title: 'Поиск заказов',
      localPage: true
    }
  },
  {
    path: '/requests__assigning_resources',
    name: 'requests__assigning_resources',
    component: AccountPage,
    meta: {
      title: 'Назначение ресурсов',
      localPage: true
    }
  },
  {
    path: '/requests__executed-orders',
    name: 'requests__executed-orders',
    component: ExecutedOrders,
    meta: {
      title: 'Исполняемые заказы',
      localPage: true
    }
  },
  {
    path: '/requests__order-archive',
    name: 'requests__order-archive',
    component: ArchiveOrders,
    meta: {
      title: 'Архив заказов',
      localPage: true
    }
  },
  {
    path: '/spot',
    name: 'spot',
    redirect: {
      name: 'spot__list'
    }
  },
  {
    path: '/spot-list',
    name: 'spot__list',
    component: SpotListActual,
    meta: {
      title: 'Спот аукцион актуальные'
    }
  },
  {
    path: '/spot-list-completed',
    name: 'spot__list_completed',
    component: SpotListCompleted,
    meta: {
      title: 'Спот аукцион завершенные'
    }
  },
  {
    path: '/spot-list-draft',
    name: 'spot__list_draft',
    component: SpotListCompleted,
    meta: {
      title: 'Спот'
    }
  },
  {
    path: '',
    component: SpotItem,
    meta: {
      title: 'Спот',
      guestOnly: true,
      localPage: true
    },
    children: [
      {
        path: '/spot-item/:uuid', // add_new_spot
        name: 'spot__item',
        component: SpotItem,
        meta: {
          title: 'Спот'
        }
      },
    ]
  },
  {
    path: '/spot-draft-item',
    name: 'spot__draft__item',
    redirect: {
      name: 'spot__draft__new__item'
    },
    meta: {
      title: 'Спот',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/spot-draft-item',
    name: 'spot__draft__new__item',
    component: SpotDraftNewItem,
    meta: {
      title: 'Черновик',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/spot-draft-item/:uuid',
    name: 'spot__draft__item__final',
    component: SpotDraftItem,
    meta: {
      title: 'Черновик',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/service-and-security',
    name: 'dictionaries__common__service_and_security',
    component: ServiceAndSecurity,
    meta: {
      title: 'Требования к сервису и безопасности',
      localPage: true
    },
  },
  // todo в дальнейшем удалить  данные ссылки
  // {
  //   path: '/scoring',
  //   name: 'scoring',
  //   component: ScoringPage,
  //   meta: {
  //     title: 'Скоринг',
  //   },
  // },
  {
    path: '/logchain-list',
    name: 'logchain__list',
    component: logchainList,
    meta: {
      title: 'Браузер логцепочек',
      localPage: true
    }
  },
  {
    path: '/logchain-draft-list',
    name: 'logchain__draft-list',
    component: logchainDraftList,
    meta: {
      title: 'Браузер черновиков логцепочек',
      localPage: true
    }
  },
  {
    path: '/logchain-item/:id',
    name: 'logchain_item',

    component: logchainItem,
    meta: {
      title: 'Логцепочка',
      localPage: true
    }
  },
  // todo удалить комментарий, новая версия логцепочек
  {
    path: '/logchain',
    name: 'logchain',
    component: PageInWorking, //logchainList,
    redirect: {
      name: 'logchain__list'
    },
    meta: {
      title: 'Логцепочки',
      localPage: true
    }
  },
  {
    path: '/logchain__my_logchain',
    name: 'logchain__my_logchain',
    component: PageInWorking, //logchainList,
    meta: {
      title: 'Мои логцепочки',
      localPage: true
    }
  },
  {
    path: '/logchain__library',
    name: 'logchain__library',
    component: PageInWorking, //logchainDraftList,
    meta: {
      title: 'Библиотека',
      localPage: true
    }
  },
  {
    path: '/rating',
    name: 'rating',
    component: Rating,
    meta: {
      title: 'Рейтинг'
    }
  },
  {
    path: '/settings__admin__settings',
    name: 'settings__admin__settings',
    component: Settings,
    meta: {
      title: 'Настройки'
    }
  },
  {
    path: '/profile',
    name: 'profile',
    redirect: {
      name: 'profile__card_participent'
    }
  },
  {
    path: '/profile__card_participent',
    name: 'profile__card_participent',
    component: CardParticipant,
    meta: {
      title: 'Карточка участника',
      localPage: true
    }
  },
  {
    path: '/profile__rating',
    name: 'profile__rating',
    component: Rating,
    meta: {
      title: 'Рейтинг',
      localPage: true
    }
  },
  {
    path: '/profile__users',
    name: 'profile__users',
    component: UsersPage,
    meta: {
      title: 'Пользователи',
      localPage: true
    }
  },
  {
    path: '/profile__notice',
    name: 'profile__notice',
    component: NoticePage,
    meta: {
      title: 'Уведомления',
      localPage: true
    }
  },
  {
    path: '/profile__setting',
    name: 'profile__setting',
    component: Settings,
    meta: {
      title: 'Настройки',
      localPage: true
    }
  },
  {
    path: '/commercial__policy',
    name: 'commercial__policy',
    component: CommercialPolicy,
    meta: {
      title: 'Коммерческая политика',
      localPage: true
    }
  },
  {
    path: '/personal__account__settings',
    name: 'personal__account__settings',
    component: PersonalAccountSettings,
    meta: {
      title: 'Настройки ЛК',
      localPage: true
    }
  },
  {
    path: '/profile__setting/:uuid',
    name: 'profile__setting',
    component: SettingsDirection,
    meta: {
      title: 'Настройки',
      localPage: true
    }
  },
  {
    path: '/profile__notifications',
    name: 'profile__notifications',
    component: NoticePage,
    meta: {
      title: 'Уведомления',
      localPage: true
    }
  },
  {
    path: '/profile__subscriptions',
    name: 'profile__subscriptions',
    component: Subscriptions,
    meta: {
      title: 'Подписки',
      localPage: true
    }
  },
  {
    path: '/log_domains',
    name: 'log_domains',
    component: PageInWorking,
    meta: {
      title: 'Логдомены',
      localPage: true
    }
  },
  // Resources routes for "Moderator"
  // Difference in components names and url
  // "Career" top-menu navigation should use "Moderator" components names in future
  {
    path: '/sb_resource',
    component: NestedRoutesMain,
    children: [
      {
        path: '',
        component: ResourcesPage,
        name: 'settings__sb_settings__sb_resource',
        meta: {
          title: 'Ресурсы'
        },
      },
      {
        path: ':uuid',
        name: 'settings__sb_settings__sb_resource',
        component: ResourcePage,
        meta: {
          title: 'Ресурс'
        }
      },
    ]
  },
  // Resources routes for "Carrier"
  // Difference in components names and url
  // "Careier" top-menu navigation should use "Moderator" components names in future
  {
    path: '/resources',
    component: NestedRoutesMain,
    children: [
      {
        path: '',
        component: ResourcesPage,
        name: 'resources',
        meta: {
          title: 'Ресурсы'
        },
      },
      {
        path: ':uuid',
        name: 'resources_resource',
        component: ResourcePage,
        meta: {
          title: 'Ресурс'
        }
      },
    ]
  },

  {
    path: '/logdomains',
    name: 'logistic__domains',
    component: logDmainsAll,
    meta: {
      title: 'Логистические домены | Все контрагенты',
      localPage: true
    }
  },
  {
    path: '/logdomains/groups',
    name: 'logistic__domains__groups',
    component: logDmainsGroups,
    meta: {
      title: 'Логистические домены | Группы',
      localPage: true
    }
  },
  {
    path: '/logdomains/blocked',
    name: 'logistic__domains__blocked',
    component: logDmainsBlocked,
    meta: {
      title: 'Логистические домены | Заблокированные контрагенты',
      localPage: true
    }
  },
  {
    path: '/logdomains/agent/:id',
    name: 'logistic_domains_agent_info',
    component: AgentCard,
    meta: {
      title: 'Логистические домены | Контрагент',
      guestOnly: true,
      localPage: true
    },
  },
  {
    path: '/logdomains/group/:id',
    name: 'logistic_domains_group_info',
    component: GroupPage,
    meta: {
      title: 'Логистические домены | Группа',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/tms',
    name: 'tms_page',
    component: TmsPage,
    meta: {
      title: 'ТМС',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/tms/:id',
    name: 'new_tms_page',
    component: NewTmsPage,
    meta: {
      title: 'ТМС | Новая политика',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/tms/:id/directions',
    name: 'tms_directions',
    component: TmsDirections,
    meta: {
      title: 'ТМС | Распределение по направлениям',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/websockets-test',
    name: 'websocketsTets',
    component: websocketsTest,
    meta: {
      title: 'Тест веб-сокетов',
      localPage: true
    }
  },
  {
    path: '/browser_link',
    name: 'browser_link',
    redirect: {
      name: 'browser_link__all-contactors'
    }
  },
  {
    path: '/browser_link__all-contactors',
    name: 'browser_link__all-contactors',
    component: AllAgents,
    meta: {
      title: 'Все контрагенты',
      localPage: true
    }
  },
  {
    path: '/browser_link__groups',
    name: 'browser_link__groups',
    component: AgentGroups,
    meta: {
      title: 'Группы',
      localPage: true
    }
  },
  {
    path: '/browser_link__groups/group/:id',
    name: 'browser_link__group_info',
    component: GroupPage,
    meta: {
      title: 'Группа',
      guestOnly: true,
      localPage: true
    }
  },
  {
    path: '/browser_link__blocked',
    name: 'browser_link__blocked',
    component: BlockedAgents,
    meta: {
      title: 'Заблокированные контрагенты',
      localPage: true
    }
  },
  {
    path: '/fintech',
    name: 'fintech',
    redirect: {
      name: 'fintech__advance-payment'
    }
  },
  {
    path: '/fintech__advance-payment',
    name: 'fintech__advance-payment',
    component: AdvancePayment,
    meta: {
      title: 'Авансирование',
      localPage: true
    }
  },
  {
    path: "/order__simple",
    name: "order__simple",
    component: orderAllSimple,
    meta: {
      title: "Черновики",
      guestOnly: true,
      localPage: true,
    }
  },
  {
    path: "/order__client_desktop",
    name: "order__client_desktop",
    component: orderAllSimple,
    meta: {
      title: "Рабочий стол клиента",
      guestOnly: true,
      localPage: true,
    }
  },
  {
    path: "/unauthorized-tracking-map/:uuid",
    name: "unauthorized-tracking-map",
    component: UnauthorizedTrackingMap,
    meta: {
      title: "Просмотр карты трекингового заказа",
      guestOnly: true,
      localPage: true,
    }
  },
  {
    path: "/tracking",
    name: "tracking",
    component: tracking,
    meta: {
      title: "Трекинг",
    },
    children: [
      {
        path: '',
        component: trackingDevices,
        name: 'trackingDevices',
        meta: {
          title: 'Бортовые блоки'
        },
      },
      {
        path: 'settings',
        name: 'trackingSettings',
        component: trackingSettings,
        meta: {
          title: 'Телефоны'
        }
      },
    ]
  },
  {
    path: '/service-replacement',
    name: 'dictionaries__services__replaceability',
    component: DictionaryServiceReplacement,
    meta: {
      title: 'Настройки заменяемости услуг'
    }
  },
  {
    path: '/containers-compatibility',
    name: 'dictionaries__locations__containers__compatibility',
    component: DictionaryContainersCompatibility,
    meta: {
      title: 'Совместимость для контейнеров'
    }
  },
  {
    path: '/raylway-agglomerations',
    name: 'dictionaries__locations__containers__agglomeration',
    component: RaylwayAgglomerations,
    meta: {
      title: 'Агломерация для контейнеров'
    }
  },
  {
    path: '/events-types',
    name: 'dictionaries__common__event_types',
    component: DictionaryEventsTypes,
    meta: {
      title: 'Типы событий'
    }
  }
]

const router = new Router({
  mode: 'history',
  base: '/',
  linkActiveClass: 'is-active',
  routes: [
    ...routes,
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '*',
      redirect: { name: 'not_found' }
    },
    {
      path: '/404',
      name: 'not_found',
      component: pageNotFound
    },
    {
      path: '/403',
      name: 'access_denied',
      component: accessDenied
    },
    {
      path: '/500',
      name: 'server_error',
      component: serverError
    }
  ]
})

const hideMenuRoutes = ['entry', 'registration', 'login', 'confirm_email', 'restore', 'change-password', 'home']
const body = document.getElementsByTagName('body')[0]

router.beforeEach(async(to, from, next) => {
  body.setAttribute('renderFinish', false);
  sessionStorage.numOpenConnections = 0;
  sessionStorage.initRest = 0;

  store.commit('user/setHistoryPage', from);

  if (store.getters['user/historyPage'].name) {
    sessionStorage.clear();
    const route = {
      name: from.name,
      query: from.query
    };
    sessionStorage.setItem('route', JSON.stringify(route));
  }

  if (!getCookie('auth_token') && to && to.name && !hideMenuRoutes.includes(to.name)) {
    if (to.path === '/order-item-guest/new-order' || to.path === '/order-item-guest-promo') {
      await store.dispatch('user/setAnonUser')
      next()
    } else if (to.name === 'unauthorized-tracking-map') {
      next()
    } else {
      router.push({ name: 'entry' })
    }
  } else if (to && from && to.name === from.name) {
    next()
  } else {
    // const role = store.getters['user/role']
    // if (role === 'anonymous') {
    //   router.push({ name: 'entry' })
    // }
    const nearestWithTitle = to.matched.slice().reverse().find(route => route.meta && route.meta.title)
    if (nearestWithTitle) {
      document.title = nearestWithTitle.meta.title
    }

    Promise.resolve().then(async () => {
      await window.delay(50);
      next();
    });

    // i18n.loadLanguageAsync({ lang: 'ru', page: to.name }).then(async () => {
    // store.dispatch('functions/setCurrentCode', to.name)
    // await window.delay(50)
    // next()
    // })
  }
})

function generateDataQa(elementName, to) {
  const elements = document.getElementsByTagName(elementName)
  const filteredElements = []

  for (let i=0; i<elements.length; i++) {
    if (!elements.item(i).dataset.qa) filteredElements.push(elements.item(i))
  }
  if (!filteredElements.length) return

  filteredElements.forEach((el, idx) => {
    el.dataset.qa = `${elementName}__${to}__${idx}`
  })
}

router.afterEach((to) => {
  function init() {
    if(!sessionStorage.initRest)sessionStorage.initRest=0
    if (
            sessionStorage.initRest &&
            Number(sessionStorage.numOpenConnections) === 0 &&
            to.path !== '/contracts-draft/transportation'
    ) {
      generateDataQa('input', to.name)
      generateDataQa('button', to.name)
      generateDataQa('textarea', to.name)
      generateDataQa('select', to.name)

      document.getElementsByTagName('body')[0].setAttribute('renderFinish', true);
    } else {
      setTimeout(init, 1000)
    }
  }
  setTimeout(init,1000)
})

export default router
