import Vue from 'vue';
import store from '../store/index'
import Router from 'vue-router';
import CountriesPage from '../pages/countriesPage/index'
import CustomsUnionsPage from '../pages/customsUnionsPage/index'
import CurrenciesPage from '../pages/currenciesPage/index'
import LawArticlesPage from '../pages/lawArticlesPage/index'
import LocationTypesPage from '../pages/locationTypesPage/index'
import LocationsPage from '../pages/locationsPage/index'
import TemperatureRecordersPage from '../pages/temperatureRecordersPage/index'
import TemperatureTransportationPage from '../pages/temperatureTransportationPage/index'
import CargoTypesPage from '../pages/cargoTypesPage/index'
import DangerousGoodsClassesPage from '../pages/dangerousGoodsClassesPage/index'
import EMSCodePage from '../pages/emsCodePage/index'
import HSCodesPage from '../pages/hsCodesPage/index'
import ResourceTypesPage from '../pages/resourceTypesPage/index'
import ResourceSpeciesPage from '../pages/resourceSpeciesPage/index'
import ResourcePatternsPage from '../pages/resourcePatternsPage/index'
import ResourceMasksPage from '../pages/resourceMasksPage/index'
import ForwardingUnitPage from '../pages/forwardingUnitPage/index'
import TrackingMethodsPage from '../pages/trackingMethodsPage/index'
import AttendancePage from '../pages/attendancePage/index'
import ServicesPage from '../pages/servicesPage/index'
import ServiceTypesPage from '../pages/serviceTypesPage/index'
import ServiceMeasurementUnitsPage from '../pages/serviceMeasurementUnitsPage/index'
import DocumentTypesPage from '../pages/documentTypesPage/index'
import ActivitiesPage from '../pages/activitiesPage/index'
import ContractorRolesPage from '../pages/contractorRolesPage/index'
import AccessPage from '../pages/accessPage/index'
import UserRolesPage from '../pages/userRolesPage/index'
import UsersPage from '../pages/usersPage/index'
import AllUsersPage from '../pages/allUsersPage/index'
import DevelopingPage from '../pages/developingPage/index'
import TaskMonitorPage from '../pages/taskMonitorPage/index'
import ResourcesPage from '../pages/resourcesPage/index'
import OrganizationsCardConfiguratorPage from '../pages/organizationsCardConfiguratorPage/index'
import ResourcesCardConfiguratorPage from '../pages/resourcesCardConfiguratorPage/index'
import GroupsRequisitesPage from '../pages/groupsRequisitesPage/index'
import TypeRegistrationsDocsPage from '../pages/typeRegistrationsDocsPage/index'
import VerificationPage from '../pages/verificationPage/index'
import CostCategoriesPage from '../pages/costCategoriesPage/index'
import RisksLossCargoPage from '../pages/risksLossCargoPage/index'
import OrganizationPage from '../pages/organizationPage/index'
import OrganizationsCardConstructorPage from '../pages/organizationsCardConstructorPage/index'
import ResourceCardConstructorPage from '../pages/resourceCardConstructorPage/index'
import LogisticDomainsForwardersPage from '../pages/logisticDomainsForwardersPage/index'
import LogisticCustomerSupplierDomainsPage from '../pages/logisticCustomerSupplierDomainsPage/index'
import LogisticDomainsModeratorPage from '../pages/logisticDomainsModeratorPage/index'

Vue.use(Router);

let router = new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'is-active',
    routes: [
        {
            path: '/',
            name: 'oboz',
            meta: {
                title: 'Oboz'
            },
            redirect: () => ({
                name: 'countriesPage'
            })
        },
        {
            path: '/countries',
            name: 'CountriesPage',
            component: CountriesPage,
            meta: {
                title: 'Страны -'
            }
        },
        {
            path: '/customs-unions',
            name: 'CustomsUnionsPage',
            component: CustomsUnionsPage,
            meta: {
                title: 'Таможенные союзы -'
            }
        },
        {
            path: '/currencies',
            name: 'CurrenciesPage',
            component: CurrenciesPage,
            meta: {
                title: 'Валюты -'
            }
        },
        {
            path: '/law-articles',
            name: 'LawArticlesPage',
            component: LawArticlesPage,
            meta: {
                title: 'Статьи ГК и УК -'
            }
        },
        {
            path: '/location-types',
            name: 'LocationTypesPage',
            component: LocationTypesPage,
            meta: {
                title: 'Типы локаций -'
            }
        },
        {
            path: '/locations',
            name: 'LocationsPage',
            component: LocationsPage,
            meta: {
                title: 'Локации -'
            }
        }, {
            path: '/temperature-recorders',
            name: 'TemperatureRecordersPage',
            component: TemperatureRecordersPage,
            meta: {
                title: 'Температурные регистраторы -'
            }
        }, {
            path: '/temperature-transportation',
            name: 'TemperatureTransportationPage',
            component: TemperatureTransportationPage,
            meta: {
                title: 'Температурные режимы транспортировки -'
            }
        },
        {
            path: '/cargo-types',
            name: 'CargoTypesPage',
            component: CargoTypesPage,
            meta: {
                title: 'Типы грузов -'
            }
        },
        {
            path: '/cargo-types',
            name: 'DangerousGoodsClassesPage',
            component: DangerousGoodsClassesPage,
            meta: {
                title: 'Классы опасных грузов -'
            }
        },
        {
            path: '/emc-code',
            name: 'EMSCodePage',
            component: EMSCodePage,
            meta: {
                title: 'Коды ЕТСНГ -'
            }
        },
        {
            path: '/hs-code',
            name: 'HSCodesPage',
            component: HSCodesPage,
            meta: {
                title: 'Коды ТН ВЭД -'
            }
        },

        {
            path: '/resource-types',
            name: 'ResourceTypesPage',
            component: ResourceTypesPage,
            meta: {
                title: 'Типы ресурсов -'
            }
        },
        {
            path: '/resource-species',
            name: 'ResourceSpeciesPage',
            component: ResourceSpeciesPage,
            meta: {
                title: 'Виды ресурсов -'
            }
        },

        {
            path: '/resource-patterns',
            name: 'ResourcePatternsPage',
            component: ResourcePatternsPage,
            meta: {
                title: 'Шаблоны характеристик ресурсов -'
            }
        },

        {
            path: '/resource-masks',
            name: 'ResourceMasksPage',
            component: ResourceMasksPage,
            meta: {
                title: 'Маски ресурсов -'
            }
        }, {
            path: '/forwarding-unit',
            name: 'ForwardingUnitPage',
            component: ForwardingUnitPage,
            meta: {
                title: 'Единицы экспедиторского учета -'
            }
        }, {
            path: '/tracking-methods',
            name: 'TrackingMethodsPage',
            component: TrackingMethodsPage,
            meta: {
                title: 'Методы трекинга -'
            }
        },
        {
            path: '/attendance',
            name: 'AttendancePage',
            component: AttendancePage,
            meta: {
                title: 'Услуги -'
            }
        },
        {
            path: '/service-types',
            name: 'ServiceTypesPage',
            component: ServiceTypesPage,
            meta: {
                title: 'Типы услуг -'
            }
        },

        {
            path: '/service-measurement-units',
            name: 'ServiceMeasurementUnitsPage',
            component: ServiceMeasurementUnitsPage,
            meta: {
                title: 'Единицы измерения -'
            }
        },

        {
            path: '/document-types',
            name: 'DocumentTypesPage',
            component: DocumentTypesPage,
            meta: {
                title: 'Типы документов -'
            }
        },

        {
            path: '/activities',
            name: 'ActivitiesPage',
            component: ActivitiesPage,
            meta: {
                title: 'Направления деятельности -'
            }
        },
        {
            path: '/contractor-roles',
            name: 'ContractorRolesPage',
            component: ContractorRolesPage,
            meta: {
                title: 'Роли участников -'
            }
        }, {
            path: '/access',
            name: 'AccessPage',
            component: AccessPage,
            meta: {
                title: 'Доступы -'
            }
        }, {
            path: '/user-roles',
            name: 'UserRolesPage',
            component: UserRolesPage,
            meta: {
                title: 'Роли пользователей -'
            }
        }, {
            path: '/users',
            name: 'UsersPage',
            component: UsersPage,
            meta: {
                title: 'Пользователи -'
            }
        }, {
            path: '/all-users',
            name: 'AllUsersPage',
            component: AllUsersPage,
            meta: {
                title: 'Все пользователи -'
            }
        },  {
            path: '/developing',
            name: 'DevelopingPage',
            component: DevelopingPage,
            meta: {
                title: 'В процессе разработки -'
            }
        },
        {
            path: '/task-monitor',
            name: 'TaskMonitorPage',
            component: TaskMonitorPage,
            meta: {
                title: 'Монитор задач -'
            }
        },
        {
            path: '/resources',
            name: 'ResourcesPage',
            component: ResourcesPage,
            meta: {
                title: 'Ресурсы -'
            }
        },
        {
            path: '/organization-card-configurator',
            name: 'OrganizationsCardConfiguratorPage',
            component: OrganizationsCardConfiguratorPage,
            meta: {
                title: 'Конфигуратор карточки участника -'
            }
        },
        {
            path: '/resources-card-configurator',
            name: 'ResourcesCardConfiguratorPage',
            component: ResourcesCardConfiguratorPage,
            meta: {
                title: 'Конфигуратор карточки ресурса'
            }
        },
        {
            path: '/groups-requisites',
            name: 'GroupsRequisitesPage',
            component: GroupsRequisitesPage,
            meta: {
                title: 'Группы реквизитов-'
            }
        }, {
            path: '/services',
            name: 'ServicesPage',
            component: ServicesPage,
            meta: {
                title: 'Сервисы -'
            }
        }, {
            path: '/type-registrations-docs',
            name: 'TypeRegistrationsDocsPage',
            component: TypeRegistrationsDocsPage,
            meta: {
                title: 'Типы регистрационных документов -'
            }
        },
        {
            path: '/verification',
            name: 'VerificationPage',
            component: VerificationPage,
            meta: {
                title: 'Сроки действия проверки -'
            }
        }, {
            path: '/cost-categories',
            name: 'CostCategoriesPage',
            component: CostCategoriesPage,
            meta: {
                title: 'Категории стоимости -'
            }
        }, {
            path: '/risks-loss-cargo',
            name: 'RisksLossCargoPage',
            component: RisksLossCargoPage,
            meta: {
                title: 'Риски утери груза -'
            }
        },

        {
            path: '/organizations',
            name: 'OrganizationPage',
            component: OrganizationPage,
            meta: {
                title: 'Участники -'
            }
        },
        {
            path: '/organization-card-constructor',
            name: 'OrganizationsCardConstructorPage',
            component: OrganizationsCardConstructorPage,
            meta: {
                title: 'Конструктор карточки участника -'
            }
        },
        {
            path: '/resource-card-constructor',
            name: 'ResourceCardConstructorPage',
            component: ResourceCardConstructorPage,
            meta: {
                title: 'Конструктор карточки ресурса -'
            }
        },
        /* TODO-frontend: Добавить роутинг из проектов: logistic-domain-panel, oboz-one-auth */
        {
            path: '*',
            redirect: {name: 'testpage'},
        },
    ]
});


router.beforeEach((to, from, next) => {
    store.dispatch('changeLocale', {lang: store.getters.getCurrentLocale, page: to.name});
    next();
});

export default router;
