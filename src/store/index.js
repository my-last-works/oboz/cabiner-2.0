import Vue from 'vue'
import Vuex from 'vuex'
// import i18n from '../translations/lang'
import modules from './modules'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentLocale: 'ru',
    $notification: null
  },
  mutations: {
    SET_LOCALE (state, payload) {
      state.currentLocale = payload
    },
    setNotification (state, payload) {
      state.$notification = payload
    }
  },
  getters: {
    getCurrentLocale: state => {
      return state.currentLocale
    },
    $notification (state) {
      return state.$notification
    }
  },
  actions: {
    // changeLocale ({ commit }, payload) {
    //   i18n.loadLanguageAsync(payload).then(() => {
    //     commit('SET_LOCALE', payload.lang)
    //   })
    // }
  },
  modules
})
