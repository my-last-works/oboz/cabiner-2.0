import api from '../../api'
// import data from '../../resources/data';

export const GET_PARAMS = 'GET_PARAMS'
export const SET_PARAMS = 'SET_PARAMS'
export const CREATE_PARAMETER ='CREATE_PARAMETER'
export const UPDATE_PARAMETER ='UPDATE_PARAMETER'
export const DELETE_PARAMETER ='DELETE_PARAMETER'

export default {
 state: {
    list: []
  },
  getters: {
    getParams: state => {
      return state.list
    }
  },
  actions: {
    [GET_PARAMS]: async ({ commit }, uuid) => {
      try {
        const res = await api.parameter.fetchParametersOfMethodology(uuid)
        commit(SET_PARAMS, res)
      } catch (ex) {
        console.log(ex)
      }
    },
    [CREATE_PARAMETER]: async ({ dispatch }, { uuid, payload }) => {
      try {
        const res = await api.parameter.createValueParameterOfMethodology(uuid, payload)
        dispatch(GET_PARAMS, uuid)
        return res
      } catch (ex) {
        console.log(ex)
      }
    },
    [UPDATE_PARAMETER]: async ({ dispatch }, payload) => {
        try {
            const res = await api.parameter.updateValueParameterOfMethodology(
                payload.methodology_uuid,
                payload.parameter_value_uuid,
                payload.value
            )
            dispatch(GET_PARAMS, payload.methodology_uuid)
            return res
        } catch (ex) {
            console.log(ex)
        }
    },
    [DELETE_PARAMETER]: async ({ dispatch }, payload) => {
      try {
        const res = await api.parameter.deleteValueParameterOfMethodology(payload.methodology_uuid, payload.parameter_uuid)
        dispatch(GET_PARAMS, payload.methodology_uuid)
        return res
      } catch (ex) {
        console.log(ex)
      }
    }
  },
  mutations: {
    [SET_PARAMS]: (state, res) => {
      state.list = res
    }
  }
}
