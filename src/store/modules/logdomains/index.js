export default {
  namespaced: true,

  state: {
    generalInfo: {
      email: '',
      fullTitle:'',
      inn:'',
      kpp:'',
      legalAddress:'',
      linkType:'',
      ogrn:'',
      okpo:'',
      participantStats: {
        contracts: [],
        groups: [],
        ownerOrders: 0,
        resources: [],
        totalOrders: 0
      },
      phone: '',
      rating: '',
      riskColor: '',
      roleCode: '',
      scoring: '',
      title: '',
      uuid: ''
    }
  },

  getters: {},

  mutations: {
    SET_GENERAL_INFO(state, info) {
      state.generalInfo = info;
    }
  },

  actions: {
    setGeneralInfo({ commit }, info) {
      commit('SET_GENERAL_INFO', info);
    }
  }
}
