import api from '../../api'
// import data from '../../resources/data';

export const GET_THRESHOLD = 'GET_THRESHOLD'
export const SET_THRESHOLD = 'SET_THRESHOLD'
export const UPDATE_THRESHOLD ='UPDATE_THRESHOLD'

export default {
 state: {
    currentThreshold: {}
  },
  getters: {
    getCurrentThreshold: state => {
      return state.currentThreshold
    }
  },
  actions: {
    [GET_THRESHOLD]: async ({ commit }, uuid) => {
      try {
        const res = await api.threshold.fetchRagingThresholdOfMethodology(uuid)
        // const res = data.threshold
        commit(SET_THRESHOLD, res)
      } catch (ex) {
        console.log(ex)
      }
    },
    [UPDATE_THRESHOLD]: async ({ dispatch }, { uuid, payloadThreshold }) => {
      try {
        await api.threshold.updateRagingThresholdOfMethodology(uuid, payloadThreshold)
        dispatch(GET_THRESHOLD, uuid)
      } catch (ex) {
        console.log(ex)
      }
    }
  },
  mutations: {
    [SET_THRESHOLD]: (state, res) => {
      state.currentThreshold = res
    }
  }
}
