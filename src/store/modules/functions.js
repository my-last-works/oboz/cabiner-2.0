import api from '@/api'

export default {
  namespaced: true,
  state: {
    funcs: {},
    currentCode: '',
    isLoading: true,
    topMenu: [],
    functionalRoles: null
  },
  getters: {
    functions (state) {
      return state.funcs
    },
    functionalRoles (state) {
      return state.functionalRoles
    },
    read: (state) => {
      if (state.currentCode && state.funcs[state.currentCode]) {
        return state.funcs[state.currentCode].read || false
      }
      return false
    },
    create: (state) => {
      if (state.currentCode && state.funcs[state.currentCode]) {
        return state.funcs[state.currentCode].create || false
      }
      return false
    },
    update: (state) => {
      if (state.currentCode && state.funcs[state.currentCode]) {
        return state.funcs[state.currentCode].update || false
      }
      return false
    },
    delete: (state) => {
      if (state.currentCode && state.funcs[state.currentCode]) {
        return state.funcs[state.currentCode].delete || false
      }
      return false
    }
  },
  mutations: {
    setFuncs (state, functions = []) {
      let result = {}

      functions.forEach(i => {
        result[i.code] = {}
        i.accesses.forEach(a => result[i.code][a] = true)
      })

      state.funcs = {
        ...state.funcs,
        ...result
      }
    },
    setFunctionalRoles (state, data = []) {
      state.functionalRoles = data
    },
    currentCode(state, code = '') {
      state.currentCode = code
    },
    setLoading (state, flag) {
      state.isLoading = flag
    },
    setMenu (state, menu) {
      state.topMenu = menu
    }
  },
  actions: {
    async loadFuncs ({ commit }) {
      try {
        let roles = await api.user_roles.getUserFunctionalRoles()
        sessionStorage.setItem('userFunctionalRoles', roles ? JSON.stringify(roles) : null)
        commit('setFuncs', roles)
        commit('setFunctionalRoles', roles)
        commit('setLoading', false)
      } catch (e) {
        console.error(e)
        commit('setLoading', true)
      }
    },
    async fetchMenu ({ commit, dispatch, rootState }) {
      if (rootState.user.tokenData.contractor_role_code === "anonymous") {
        commit('setLoading', false)
        return
      }
      try {
        const menu = await api.menu.fetch()
        menu.sort((a, b) => a.position - b.position)
        commit('setMenu', menu)
      } catch (e) {
        commit('setMenu', [])
      }
      dispatch('loadFuncs');
    },
    setCurrentCode({ commit }, code) {
      commit('currentCode', code)
    }
  }
}
