import api from '@/api'
import cloneDeep from 'clone-deep'

const setToValue = (obj, value, path) => {
  let i
  path = path.split('.')
  for (i = 0; i < path.length - 1; i++) {
    obj = obj[path[i]]
    if (!obj) break
  }

  if (obj) {
    obj[path[i]] = value
  }
}

const formattedNum = val => {
  if (typeof val === 'number') return val

  if (val.indexOf(',') !== -1) val = val.replace(',', '.')

  return parseFloat(val)
}

export default {
  namespaced: true,
  state: {
    settingsType: '',
    list_docs: [],
    loader: false,
    settings: {},
    facsimile: {},
    facsimileImages: {},
    signatureImage: '',
    stampImage: '',
    markup: '',
    markupClients: [],
    markupClient: '',
    searchData: {
      auction_step: 0,
      auction_duration: 0,
      booking_duration: 0,
      arrival_lead_time: 0,
      empty_supply_lead_time: 0,
      resource_assignment_lead_time: 0,
      doc_signing_lead_time: 0,
      round_trip_radius: 0,
      round_trip_time: 0,
      carrier_rate_min: 0
    },
    userUuid: 'null-null'
  },
  getters: {
    settingsType: state => state.settingsType,
    list_docs: state => state.list_docs,
    loader: state => state.loader,
    settings: state => state.settings,
    facsimile: state => state.facsimile,
    facsimileImages: state => state.facsimileImages,
    signatureImage: state => state.signatureImage,
    stampImage: state => state.stampImage,
    markup: state => state.markup,
    markupClients: state => state.markupClients,
    markupClient: state => state.markupClient,
    searchData: state => state.searchData
  },
  mutations: {
    SET_SETTINGS_TYPE (state, payload) {
      state.settingsType = payload
    },
    SET_SETTINGS (state, payload) {
      state.settings = payload
    },
    SET_LIST_DOCS (state, payload) {
      state.list_docs = payload
    },
    TURN_ON_LOADER (state, payload) {
      state.loader = payload
    },
    SET_USER_UUID (state, payload) {
      state.userUuid = payload
    },
    LOAD_SEARCH_DATA (state, payload) {
      payload.forEach(key => {
        state.searchData[key.name] = formattedNum((key.value / 60));
        if (key.name === 'auction_step') {
          state.searchData['auction_step'] = formattedNum((key.value / 100));
        }
        if (key.name === 'round_trip_radius') {
          state.searchData['round_trip_radius'] = formattedNum(key.value)
        }
        if (key.name === 'carrier_rate_min') {
          state.searchData['carrier_rate_min'] = formattedNum(key.value)
        }
      })
    },
    SET_SETTING_VALUE (state, { path, value }) {
      let result = cloneDeep(state.settings)

      setToValue(result, value, path)

      state.settings = result
    },
    setFacsimile (state, data) {
      state.facsimile = {
        ...state.facsimile,
        ...data
      }
    },
    setFacsimileImages (state, data) {
      state.facsimileImages = {
        ...state.facsimileImages,
        ...data
      }
    }
  },
  actions: {
    getSettingsType ({ commit }, current) {
      commit('SET_SETTINGS_TYPE', current)
    },
    async getSettings ({ state, commit }) {
      await api.clicar_setting_2.getSettings(state.userUuid, state.settingsType)
        .then(res => {
          if (state.settingsType === 'SEARCH_EXECUTORS_RESOURCES') {
            commit('LOAD_SEARCH_DATA', res.data.searchExecutorsResources)
          }
          else {
            commit('SET_SETTINGS', res.data || {})
          }
        })
        .catch(err => console.log('err', err))
    },
    setSettingValue ({ commit }, { path, value }) {
      commit('SET_SETTING_VALUE', { path, value })
    },
    async changeSearchData ({ state, commit }) {
      const searchExecutorsResources = [
        { name: "auction_step", value: parseInt(formattedNum(state.searchData.auction_step) * 100) },
        { name: "auction_duration", value: parseInt(formattedNum(state.searchData.auction_duration) * 60) },
        { name: "booking_duration", value: parseInt(formattedNum(state.searchData.booking_duration) * 60) },
        { name: "arrival_lead_time", value: parseInt(formattedNum(state.searchData.arrival_lead_time) * 60) },
        { name: "empty_supply_lead_time", value: parseInt(formattedNum(state.searchData.empty_supply_lead_time) * 60) },
        { name: "resource_assignment_lead_time", value: parseInt(formattedNum(state.searchData.resource_assignment_lead_time) * 60) },
        { name: "doc_signing_lead_time", value: parseInt(formattedNum(state.searchData.doc_signing_lead_time * 60) )},
        { name: "round_trip_radius", value: (formattedNum(state.searchData.round_trip_radius)) },
        { name: "round_trip_time", value: parseInt(formattedNum(state.searchData.round_trip_time * 60)) },
        { name: "carrier_rate_min", value: formattedNum(state.searchData.carrier_rate_min) },
      ]

      const data = {
        searchExecutorsResources,
        advance: null,
        archive: null,
        clicarUuid: state.userUuid,
        docs: null,
        facsimile: null,
        limit: null,
        markup: null,
        tariff: null
      }

      await api.clicar_setting_2.putSettings(data).then(res => {
        if (res.hasOwnProperty('data')) {
          commit('LOAD_SEARCH_DATA', res.data.searchExecutorsResources)
        }
      });
    },
    async putSettings({ state }) {
      await api.clicar_setting_2.putSettings({ ...state.settings, clicarUuid: state.userUuid })
    },
    async getFacsimileOld ({ state }, current) {
      await api.clicar_setting_2.getFacsimile(current)
        .then(res => {
          state.facsimile = res.data.facsimile;
          if (state.facsimile.stampUuid) {
            api.files.getFile(state.facsimile.stampUuid).then(res => {
              const url = window.URL.createObjectURL(new Blob([res.data]));
              state.stampImage = url;
            })
          }
          if (state.facsimile.signatureUuid) {
            api.files.getFile(state.facsimile.signatureUuid).then(res => {
              const url = window.URL.createObjectURL(new Blob([res.data]));
              state.signatureImage = url;
            })
          }
        })
    },
    async getFacsimile ({ commit, state }) {
      const facsimileData = await api.clicar_setting_2.getFacsimileV3()
      const facsimileImagesState = state.facsimileImages
      let facsimileImages = {}

      for (let key in facsimileData) {
        if (facsimileData[key] && (!facsimileImagesState[key] || facsimileImagesState[key].uuid !== facsimileData[key])) {
          const fileData = await api.files.getFile(facsimileData[key])

          facsimileImages[key] = {
            uuid: facsimileData[key],
            url: window.URL.createObjectURL(new Blob([ fileData.data ]))
          }
        } else {
          facsimileImages[key] = facsimileData[key] ? facsimileImagesState[key] || null : null
        }
      }

      commit('setFacsimile', facsimileData)
      commit('setFacsimileImages', facsimileImages)
    },
    async saveFacsimile ({ state, commit, dispatch }, { type, fileUuid = null }) {
      const facsimileData = { ...state.facsimile, [type]: fileUuid }

      await api.clicar_setting_2.saveFacsimileV3({ data: facsimileData })

      commit('setFacsimile', facsimileData)
      dispatch('getFacsimile')
    },
    async getFacsimileForClicar({state}, uuid) {
      await api.clicar_setting_2.getFacsimileForClicar(uuid)
        .then(res => {
          state.facsimile = res.data.facsimile;
          if (state.facsimile.stampUuid) {
            api.files.getFile(state.facsimile.stampUuid).then(res => {
              const url = window.URL.createObjectURL(new Blob([res.data]));
              state.stampImage = url;
            })
          }
          if (state.facsimile.signatureUuid) {
            api.files.getFile(state.facsimile.signatureUuid).then(res => {
              const url = window.URL.createObjectURL(new Blob([res.data]));
              state.signatureImage = url;
            })
          }
        })
    },

    // НАЦЕНКА
    async getMarkupSettings({state}, current) {
      await api.clicar_setting_2.getMarkupSettings(current)
        .then(res => state.markup = res.data)
        .catch(err => console.log('err', err))
    }
    /*async getMarkupClients({state}, current) {
      await api.clicar_setting_2.getMarkupClients(current)
        .then(res => state.markupClients = res.data.content)
        .catch(err => console.log('err', err))
    }*/
  }
}


