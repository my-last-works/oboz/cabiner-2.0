import api from '../../api'
// import data from '../../resources/data';

export const GET_INDEX = 'GET_INDEX'
export const SET_INDEX = 'SET_INDEX'
export const UPDATE_INDEX ='UPDATE_INDEX'

export default {
 state: {
    index: undefined
  },
  getters: {
    getIndexSettings: state => {
      return state.index
    }
  },
  actions: {
    [GET_INDEX]: async ({ commit }) => {
      try {
        const res = await api.index_settings.fetchIndexSettingsByDomain()
        // const res = data.resourceFields
        commit(SET_INDEX, res)
      } catch (ex) {
        console.log(ex)
      }
    },

    [UPDATE_INDEX]: async ({ dispatch }, payload) => {
      try {
        await api.index_settings.updateIndexSettingsByDomain(payload)
        dispatch(GET_INDEX)
      } catch (ex) {
        console.log(ex)
      }
    }
  },
  mutations: {
    [SET_INDEX]: (state, res) => {
      state.index = res
    }
  }
}
