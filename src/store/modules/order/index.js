import api from '@/api'
import { sortRisks, getRiskClass } from '@/helpers/risks'
import moment from 'moment'
import { parseNumber } from '@/helpers/order'

moment.locale('ru')

const statusTitleMap = {
  NEW: 'Новый',
  ON_DOER_SEARCH: 'Поиск',
  BOOKED: 'Букирован',
  ALLOCATED: 'Аллокирован',
  CONTRACTED: 'Контрактован',
  IN_PROCESS: 'На маршруте',
  PROCESSED: 'Исполнен',
  EXECUTION_CONFIRMED: 'Исполнение подтверждено',
  INVOICING: 'Инвойсирование',
  INVOICED: 'Инвойсирован',
  PAY_PROCESS: 'В процессе оплаты',
  PAID: 'Оплачен',
  FAILED: 'Сорван',
  CANCELED: 'Отменен'
};

let processedOrderUuid = null;

export default {
  namespaced: true,
  state: {
    route: {},
    order: {},
    orderEvents: [],
    subOrderEvents: [],
    tripEvents: [],
    subOrders: [],
    subOrderData: {},
    subOrderTime: {},
    orderTrips: [],
    selectedSubOrder: null,
    selectedTrip: null,
    isLoading: false,
    updateTimeout: null,
    participantsConfirmedBooking: [],
    participantWinnerAuction: null,
    lotHistory: null,
    isVisibleLotHistory: false,
    additionalServices: {},
    nonFinancialAdjustments: {},
    tripsPointsInfo: {},
    loaders: {
      update: false,
      clear: false
    },
    isVisibleAdvance: false,
    advanceInfo: {},
    podsInfo: {},
    billingInfo: {},
    requiredResources: {},
    requiredResourcesValues: {},
    statusFilter: {
      serviceOptions: [],
      resourceTypeOptions: [],
      statusOptions: []
    },
    reasonsList: {}
  },
  getters: {
    routeQuery (state) {
      return state.route.query || {}
    },
    order (state) {
      return state.order
    },
    orderEvents (state) {
      return state.orderEvents
    },
    subOrderEvents (state) {
      return state.subOrderEvents
    },
    tripEvents (state) {
      return state.tripEvents
    },
    subOrder (state) {
      return state.subOrderData
    },
    subOrderTime (state) {
      return state.subOrderTime
    },
    subOrders (state) {
      return state.subOrders
    },
    orderTrips (state) {
      return state.orderTrips
    },
    selectedSubOrder (state) {
      return state.selectedSubOrder
    },
    selectedTrip (state) {
      return state.selectedTrip
    },
    currentSubOrderUuid (state) {
      return (state.order.isSuborder && state.order.uuid) || (state.selectedSubOrder && state.selectedSubOrder.uuid) || (state.subOrders.length && state.subOrders[0].uuid) || null
    },
    isLoading (state) {
      return state.isLoading
    },
    participantsConfirmedBooking (state) {
      return state.participantsConfirmedBooking
    },
    participantWinnerAuction (state) {
      return state.participantWinnerAuction
    },
    lotHistory (state) {
      return state.lotHistory
    },
    isVisibleLotHistory (state) {
      return state.isVisibleLotHistory
    },
    additionalServices (state) {
      return state.additionalServices
    },
    nonFinancialAdjustments (state) {
      return state.nonFinancialAdjustments
    },
    billingInfo (state) {
      return state.billingInfo
    },
    isVisibleAdvance (state) {
      return state.isVisibleAdvance
    },
    advanceInfo (state) {
      return state.advanceInfo
    },
    podsInfo (state) {
      return state.podsInfo
    },
    tripsPointsInfo (state) {
      return state.tripsPointsInfo
    },
    requiredResources (state) {
      return state.requiredResources
    },
    requiredResourcesValues (state) {
      return state.requiredResourcesValues
    },
    loaders: state => state.loaders,
    statusFilter: state => state.statusFilter,
    reasonsList (state) {
      return state.reasonsList
    }
  },
  mutations: {
    setRoute (state, data) {
      state.route = {
        ...data
      }
    },
    setOrderData (state, data = {}) {
      state.order = {
        ...state.order,
        ...data
      }
    },
    setOrderEvents (state, data = []) {
      state.orderEvents = data
    },
    setSubOrderEvents (state, data = []) {
      state.subOrderEvents = data
    },
    setTripEvents (state, data = []) {
      state.tripEvents = data
    },
    setOrderTrips (state, data = []) {
      state.orderTrips = data
    },
    setSubOrders (state, data = []) {
      state.subOrders = data
    },
    setSelectedSubOrder (state, value) {
      state.selectedSubOrder = value
    },
    setSelectedTrip (state, value) {
      state.selectedTrip = value
    },
    setSubOrderData (state, { uuid, data } = {}) {
      let isExistSuborder
      if (state.subOrders.length) {
        isExistSuborder = state.subOrders.find(item => item.uuid === uuid)
      } else {
        isExistSuborder = state.orderTrips.find(item => item.uuid === uuid)
      }

      if (uuid && isExistSuborder) {
        state.subOrderData = {
          ...state.subOrderData,
          [uuid]: {
            ...state.subOrderData[uuid],
            ...data
          }
        }
      }
    },
    setSubOrderTime (state, data) {
      state.subOrderTime = data
    },
    setRequiredResources (state, { uuid, data } = {}) {
      if (uuid) {
        state.requiredResources = {
          ...state.requiredResources,
          [uuid]: data
        }
      }
    },
    setRequiredResourcesValues (state, { uuid, data } = {}) {
      if (uuid) {
        state.requiredResourcesValues = {
          ...state.requiredResourcesValues,
          [uuid]: data
        }
      }
    },
    clearOrderData (state) {
      clearTimeout(state.updateTimeout)

      processedOrderUuid = null
      state.route = {}
      state.order = {}
      state.subOrders = []
      state.subOrderData = {}
      state.selectedSubOrder = null
      state.selectedTrip = null
      state.updateTimeout = null
      state.participantsConfirmedBooking = []
      state.participantWinnerAuction = null
      state.orderEvents = []
      state.subOrderEvents = []
      state.subOrderTime = []
      state.tripEvents = []
      state.orderTrips = []
      state.lotHistory = null
      state.isVisibleLotHistory = false
      state.additionalServices = {}
      state.isVisibleAdvance = false
      state.advanceInfo = {}
      state.podInfo = {}
      state.tripsPointsInfo = {}
    },
    setLoading (state, value) {
      state.isLoading = value
    },
    setParticipantsConfirmedBooking (state, value) {
      state.participantsConfirmedBooking = value
    },
    setParticipantsAuctionWinner (state, value) {
      state.participantWinnerAuction = value
    },
    setLotHistory (state, data) {
      state.lotHistory = data
    },
    setVisibleLotHistory (state, value) {
      if (value) {
        state.isVisibleAdvance = false
      }
      state.isVisibleLotHistory = value
    },
    setAdditionalServices (state, { uuid, data } = {}) {
      state.additionalServices = {
        ...state.additionalServices,
        [uuid]: [
          ...data
        ]
      }
    },
    setNonFinancialAdjustments (state, { uuid, data } = {}) {
      state.nonFinancialAdjustments = {
        ...state.nonFinancialAdjustments,
        [uuid]: [
          ...data
        ]
      }
    },
    setBillingInfo (state, { uuid, data } = {}) {
      state.billingInfo = {
        ...state.billingInfo,
        [uuid]: {
          ...data
        }
      }
    },
    setTripsPointsInfo (state, { uuid, data } = {}) {
      let newData = {}

      if (uuid) {
        newData[uuid] = data
      } else {
        newData = {
          ...data
        }
      }

      state.tripsPointsInfo = {
        ...state.tripsPointsInfo,
        ...newData
      }
    },
    setVisibleAdvance (state, value) {
      state.isVisibleAdvance = value
    },
    setAdvanceInfo (state, { uuid, data }) {
      state.advanceInfo = {
        ...state.advanceInfo,
        [uuid]: {
          ...state.advanceInfo[uuid],
          ...data
        }
      }
    },
    SET_UPDATE_LOADER(state, payload) {
      state.loaders.update = payload
    },
    SET_CLEAR_LOADER(state, payload) {
      state.loaders.clear = payload
    },
    setPodInfo (state, { uuid, data } = {}) {
      state.podsInfo = {
        ...state.podsInfo,
        [uuid]: {
          ...data
        }
      }
    },
    SET_STATUS_FILTER(state, {title, params}) {
      state.statusFilter[title] = params
    },
    setReasonsList (state, { code, list }) {
      state.reasonsList = {
        ...state.reasonsList,
        [code]: [
          ...list
        ]
      }
    }
  },
  actions: {
    clearState ({ commit }) {
      commit('clearOrderData')
    },
    async getOrder ({ commit, dispatch, state, getters, rootGetters }, {
      uuid,
      getSubOrder = true,
      timeout = 0,
      isOnlyData = false,
      setLoading = true,
      init = false
    }) {
      commit('setLoading', setLoading)

      const query = getters.routeQuery
      const order = getters.order
      const isSuborder = query.is_suborder || order.isSuborder
      const isClicar = rootGetters['user/isClicar']
      const isCarrier = rootGetters['user/isCarrier']

      setTimeout(async () => {
        processedOrderUuid = uuid

        let data = isSuborder ? await api.orderProcess.getSubOrder({ data: { uuid }, isManual: true }) : await api.orderProcess.getOrder({ uuid })

        if (isSuborder) {
          dispatch('getAdvanceInfo', { uuid, price: data.advanceAvailable, clientUuid: data.clientUuid, status: data.status })
          dispatch('getRequiredResources', { data, isSetActive: init })
        } else {
          try {
            if (isClicar) {
              data.isCancelable = await api.orderProcess.cancelCheck({ uuid })
            }
          } catch (e) {
            console.log(e)
          }
        }

        if (processedOrderUuid !== uuid) {
          return false
        }

        let processInfo = {}
        if (init && isClicar) {
          processInfo = await api.orderProcess.getProcessOrderInfo({ uuid, isSuborder })
        }

        data.uuid = uuid

        let setOrderData = { uuid, ...(isSuborder ? processSubOrder(data) : processOrder(data)), ...processInfo }
        commit('setOrderData', setOrderData)

        const orderTrips = await dispatch('getOrderTrips', { ...setOrderData, isSuborder })

        if (!query.suborder && !query.trip && !isCarrier) {
          dispatch('getDataByTab')
        }

        if (isOnlyData) return

        let subOrders
        if (getSubOrder && !isSuborder) {
          subOrders = await dispatch('getSubOrders', {})
          commit('setLoading', false)
        } else {
          commit('setLoading', false)
        }

        const suborderUuid = query.suborder
        const tripUuid = query.trip

        if (init && (suborderUuid || tripUuid)) {
          if (suborderUuid) {
            const targetSubOrder = subOrders.find(item => item.uuid === suborderUuid || item.uuid === tripUuid)
            await dispatch('selectSubOrder', targetSubOrder)
          }

          if (tripUuid && orderTrips && orderTrips.length) {
            await dispatch('selectTrip', {
              data: orderTrips.find(item => item.uuid === tripUuid),
              isGetSuborder: !suborderUuid && !isCarrier
            })
          }
        }

        if (isCarrier) {
          dispatch('getDataByTab')
        }

        if (!state.updateTimeout && (!getters.subOrder.status || (getters.subOrder.status !== 'PAID'))) {
          clearTimeout(state.updateTimeout)
          state.updateTimeout = setTimeout(async () => {
            await dispatch('updateSubOrders')
          }, 12000)
        }
      }, timeout)
    },
    setOrderData ({ commit }, { data }) {
      commit('setOrderData', data)
    },
    async getRequiredResources ({ commit, getters, rootGetters }, { data, isSetActive }) {
      const isClient = rootGetters['user/isClient']
      const isGetResources = !['NEW', 'ON_DOER_SEARCH'].includes(data.status) && data.species === 'AUTO' && !isClient
      const isExistResourcesInOrder = data.resources?.length > 0
      const isCurrentValue = Boolean(getters.requiredResourcesValues[data.uuid])

      if (!isGetResources) return false

      let resourcesData = await api.orderProcess.getRequiredResources({ uuid: data.uuid })
      commit('setRequiredResources', { uuid: data.uuid, data: resourcesData })

      let activeItem

      if (isSetActive || !isCurrentValue || data.status !== 'BOOKED') {
        if (isExistResourcesInOrder) {
          activeItem = resourcesData.find((item) => {
            return data.resources.every(({ superclass }) => {
              return item.requiredResources?.find(({ resourceSpeciesCode }) => resourceSpeciesCode === superclass)
            })
          })
        } else {
          activeItem = resourcesData.find(item => item.isMain)
        }

        commit('setRequiredResourcesValues', { uuid: data.uuid, data: activeItem })
      }
    },
    async getOrderEvents ({ commit }, uuid) {
      let events = await api.orderProcess.getOrderEvents({ uuid })
      commit('setOrderEvents', events)
    },
    async getOrderTrips ({ commit }, data) {
      let trips = []
      if (data.isSuborder) {
        const resourceNumberObj = data.resources && data.resources.find((item) => item.superclass === 'tractor')
        const resourceNumber = resourceNumberObj ? parseNumber(resourceNumberObj.number) : {}
        trips = [
          {
            containersNumbers: data.containersNumbers,
            contractorTitle: data.contractorTitle,
            groupOrdinal: 1,
            shipmentPoints: data.shipmentPoints,
            status: data.status,
            suborderSpecies: data.suborderSpecies,
            tripUuid: data.tripUuid,
            uuid: data.uuid,
            ...resourceNumber
          }
        ]
      } else {
        trips = await api.orderProcess.getTrips({ uuid: data.uuid })
      }
      const result = processTrips({ trips }, data)
      commit('setOrderTrips', result)

      return result
    },
    async getTripPointsInfo ({ commit }, { uuid }) {
      try {
        let data = await api.orderProcess.getTripPointsInfo({ uuid })
        commit('setTripsPointsInfo', { data })
      } catch (e) {
        throw e
      }
    },
    async getSubOrders ({ commit, state }) {
      let data = await api.orderProcess.getSubOrders({ uuid: state.order.uuid })
      const result = processSubOrders(data)
      commit('setSubOrders', result)

      return result
    },
    async getSubOrder ({ commit, dispatch, rootGetters, state }, {
      data,
      setLoading = true,
      timeout = 0,
      isManual = false
    }) {
      commit('setLoading', setLoading)
      setTimeout(async () => {
        const isClient = rootGetters['user/isClient']
        const requestParams = {
          isClient,
          data,
          orderUuid: state.order.uuid,
          isManual
        }

        let response = await api.orderProcess.getSubOrder(requestParams)

        let subOrderData = processOrder({ uuid: data.uuid, tripUuid: data.tripUuid, ...response }, true)

        commit('setSubOrderData', { uuid: data.uuid, data: subOrderData })

        dispatch('getRequiredResources', { data: subOrderData, isSetActive: isManual })
        dispatch('getDataByTab')

        commit('setLoading', false)
      }, timeout)
    },
    async getSubOrderEvents ({ commit }, data) {
      let events
      try {
        events = await api.orderProcess.getSubOrderEvents({ subOrder: data })
      } catch (e) {
        events = []
      }
      commit('setSubOrderEvents', events)
    },
    async selectSubOrder ({ commit, dispatch, state }, data) {
      clearTimeout(state.updateTimeout)
      commit('setSelectedTrip', null)
      commit('setParticipantsAuctionWinner', null)
      commit('setParticipantsConfirmedBooking', [])

      if (data) {
        await dispatch('getSubOrder', { data, isManual: true })
        commit('setSelectedSubOrder', data)
      } else {
        commit('setSelectedSubOrder', null)
        setTimeout(() => {
          dispatch('getDataByTab')
        }, 100)
      }

      state.updateTimeout = setTimeout(async () => {
        await dispatch('updateSubOrders')
      }, 12000)
    },
    async selectTrip ({ commit, dispatch }, { data, isGetSuborder = false } = {}) {
      commit('setParticipantsAuctionWinner', null)
      commit('setParticipantsConfirmedBooking', [])

      if (!data) {
        commit('setSelectedTrip', null)
        dispatch('getDataByTab')
        return false
      } else {
        if (isGetSuborder) {
          await dispatch('getSubOrder', { data, isManual: true })
        }
      }

      commit('setSelectedTrip', { tripUuid: data.tripUuid, uuid: data.uuid })
    },
    async getTripEvents ({ commit }, { uuid, suborderUuid }) {
      let events
      try {
        events = await api.orderProcess.getTripEvents({
          uuid,
          suborderUuid
        })
      } catch (e) {
        events = []
      }
      commit('setTripEvents', events)
    },
    async getSuborderTime ({ commit, getters, rootGetters }, { data }) {
      const query = getters.routeQuery
      const isClient = rootGetters['user/isClient']
      const orderTrips = getters.orderTrips
      const selectedTrip = orderTrips.length && query.trip ? orderTrips.find(item => item.uuid === query.trip) : null
      const isAuto = data.species === 'AUTO'
      const status = data.status

      let time = {}

      if (selectedTrip && !isClient && isAuto && ['NEW', 'ON_DOER_SEARCH', 'BOOKED', 'ALLOCATED'].includes(status)) {
        try {
          time = await api.orderProcess.getSubOrderTime({ uuid: data.uuid })
        } catch (e) {
          time = {}
        }
      }

      commit('setSubOrderTime', time)
    },
    async getDataByTab ({ dispatch, getters, rootGetters, state, commit }) {
      clearTimeout(state.updateTimeout)
      state.updateTimeout = setTimeout(async () => {
        await dispatch('updateSubOrders')
      }, 12000)

      const isClicar = rootGetters['user/isClicar']
      const isCarrier = rootGetters['user/isCarrier']
      const query = getters.routeQuery
      const tab = query.tab || 'execution'
      const ordersList = isClicar ? getters.subOrders : getters.orderTrips
      const data = (query.suborder || (query.trip && tab === 'execution')) && !isCarrier
        ? getters.subOrder[query.trip || query.suborder] || ordersList.find(item => item.uuid === query.suborder || item.uuid === query.trip)
        : getters.order

      if (!data || !data.uuid) {
        setTimeout(() => {
          dispatch('getDataByTab')
        }, 100)
        return false
      }

      const species = data?.species || data?.subOrderType
      const isSuborder = Boolean(data.tripUuid)
      const transportationWay = isSuborder ? getters.order?.transportationWay : data.transportationWay
      const isSelectedSuborder = Boolean(query.suborder)
      const isSelectedTrip = Boolean(query.trip)
      const isAuto = data.species === 'AUTO' || data.subOrderType === 'AUTO'
      const isService = data.species && !['AUTO', 'TRAIN'].includes(species)
      const status = data.status
      const orderTrips = getters.orderTrips
      const selectedTrip = orderTrips.length && query.trip ? orderTrips.find(item => item.uuid === query.trip) : null
      const tripUuid = selectedTrip ? selectedTrip.tripUuid : data.tripUuid

      if (tab === 'execution') {
        if (isClicar && isSelectedTrip && isSelectedSuborder) {
          const isUpdateParticipantsInfo = ['ON_DOER_SEARCH', 'BOOKED', 'ALLOCATED', 'CONTRACTED', 'IN_PROCESS'].includes(data.status)

          if (isUpdateParticipantsInfo && isAuto) {
            await dispatch('getParticipantsConfirmedBooking')
            await dispatch('getParticipantsAuctionWinner')
          }

          dispatch('getSuborderTime', { data })
        }

        if (!isService && (isSelectedSuborder || isSelectedTrip)) {
          await dispatch('getTripPointsInfo', { uuid: tripUuid })
        }

        if (selectedTrip && !isService) {
          await dispatch('getTripEvents', { uuid: tripUuid, suborderUuid: selectedTrip.uuid })
        } else if (isSelectedSuborder || (isSuborder && !query.trip) || isService) {
          await dispatch('getSubOrderEvents', { uuid: data.uuid })
        } else {
          await dispatch('getOrderEvents', data.uuid)
        }
      }

      if (tab === 'confirmation') {
        const processInfo = await api.orderProcess.getProcessOrderInfo({ uuid: data.uuid, isSuborder })

        if (isCarrier || !isSuborder) {
          commit('setOrderData', { ...processInfo })
        } else {
          commit('setSubOrderData', { uuid: data.uuid, data: { ...processInfo } })
        }

        if ((!isSuborder || transportationWay === 'FTL') && !['NEW', 'ON_DOER_SEARCH'].includes(status)) {
          dispatch('getPodInfo', { uuid: processInfo.podUuid })
        }
      }

      if (tab === 'payments') {
        dispatch('getBillingInfo', { uuid: data.uuid })
      }

      if (tab === 'services') {
        dispatch('getAdditionalServices', { uuid: data.uuid })
        dispatch('getNonFinancialAdjustments', { uuid: data.uuid })
      }
    },
    async updateSubOrders ({ dispatch, state, getters, rootGetters }) {
      clearTimeout(state.updateTimeout)
      const isClient = rootGetters['user/isClient']
      const isCarrier = rootGetters['user/isCarrier']
      const isClicar = rootGetters['user/isClicar']
      const isSuborder = getters.order.isSuborder
      const orderItems = isClient || isSuborder ? state.orderTrips : state.subOrders
      const targetSubOrder = state.selectedTrip ? orderItems.find((item) => {
        return item.uuid === state.selectedTrip.uuid
      }) : getters.selectedSubOrder

      if (orderItems.every(item => item.status === 'PAID')) {
        return false
      } else {
        if (!state.isLoading) {
          const isVisibleLotHistory = getters.isVisibleLotHistory
          await  dispatch('getOrder', { uuid: state.order.uuid, getSubOrder: false, setLoading: false, isOnlyData: true })

          if ((state.selectedSubOrder || state.selectedTrip) && !isCarrier) {
            await dispatch('getSubOrder', {
              data: targetSubOrder,
              setLoading: false
            })
          }

          if (isVisibleLotHistory) {
            dispatch('getLotHistory', {
              uuid: isSuborder ? getters.order.uuid : getters.currentSubOrderUuid,
              routeCategory: isSuborder ? getters.order.suborderSpecies.routeCategory : targetSubOrder.suborderSpecies.routeCategory
            })
          }

          if (isClicar) {
            await dispatch('getSubOrders', {})
          }

          if (isCarrier) {
            dispatch('getDataByTab')
          }
        }

        state.updateTimeout = setTimeout(async () => {
          await dispatch('updateSubOrders')
        }, 12000)
      }
    },
    async getParticipantsConfirmedBooking ({ state, getters, commit }) {
      try {
        let list = await api.orderProcess.getParticipantsConfirmedBooking({ uuid: getters.currentSubOrderUuid, limit: state.subOrderData.tariffLimit })
        commit('setParticipantsConfirmedBooking', list)
      } catch (e) {
        console.log(e)
      }
    },
    async getParticipantsAuctionWinner ({ getters, commit, state }) {
      if (state.participantWinnerAuction) return false
      try {
        let data = await api.orderProcess.getParticipantsAuctionWinner({ uuid: getters.currentSubOrderUuid })
        commit('setParticipantsAuctionWinner', data)
      } catch (e) {
        commit('setParticipantsAuctionWinner', null)
      }
    },
    async selectExecutor (context, { subOrder, doer }) {
      let { tripTariff, isVatInclude, assignedTariff,  contractUuid, variantId, preorderId, fileUuids } = doer

      try {
        subOrder.species !== 'AUTO'
          ? await api.orderProcess.selectExecutorNotAuto({subOrder, contractorUuid: doer.uuid, tripTariff, isVatInclude, assignedTariff, variantId, preorderId, fileUuids })
          : await api.orderProcess.selectExecutor({uuid: subOrder.uuid, doerUuid: doer.uuid, tariff: tripTariff, isVatInclude, assignedTariff, contractUuid, fileUuids })
      } catch (e) {
        throw e
      }
    },
    async confirmOrder ({ dispatch, commit }, { uuid, data }) {
      try {
        commit('setLoading', true)
        await api.orderProcess.confirmOrder({ uuid, data })
        await dispatch('getOrder', { uuid, timeout: 1000 })
      } catch (e) {
        throw e
      }
    },
    async confirmSubOrder ({ dispatch, commit }, { subOrder, data }) {
      try {
        commit('setLoading', true)
        await api.orderProcess.confirmSubOrder({ subOrder, data })
        await dispatch('getSubOrder', { data: subOrder, timeout: 1000 })
      } catch (e) {
        throw e
      }
    },
    async confirmationDispatchPod ({ dispatch }, { orderData, data }) {
      try {
        await api.orderProcess.confirmationDispatchPod({ ...data })

        if (orderData.isSuborder) {
          await dispatch('getSubOrder', { data: orderData, timeout: 1000 })
        } else {
          await dispatch('getOrder', { uuid: orderData.uuid, timeout: 1000 })
        }
      } catch (e) {
        throw e
      }
    },
    async getLotHistory ({ commit }, { uuid, routeCategory }) {
      let data
      try {
        data = await api.orderProcess.getLotHistory({ uuid })

        if (data.bidsDetails.length) {
          const bestBid = [...data.bidsDetails].sort((a, b) => a.bid - b.bid)[0]
          const isVatPayer = bestBid.isVatPayer
          let postData = {
            quantity: 1,
            priceWithoutVat: bestBid.bid
          }

          if (['INTERNATIONAL'].includes(routeCategory) || !isVatPayer) {
            postData.vatRate = 0
          } else if (['URBAN', 'INTERURBAN'].includes(routeCategory)) {
            postData.vatRate = 20
          }

          let calculation = await api.orderProcess.vatCalculation({ list: [ postData ] })

          data.bestBidAmount = calculation.itemsList[0]
        }
      } catch (e) {
        data = {
          error: 'Нет информации по данному заказу'
        }
      }
      commit('setLotHistory', data)
    },
    async getAdvanceInfo ({ commit }, { uuid, status }) {
      if (!['CONTRACTED', 'IN_PROCESS', 'PROCESSED'].includes(status)) {
        return false
      }
      try {
        const data = await api.orderProcess.getAdvanceInfo({ uuid })
        let prices = {}
        // if (data.status === null && price) {
        //   prices = await api.orderProcess.getAdvancePrices({ client_uuid: clientUuid, price })
        // }
        commit('setAdvanceInfo', { uuid, data: { ...data, ...prices } })
      } catch (e) {
        throw e
      }
    },
    async getAdditionalServices ({ commit }, { uuid }) {
      try {
        let data = await api.orderProcess.getAdditionalServicesByOrder({ uuid })
        commit('setAdditionalServices', { uuid, data })
      } catch (e) {
        console.log(e)
      }
    },
    async getNonFinancialAdjustments ({ commit }, { uuid }) {
      try {
        let data = await api.orderProcess.orderNonFinancialAdjustmentsList({ uuid })
        commit('setNonFinancialAdjustments', { uuid, data })
      } catch (e) {
        console.log(e)
      }
    },
    async getBillingInfo ({ commit }, { uuid }) {
      try {
        let data = await api.orderProcess.getBillingDocs({ uuid })
        commit('setBillingInfo', { uuid, data })
      } catch (e) {
        console.log(e)
      }
    },
    async getPodInfo ({ commit }, { uuid }) {
      try {
        let data = await api.orderProcess.getPodList({ uuid })
        commit('setPodInfo', { uuid, data })
      } catch (e) {
        console.log(e)
      }
    },
    async getReasonsList ({ commit }, { code }) {
      try {
        if (code) {
          const { reasons } = await api.orderProcess.getDictionaryEventInfo({
            entityType: 'TRIP',
            eventTypeCode: code
          })
          commit('setReasonsList', { code, list: reasons })
        }
      } catch (e) {
        console.log(e)
      }
    },
    setUpdateLoader({commit}, status) {
      commit('SET_UPDATE_LOADER', status)
    },
    setClearLoader({commit}, status) {
      commit('SET_CLEAR_LOADER', status)
    }
  }
}

const processOrder = (data, isSubOrder = false) => {
  let processedData = isSubOrder ? processSubOrder(data) : processFirstOrder(data)

  return {
    uuid: data.uuid,
    ...data,
    ...processedData
  }
}

const processFirstOrder = (data) => {
  let result = {
    ...data
  }
  result.isEditable = data.responseOrder.isEditable
  result.orderType = data.responseOrder.orderType
  result.orderCondition = data.responseOrder.orderCondition
  result.number = data.responseOrder.number
  result.externalNumber = data.responseOrder.externalNumber
  result.status = data.responseOrder.status
  result.pvpDt = data.brif?.visitPoints[0]?.timeSlot?.startAt
  result.totalPrice = data.actualTariff?.totalPrice
  result.tariffLimit = data.actualTariff?.tariffLimit
  result.totalPriceWithoutVat = data.actualTariff?.orderTotalAmount?.priceWithoutVat
  result.totalPriceWithVat = data.actualTariff?.orderTotalAmount?.priceWithVat
  result.baseServicePrice = data.actualTariff?.orderCost?.priceWithVat
  result.vatType = data.orderCost?.vatType
  result.vatValue = data.actualTariff?.orderTotalAmount?.vatPrice
  result.orderTotalAmount = data.actualTariff?.orderTotalAmount
  result.orderCost = data.actualTariff?.orderCost
  result.logistTitle = data.responseOrder?.logistTitle
  result.logistEmail = data.responseOrder?.logistEmail
  result.logistPhone = data.responseOrder?.logistPhone
  result.contractorTitle = data.responseOrder?.contractorTitle
  result.contractorUuid = data.responseOrder?.contractorUuid
  result.contractorRoleCode = data.responseOrder?.contractorRoleCode
  result.contractorRiskColor = null
  result.clientTitle = data.responseOrder?.clientTitle
  result.clientUuid = data.responseOrder?.clientUuid
  result.clientRoleCode = data.responseOrder?.clientRoleCode
  result.titleGeography = data.services.titleGeoraphy
  result.createdAt = data.responseOrder?.createdAt
  result.clientLogistTitle = data.parameters?.contactPerson?.contactFIO
  result.clientLogistPhone = data.parameters?.contactPerson?.phone
  result.cargoTypeTitle = data.parameters.cargoTypeTitle
  result.cargoWeight = (data.cargo?.weightInKg || 0) / 1000
  result.cargoVolume = data.cargo?.volumeInCubicMeters || 0
  result.palletCount = data.cargo?.palletCount || 0
  result.cargoInsuranceCost = data.parameters?.insuranceCargo?.cost
  result.riskColor = data.responseOrder?.riskColor
  result.trackingMethodUuid = data.responseOrder?.needTracking || true
  result.isDangerous = data.responseOrder?.isDangerous
  result.temperatureRegimeUuid = data.responseOrder?.temperatureRegimeUuid
  result.comment = data.parameters?.comments
  result.consignors = data.cargo.counterAgents?.consignors
  result.consignees = data.cargo.counterAgents?.consignees
  result.requirements = data.responseOrder.requirements || []
  result.transportationWay = data.responseOrder?.transportation_way
  result.costPrice = data.margin?.costPrice
  result.margin = data.margin?.margin
  result.marginInPercent = data.margin?.marginInPercent
  result.costAccuracy = data.margin?.costAccuracy
  result.servicesList = data.services?.baseServices
  result.baseServiceClientName = data.services?.baseServices?.[0]?.shortTitle
  result.serviceTypeTitle = data.services?.baseServices?.[0]?.title
  result.contractNumber = data.services?.contractNumber
  result.contractUuid = data.services?.contractUuid
  result.contractEffectiveD = data.services?.contractEffectiveD
  result.servicesTotalPrice = data.services?.totalPrice
  result.carrierRiskColors = sortRisks({ data: data.cargo?.acceptedCarrierColors || [] })
  result.statusTitle = data.responseOrder?.statusTitle || statusTitleMap[data.responseOrder?.status]
  result.temperatureModeTitle = data.responseOrder?.temperatureModeTitle
  result.etsngs = (data.etsngs || []).map((item) => {
    return {
      ...item,
      title: `${item.code} ${item.title}`
    }
  })
  result.addServicesList = (data.services.addServices || []).map((item) => item)
  result.addServicesSumm = 0
  if (data.services?.addServices) {
    data.services.addServices.forEach((item) => {
      result.addServicesSumm += item.price
    })
  }

  if (Array.isArray(data.brif?.visitPoints)) {
    const points = data.brif.visitPoints
    result.routes = {
      pointFrom: points[0]?.address || '',
      pointTo: points[0]?.address || '',
      items: points.map((item) => {
        return {
          ...item,
          uuid: item.uuid,
          type: 'location',
          done: false,
          current: false,
          point: (item.type || 'from').toLowerCase(),
          status: 'ожидание начала рейса',
          pointName: item.title,
          pointAddress: item.address,
          dateStart: item.timeSlot?.startAt,
          dateEnd: item.timeSlot?.endAt
        }
      })
    }
  }

  if (data.responseOrder?.route?.points?.length) {
    result.startPoint = data.responseOrder.route.points[0].params?.addressTitle
    result.endPoint = data.responseOrder.route.points[data.responseOrder.route.points.length - 1].params?.addressTitle
  }

  result.transboundary = data.responseOrder?.route?.transboundary
  result.routeCategory = data.responseOrder?.route?.routeCategory

  return result
}

const processSubOrder = (data) => {
  let result = {
    ...data
  }
  result.isEditable = data.isEditable
  result.isSuborder = true
  result.orderType = data.assignmentType
  result.comment = data.comment
  result.autosearchAvailable = Boolean(data.autosearchAvailable === undefined || data.autosearchAvailable)
  result.totalPriceWithoutVat = data.orderTotalAmount?.priceWithoutVat
  result.totalPriceWithVat = data.orderTotalAmount?.priceWithVat
  result.baseServicePrice = data.orderCost?.priceWithVat
  result.vatType = data.orderCost?.vatType
  result.vatValue = data.orderTotalAmount?.vatPrice
  result.logistTitle = data.contractorLogistTitle
  result.logistEmail = data.contractorLogistEmail
  result.logistPhone = data.contractorLogistPhone
  result.titleGeography = data.titleGeography || (data.startAddress + (data.endAddress ? ' → ' + data.endAddress : ''))
  result.startPoint = data.startAddress
  result.endPoint = data.endAddress
  result.cargoTypeTitle = data.cargoTypeName
  result.cargoWeight = data.cargoWeight / 1000
  result.cargoVolume = data.volume
  result.requirements = data.requirements || []
  result.etsngs = (data.etsngs || []).map((item) => {
    return {
      ...item,
      title: `${item.code} ${item.title}`
    }
  })
  result.addServicesList = (data.addServices?.services || []).map((item) => item)
  result.addServicesSumm = data.addServices?.totalSum || 0

  if (data.contractorUuid) {
    result.executor = {
      uuid: result.contractorUuid,
      title: result.contractorTitle,
      icon: getRiskClass(result.contractorRiskColor)
    }
  }

  result.speciesType = data.suborderSpecies?.species || null
  result.species = data.suborderSpecies && data.suborderSpecies.suborderType ? data.suborderSpecies.suborderType[data.suborderSpecies.species] : null

  result.resources = (data.resources || []).map((item) => {
    return {
      ...item,
      resourceTypeTitle: item.title
    }
  })
  if (data.service) {
    const resourceSpeciesTitle = data.service.resourceSpeciesTitle ? `. ${data.service.resourceSpeciesTitle}` : ''
    const resourceSubtypeTitle = data.service.resourceSubtypeTitle ? ` ${data.service.resourceSubtypeTitle}` : ''
    const resourcePatternParameter = data.service.resourcePatternParameter ? ` ${data.service.resourcePatternParameter}` : ''

    result.servicesList = [
      {
        title: `${data.service.serviceTypeTitle}${resourceSpeciesTitle}${resourceSubtypeTitle}${resourcePatternParameter}`,
        shortTitle: data.service?.serviceTypeTitle,
        geography: null,
        count: null,
        price: null,
        unit: data.service?.serviceUnits?.[0]?.title,
        serviceUuid: ''
      }
    ]
    result.serviceTypeTitle = ['AUTO', 'TRAIN'].includes(result.species)
      ? `${data.service?.serviceTypeTitle}. ${data.service?.resourceSpeciesTitle}. ${data.service?.resourceSubtypeTitle} ${data.service?.resourcePatternParameter}`
      : data.service?.serviceTypeTitle
    result.baseServiceClientName = data.service?.resourceSpeciesTitle
  }
  result.servicesTotalPrice = data.totalPrice
  result.carrierRiskColors = sortRisks({ data: data.acceptedCarrierColors || data.availableCarriersRisks || [] })
  result.statusTitle = statusTitleMap[data.status]

  if (Array.isArray(data.shipmentPoints)) {
    result.routes = {
      pointFrom: data.shipmentPoints?.[0]?.title || '',
      pointTo: data.shipmentPoints?.[1]?.title || '',
      items: data.shipmentPoints.map((item) => {
        return {
          ...item,
          suborderUuid: data.uuid,
          uuid: item.shipmentPointUuid || item.uuid,
          type: 'location',
          done: false,
          current: false,
          point:  item.direction ? item.direction.toLowerCase() : item.ordinal === 1 ? 'from' : 'to',
          status: 'ожидание начала рейса',
          pointName: item.title,
          pointAddress: item.addressTitle,
          dateStart: item.plannedSlotStartAt,
          dateEnd: item.plannedSlotEndAt
        }
      })
    }
  }

  result.transboundary = data.suborderSpecies?.transboundary
  result.routeCategory = data.suborderSpecies?.routeCategory

  return result
}

const processSubOrders = (data) => {
  return data.map((item) => {
    const subOrderType = item.suborderSpecies?.suborderType[item.suborderSpecies.species]

    let computedParams = {
      subOrderType,
      typeTitle: (item.service?.serviceTypeTitle) || enumTripsMap[subOrderType],
      species: item.suborderSpecies.species,
    }

    return {
      ...item,
      ...computedParams
    }
  })
}

const processTrips = (data, order) => {
  let result = []
  for (let key in data) {
    if (data[key]) {
      data[key].forEach((item) => {
        const serviceCodeToType = {
          FTL: 'AUTO',
          TRAIN: 'TRAIN',
          CONTAINER: 'CONTAINER',
        }
        let computedParams = {}

        if (item.suborderSpecies) {
          const type = item.suborderSpecies.suborderType[item.suborderSpecies.species]
          computedParams = {
            mainType: item.suborderSpecies.species,
            type,
            subOrderType: type,
            typeTitle: (item.service?.serviceTypeTitle) || enumTripsMap[type],
          }
        }
        result.push({
          ...item,
          mainType: 'order',
          isContainer: (order.transportation_way || order.transportationWay) === 'CONTAINER',
          type: serviceCodeToType[item.serviceCode],
          ...computedParams,
        })
      })
    }
  }
  return result
}

const enumTripsMap = {
  "RAILWAY_TARIFF": "Провозной тариф",
  "CONTAINER_FREIGHT": "Аренда контейнера",
  "TERMINAL_PROCESSING": "Услуга в точке"
}
