import {test_orders} from './mocks/orders'
export default {
  namespaced: true,
  state: {
    orders: test_orders // []
  },
  getters: {
    orders: state => state.orders,
  },
  mutations: {},
  actions: {
    selectedOrder(arg) {
      console.log('selectedOrder', arg)
    }
  }
}