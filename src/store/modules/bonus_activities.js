import api from '../../api'

export const GET_BONUS_ACTIVITIES = 'GET_BONUS_ACTIVITIES';
export const SET_BONUS_ACTIVITIES = 'SET_BONUS_ACTIVITIES';

export default {
  state: {
    items: []
  },
  getters: {
    getBonusActivities: state => {
      return state.items
    }
  },
  actions: {
    [GET_BONUS_ACTIVITIES]: async ({ commit }) => {
      try {
        const res = await api.bonus_activities.fetchBonusParticipantActivities();
        commit(SET_BONUS_ACTIVITIES, res)
      } catch (ex) {
        console.log(ex)
      }
    },
  },
  mutations: {
    [SET_BONUS_ACTIVITIES]: (state, res) => {
      state.items = res;
    }
  }
}