import api from '../../api'
// import data from '../../resources/data';

export const GET_METHODOLOGY = 'GET_METHODOLOGY'
export const SET_METHODOLOGY = 'SET_METHODOLOGY'
export const UPDATE_METHODOLOGY ='UPDATE_METHODOLOGY'
export const DELETE_METHODOLOGY ='DELETE_METHODOLOGY'

export default {
 state: {
    current: undefined
  },
  getters: {
    getCurrentMethodology: state => {
      return state.current
    }
  },
  actions: {
    [GET_METHODOLOGY]: async ({ commit }, uuid) => {
      try {
        const res = await api.methodology.fetchMethodologyByUuid(uuid)
        commit(SET_METHODOLOGY, res)
      } catch (ex) {
        console.log(ex)
      }
    },

    [UPDATE_METHODOLOGY]: async ({ dispatch }, payload) => {
      try {
        await api.methodology.updateMethodologyByUuid(payload.uuid, payload)
        dispatch(GET_METHODOLOGY, payload.uuid)
      } catch (ex) {
        console.log(ex)
      }
    },
    [DELETE_METHODOLOGY]: async (context) => {
      const uuid = context.getters.getCurrentMethodology.uuid;
      try {
          await api.methodology.deleteMethodologyByUuid(uuid);
          context.commit(SET_METHODOLOGY, null);
      } catch (ex) {
          console.log(ex)
      }
    }
    // [DELETE_METHODOLOGY]: async ({ dispatch, rootState }, payload) => {
    //   try {
    //     console.log(payload)
    //     // const res = await api.methodology.deleteMethodologyByUuid(uuid)
    //     const res = await api.methodology.deleteMethodologyByUuid(payload)
    //     dispatch(GET_METHODOLOGY)
    //     // commit(DELETE_METHODOLOGY, uuid)
    //     console.log(res)
    //     console.log(rootState)
    //     // TO DO:
    //     // list in rootState, filter list, dispatch to methodologies
    //   } catch (ex) {
    //     console.log(ex)
    //   }
    // }
  },
  mutations: {
    [SET_METHODOLOGY]: (state, res) => {
      state.current = res
    },
  }
}
