import api from '@/api'
import { LOADED_STATE } from '@/resources/enum'

export default {
  namespaced: true,
  state: {
    spotItem: {},
    spotTableItems: [],
    spotNumber: '',
    spotErrors: [],
    spotOrderByCarrier: {},
    spotAuctionByCarrier: {},
    spotBidsInfoByCarrier: {},
    clientFilter: {},
    carrierFilter: {},
    isLoaderClientFilter: false,
    loadedState: LOADED_STATE.LOADING,
    loadedCarrierState: LOADED_STATE.LOADING,
    loadedOrderState: LOADED_STATE.LOADING,
    loadedAuctionState: LOADED_STATE.LOADING,
    userInfo: {},
    urlMap: {
      'spot__list_draft': {
        tab: 'draft'
      },
      'spot__list_completed': {
        tab: 'ended'
      }
    },
  },
  getters: {
    spotItem: state => state.spotItem,
    spotTableItems: state => state.spotTableItems,
    spotNumber: state => state.spotNumber,
    loadedState: state => state.loadedState,
    loadedCarrierState: state => state.loadedCarrierState,
    loadedOrderState: state => state.loadedOrderState,
    loadedAuctionState: state => state.loadedAuctionState,
    spotErrors: state => state.spotErrors,
    spotOrderByCarrier: state => state.spotOrderByCarrier,
    spotBidsInfoByCarrier: state => state.spotBidsInfoByCarrier,
    spotAuctionByCarrier: state => state.spotAuctionByCarrier,
    clientFilter: state => state.clientFilter,
    carrierFilter: state => state.carrierFilter,
    isLoaderClientFilter: state => state.isLoaderClientFilter,
    userInfo: state => state.userInfo,
    urlMap: state => state.urlMap
  },
  mutations: {
    SET_SPOT_NUMBER (state, value) {
      state.spotNumber = value
    },
    SET_SPOT_ITEM (state, data = {}) {
      state.spotItem = {
        ...state.spotItem,
        ...data
      }
    },
    SET_SPOT_ORDER_CARRIER (state, value) {
      state.spotOrderByCarrier = value
    },
    SET_SPOT_AUCTION_CARRIER (state, value) {
      state.spotAuctionByCarrier = value
    },
    SET_SPOT_BIDS_INFO_CARRIER (state, value) {
      state.spotBidsInfoByCarrier = value
    },
    SET_SPOT_STATE (state, value) {
      state.loadedState = value
    },
    SET_SPOT_CARRIER_STATE (state, value) {
      state.loadedCarrierState = value
    },
    SET_SPOT_ERRORS (state, value) {
      state.spotErrors = value
    },
    SET_SPOT_TABLE_ITEMS (state, value) {
      state.spotTableItems = value
    },
    SET_SPOT_CLIENT_FILTER (state, value) {
      state.clientFilter = value
    },
    SET_SPOT_CARRIER_FILTER (state, value) {
      state.carrierFilter = value
    },
    SET_SPOT_CLIENT_FILTER_LOADER (state, value) {
      state.isLoaderClientFilter = value
    },
    SET_SPOT_ORDER_STATE (state, value) {
      state.loadedOrderState = value
    },
    SET_SPOT_AUCTION_STATE (state, value) {
      state.loadedAuctionState = value
    },
    SET_SPOT_USER_INFO (state, value) {
      state.userInfo = value
    }
  },
  actions: {
    async getSpotItem ({ commit }, data) {
      try {
        commit('SET_SPOT_STATE', LOADED_STATE.LOADING)
        const response = await api.spotAuction.getSpotItem(data.uuid)
        if(data.isCopy){
          response.transportation.needTracking = false
          response.transportation.temperatureModeUuid = null
          response.parameters.serviceRequirements = []
          response.parameters.comment = ""
        }
        const spotItem = {
          main: { ...response.main },
          points: [...response.points],
          transportation: { ...response.transportation },
          cargo: { ...response.cargo },
          parameters: { ...response.parameters },
          auction: { ...response.auction }
        }

        commit('SET_SPOT_ITEM', spotItem)
        commit('SET_SPOT_NUMBER', spotItem.main.number)
        commit('SET_SPOT_STATE', LOADED_STATE.READY)
      } catch (e) {
        console.error(e);
        // this.$notification({
        //   type: 'system_notification_error',
        //   message: 'Ошибка загрузки данных'
        // })
        commit('SET_SPOT_STATE', LOADED_STATE.FAIL)
      }
    },
    async validationSpotDraft ({ commit }, uuid) {
      const errors = await api.spotAuction.validationSpotDraft(uuid)
      commit('SET_SPOT_ERRORS', errors)
    },
    async getSpotOrderByCarrier ({ commit }, spot) {
      try {
        commit('SET_SPOT_ORDER_STATE', LOADED_STATE.LOADING)
        const response = await api.spotAuction.getSpotOrderByCarrier(spot.spotOrder.orderUuid)
        commit('SET_SPOT_ORDER_CARRIER', {
          ...response,
          ...spot
        })
        commit('SET_SPOT_ORDER_STATE', LOADED_STATE.READY)
      } catch (e) {
        console.error(e);
        // this.$notification({
        //   type: 'system_notification_error',
        //   message: 'Ошибка загрузки данных'
        // })
        commit('SET_SPOT_ORDER_STATE', LOADED_STATE.FAIL)
      }
    },
    async getSpotAuctionByCarrier ({ commit }, uuid) {
      try {
        commit('SET_SPOT_AUCTION_STATE', LOADED_STATE.LOADING)
        const spotAuctionByCarrier = await api.spotAuction.getSpotAuctionByCarrier(uuid)
        commit('SET_SPOT_AUCTION_CARRIER', spotAuctionByCarrier)
        commit('SET_SPOT_AUCTION_STATE', LOADED_STATE.READY)
      } catch (e) {
        console.error(e)
        // this.$notification({
        //   type: 'system_notification_error',
        //   message: 'Ошибка загрузки данных'
        // })
        commit('SET_SPOT_AUCTION_STATE', LOADED_STATE.FAIL)
      }
    },
    async getSpotBidsInfoByCarrier ({ commit }, {auctionUuid, orderUuid}) {
      try {
        commit('SET_SPOT_AUCTION_STATE', LOADED_STATE.LOADING)
        const spotBidsInfo = await api.spotAuction.getActualAuction(auctionUuid, orderUuid)
        commit('SET_SPOT_BIDS_INFO_CARRIER', spotBidsInfo.data)
        commit('SET_SPOT_BIDS_INFO_CARRIER', spotBidsInfo.data)
        commit('SET_SPOT_AUCTION_STATE', LOADED_STATE.READY)
      } catch (e) {
        console.error(e)
        // this.$notification({
        //   type: 'system_notification_error',
        //   message: 'Ошибка загрузки данных'
        // })
        commit('SET_SPOT_AUCTION_STATE', LOADED_STATE.FAIL)
      }
    },
    async getSpotTableItemsByCarrier ({ commit }, filters) {
      try {
        commit('SET_SPOT_CARRIER_STATE', LOADED_STATE.LOADING)
        const spotTableItems = await api.spotAuction.getSpotsByCarrier(
          filters
        )
        commit('SET_SPOT_TABLE_ITEMS', spotTableItems)
        commit('SET_SPOT_CARRIER_STATE', LOADED_STATE.READY)
      } catch (e) {
        console.error(e);
        // this.$notification({
        //   type: 'system_notification_error',
        //   message: 'Ошибка загрузки данных'
        // })
        commit('SET_SPOT_TABLE_ITEMS', [])
        commit('SET_SPOT_CARRIER_STATE', LOADED_STATE.FAIL)
      }
    },
    async getUserInfo ({ commit }) {
      const userInfo = await api.accountOrder.getUser()
      commit('SET_SPOT_USER_INFO', userInfo.data)
    },
  }
}
