import api from '@/api'
import { getTokenData } from '@/tokenData'

const CLIENT_CODE = 'client'
const DOER_CODE = 'doer'

const STATUSES = {
  draft: 'DRAFT',
  signing: 'ON_SIGNING',
  signed: 'SIGNED',
}

const VIEW_STATES = {
  main: 'main',
  serviceDetail: 'serviceDetail'
}

export default {
  namespaced: true,
  state: {
    contract: {
      uuid: null,
      status: null
    },
    viewState: VIEW_STATES.main,
    currentRole: CLIENT_CODE,
    existServices: false,
    existTariffAgreements: false,
    editedService: {},
    doerInfo: {},
    participantInfo: {},
    isVisible: true
  },
  getters: {
    contract (state) {
      return state.contract
    },
    CLIENT_CODE () {
      return CLIENT_CODE
    },
    DOER_CODE () {
      return DOER_CODE
    },
    VIEW_STATES () {
      return VIEW_STATES
    },
    currentRole (state) {
      return state.currentRole
    },
    isViewMain (state) {
      return Boolean(state.viewState === VIEW_STATES.main )
    },
    isViewServiceDetail (state) {
      return Boolean(state.viewState === VIEW_STATES.serviceDetail )
    },
    isDraft (state) {
      return Boolean(state.contract.status === STATUSES.draft)
    },
    isSigning (state) {
      return Boolean(state.contract.status === STATUSES.signing)
    },
    isSigned (state) {
      return Boolean(state.contract.status === STATUSES.signed)
    },
    isStatus (state) {
      return Boolean(state.contract.status)
    },
    isNotCreator (state) {
      return Boolean(getTokenData().contractor_uuid !== state.contract.creatorUuid)
    },
    isTracking (state) {
      return state.contract.contractTypeCode === 'TRACKING'
    },
    isTransportation (state) {
      return state.contract.contractTypeCode === 'TRANSPORTATION'
    },
    isTariffAgreement (state) {
      return state.contract.contractTypeCode === 'TARIFF_AGREEMENT'
    },
    isTitul (state) {
      return state.contract.contractTypeCode === 'TRANSPORTATION_PARENT_CONTRACT'
    },
    signedDate (state) {
      if (state.contract.creatorUuid === state.contract.clientUuid) {
        return state.contract.signedByDoerAt
      } else {
        return state.contract.signedByClientAt
      }
    },
    isNotSelectClientUser (state) {
      return state.currentRole === CLIENT_CODE ? false : !state.contract.clientUuid
    },
    isNotSelectDoerUser (state) {
      return state.currentRole === DOER_CODE ? false : !state.contract.doerUuid
    },
    isClientAccountUser (state) {
      return Boolean(state.contract.clientCuratorUuid)
    },
    isDoerAccountUser (state) {
      return Boolean(state.contract.doerCuratorUuid)
    },
    isClientAuthority (state) {
      // return Boolean(state.contract.clientAuthoritySource && state.contract.clientProcuratoryFileUuid && state.contract.clientSignerFio)
      return Boolean(state.contract.clientSignerFio)
    },
    isDoerAuthority (state) {
      // return Boolean(state.contract.doerAuthoritySource && state.contract.doerProcuratoryFileUuid && state.contract.doerSignerFio)
      return Boolean(state.contract.doerSignerFio)
    },
    contractId (state) {
      return state.contract.uuid
    },
    isTimeless (state) {
      return state.contract.isTimeless
    },
    isExistServices (state) {
      return state.existServices
    },
    isExistTariffAgreements (state) {
      return state.existTariffAgreements
    },
    service (state) {
      return state.editedService
    },
    doerInfo (state) {
      return state.doerInfo
    },
    isDoerVatPayer (state) {
      return Boolean(state.doerInfo.isVatPayer)
    },
    isVisible (state) {
      return state.isVisible
    },
    participantInfo (state) {
      return state.participantInfo
    },
    isExistClicar ({ contract, participantInfo }) {
      return [ contract.clientUuid, contract.doerUuid].some(uuid => {
        return uuid && participantInfo[uuid]?.role === 'CLICAR'
      })
    },
    isManualCreated ({ contract }) {
      return contract?.isManualCreated
    },
  },
  mutations: {
    setContractData (state, data = {}) {
      state.contract = {
        ...state.contract,
        ...data
      }
    },
    setViewState (state, value = '') {
      state.viewState = value
    },
    clearViewState (state) {
      state.viewState = VIEW_STATES.main
    },
    setCurrentRole (state, value = '') {
      state.currentRole = value
    },
    setExistServices (state, value = false) {
      state.existServices = value
    },
    setExistTariffAgreements (state, value = false) {
      state.existTariffAgreements = value
    },
    setEditedService (state, data = {}) {
      state.editedService = data
    },
    clearContractData (state) {
      state.contract = {}
    },
    setDoerInfo (state, data) {
      state.doerInfo = data
    },
    setIsVisible (state, value) {
      state.isVisible = value
    },
    setParticipantInfo (state, { uuid, data } = {}) {
      if (uuid) {
        state.participantInfo = {
          ...state.participantInfo,
          [uuid]: data
        }
      }
    }
  },
  actions: {
    clearState ({ commit }) {
      commit('clearContractData')
      commit('clearViewState')
      commit('setExistServices', false)
      commit('setCurrentRole', CLIENT_CODE)
    },
    async getParticipantInfo ({ commit }, { uuid, isDoer = false, }) {
      let data = await api.contract.getPaticipantInfo({ uuid })
      commit('setParticipantInfo', { uuid, data })

      if (isDoer) {
        commit('setDoerInfo', data)
      }

      return data
    },
    async setContractField ({ dispatch, commit }, { field, value, save = false }) {
      let data = {}

      if (['clientUuid', 'doerUuid'].includes(field)) {
        save = true
        const role = field === 'clientUuid' ? CLIENT_CODE : DOER_CODE
        const { legalAddress, postAddress} = await dispatch('getParticipantInfo', { uuid: value, isDoer: field === 'doerUuid' })

        data[role + 'LegalAddress'] = legalAddress
        data[role + 'PostAddress'] = postAddress

        if (field === 'clientUuid') {
          data.clientCuratorUuid = null
        }
        if (field === 'doerUuid') {
          data.doerCuratorUuid = null
        }
      }

      if (field === 'isTimeless') {
        if (value) {
          data.toDate = ''
        }
      }

      data[field] = value

      commit('setContractData', data)

      if (save) {
        dispatch('saveContract', data)
      }
    },
    async reverseContractData ({ commit, dispatch, state }) {
      let reverseData = {
        clientUuid: state.contract.doerUuid,
        doerUuid: state.contract.clientUuid,
        clientTitle: state.contract.doerTitle,
        doerTitle: state.contract.clientTitle,
        contractTypeUuid: state.contract.contractTypeUuid,
        fromDate: state.contract.fromDate,
        toDate: state.contract.toDate,
        clientSignerFio: state.contract.doerSignerFio,
        doerSignerFio: state.contract.clientSignerFio,
        clientCuratorUuid: state.contract.doerCuratorUuid,
        doerCuratorUuid: state.contract.clientCuratorUuid,
        clientAuthoritySource: state.contract.doerAuthoritySource,
        doerAuthoritySource: state.contract.clientAuthoritySource,
        clientEmail: state.contract.doerEmail,
        doerEmail: state.contract.clientEmail,
        clientPhone: state.contract.doerPhone,
        doerPhone: state.contract.clientPhone,
        clientPostAddress: state.contract.doerPostAddress,
        doerPostAddress: state.contract.clientPostAddress,
        clientLegalAddress: state.contract.doerLegalAddress,
        doerLegalAddress: state.contract.clientLegalAddress,
        clientProcuratoryFileUuid: state.contract.doerProcuratoryFileUuid,
        doerProcuratoryFileUuid: state.contract.clientProcuratoryFileUuid,
        clientFileTitle: state.contract.doerFileTitle,
        doerFileTitle: state.contract.clientFileTitle,
        clientTerminationUnilateralPeriod: state.contract.doerTerminationUnilateralPeriod,
        doerTerminationUnilateralPeriod: state.contract.clientTerminationUnilateralPeriod,
        needNotifyClientByMail: state.contract.needNotifyDoerByMail,
        needNotifyDoerByMail: state.contract.needNotifyClientByMail,
        needNotifyDoerByJurMail: state.contract.needNotifyClientByJurMail,
        needNotifyClientByJurMail: state.contract.needNotifyDoerByJurMail,
        needNotifyDoerByEmail: state.contract.needNotifyClientByEmail,
        needNotifyClientByEmail: state.contract.needNotifyDoerByEmail,
        needNotifyDoerByPhone: state.contract.needNotifyClientByPhone,
        needNotifyClientByPhone: state.contract.needNotifyDoerByPhone,
        isTerminationClient: state.contract.isTerminationDoer,
        isTerminationDoer: state.contract.isTerminationClient,
      }

      commit('setContractData', reverseData)

      if (reverseData.doerUuid) {
        await dispatch('getParticipantInfo', { uuid: reverseData.doerUuid, isDoer: true })
      }

      dispatch('saveContract')
    },
    async saveContract ({ state }, data = {}) {
      try {
        let saveData = {
          ...state.contract,
          ...data
        }
        await api.contract.saveContractDraft({ data: saveData, type: state.contract.contractTypeCode })
      } catch (e) {
        throw e
      }
    },
    async getContractData ({commit, dispatch }, { uuid, init = false }) {
      try {
        let data = await api.contract.getContractDraft({ uuid })
        let files = await dispatch('getFiles', data)
        const { email, contractor_uuid, user_uuid } = getTokenData()
        const isTitul = data.contractTypeCode === 'TRANSPORTATION_PARENT_CONTRACT'
        const isCreator = data.creatorUuid === contractor_uuid
        const isUserClient = data.clientUuid === contractor_uuid
        const isSigning = data.status === 'ON_SIGNING'

        await dispatch('calcExistServices', { data })
        await dispatch('calcExistTariffAgreements', { data })

        data.authorityFiles = {}
        files.forEach((file) => {
          data.authorityFiles[file.role] = {
            uuid: file.uuid,
            file: {
              name: file.name
            }
          }
        })

        data.files = data.files.map((file) => {
          return {
            uuid: file.file_uuid,
            file: {
              name: file.filename
            }
          }
        })

        data.isTerminationClient = Boolean(data.clientTerminationUnilateralPeriod)
        data.isTerminationDoer = Boolean(data.doerTerminationUnilateralPeriod)


        if (isTitul && !isCreator && isSigning) {
          // const signerKey = isUserClient ? 'clientSignerFio' : 'doerSignerFio'
          const curatorKey = isUserClient ? 'clientCuratorUuid' : 'doerCuratorUuid'
          const mailKey = isUserClient ? 'clientEmail' : 'doerEmail'
          const mailNotifyKey = isUserClient ? 'needNotifyClientByEmail' : 'needNotifyDoerByEmail'

          // data[signerKey] = !data[signerKey] ? rootGetters['user/fullName'] : data[signerKey]
          data[curatorKey] = !data[curatorKey] ? user_uuid : data[curatorKey]
          data[mailKey] = !data[mailKey] ? email : data[mailKey]
          data[mailNotifyKey] = true
        }

        commit('setCurrentRole', data.creatorUuid === data.clientUuid ? CLIENT_CODE : DOER_CODE)
        commit('setContractData', data)
        commit('setIsVisible', true)

        if (init) {
          const participantUuids = [data.clientUuid, data.doerUuid]
          const promiseParticipantInfo = participantUuids.filter(item => item).map(async (uuid, index) => {
            return await dispatch('getParticipantInfo', { uuid: uuid, isDoer: index === 1 })
          })

          await Promise.all(promiseParticipantInfo)
        }
      } catch (e) {
        throw e
      }
    },
    async calcExistServices ({ commit }, { data }) {
      const isAllowedType = ['TRACKING', 'TARIFF_AGREEMENT'].includes(data.contractTypeCode)
      const isTracking = data.contractTypeCode === 'TRACKING'
      const uuid = data.uuid
      let existServices = false

      if (isAllowedType) {
        const simple = await api.contract.getContractServices({ uuid, page: 0, size: 1, caste: isTracking ? 'TRACKING' : 'SIMPLE' })
        const multistop = await api.contract.getContractServices({ uuid, page: 0, size: 1, caste: 'MULTISTOP' })
        const additional = await api.contract.getContractServices({ uuid, page: 0, size: 1, caste: 'ADDITIONAL' })

        existServices = (simple.totalElements + multistop.totalElements + additional.totalElements) > 0
      }
      commit('setExistServices', existServices)
    },
    async calcExistTariffAgreements ({ commit }, { data }) {
      const isTitul = data.contractTypeCode === 'TRANSPORTATION_PARENT_CONTRACT'
      const uuid = data.uuid
      let existTariffAgreements = false

      if (isTitul) {
        const tariffAgreements = await api.contract.getTariffAgreements({ uuid })

        existTariffAgreements = tariffAgreements?.length > 0
      }
      commit('setExistTariffAgreements', existTariffAgreements)
    },
    async getFiles (context, data) {
      try {
        let files = [
          {
            uuid: data.clientProcuratoryFileUuid,
            role: CLIENT_CODE
          },
          {
            uuid: data.doerProcuratoryFileUuid,
            role: DOER_CODE
          }
        ]

        let filesUuids = files.filter(item => Boolean(item.uuid)).map(item => item.uuid)
        let names = filesUuids.length ? await api.contract.getFilesName({ uuids: filesUuids }) : []

        return names.map(({ fileUuid, fileName }) => {
          let fileData = files.find(item => item.uuid === fileUuid)
          fileData = fileData || {}

          return {
            name: fileName,
            uuid: fileUuid,
            role: fileData.role
          }
        })
      } catch (e) {
        throw e
      }
    },
    async postFiles (context, { files, contract_uuid }) {
      let promises = []

      try {
        files.forEach((file) => {
          promises.push(api.contract.uploadFile(file, contract_uuid))
        })

        return Promise.all(promises)
      } catch (e) {
        throw e
      }
    },
    async deleteFiles (context, { uuids }) {
      let promises = []

      try {
        uuids.forEach((uuid) => {
          promises.push(api.contract.deleteFile({ uuid }))
        })

        return Promise.all(promises)
      } catch (e) {
        throw e
      }
    }
  }
}
