import api from '../../api'

export const GET_PARAMETERS = 'GET_PARAMETERS'
export const SET_PARAMETERS = 'SET_PARAMETERS'

export default {
  state: {
    items: []
  },
  getters: {
    getParameters: state => {
      return state.items
    }
  },
  actions: {
    [GET_PARAMETERS]: async ({ commit }, uuid) => {
      try {
        const res = await api.parameters.fetchParameters(uuid)
        commit(SET_PARAMETERS, res)
      } catch (ex) {
        console.log(ex)
      }
    },
  },
  mutations: {
    [SET_PARAMETERS]: (state, res) => {
      state.items = res.map(item => {
        item.title = item.name
        return item
      })
    }
  }
}