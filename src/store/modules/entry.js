export default {
  namespaced: true,
  state: {
    accreditationFormData: {
      organisation_name: '', //Not needed
      activities: [],
      user_firstname: '',
      user_lastname: '',
      user_middlename: '',
      ceo_lastname: '',
      ceo_firstname: '',
      ceo_middlename: '',
      passport_number: '',
      issued_date: '',
      dob: '',
      passport: '',
      bik: '',
      payment_account: '',
      correspondent_account: '', // Must be filled automaticly
      bank: '', // Must be filled automaticly
      vat: false,
      passportFileUuid: '',
      innFileUuid: '',
      isLightRegistration: true
    },
    email: null,
    registrationDone: false
  },
  getters: {
    accreditationFormData: state => state.accreditationFormData,
    email: state => state.email,
    registrationDone: state => state.registrationDone
  },
  mutations: {
    setAccreditationFormData(state, { key, value }) {
      state.accreditationFormData[key] = value;
    },
    setEmail(state, email) {
      state.email = email
    },
    setRegistrationDone(state) {
      state.registrationDone = true;
    }
  },
  actions: {
    setAccreditationFormData({ commit }, data) {
      commit('setAccreditationFormData', data);
    },
    setEmail({ commit }, email) {
      commit('setEmail', email);
    },
    setRegistrationDone({ commit }) {
      commit('setRegistrationDone')
    }
  }
}