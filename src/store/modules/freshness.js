import api from '../../api'
// import data from '../../resources/data';

export const GET_FRESHNESS = 'GET_FRESHNESS'
export const SET_FRESHNESS = 'SET_FRESHNESS'
export const UPDATE_FRESHNESS ='UPDATE_FRESHNESS'

export default {
 state: {
    currentFreshness: {}
  },
  getters: {
    getCurrentFreshness: state => {
      return state.currentFreshness
    }
  },
  actions: {
    [GET_FRESHNESS]: async ({ commit }, uuid) => {
      try {
        const res = await api.freshness.fetchDateFreshnessOfMethodology(uuid)
        commit(SET_FRESHNESS, res)
      } catch (ex) {
        console.log(ex)
      }
    },
    [UPDATE_FRESHNESS]: async ({ dispatch }, { uuid, payloadFreshness }) => {
      try {
        await api.freshness.updateDataFreshnessOfMethodology(uuid, payloadFreshness)
        dispatch(GET_FRESHNESS, uuid)
      } catch (ex) {
        console.log(ex)
      }
    }
  },
  mutations: {
    [SET_FRESHNESS]: (state, res) => {
      state.currentFreshness = res
    }
  }
}
