import api from '../../api'
// import data from '../../resources/data';

export const GET_METHODOLOGIES = 'GET_METHODOLOGIES'
export const SET_METHODOLOGIES = 'SET_METHOLOGIES'
export const CREATE_METHODOLOGY ='CREATE_METHODOLOGY'

export default {
 state: {
    list: []
  },
  getters: {
    getMethodologies: state => {
      return state.list
    }
  },
  actions: {
    [GET_METHODOLOGIES]: async ({ commit }) => {
      try {
        const res = await api.methodologies.fetchMethodologies()
        // const res = data.resourceFields
        commit(SET_METHODOLOGIES, res)
      } catch (ex) {
        console.log(ex)
      }
    },
    [CREATE_METHODOLOGY]: async ({ dispatch }, payload) => {
      try {
        await api.methodologies.createMethodology(payload)
        dispatch(GET_METHODOLOGIES)
      } catch (ex) {
        console.log(ex)
      }
    }
  },
  mutations: {
    [SET_METHODOLOGIES]: (state, res) => {
      state.list = res
    }
  }
}
