import api from '@/api'
import { getRiskClass } from '@/helpers/risks'
import { parseNumber } from '@/helpers/order'
import cloneDeep from 'clone-deep'
import WebSocketClient from '@/api/WebSocketClient'
import throttle from 'lodash.debounce'
import STEPS from '@/constants/steps.js'

const OFFER_SUBORDER_KEYS = {
  TO: 'targetSuborderUuid',
  FROM: 'fromSuborderUuid'
}

let processOrderUuid = null
let initOrderResources = null

const getVatByRouteCategory = ({ routeCategory, isVatPayer, defaultVatRate = 0 }) => {
  let vatRate

  if (routeCategory && isVatPayer) {
    if (['INTERNATIONAL'].includes(routeCategory)) {
      vatRate = 0
    } else if (['URBAN', 'INTERURBAN'].includes(routeCategory)) {
      vatRate = 20
    }
  } else {
    vatRate = defaultVatRate
  }

  return vatRate || 0
}

export default {
  namespaced: true,
  state: {
    vat: null,
    filters: {
      searchType: 'ALL',
      resourceSpeciesUuid: null,
      resourceTypeUuid: null,
      resourceClassUuid: null,
      beforeDt: null,
      afterDt: null,
      fromLocationUuid: null,
      fromLocation: null,
      toLocationUuid: null,
      toLocation: null
    },
    loaderFilter: false,
    orders: [],
    loadListMethod: null,
    ordersResponse: {},
    order: {},
    tab: 'ALL',
    noticeTab: [],
    statementUuid: null,
    resources: [],
    listHelperLogistic: [],
    listSuitableCars: {},
    tableLoader: false,
    listSignalLoader: false,
    rightPanel: false,
    orderSignalLoader: false,
    buttonRate: false,
    buttonAcceptResource: false,
    buttonSignResource: false,
    statusFilter:'ALLOCATABLE',
    advance: {},
    hasAdvance: false,
    hideAdvanceButton: false,
    requiredResources: {},
    requiredResourcesValues: {},
    wsClient: null,
    offerSuborderUuid: OFFER_SUBORDER_KEYS.TO
  },
  getters: {
    isAuction (state) {
      return state.order?.carrierSearchType === STEPS.AUCTION
    },
    isBooking (state) {
      return state.order?.carrierSearchType === STEPS.BOOKING
    },
    isOffer (state) {
      return state.order?.carrierSearchType === STEPS.OFFER
    },
    vat: state => state.vat,
    suborders: state => state.suborders,
    loaderFilter: state => state.loaderFilter,
    orders: state => state.orders,
    order: state => state.order,
    tab: state => state.tab,
    noticeTab: state => state.noticeTab,
    listSuitableCars: state => state.listSuitableCars,
    tableLoader: state => state.tableLoader,
    rightPanel: state => state.rightPanel,
    listSignalLoader: state => state.listSignalLoader,
    orderSignalLoader: state => state.orderSignalLoader,
    buttonRate: state => state.buttonRate,
    buttonAcceptResource: state => state.buttonAcceptResource,
    buttonSignResource: state => state.buttonSignResource,
    resources: state => state.resources,
    canAcceptResource: state => state.resources.length >= 2,
    canSubmitDoc: state => state.statementUuid,
    statusFilter: state => state.statusFilter,
    advance: state => state.advance,
    hasAdvance: state => state.hasAdvance,
    hideAdvanceButton: state => state.hideAdvanceButton,
    filters: state => state.filters,
    isVatPayer: state => Boolean(state.vat),
    vatRate (state) {
      const orderType = state.order?.type
      const routeCategory = state.order?.suborderSpecies?.routeCategory
      const isVatPayer = Boolean(state.vat)

      if (orderType === 'auction' && isVatPayer) {
        if (['INTERNATIONAL'].includes(routeCategory)) {
          return 0
        } else if (['URBAN', 'INTERURBAN'].includes(routeCategory)) {
          return 20
        }
      } else {
        return state.vat || 0
      }
    },
    requiredResources (state) {
      return state.requiredResources
    },
    requiredResourcesValues (state) {
      return state.requiredResourcesValues
    },
    statementUuid (state) {
      return state.statementUuid
    },
    ordersResponse (state) {
      return state.ordersResponse
    },
    currentOrdersList (state) {
      return state.ordersResponse?.content || state.ordersResponse?.suborderList || []
    },
    currentOrdersListPage (state) {
      return state.ordersResponse?.page || 0
    },
    loadListMethod (state) {
      return state.loadListMethod
    },
    wsClient (state) {
      return state.wsClient
    },
    offerSuborderUuid (state) {
      return state.offerSuborderUuid
    }
  },
  mutations: {
    setVat (state, vat) {
      state.vat = vat
    },
    order (state, payload) {
      state.order = payload
    },
    orders (state, payload) {
      state.orders = payload
    },
    setLoadListMethod (state, payload) {
      state.loadListMethod = payload
    },
    setOrdersResponse (state, payload) {
      state.ordersResponse = payload
    },
    setFilters (state, payload) {
      state.filters = payload
    },
    setLoaderFilter (state, payload) {
      state.loaderFilter = payload
    },
    updateListHelper (state, payload) {
      state.listHelperLogistic = payload;
    },
    setHelperLogistic (state, payload) {
      state.listSuitableCars = {
        startAddress: state.order.startAddress,
        endAddress: state.order.endAddress,
        coupleList: payload.coupleList || []
      }
    },
    clearHelperLogistic (state) {
      state.listSuitableCars = {}
    },
    setListSignalLoader (state, payload) {
      state.listSignalLoader = payload
    },
    setOrderSignalLoader (state, payload) {
      state.orderSignalLoader = payload
    },
    setActiveTab (state, tab) {
      state.tab = tab
    },
    setNotice (state) {
      state.noticeTab.splice(0, state.noticeTab.length)

      if (state.orders.some(key => key.type === 'auction' && key.status === null && key.currentState === 'NO_BIDS')) {
        state.noticeTab.push('AUCTION')
      } else if (state.orders.some(key => key.type === 'auction' && key.status === 'ACTIVE' && key.currentState === 'LOSING')) {
        state.noticeTab.push('LOSING')
      } else if (state.orders.some(key => key.type === 'booking' && key.status === 'ACTIVE')) {
        state.noticeTab.push('BOOKING')
      } else if(state.orders.some(key => key.status === 'LOSER' || key.status === 'PROCESSED')) {
        state.noticeTab.push('IRRELEVANT')
      }
    },
    resource (state, resource) {
      if (resource.data !== null) {
        let _resource = state.resources.find(r => r.superclass === resource.data.superclass);
        if (!_resource) {
          state.resources.push(resource.data);
        }
        else {
          _resource.uuid = resource.data.uuid
        }
      }
      else {
        let _resource = state.resources.find(r => r.superclass === resource.superclass);
        state.resources.forEach(function(d, index){
          if (d == _resource) {
            state.resources.splice(index, 1)
          }
        });
      }
    },
    setResources (state, data) {
      state.resources = data
    },
    statement (state, uuid) {
      state.statementUuid = uuid
    },
    clearOrder (state) {
      state.order = {};
    },
    turnLoaderTable (state, payload) {
      state.tableLoader = payload
    },
    loaderRightPanel (state, payload) {
      state.rightPanel = payload
    },
    setDisabledRate (state, payload) {
      state.buttonRate = payload
    },
    setDisabledAcceptResource (state, payload) {
      state.buttonAcceptResource = payload
    },
    setDisabledSignResource (state, payload) {
      state.buttonSignResource = payload
    },
    setSearchTyp (state, payload){
      state.filters.searchType = payload
    },
    setAdvance (state, payload){
      state.advance = payload
    },
    setHasAdvance (state, payload){
      state.hasAdvance = payload
    },
    setHideAdvanceButton (state, payload){
      state.hideAdvanceButton = payload
    },
    setSearchType(state, payload) {
      state.filters.searchType = payload
    },
    setRequiredResources (state, { uuid, data } = {}) {
      if (uuid) {
        state.requiredResources = {
          ...state.requiredResources,
          [uuid]: data
        }
      }
    },
    setRequiredResourcesValues (state, { uuid, data } = {}) {
      if (uuid) {
        state.requiredResourcesValues = {
          ...state.requiredResourcesValues,
          [uuid]: data
        }
      }
    },
    setWSClient (state, wsClient) {
      state.wsClient = wsClient
    },
    setOfferSuborderUuid (state, value) {
      state.offerSuborderUuid = value
    }
  },
  actions: {
    async getVat ({ commit }) {
      try {
        const { data } = await api.accountOrder.getUser()

        commit('setVat', data.contractorInfo.vat)
      } catch (e) {
        console.log(e)
        commit('setVat', null)
      }
    },
    async initWSClient ({ state, commit, dispatch }) {
      if (!state.wsClient) {
        let WsClient = new WebSocketClient({ handler: (data) => dispatch('websocketMessageHandler', data)  })
        await WsClient.connect()
        commit('setWSClient', WsClient)
      }
    },
    async websocketMessageHandler ({ dispatch, state, rootGetters }, { message, type }) {
      setTimeout(() => {
        const itemUuid = message.parentEntityUuid
        const isDetailOpen = state.order?.suborderUuid === itemUuid
        const $notification = rootGetters.$notification
        const isTabAll = state.tab === 'ALL'
        const isTabBooking = state.tab === 'BOOKING'
        const isTabAuction = state.tab === 'AUCTION'
        const isFirstPage = state.ordersResponse?.page === 0
        const isAssignedResourcesPage = state.ordersResponse?.searchStatus === 'BOOKED'
        const isExistItemInList = state.ordersResponse?.suborderList?.find(item => item.uuid === itemUuid)

        if (type === 'BOOKING_NEW_REQUEST') {
          if ((isTabAll || isTabBooking) && isFirstPage && !isExistItemInList && !isAssignedResourcesPage) {
            dispatch('updateTableList')
          } else {
            $notification({type: 'snackbars', message: `Появился новый букинг-запрос` })
          }
        }
        if (type === 'BOOKING_REQUEST_ACCEPTED' && !isAssignedResourcesPage) {
          if (isExistItemInList) {
            dispatch('updateTableList')
          }
        }
        if (type === 'BOOKING_LOT_CANCELLED' && !isAssignedResourcesPage) {
          if (isExistItemInList) {
            dispatch('updateTableList')
          }
          if (isDetailOpen) {
            dispatch('updateOrder')
          }
        }
        if (type === 'BOOKING_ENDED') {
          if (isAssignedResourcesPage && (isTabAll || isTabBooking) && isFirstPage && !isExistItemInList) {
            dispatch('updateTableList')
          } else if (isExistItemInList) {
            dispatch('updateTableList')
          }
        }
        if (type === 'AUCTION_NEW_LOT') {
          if ((isTabAll || isTabAuction) && isFirstPage && !isExistItemInList && !isAssignedResourcesPage) {
            dispatch('updateTableList')
          } else {
            $notification({type: 'snackbars', message: `Появился новый аукцион` })
          }
        }
        if (type === 'AUCTION_ENDED') {
          if (isAssignedResourcesPage && (isTabAll || isTabAuction) && isFirstPage && !isExistItemInList) {
            dispatch('updateTableList')
          } else if (isExistItemInList) {
            dispatch('updateTableList')
          }
          if (isDetailOpen) {
            dispatch('updateOrder')
          }
        }
        if (type === 'AUCTION_CANCELLED') {
          if (isAssignedResourcesPage && (isTabAll || isTabAuction) && isFirstPage && !isExistItemInList) {
            dispatch('updateTableList')
          } else if (isExistItemInList) {
            dispatch('updateTableList')
          }
          if (isDetailOpen) {
            dispatch('updateOrder')
          }
        }
        if (type === 'AUCTION_DURATION_UPDATED' && !isAssignedResourcesPage) {
          if (isExistItemInList) {
            dispatch('updateTableList')
          }
          if (isDetailOpen) {
            dispatch('updateOrder')
          }
        }
        if (type === 'AUCTION_NEW_BID' && !isAssignedResourcesPage) {
          if (isExistItemInList) {
            dispatch('updateTableList')
          }
          if (isDetailOpen) {
            dispatch('updateOrder')
          }
        }
      }, 300)
    },
    updateTableList: throttle(async ({ getters }) => {
      if (getters.loadListMethod) {
        getters.loadListMethod()
      }
    }, 1000),
    updateOrder: throttle(async ({ dispatch, state }) => {
      dispatch('getOrder', state.order)
    }, 1000),
    async getOrders ({ commit, dispatch, state }, { carrierSearchType, searchStatus, page = 0, size= 50, q, sort } = {}) {
      try {
        // dispatch('initWSClient')
        let data = await api.accountOrder.getExecutorsOrders({ filters: state.filters, carrierSearchType, searchStatus, page, size, q, sort })

        data.searchStatus = searchStatus
        data.content = data.content.map(item => {
          return {
            ...item,
            suborderUuid: item.uuid,
            orderTotalAmount: item.orderTotalAmount || {},
            completionDt: item.secondsBeforeExpiration > 0 ? +new Date() + (item.secondsBeforeExpiration * 1000) : new Date(),
            type: item.auctionLotUuid ? 'auction' : 'booking',
            uuid: item.auctionLotUuid || item.bookingRequestUuid || item.personalOfferUuid
          }
        })

        commit('orders', data.content)
        commit('setOrdersResponse', data)

        dispatch('sortOrders', [ ...data.content ])

        // https://oboz.myjetbrains.com/youtrack/issue/OR-7899
        // if (!state.listHelperLogistic.length) {
        //   const { data } = await api.accountOrder.listHelperLogistic(state.orders.map(key => key.suborderUuid))
        //
        //   dispatch('addHelperLogistic', data.matchableCoupleList)
        //   commit('updateListHelper', data.matchableCoupleList)
        // }
        // else {
        //   dispatch('addHelperLogistic', state.listHelperLogistic);
        // }

        return data
      } catch (e) {
        console.log('ERROR: ', e)
        commit('orders', [])
        // dispatch('addHelperLogistic', [])
        // commit('updateListHelper', [])
      } finally {
        commit('turnLoaderTable', false)
        commit('setListSignalLoader', false)
      }
    },
    sortOrders ({ commit }, result) {
      let sortData = result

      commit('orders', sortData)
      commit('setNotice', sortData)
    },
    async addHelperLogistic ({ commit, state }, helper) {
      let data = state.orders.reduce((acc, value) => {
        const suborder = helper.find(key => key.suborderUuid === value.suborderUuid)
        if (suborder) {
          acc.push({
            ...value,
            helper_logistic: {
              ...suborder
            }
          })
        } else {
          acc.push(value)
        }
        return acc
      }, [])

      commit('orders', data)
    },
    async updateFilters ({ commit }, filters) {
      commit('setLoaderFilter', true)
      commit('setFilters', filters)
      commit('clearOrder')
    },
    async getOrder ({ commit, dispatch, state, getters }, current ) {
      if (current?.isAuto && current?.uuid !== processOrderUuid) return false

      if (current?.uuid !== processOrderUuid) {
        initOrderResources = null
      }

      processOrderUuid = current?.uuid || null

      try {
        commit('setDisabledAcceptResource', false)

        const carrierSearchType = current.carrierSearchType?.toLowerCase()

        let info = {}
        let totalPrice
        let suborderUuid
        let resources

        if (['auction', 'booking'].includes(carrierSearchType)) {
          info = await api.accountOrder.getAdditionalInfoOrder(current['uuid'], current['type'])
          totalPrice = info.data.totalPrice
          suborderUuid = current.suborderUuid
        } else if (['offer'].includes(carrierSearchType)) {
          info = await api.accountOrder.getPersonalOfferInfo(current['uuid'] || current['offerUuid'])
          totalPrice = info.data.targetSuborderPrice && info.data.fromSuborderPrice ? info.data.offerPrice : info.data.targetSuborderPrice
          suborderUuid = info.data[state.offerSuborderUuid]
        }

        const { data } = await api.accountOrder.getAccountOrder(suborderUuid)

        const isVatPayer = getters.isVatPayer
        const routeCategory = data.carrierResponse?.suborderSpecies?.routeCategory
        const requiredResources = data.carrierResponse.resources.requiredResources

        if (['auction', 'booking'].includes(carrierSearchType) || data.carrierResponse.status !== 'NEW') {
          resources = data.carrierResponse.resources.assignedResources
        } else if (['offer'].includes(carrierSearchType)) {
          resources = info.data.resources || []
        }

        let resourceList = resources.map((item) => {
          return {
            ...item,
            icon: getRiskClass(item.riskColor),
            ...parseNumber(item.number, item.superclass)
          }
        })

        const isFirstLoad = !initOrderResources

        if (!initOrderResources) {
          initOrderResources = cloneDeep(resourceList)
        }

        const isChangeResources = JSON.stringify(initOrderResources.map(item => item.uuid)) !== JSON.stringify(resourceList.map(item => item.uuid))
        const isSetResources = isFirstLoad || isChangeResources

        dispatch('getRequiredResources', { data: { ...current, resources: resourceList }, status: info.data.status, orderStatus: data.carrierResponse?.status, isChangeResources })

        const vatRate = getVatByRouteCategory({ isVatPayer, routeCategory, defaultVatRate: state.vat })
        let postData = []

        postData.push({
          vatRate,
          quantity: 1,
          priceWithoutVat: totalPrice
        })
        postData.push({
          vatRate,
          quantity: 1,
          priceWithoutVat: info.data.biddingStep
        })
        postData.push({
          vatRate,
          quantity: 1,
          priceWithoutVat: info.data.targetSuborderPrice
        })
        postData.push({
          vatRate,
          quantity: 1,
          priceWithoutVat: info.data.fromSuborderPrice
        })

        const calculation = await api.orderProcess.vatCalculation({ list: postData })
        const totalPriceCalc = calculation.itemsList[0] || {}
        const biddingStepCalc = calculation.itemsList[1] || {}
        const targetSuborderCalc = calculation.itemsList[2] || {}
        const fromSuborderCalc = calculation.itemsList[3] || {}
        const secondsBeforeExpiration = data.carrierResponse.secondsBeforeExpiration

        const extraCost = await api.accountOrder.getExtraRunAndPoint(current['suborderUuid'])

        let common = {
          ...data.carrierResponse,
          ...info.data,
          suborderUuid: current.suborderUuid,
          type: current['type'],
          carrierSearchType: carrierSearchType?.toLowerCase(),
          orderStatus: data.carrierResponse.status,
          status: info.data.status,
          resources: isSetResources ? resourceList : state.order.resources,
          requiredResources,
          totalPriceAmount: { ...totalPriceCalc },
          biddingStepAmount: { ...biddingStepCalc },
          targetSuborderAmount: { ...targetSuborderCalc },
          fromSuborderAmount: { ...fromSuborderCalc },
          expirationStatusDate: secondsBeforeExpiration > 0 ? +new Date() + secondsBeforeExpiration : new Date(),
          extraCost: extraCost
        }

        if (current['helper_logistic']) {
          common = {
            ...common,
            helper_logistic: current['helper_logistic']
          }
        }

        commit('order', {});
        commit('order', common)

        if (isSetResources) {
          commit('setResources', resourceList)
        }

      } catch (e) {
        console.log(e)
        commit('order', {})
      } finally {
        commit('loaderRightPanel', false)
      }
    },
    async getRequiredResources ({ commit, getters }, { data, orderStatus, status }) {
      const isGetResources = ['WINNER'].includes(status) && ['BOOKED', 'ALLOCATED', 'CONTRACTED'].includes(orderStatus) || (data.carrierSearchType === 'OFFER' && data.offerStatus !== 'NEW')
      const isExistResourcesInOrder = data.resources?.length > 0
      const currentValue = getters.requiredResourcesValues[data.uuid]

      if (!initOrderResources) {
        initOrderResources = cloneDeep(data.resources)
      }

      const isChangeResources = JSON.stringify(initOrderResources.map(item => item.uuid)) !== JSON.stringify(data.resources.map(item => item.uuid))

      if (!isGetResources) return false

      let resourcesData = await api.orderProcess.getRequiredResources({ uuid: data.suborderUuid })
      commit('setRequiredResources', { uuid: data.uuid, data: resourcesData })

      const isCorrectCurrentValue = resourcesData.find(item => item.serviceTypeUuid === currentValue?.serviceTypeUuid)
      let activeItem
      let isSetActive

      if (data.isAuto) {
        isSetActive = !currentValue || !isCorrectCurrentValue || orderStatus !== 'BOOKED' || isChangeResources
      } else {
        isSetActive = true
      }

      if (isSetActive) {
        if (isExistResourcesInOrder) {
          activeItem = resourcesData.find((item) => {
            return data.resources.every(({ superclass }) => {
              return item.requiredResources?.find(({ resourceSpeciesCode }) => resourceSpeciesCode === superclass)
            })
          })
        } else {
          activeItem = resourcesData.find(item => item.isMain)
        }

        initOrderResources = cloneDeep(data.resources)

        commit('setRequiredResourcesValues', { uuid: data.uuid, data: activeItem })
      }
    },
    async writeDataLogistic ({ dispatch, commit, state }) {
      try {
        const { data } = await api.accountOrder.getHelperLogisticAccount(state.order.suborderUuid)

        commit('setHelperLogistic', data)
      } catch (e) {
        console.log(e)
      } finally {
        dispatch('turnOrderHelperLoader', false)
      }
    },
    clearDataLogistic ({ commit }) {
      commit('clearHelperLogistic')
    },
    turnOrderHelperLoader ({ commit }, relay) {
      commit('setOrderSignalLoader', relay)
    },
    async acceptOrder (context, { uuid }) {
      await api.accountOrder.acceptOrder(uuid);
    },
    async makeBid ({ commit, dispatch, state }, { bid, lotUuid }) {
      commit('setDisabledRate', true)
      commit('loaderRightPanel', true)

      try {
        await api.accountOrder.makeBid(bid, lotUuid)
      } catch (e) {
        console.log(e)
      } finally {
        commit('setDisabledRate', false)
        dispatch('getOrder', { ...state.order, isAuto: true })
      }
    },
    async activeTab ({ commit }, tab) {
      commit('loaderRightPanel', false)
      commit('setActiveTab', tab)
      commit('clearOrder')
    },
    setResource ({commit}, resource) {
      commit('resource', resource)
    },
    setStatement ({commit}, uuid) {
      commit('statement', uuid)
    },
    async acceptResource ({commit, dispatch, state}) {
      try {
        commit('setDisabledAcceptResource', true)

        commit('loaderRightPanel', true)
        await api.orderProcess.selectResourcesList({uuid: state.order.suborderUuid, data: state.resources})

        setTimeout(() => {
          dispatch('getOrder', { ...state.order, isAuto: true })
          commit('setResources', [])
        }, 5000)
      } catch (e) {
        console.log(e)
      } finally {
        commit('setDisabledAcceptResource', false)
      }
    },
    async requestExecution ({dispatch, commit, state}) {
      try {
        commit('setDisabledSignResource', true)
        commit('loaderRightPanel', true)

        let statementUuid = state.statementUuid

        if (!statementUuid) {
          let res = await api.accountOrder.getAutoSignedFile({ suborderUuid: state.order.suborderUuid, })
          statementUuid = res.fileUuid
        }

        if (state.statementUuid === 'facsimile') {
          const res = await api.accountOrder.postFacsimile( state.order.suborderUuid,  'pdf')
          commit('statement', res.data.fileUuid)

          await api.orderProcess.requestExecution({ uuid: state.order.suborderUuid, data: { statement_uuid: res.data.fileUuid } })
          commit('statement', '')
        } else {
          await api.orderProcess.requestExecution({ uuid: state.order.suborderUuid, data: { statement_uuid: statementUuid } })
        }
      }
      catch (e) {
        console.log(e)
      } finally {
        setTimeout(async () => {
          commit('setDisabledSignResource', false)
          await dispatch('updateTableList')
          await dispatch('getOrder', state.order)
        }, 2000)
      }
    },
    async getAdvance ({ commit, state }) {
      try {
        commit('setHasAdvance',false)
        const response = await api.accountOrder.getAccountAdvance(state.order.suborderUuid, state.statusFilter)

        if (response.data && state.order.advanceAvailable !== null) {
          commit('setAdvance', response.data)
          commit('setHasAdvance', true)

          if (response.data.uuid !== null ) {
            commit('setHideAdvanceButton', true)
          } else {
            commit('setHideAdvanceButton', false)
            commit('setAdvance', {
              status: '',
              advanceWithNds: state.order.advanceAvailable,
              docServicePrice: state.order.advanceServicePrice
            })
          }
        } else {
          commit('setHasAdvance',false)
        }
      } catch (e) {
        console.log(e)
      }
    },
    async newAdvance ({ commit, dispatch, state }) {
      await api.accountOrder.newAccountAdvance(state.order.suborderUuid, state.order.advanceAvailable, state.statusFilter)

      commit('setHideAdvanceButton', true)
      dispatch('getAdvance')
    },
    async cancelTheOrder ({ state }, type) {
      await api.accountOrder.cancelTheOrder(type, state.order.suborderUuid)
    },
    changeSearchType ({ commit }, status){
      if (status === 'requests__assigning_resources') {
        commit('setSearchType', 'BOOKED')
      }
      else {
        commit('setSearchType', 'ALL')
      }
    },
    setOfferSuborderUuid ({ commit, state, dispatch }, data) {
      commit('loaderRightPanel', true)
      commit('setOfferSuborderUuid', data?.uuid)

      setTimeout(() => {
        dispatch('getOrder', state.order)
      }, 0)
    }
  }
}


