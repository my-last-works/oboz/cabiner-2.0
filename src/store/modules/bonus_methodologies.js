import api from '../../api'
// import data from '../../resources/data';

export const GET_BONUSES = 'GET_BONUSES'
export const SET_BONUSES = 'SET_BONUSES'
export const CREATE_BONUS = 'CREATE_BONUS'
export const GET_USER_INFO = 'GET_USER_INFO'
export const SET_USER_INFO = 'SET_USER_INFO'

export default {
    state: {
        list: [],
        userInfo: undefined
    },
    getters: {
        getBonusMethodologies: state => {
            return state.list
        },
        getUserInfo: state => {
            return state.userInfo
        },
        getUserRole: state => {
            if (state.userInfo) {
                return state.userInfo.company_role;
            } else {
                return false;
            }
        }
    },
    actions: {
        [GET_BONUSES]: async ({commit}) => {
            try {
                const res = await api.bonus_methodologies.fetchBonusMethodologies();
                commit(SET_BONUSES, res)
            } catch (ex) {
                console.log(ex)
            }
        },
        [CREATE_BONUS]: async ({dispatch}, payload) => {
            try {
                await api.bonus_methodologies.createBonusMethodology(payload);
                dispatch(GET_BONUSES);
            } catch (ex) {
                console.log(ex)
            }
        },
        [GET_USER_INFO]: async ({commit}) => {
            try {
                const res = await api.bonus_methodologies.fetchUserInfo();
                commit(SET_USER_INFO, res)
            } catch (e) {
                console.log(e);
            }
        }
    },
    mutations: {
        [SET_BONUSES]: (state, res) => {
            state.list = res
        },
        [SET_USER_INFO]: (state, res) => {
            state.userInfo = res
        }
    }
}
