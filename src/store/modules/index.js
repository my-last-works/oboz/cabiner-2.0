import user from './user'
import functions from './functions'
import contract from './contract'
import order from './order'
import dictionaries from './dictionaries'
import account from './account/index'
import tenders from './tenders/index'
import tendersScenario from './tenders/scenario'
import customer from './customer'
import entry from './entry'
import locale from './locale'
import methodologies from './methodologies'
import methodology from './methodology'
import activities from './activities'
import parameters from './parameters'
import parameter from './parameter'
import index_settings from './index_settings'
import bonus_methodologies from './bonus_methodologies'
import bonus_activities from './bonus_activities'
import bonus_methodology from './bonus_methodology'
import threshold from './threshold'
import freshness from './freshness'
import base_prices from './base_prices'
import spot from './spot'
import advances from './advances'
import clicarSettings from './clicarSettings'
import clicarSettings_2 from './clicarSettings_2'
import clicarSettings_3 from './clicarSettings_3'
import tms from './tms'
import logdomains from './logdomains'
import subscriptions from './subscriptions'
import documents from './documents'

export default {
  user,
  functions,
  contract,
  order,
  dictionaries,
  account,
  tenders,
  tendersScenario,
  locale,
  index_settings,
  bonus_methodologies,
  bonus_activities,
  bonus_methodology,
  methodologies,
  methodology,
  activities,
  parameters,
  parameter,
  threshold,
  freshness,
  base_prices,
  customer,
  entry,
  spot,
  advances,
  clicarSettings,
  clicarSettings_2,
  clicarSettings_3,
  tms,
  logdomains,
  documents,
  subscriptions
}
