import api from "@/api";
import { uuid } from "vue-uuid";
import cloneDeep from "clone-deep";

const defaultParams = {
  PRICE_PRIORITY: false,                          // Приоритет цен над гарантированными объемами, BOOLEAN
  GUARANTEED_VOLUMES_PRIORITY: false,             // Приоритет гарантированными объемами над ценой, BOOLEAN
  ACCOUNT_GUARANTEED_VOLUMES: false,              // Учитывать гарантированные объемы, BOOLEAN
  CONTRACTOR_FILTER: [],                          // Фильтр по поставщикам (включение или выключение), UUID_ARRAY
  CONTRACTOR_COUNT_LIMIT_PER_LOT_FROM: '',        // Количество Поставщиков в лоте, от, NUMERIC
  CONTRACTOR_COUNT_LIMIT_PER_LOT_TO: '',          // Количество Поставщиков в лоте, до, NUMERIC
  CONTRACTOR_COUNT_LIMIT_PER_PURCHASE_FROM: '',   // Количество Поставщиков в целом по закупке, от, NUMERIC
  CONTRACTOR_COUNT_LIMIT_PER_PURCHASE_TO: '',     // Количество Поставщиков в целом по закупке, до, NUMERIC
  CONTRACTOR_COST_LIMIT_PER_LOT_FROM: '',         // Доля затрат на Поставщика в лоте, от, NUMERIC
  CONTRACTOR_COST_LIMIT_PER_LOT_TO: '',           // Доля затрат на Поставщика в лоте, до, NUMERIC
  CONTRACTOR_COST_LIMIT_PER_PURCHASE_FROM: '',    // Доля затрат на Поставщика в целом по закупке, от, NUMERIC
  CONTRACTOR_COST_LIMIT_PER_PURCHASE_TO: '',      // Доля затрат на Поставщика в целом по закупке, до, NUMERIC
  CONTRACTOR_VOLUME_LIMIT_PER_LOT_FROM: '',       // Доля объемов Поставщика в лоте, от, NUMERIC
  CONTRACTOR_VOLUME_LIMIT_PER_LOT_TO: '',         // Доля объемов Поставщика в лоте, до, NUMERIC
  CONTRACTOR_VOLUME_LIMIT_PER_PURCHASE_FROM: '',  // Доля объемов Поставщика в целом по закупке, от, NUMERIC
  CONTRACTOR_VOLUME_LIMIT_PER_PURCHASE_TO: '',    // Доля объемов Поставщика в целом по закупке, до, NUMERIC
  DEVIATION_TYPE: null,
  DEVIATION_PER_LOT_FROM: 0,
  DEVIATION_PER_LOT_TO: 10,
  INCLUDED_DEVIATIONS: []
}

const defaultOptions = {
  priority: '',
  edges: {
    type: null,
    min: 0,
    max: 0
  },
  includedDeviations: [],
  includedDeviationsItems: []
}

const getDefaultState = () => {
  return {
    scenario: {
      params: {}
    },
    options: defaultOptions,
    initialParams: {},
    contractorsCount: 0,
    tenderInfo: {}
  }
}

export default {
  namespaced: true,
  state: getDefaultState(),
  getters: {
    scenario: state => state.scenario,
    scenarioParams: state => state.scenario.params,
    tenderInfo: state => state.tenderInfo,
    isOptionsDisabled: state => state.tenderInfo.status === 'COMPLETED',
    options: state => state.options,
    initialParams: state => state.initialParams,
    contractorsCount: state => state.contractorsCount
  },
  mutations: {
    setScenario (state, value) {
      state.scenario = value;
    },
    setOptions (state, value) {
      state.options = value;
    },
    setInitialParams (state, value) {
      state.initialParams = value;
    },
    setContractorsCount (state, count) {
      state.contractorsCount = count;
    },
    setTenderInfo (state, data) {
      state.tenderInfo = data;
    },
    setParams (state, data) {
      state.scenario.params = data;
    },
    resetState (state) {
      Object.assign(state, getDefaultState());
    }
  },
  actions: {
    async initScenario ({ dispatch }, data) {
      await dispatch("getTender", data);
      await dispatch("getScenario", data);
      await dispatch("getOptions", data);
      await dispatch("getContractorsCount", data);
    },
    async getTender ({ commit }, data) {
      const { tenderInfo } = await api.tenders.getTenderByTypeAndUuid('rfq', data.tenderUuid);

      commit("setTenderInfo", tenderInfo);
    },
    async getScenario ({ commit }, data) {
      const { scenario } = await api.tendersScenarios.getTenderScenarioByUuids(data.tenderUuid, data.scenarioUuid);

      const scenarioActualParams = scenario.params;

      scenario.params = Object.assign({}, defaultParams);

      scenarioActualParams.forEach(parameter => scenario.params[parameter.code] = parameter.value.value);

      commit("setScenario", scenario);
      commit("setInitialParams", JSON.stringify(scenario.params));
    },
    setParams ({ commit, getters }, data) {
      commit("setParams", {...getters.scenarioParams, ...data});
    },
    async getOptions ({ commit, getters }, data) {
      const options = cloneDeep(defaultOptions);

      // Внесение настроек, в зависимости от параметров сценария
      if (getters.scenarioParams.PRICE_PRIORITY) {
        options.priority = "PRICE_PRIORITY";
      } else if (getters.scenarioParams.GUARANTEED_VOLUMES_PRIORITY) {
        options.priority = "GUARANTEED_VOLUMES_PRIORITY";
      }

      options.edges.type = getters.scenarioParams.DEVIATION_TYPE;
      options.edges.min = getters.scenarioParams.DEVIATION_PER_LOT_FROM;
      options.edges.max = getters.scenarioParams.DEVIATION_PER_LOT_TO;
      options.includedDeviations = getters.scenarioParams.INCLUDED_DEVIATIONS; // Включенные вручную ставки

      const { lots } = await api.tendersScenarios.getTenderScenarioIncludedDeviations({
        tenderUuid: data.tenderUuid,
        scenarioUuid: data.scenarioUuid
      });

      // Crutch for unique table keys:
      // - "uuid" is a frontend generated value to escape unique keys by uuid error
      // - "lUuid" is an original uuid
      options.includedDeviationsItems = lots.map((lot) => ({
        ...lot,
        lUuid: lot.uuid,
        uuid: uuid.v4()
      }));

      commit("setOptions", options);
    },
    setOptions ({ commit, getters }, data) {
      commit("setOptions", {...getters.options, ...data});
    },
    async getContractorsCount ({ commit }, data) {
      const { carriers } = await api.tendersScenarios.getTenderScenarioPostedOffersCarriers({
        tenderUuid: data.tenderUuid,
        scenarioUuid: data.scenarioUuid
      });

      commit("setContractorsCount", carriers.length);
    },
    resetState ({ commit }) {
      commit("resetState");
    }
  }
}
