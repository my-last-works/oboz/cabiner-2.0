import api from '@/api'
import { getTokenData } from "@/tokenData";

export default {
  namespaced: true,

  state: {
    tender: {},
    tenderFormData: {},
    tenderFormDataChanged: false,
    button: {},
    currentRole: null,
    winners: [],
    datesValid: false,
    isScenarioWinnerExists: false
  },

  getters: {
    currentRole: state => state.currentRole,
    tender: state => state.tender,
    tenderInfo: state => state.tender.tenderInfo,
    tenderFormData: state => state.tenderFormData,
    tenderFormDataChanged: state => state.tenderFormDataChanged,
    isEditable: state => ['DRAFT'].includes(state.tender.tenderInfo.status),
    button: state => state.button,
    isButtonDisabled: state => {

      if (state.tender.tenderInfo.type === 'RFQ') {

        if (state.currentRole === 'client') {

          switch (state.tender.tenderInfo.status) {
            case 'DRAFT': {
              const schema = {
                title: value => value,
                description: value => value,
                targetFinishDt: value => value,
                summarizingDt: value => value,
                contacts: value => value,
                startContractDt: value => value,
                endContractDt: value => value,
                vatUuid: value => value,
                transportationTypeUuid: value => value
              }

              const validate = object => Object
                .keys(schema)
                .filter(key => !schema[key](object[key]))
                .map(key => new Error(`${key} is invalid.`)).length;

              return !state.tender.lots ||
                     !state.tender.participants ||
                     !!validate(state.tenderFormData) ||
                     !state.datesValid
            }

            default: return true;
          }

        } else {

          switch (state.tender.tenderInfo.status) {
            case 'HELD': {
              switch(state.tender.tenderInfo.participationStatus) {
                case 'NOT_VIEWED':
                case 'VIEWED':
                  return;
                case 'DRAFT': return; // Временно раздизейбливаем, пока не будет решения

                default: return true;
              }
            }

            default: return true;
          }

        }

      } else {

        if (state.currentRole === 'client') {

          switch (state.tender.tenderInfo.status) {
            case 'DRAFT': {
              const schema = {
                title: value => value,
                description: value => value,
                targetFinishDt: value => value,
                summarizingDt: value => value,
                contacts: value => value,
              }

              const validate = object => Object
                .keys(schema)
                .filter(key => !schema[key](object[key]))
                .map(key => new Error(`${key} is invalid.`)).length;

              return !state.tender.questions ||
                     !state.tender.participants ||
                     !!validate(state.tenderFormData) ||
                     !(state.tender.tenderInfo.minWeightSum === null || state.tender.tenderInfo.minWeightSum > 0) ||
                     !state.datesValid
            }

            default: return true;

          }
        } else {

          switch (state.tender.tenderInfo.status) {
            case 'HELD': {
              switch(state.tender.tenderInfo.participationStatus) {
                case 'NOT_VIEWED':
                case 'VIEWED':
                  return;

                case 'DRAFT': {
                  const result = state.tender.filledForm.questions.some(question => question.value === null || question.value === undefined || question.value === '');
                  return result;
                }

                default: return true;
              }
            }

            default: return true;
          }

        }

      }
    },
    winners: state => state.winners,
    isDatesValid: state => state.datesValid,
    isScenarioWinnerExists: state => state.isScenarioWinnerExists
  },

  mutations: {
    setCurrentUserRole(state, data) {
      state.currentRole = data;
    },
    setTender(state, data) {
      state.tender = data;
    },
    setTenderFormData(state, data) {
      state.tenderFormData = data;
    },
    activateTenderFormDataChanged(state) {
      state.tenderFormDataChanged = true;
    },
    deactivateTenderFormDataChanged(state) {
      state.tenderFormDataChanged = false;
    },
    updateTenderLots(state, data) {
      state.tender.lots = data;
    },
    updateTenderParticipants(state, data) {
      state.tender.participants = data;
    },
    updateTenderQuestionnaire(state, data) {
      state.tender.questions = data;
    },
    fillTenderQuestionnaire(state, data) {
      state.tender.filledForm = data;
    },
    resetTender(state) {
      state.tender = {};
      state.tenderFormData = {};
    },
    setTenderButton(state, data) {
      state.button = data;
    },
    setWinners(state, data) {
      state.winners = data;
    },
    setDatesValidStatus(state, status) {
      state.datesValid = status;
    },
    setScenarioWinnerStatus(state, status) {
      state.isScenarioWinnerExists = status;
    }
  },

  actions: {
    setCurrentUserRole({ commit }) {
      commit('setCurrentUserRole', getTokenData().contractor_role_code)
    },

    async getTender({ commit, dispatch }, { type, uuid, reset = false }) {
      if (reset) dispatch('resetTender');
      const response = await api.tenders.getTenderByTypeAndUuid(type, uuid);
      if (!response.hasOwnProperty('tenderInfo')) response.tenderInfo = response; //RFI
      dispatch('setTenderFormData', response.tenderInfo);

      if (response.hasOwnProperty('filledForms')) response.offers = response.filledForms;

      commit('setTender', response);

      // "New stage" button: TND-1266
      if (type === 'RFQ' && ['SUMMARIZING', 'COMPLETED'].includes(response.tenderInfo.status)) {
        const { scenarioUuid } = await api.tendersScenarios.getTenderWinnerScenarioCarriers(uuid)
        commit('setScenarioWinnerStatus', !!scenarioUuid)
      }

      dispatch('setTenderButton');
    },

    async deleteTender({ state }) {
      await api.tenders.deleteTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid);
    },

    async cancelTender({ state }, payload) {
      await api.tenders.cancelTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid, payload);
    },

    async manualFinishTender({ state }) {
      await api.tenders.manualFinishTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid);
    },

    async duplicateTender({ state }, payload) {
      return await api.tenders.duplicateTender(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid, payload);
    },

    async publishTender({ state }) {
      const response = await api.tenders.publishTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid);
      return response;
    },

    setTenderFormData({ commit }, data) {
      commit('setTenderFormData', data);
      commit('activateTenderFormDataChanged');
    },

    async updateTenderInfo({ state, commit }) {
      await api.tenders.updateTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid, state.tenderFormData);
      commit('deactivateTenderFormDataChanged');
    },

    async updateTenderLots({ state, commit }) {
      const { lots } = await api.tenders.getTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid);
      commit('updateTenderLots', lots);
    },

    async updateTenderParticipants({ state, commit }) {
      const { participants } = await api.tenders.getTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid);
      commit('updateTenderParticipants', participants);
    },

    async updateTenderQuestionnaire({ state, commit }) {
      const { questions } = await api.tenders.getTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid);
      commit('updateTenderQuestionnaire', questions);
    },

    async fillTenderQuestionnaire({ state, commit }) {
      const { filledForm } = await api.tenders.getTenderByTypeAndUuid(state.tender.tenderInfo.type, state.tender.tenderInfo.uuid);
      commit('fillTenderQuestionnaire', filledForm);
    },

    setTenderButton({ commit, state }) {
      let button = null;

      if (state.currentRole === 'client') {

        switch (state.tender.tenderInfo.status) {
          case 'DRAFT': {
            button = {
              label: 'Опубликовать',
              action: {
                type: 'open-modal',
                refName: 'publish-modal'
              }
            }
            break;
          }
        }

      } else {

        switch (state.tender.tenderInfo.status) {
          case 'HELD': {
            switch(state.tender.tenderInfo.participationStatus) {
              case 'NOT_VIEWED':
              case 'VIEWED': {
                button = {
                  label: 'Участвовать',
                  action: {
                    type: 'component-method',
                    methodName: 'onAcceptParticipation'
                  }
                }
                break;
              }
              case 'DRAFT': {
                button = {
                  label: 'Отправить предложение',
                  action: {
                    type: 'component-method',
                    methodName: 'onSendOffer'
                  }
                }
                break;
              }
            }
            break;
          }
        }

      }

      commit('setTenderButton', button);
    },

    async getWinners({ commit }, tenderUuid) {
      const { filledForms } = await api.tenders.getTenderOffersWinners(tenderUuid)

      commit('setWinners', filledForms)
    },

    setDatesValidStatus ({ commit }, status) {
      commit('setDatesValidStatus', status)
    },

    resetTender({ commit }) {
      commit('resetTender');
    }
  }
}