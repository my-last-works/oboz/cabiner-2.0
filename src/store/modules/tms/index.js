import api from '@/api'
import { uuid } from "vue-uuid";

export default {
  namespaced: true,
  state: {
    isTitulSaved: false,
    rule: {
      uuid: '',
      number: '',
      titul: {
        title: '',
        quotationType: '',
        subperiod: '',
        applyFromD: '',
        applyToD: '',
        needAutoRenewal: false,
        readyActivate: false
      },
      services: [],
      directions: [],
      directionsByContractors: []
    },
    isLoading: false,
    loaders: {
      update: false,
      clear: false
    }
  },
  getters: {
    rule (state) {
      return state.rule
    },
    isLoading (state) {
      return state.isLoading
    },
    loaders: state => state.loaders,
    isTitulSaved(state) {
      return state.isTitulSaved;
    }
  },
  mutations: {
    SET_RULE (state, data) {
      state.rule = { ...data }
    },
    SET_RULE_TITUL(state, data) {
      state.rule.titul = { ...data };
    },
    SET_RULE_SERVICES(state, data) {
      state.rule.services = [ ...data ];
    },
    SET_RULE_DIRECTIONS(state, data) {
      state.rule.directions = data;
    },
    SET_RULE_DIRECTIONS_BY_CONTRACTORS(state, data) {
      state.rule.directionsByContractors = data;
    },
    CLEAR_RULE (state) {
      state.rule = {}
    },
    SET_LOADING (state, value) {
      state.isLoading = value
    },
    SET_UPDATE_LOADER(state, payload) {
      state.loaders.update = payload
    },
    SET_CLEAR_LOADER(state, payload) {
      state.loaders.clear = payload
    },
    SET_TITUL_SAVED(state, payload) {
      state.isTitulSaved = payload;
    }
  },
  actions: {
    clearState ({commit}) {
      commit('CLEAR_RULE')
    },
    getRule ({commit}) {
      commit('SET_LOADING', true);
    },
    setRule({commit}, payload) {
      const data = {
        uuid: payload.uuid,
        number: payload.number,
        titul: {
          title: payload.title,
          status: payload.status,
          quotationType: payload.quotationType,
          subperiod: payload.subperiod,
          applyFromD: payload.applyFromD,
          applyToD: payload.applyToD,
          needAutoRenewal: payload.needAutoRenewal,
          readyActivate: payload.readyActivate
        },
        services: [],
        directions: [],
        directionsByContractors: []
      }
      commit('SET_RULE', data);
    },
    setRuleTitul({commit}, data) {
      commit('SET_RULE_TITUL', data);
    },
    setRuleServices({commit}, data) {
      commit('SET_RULE_SERVICES', data);
    },
    setRuleDirections({commit}, data) {
      commit('SET_RULE_DIRECTIONS', data);
    },
    setRuleDirectionsByContractors({commit}, data) {
      commit('SET_RULE_DIRECTIONS_BY_CONTRACTORS', data);
    },
    setUpdateLoader({commit}, status) {
      commit('SET_UPDATE_LOADER', status)
    },
    setClearLoader({commit}, status) {
      commit('SET_CLEAR_LOADER', status)
    },
    setTitulSaved({commit}, data) {
      commit('SET_TITUL_SAVED', data);
    },
    
    async init({commit}) {
      const number = await api.tms.getPolicyNumber();
      const newUUID = uuid.v4();
      const payload = {
        uuid: newUUID,
        number: number.data.code,
        titul: {
          title: '',
          status: 'DRAFT',
          quotationType: 'VOLUME_SHARE',
          subperiod: '',
          applyFromD: '',
          applyToD: '',
          needAutoRenewal: false,
          readyActivate: false
        },
        services: [],
        directions: [],
        directionsByContractors: []
      }
      commit('SET_RULE', payload);
    }
  }
}
