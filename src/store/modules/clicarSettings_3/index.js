import api from '@/api'
import cloneDeep from 'clone-deep'
import _ from 'lodash'

const MINUTES_IN_HOURS = 60
const CENTS_IN_RUBLES = 100

const hoursToMinutes = (hours) => {
  return hours * MINUTES_IN_HOURS
}
const clearFormatNumber = (value) => {
  return typeof value === 'number' ? value : value.replace(/\s/g, '')
}
const clearFormatMoney = (value) => {
  return +clearFormatNumber(value) * CENTS_IN_RUBLES
}
const prepareValueMap = {
  bookingDurationLeadtime1: hoursToMinutes,
  bookingDurationLeadtime2: hoursToMinutes,
  bookingDurationLeadtime3: hoursToMinutes,
  bookingMinimumDuration: hoursToMinutes,
  resourceAssignmentLeadTime: hoursToMinutes,
  emptySupplyLeadTime: hoursToMinutes,
  arrivalLeadTime: hoursToMinutes,
  docSigningLeadTime: hoursToMinutes,
  roundTripTime: hoursToMinutes,
  auctionActivityMax: hoursToMinutes,
  autosearchLeadtime: hoursToMinutes,
  personalOfferDuration: hoursToMinutes,
  auctionStep: clearFormatMoney
}

const setToValue = (obj, value, path) => {
  let i
  path = path.split('.')
  for (i = 0; i < path.length - 1; i++) {
    obj = obj[path[i]]
    if (!obj) break
  }

  if (obj) {
    if (prepareValueMap[path[i]]) {
      value = prepareValueMap[path[i]](value)
    }
    obj[path[i]] = value
  }
}

const settingsType = {
  markup: 'markup',
  costReduction: 'costReduction',
  autosearch: 'autosearch',
  generalSettings: 'generalSettings'
}

export default {
  namespaced: true,
  state: {
    $notification: null,
    settingsType: '',
    loader: false,
    settings: {
      markup: {},
      costReduction: {},
      autosearch: {},
      generalSettings: {}
    }
  },
  getters: {
    settingsType: state => state.settingsType,
    loader: state => state.loader,
    settings: state => state.settings,
    currentSettings (state) {
      return state.settings[state.settingsType] || {}
    }
  },
  mutations: {
    SET_SETTINGS_TYPE (state, type) {
      state.settingsType = type || settingsType.markup
    },
    SET_SETTINGS (state, { type, data }) {
      state.settings[type] = data
    },
    TURN_ON_LOADER (state, payload) {
      state.loader = payload
    },
    SET_SETTING_VALUE (state, { path, value }) {
      let result = cloneDeep(state.settings)

      setToValue(result, value, path)

      state.settings = result
    },
    setNotification (state, object) {
      state.$notification = object
    }
  },
  actions: {
    setNotification ({ commit }, object) {
      commit('setNotification', object)
    },
    setSettingsType ({ commit }, type) {
      commit('SET_SETTINGS_TYPE', type)
    },
    setSettingValue ({ commit, state, getters, dispatch }, { path, value }) {
      commit('SET_SETTING_VALUE', { path: `${state.settingsType}.${path}`, value })
      dispatch('saveSettings', { data: getters.currentSettings, type: state.settingsType })
    },
    saveSettings: _.throttle(async ({ state }, { data, type }) => {
      try {
        if (type === settingsType.markup) {
          await api.clicar_setting_3.saveMarkupSettings({ data })
        } else if (type === settingsType.costReduction) {
          await api.clicar_setting_3.saveCostReductionSettings({ data })
        } else if (type === settingsType.autosearch) {
          await api.clicar_setting_3.saveAutosearchSettings({ data })
        } else if (type === settingsType.generalSettings) {
          await api.clicar_setting_3.savePersonalAccountSettings({ data })
        }

        state.$notification && state.$notification({
          type: 'system_notification_success',
          message: 'Сохранено'
        })
      } catch (e) {
        state.$notification && state.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        })
      }
    }, 1000),
    // НАЦЕНКА
    async getMarkupSettings({ commit, rootGetters, state }) {
      const userUuid = rootGetters['user/participantUuid']
      const data = await api.clicar_setting_3.getMarkupSettings({ uuid: userUuid })

      commit('SET_SETTINGS', { type: state.settingsType, data })
    },
    saveMarkupClientSettings: _.throttle(async ({ state }, { data }) => {
      try {
        await api.clicar_setting_3.setMarkupClientSettings(data)

        state.$notification && state.$notification({
          type: 'system_notification_success',
          message: 'Сохранено'
        })
      } catch (e) {
        state.$notification && state.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        })
      }
    }, 1000),
    // ЛИМИТ
    async getCostReductionSettings ({ commit, rootGetters, state }) {
      const userUuid = rootGetters['user/participantUuid']
      const data = await api.clicar_setting_3.getCostReductionSettings({ uuid: userUuid })

      commit('SET_SETTINGS', { type: state.settingsType, data })
    },
    saveCostReductionClientSettings: _.throttle(async ({ state }, { data }) => {
      try {
        await api.clicar_setting_3.setCostReductionClientSettings(data)

        state.$notification && state.$notification({
          type: 'system_notification_success',
          message: 'Сохранено'
        })
      } catch (e) {
        state.$notification && state.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        })
      }
    }, 1000),
    // Автопоиск
    async getAutosearchSettings ({ commit, state }) {
      const data = await api.clicar_setting_3.getAutosearchSettings()

      commit('SET_SETTINGS', { type: state.settingsType, data })
    },
    saveAutosearchClientSettings: _.throttle(async ({ state }, { data }) => {
      try {
        await api.clicar_setting_3.saveAutosearchSettings(data)

        state.$notification && state.$notification({
          type: 'system_notification_success',
          message: 'Сохранено'
        })
      } catch (e) {
        state.$notification && state.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        })
      }
    }, 1000),
    // ЛК
    async getPersonalAccountSettings ({ commit, state }) {
      const data = await api.clicar_setting_3.getPersonalAccountSettings()

      commit('SET_SETTINGS', { type: state.settingsType, data: data.generalSettings })
    },
    savePersonalAccountSettings:  _.throttle(async ({ state }, { data }) => {
      try {
        await api.clicar_setting_3.getPersonalAccountSettings(data)

        state.$notification && state.$notification({
          type: 'system_notification_success',
          message: 'Сохранено'
        })
      } catch (e) {
        state.$notification && state.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        })
      }
    }, 1000)
  }
}
