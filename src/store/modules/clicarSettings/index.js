import api from '@/api'
import router from '@/router'

export default {
  namespaced: true,
  state: {
    clicar_uuid: '',
    /**
     * Табс может иметь следующие свойства:
     * ALL - Для всех заказов по умолчанию;
     * FOR_CLIENT - Индивидуально для Клиентов;
     * BY_CARRIER - Дополнительно для Перевозчиков;
     */
    tab: 'ALL',
    terms: {
      executionConfirmationTerm: 0,
      paymentTerm: 0,
      provisionTerm: [
        {
          term: 0
        }
      ]
    },
    terms_by_carrier: {
      executionConfirmationTerm: 0,
      paymentTerm: 0,
      provisionTerm: [
        {
          tripDurationMin: 0,
          tripDurationMax: 0,
          term: 0
        },
        {
          tripDurationMin: 0,
          tripDurationMax: 0,
          term: 0
        },
        {
          tripDurationMin: 0,
          tripDurationMax: 0,
          term: 0
        }
      ]
    },
    list_docs: [],
    loader: false,
    list_customers: [],
    client_uuid: '',
    /**
     * personal_setting_customer - таблица персольнальных настроек Табс (Настроить направления)
     */
    personal_setting_customer: [],
    /**
     * personal_direction - таблица персольнальных настроек в отдельном окне
     */
    personal_direction: {},
    /**
     * answerByServer = '' - первоначальное состояние при запросе с сервера, ответ не известен
     * success - ответ от сервера успешен
     * error - ошибка от сервера
     */
    answerByServer: '',
    flag_setting_direction: false
  },
  getters: {
    tab: state => state.tab,
    terms: state => state.terms,
    terms_by_carrier: state => state.terms_by_carrier,
    clicar_uuid: state => state.clicar_uuid,
    list_docs: state => state.list_docs,
    loader: state => state.loader,
    list_customers: state => state.list_customers,
    client_uuid: state => state.client_uuid,
    personal_setting_customer: state => state.personal_setting_customer,
    personal_direction: state => state.personal_direction,
    answerByServer: state => state.answerByServer,
    flag_setting_direction: state => state.flag_setting_direction
  },
  mutations: {
    SET_TABS(state, payload) {
      state.tab = payload
    },
    SET_LIST_DOCS(state, payload) {
      state.list_docs = payload
    },
    WRITE_CLICAR_UUID(state, payload) {
      state.clicar_uuid = payload
    },
    TURN_ON_LOADER(state, payload) {
      state.loader = payload
    },
    SET_TERMS(state, payload) {
      state.terms = {
        provisionTerm: [
          {
            term: Number(payload.provisionTerm[0].term)
          }
        ],
        executionConfirmationTerm: Number(payload.executionConfirmationTerm),
        paymentTerm: Number(payload.paymentTerm)
      }
    },
    SET_TERMS_BY_CARRIER(state, payload) {
      state.terms_by_carrier = {
        provisionTerm: [
          {
            tripDurationMin: Number(payload.provisionTerm[0].tripDurationMin),
            tripDurationMax: Number(payload.provisionTerm[0].tripDurationMax),
            term: Number(payload.provisionTerm[0].term)
          },
          {
            tripDurationMin: Number(payload.provisionTerm[1].tripDurationMin),
            tripDurationMax: Number(payload.provisionTerm[1].tripDurationMax),
            term: Number(payload.provisionTerm[1].term)
          },
          {
            tripDurationMin: Number(payload.provisionTerm[2].tripDurationMin),
            tripDurationMax: Number(payload.provisionTerm[2].tripDurationMax),
            term: Number(payload.provisionTerm[2].term)
          },
        ],
        executionConfirmationTerm: Number(payload.executionConfirmationTerm),
        paymentTerm: Number(payload.paymentTerm)
      }
    },
    RESET_TERMS(state) {
      state.terms.provisionTerm[0].term = 0;
      state.terms.executionConfirmationTerm = 0;
      state.terms.paymentTerm = 0;
    },
    RESET_TERMS_BY_CARRIER(state) {
      Object.keys(state.terms_by_carrier.provisionTerm).forEach(key => {
        state.terms_by_carrier.provisionTerm[key].tripDurationMin = 0;
        state.terms_by_carrier.provisionTerm[key].tripDurationMax = 0;
        state.terms_by_carrier.provisionTerm[key].term = 0;
      });
      state.terms_by_carrier.executionConfirmationTerm = 0;
      state.terms_by_carrier.paymentTerm = 0;
    },
    CLEAR_DOCS_LIST(state) {
      state.list_docs = []
    },
    WRITE_LIST_CUSTOMERS(state, payload) {
      state.list_customers = payload
    },
    CHANGE_SELECTED_CUSTOMER(state, payload) {
      state.client_uuid = payload
    },
    SET_PERSONAL_SETTINGS_CUSTOMER(state, payload) {
      state.personal_setting_customer = payload
    },
    SET_PERSONAL_DIRECTION(state, payload) {
      state.personal_direction = payload
    },
    WRITE_ANSWER_BY_SERVER(state, payload) {
      state.answerByServer = payload
    },
    TURN_ON_FLAG_REDIRECT_ON_PAGE_SETTING_DIRECTION(state) {
      state.flag_setting_direction = true
    }
  },
  actions: {
    async getListDocs({ commit, state }, clicar_uuid) {
      try {
        commit('TURN_ON_LOADER', true)

        const result = await api.clicar_setting.getDocsSettings(clicar_uuid, state.tab)

        if (result.data) {
          commit('SET_LIST_DOCS', result.data.docSettings)

          const provisionTerm = result.data.provisionTerm || []

          if (state.tab === 'BY_CARRIER') {
            const termsByCarrier = {
              provisionTerm: [
                {
                  tripDurationMin: provisionTerm[0]?.tripDurationMin || 0,
                  tripDurationMax: provisionTerm[0]?.tripDurationMax || 0,
                  term: provisionTerm[0]?.term || 0
                },
                {
                  tripDurationMin: provisionTerm[1]?.tripDurationMin || 0,
                  tripDurationMax: provisionTerm[1]?.tripDurationMax || 0,
                  term: provisionTerm[1]?.term || 0
                },
                {
                  tripDurationMin: provisionTerm[2]?.tripDurationMin || 0,
                  tripDurationMax: provisionTerm[2]?.tripDurationMax || 0,
                  term: provisionTerm[2]?.term || 0
                }
              ],
              executionConfirmationTerm: result.data.executionConfirmationTerm || 0,
              paymentTerm: result.data.paymentTerm || 0
            }

            commit('SET_TERMS_BY_CARRIER', termsByCarrier)
          } else {
            const termsData = {
              provisionTerm: [
                {
                  term: provisionTerm[0]?.term
                }
              ],
              executionConfirmationTerm: result.data.executionConfirmationTerm,
              paymentTerm: result.data.paymentTerm
            }

            commit('SET_TERMS', termsData)
          }
        } else {
          console.log('Ошибка при загрузке страницы: ', result)
          commit('SET_LIST_DOCS', [])
        }
      } catch (e) {
        console.log(e)
      } finally {
        commit('TURN_ON_LOADER', false)
        commit('WRITE_CLICAR_UUID', clicar_uuid)
      }
    },
    async selectRowDocs({ commit, state }, row) {
      commit('WRITE_ANSWER_BY_SERVER', '')

      let terms

      if (state.tab === 'BY_CARRIER') {
        terms = [
          {
            tripDurationMin: state.terms_by_carrier.provisionTerm[0].tripDurationMin,
            tripDurationMax: state.terms_by_carrier.provisionTerm[0].tripDurationMax,
            term: state.terms_by_carrier.provisionTerm[0].term
          },
          {
            tripDurationMin: state.terms_by_carrier.provisionTerm[1].tripDurationMin,
            tripDurationMax: state.terms_by_carrier.provisionTerm[1].tripDurationMax,
            term: state.terms_by_carrier.provisionTerm[1].term
          },
          {
            tripDurationMin: state.terms_by_carrier.provisionTerm[2].tripDurationMin,
            tripDurationMax: state.terms_by_carrier.provisionTerm[2].tripDurationMax,
            term: state.terms_by_carrier.provisionTerm[2].term
          }
        ]
      } else {
        terms = [
          {
            term: state.terms.provisionTerm[0].term
          }
        ]
      }

      const params = {
        provisionTerm: terms,
        executionConfirmationTerm: state.tab === 'BY_CARRIER' ? state.terms_by_carrier.executionConfirmationTerm : state.terms.executionConfirmationTerm,
        paymentTerm: state.tab === 'BY_CARRIER' ? state.terms_by_carrier.paymentTerm : state.terms.paymentTerm,
        docSettings: Object.keys(row).length ? [{
          ...row,
          count: Number(row.count)
        }] : []
      }

      const { data, status } = await api.clicar_setting.saveDocsSettings(state.clicar_uuid, params, state.tab)

      if (data || status === 200) {
        commit('WRITE_ANSWER_BY_SERVER', 'success')
      } else {
        commit('WRITE_ANSWER_BY_SERVER', 'error')
      }
    },
    async setTerms({ commit, dispatch, state }, terms) {
      state.tab === 'BY_CARRIER' ? commit('SET_TERMS_BY_CARRIER', terms) : commit('SET_TERMS', terms)
      await dispatch('selectRowDocs', {})
    },
    async choiceTypeDirection({ commit, state, dispatch }) {
      state.tab === 'BY_CARRIER' ? commit('RESET_TERMS_BY_CARRIER') : commit('RESET_TERMS')
      await dispatch('getListDocs', state.clicar_uuid)
    },
    async sortByCustomer({ state, commit }, clientUuid) {
      try {
        commit('CLEAR_DOCS_LIST')
        commit('RESET_TERMS')
        commit('TURN_ON_LOADER', true)
        commit('CHANGE_SELECTED_CUSTOMER', clientUuid)

        const result = await api.clicar_setting.getDocsSettingsByCustomer(state.clicar_uuid, clientUuid)

        if (result.data) {
          commit('SET_LIST_DOCS', result.data.common.docSettings);
          commit('SET_TERMS', {
            provisionTerm: [
              {
                term: !result.data.common.provisionTerm.length ? 0 : result.data.common.provisionTerm[0].term,
              }
            ],
            executionConfirmationTerm: result.data.common.executionConfirmationTerm,
            paymentTerm: result.data.common.paymentTerm
          });
          commit('SET_PERSONAL_SETTINGS_CUSTOMER', result.data.perLocation)
        } else {
          console.log('Ошибка при сортировки клиента: ', result);
          commit('SET_LIST_DOCS', []);
          commit('SET_PERSONAL_SETTINGS_CUSTOMER', [])
        }
      } catch (e) {
        console.log(e)
      } finally {
        commit('TURN_ON_LOADER', false)
      }
    },
    async savePersonalTermsByClient({ commit, state }, terms) {
      try {
        const termsData = {
          provisionTerm: terms.provisionTerm,
          executionConfirmationTerm: terms.executionConfirmationTerm,
          paymentTerm: terms.paymentTerm
        }

        const saveObject = {
          locationStartUuid: terms.locationStartUuid ? terms.locationStartUuid : '',
          locationEndUuid: terms.locationEndUuid ? terms.locationEndUuid : '',
          docSettings: {
            provisionTerm: state.terms.provisionTerm,
            executionConfirmationTerm: state.terms.executionConfirmationTerm,
            paymentTerm: state.terms.paymentTerm,
            docSettings: []
          }
        }

        commit('SET_TERMS', termsData)
        commit('WRITE_ANSWER_BY_SERVER', '')

        const { data, status } = await api.clicar_setting.savePersonalSetting(state.clicar_uuid, state.client_uuid, saveObject)


        if (data || status === 200) {
          commit('WRITE_ANSWER_BY_SERVER', 'success');
        } else {
          commit('WRITE_ANSWER_BY_SERVER', 'error')
        }
      } catch (e) {
        console.log(e)
      }
    },
    async addNewDirection({ commit, state }, params) {
      try {
        commit('WRITE_ANSWER_BY_SERVER', '')
        const { data, status } = await api.clicar_setting.savePersonalSetting(state.clicar_uuid, state.client_uuid, params)

        if (data || status === 200) {
          commit('WRITE_ANSWER_BY_SERVER', 'success')
          await this.router.push({
            path: '/profile__setting/docs',
            query: {
              client: state.client_uuid,
              start: params.locationStartUuid,
              end: params.locationEndUuid
            }
          })
        } else {
          commit('WRITE_ANSWER_BY_SERVER', 'error')
        }
      } catch (e) {
        console.log(e)
      }
    },
    async saveSettingByCustomer({ commit, state }, params) {
      try {
        commit('WRITE_ANSWER_BY_SERVER', '')

        const saveObject = {
          locationStartUuid: params.locationStartUuid ? params.locationStartUuid : '',
          locationEndUuid: params.locationEndUuid ? params.locationEndUuid : '',
          docSettings: {
            provisionTerm: [
              {
                tripDurationMin: Number(state.terms.provisionTerm[0].tripDurationMin),
                tripDurationMax: Number(state.terms.provisionTerm[0].tripDurationMax),
                term: Number(state.terms.provisionTerm[0].term),
              }
            ],
            executionConfirmationTerm: Number(state.terms.executionConfirmationTerm),
            paymentTerm: Number(state.terms.paymentTerm),
            docSettings: [
              {
                required: params.required,
                typeUuid: params.typeUuid,
                name: params.name,
                count: Number(params.count),
                comment: params.comment
              }
            ]
          }
        };
        const { data, status } = await api.clicar_setting.savePersonalSetting(state.clicar_uuid, state.client_uuid, saveObject)

        if (data || status === 200) {
          commit('WRITE_ANSWER_BY_SERVER', 'success')
        } else {
          commit('WRITE_ANSWER_BY_SERVER', 'error')
        }
      } catch (e) {
        console.log(e)
      }
    },
    async saveSelectedPersonalSetting({ commit, state }, { locationStartUuid, locationEndUuid, docSettings }) {
      try {
        commit('WRITE_ANSWER_BY_SERVER', '')

        const saveObject = {
          locationStartUuid: locationStartUuid,
          locationEndUuid: locationEndUuid,
          docSettings: {
            provisionTerm: [
              {
                tripDurationMin: Number(docSettings.provisionTerm[0].tripDurationMin),
                tripDurationMax: Number(docSettings.provisionTerm[0].tripDurationMax),
                term: Number(docSettings.provisionTerm[0].term)
              }
            ],
            executionConfirmationTerm: Number(docSettings.executionConfirmationTerm),
            paymentTerm: Number(docSettings.paymentTerm),
            docSettings: []
          }
        }

        const { data, status } = await api.clicar_setting.savePersonalSetting(state.clicar_uuid, state.client_uuid, saveObject)

        if (data || status === 200) {
          commit('WRITE_ANSWER_BY_SERVER', 'success')
        } else {
          commit('WRITE_ANSWER_BY_SERVER', 'error')
        }
      } catch (e) {
        console.log(e)
      }
    },
    async loadPersonalDirection({ commit }, params) {
      try {
        commit('TURN_ON_LOADER', true)

        const result = await api.clicar_setting.getPersonalDirection(params)

        if (result.data) {
          const index = {
            page: 0,
            size: 200
          }
          const { data } = api.clicar_setting.getListCustomers(index, params.clicarUuid)

          commit('SET_PERSONAL_DIRECTION', result.data)
          commit('WRITE_CLICAR_UUID', params.clicarUuid)
          commit('CHANGE_SELECTED_CUSTOMER', params.client)
          commit('WRITE_LIST_CUSTOMERS', data.content)
          commit('SET_TERMS', {
            provisionTerm: result.data.docSettings.provisionTerm,
            executionConfirmationTerm: result.data.docSettings.executionConfirmationTerm,
            paymentTerm: result.data.docSettings.paymentTerm
          })
        } else {
          commit('SET_PERSONAL_DIRECTION', {})
          console.log('Ошибка при загрузке персональных направлений: ', result)
        }
      } catch (e) {
        console.log(e)
      } finally {
        commit('TURN_ON_LOADER', false)
      }
    },
    async redirectOnPageSettingDirection({ commit }) {
      commit('TURN_ON_FLAG_REDIRECT_ON_PAGE_SETTING_DIRECTION')

      await router.push('/profile__setting')
    },
    async deleteAllDirection({ commit, state, dispatch }) {
      try {
        commit('WRITE_ANSWER_BY_SERVER', '')

        const result = await api.clicar_setting.deleteAllPersonalSetting(state.clicar_uuid, state.client_uuid)

        if (result.data) {
          commit('WRITE_ANSWER_BY_SERVER', 'success');
          dispatch('sortByCustomer', state.client_uuid);
        } else {
          console.log('Ошибка при удаление услуги: ', result);
          commit('WRITE_ANSWER_BY_SERVER', 'error')
        }
      } catch (e) {
        console.log(e)
      }
    },
    async deleteDirection({ commit, state, dispatch }, { locationStartUuid, locationEndUuid }) {
      try {
        const direction = {
          clientUuid: state.client_uuid,
          startLocationUuid: locationStartUuid,
          endLocationUuid: locationEndUuid
        }

        commit('WRITE_ANSWER_BY_SERVER', '')

        const result = await api.clicar_setting.deletePersonalSetting(state.clicar_uuid, direction)

        if (result.data) {
          commit('WRITE_ANSWER_BY_SERVER', 'success')
          dispatch('sortByCustomer', state.client_uuid)
        } else {
          console.log('Ошибка при удаление услуги: ', result)
          commit('WRITE_ANSWER_BY_SERVER', 'error')
        }
      } catch (e) {
        console.log(e)
      }
    }
  }
}


