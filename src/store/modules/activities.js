import api from '../../api'

export const GET_ACTIVITIES = 'GET_ACTIVITIES'
export const SET_ACTIVITIES = 'SET_ACTIVITIES'

export default {
  state: {
    items: []
  },
  getters: {
    getActivities: state => {
      return state.items
    }
  },
  actions: {
    [GET_ACTIVITIES]: async ({ commit }) => {
      try {
        const res = await api.activities.fetchParticipantActivities()
        commit(SET_ACTIVITIES, res)
      } catch (ex) {
        console.log(ex)
      }
    },
  },
  mutations: {
    [SET_ACTIVITIES]: (state, res) => {
      state.items = res.map(item => {
        item.title = item.name
        return item
      })
    }
  }
}