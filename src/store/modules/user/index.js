import { getTokenData } from '@/tokenData'
import api from "@/api"
import VueCookie from 'vue-cookie'

let tokenData = { ...getTokenData() }

export default {
  namespaced: true,
  state: {
    userInfo: {},
    tokenData,
    secondaryRole: true,
    historyPage: {}
  },
  getters: {
    fullName (state) {
      if (state.userInfo.first_name && state.userInfo.last_name) {
        return `${state.userInfo.last_name} ${state.userInfo.first_name} ${state.userInfo.second_name || ''}`
      } else {
        return 'Неизвестный пользователь'
      }
    },
    shortFullName (state) {
      if (state.userInfo.first_name && state.userInfo.last_name) {
        const shortFirstName = ` ${state.userInfo.first_name.substr(0, 1)}.`
        const shortSecondName = state.userInfo.second_name ? ` ${state.userInfo.second_name.substr(0, 1)}.` : ''
        return state.userInfo.last_name + shortFirstName + shortSecondName
      } else {
        return 'Неизвестный пользователь'
      }
    },
    firstName (state) {
      if (state.userInfo.first_name) {
        return state.userInfo.first_name
      } else {
        return 'Неизвестный'
      }
    },
    lastName (state) {
      if (state.userInfo.last_name) {
        return state.userInfo.last_name
      } else {
        return 'Пользователь'
      }
    },
    companyName (state) {
      return state.userInfo.company_name
    },
    participantUuid (state) {
      return state.tokenData.contractor_uuid
    },
    userUuid (state) {
      return state.tokenData.user_uuid
    },
    role (state) {
      return state.tokenData.contractor_role_code
    },
    isClicar (state) {
      return state.tokenData.contractor_role_code === 'clicar'
    },
    isClient (state) {
      return state.tokenData.contractor_role_code === 'client'
    },
    isCarrier (state) {
      return state.tokenData.contractor_role_code === 'carrier'
    },
    isAdmin (state) {
      return state.tokenData.contractor_role_code === 'administrator'
    },
    isModerator (state) {
      return state.tokenData.contractor_role_code === 'moderator'
    },
    secondaryRole (state) {
      return state.secondaryRole
    },
    isLocalAdmin (state) {
      return state.userInfo.roles && state.userInfo.roles.some(item => item.is_local_administrator )
    },
    historyPage (state) {
      return state.historyPage
    }
  },
  mutations: {
    setUserInfo (state, data = {}) {
      state.userInfo = {
        ...state.userInfo,
        ...data
      }
    },
    setTokenData (state) {
      state.tokenData = { ...getTokenData() }
    },
    changeSecondaryRole (state, payload) {
      state.secondaryRole = payload
    },
    setHistoryPage (state, payload) {
      state.historyPage = payload
    }
  },
  actions: {
    async setAnonUser({ commit }) {
      const data = await api.registration.auth.anonLogin()
      let token = data.token
      VueCookie.set('auth_token', token, { secure: true, samesite: "None" })
      commit('setTokenData')
      commit('functions/setLoading', false, { root: true })
    }
  }
}