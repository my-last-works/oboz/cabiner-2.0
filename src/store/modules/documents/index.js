import api from "@/api";
import {uuid as vue_uuid} from 'vue-uuid';

export default {
  namespaced: true,
  state: {
    documents: []
  },
  mutations: {
    SET_DOCUMENTS(state, payload) {
      state.documents = payload
    }
  },
  getters: {
    documents: (state) => state.documents
  },
  actions: {
    async getDocuments({commit}) {
      await api.documents.getListDocuments()
        .then(result => commit('SET_DOCUMENTS', result.documents))
        .catch(error => console.log('Ошибка загрузки документов: ', error))
    },
    async addNewDocument({dispatch}, params) {
      const data = {
        docTypeUuid: params.docTypeUuid,
        files: params.files ? params.files : [],
        comment: params.comment ? params.comment : '',
      };
      await api.documents.saveNewDocument(vue_uuid.v4(), data)
        .then(() => dispatch('getDocuments'))
        .catch(error => console.log('Ошибка при сохранение документов: ', error))
    },
    async deleteDocument({dispatch}, uuid) {
      await api.documents.deleteDocument(uuid)
        .then(() => dispatch('getDocuments'))
        .catch(error => console.log('Ошибка при удаление документов: ', error))
    }
  }
}