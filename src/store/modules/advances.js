import api from '@/api';

/**
 'DOCS_CONFIRMED_BY_CLIENT',  -- Получатель аванса загрузил подписанный вторичный заказ (c информацией по авансу) в ОБОЗ 2.0
 'DRIVER_LOADED',			 -- Установлен признак того, что водитель загружен заказом
 'DRIVER_NOT_LOADED',		 -- Сброшен признак того, что водитель загружен заказом
 'ADVANCING_STARTED',         -- Начата процедура проверки на возможность выдачи аванса
 'ADVANCED',					 -- Принято решение о выдачи аванса со стороны организации, выдающей аванс
 'DOCS_SENDED_TO_1C',		 -- Информация о подтвержденном авансе передана в 1С УНФ
 'PAYMENT_MADE',				 -- Произведен платеж по переводу аванса
 'CANCELLED_BY_CLIENT',       -- Заявка отменена организацией, получающей аванс
 'CANCELLED_BY_CONTRACTOR'    -- Заявка отменена организацией, выдающей аванс
 */

export default {
  namespaced: true,
  state: {
    applications: [],
    application: {},
    loader: {
      rightPanel: false
    },
    blocker: false,
    blockForDriver: true
  },
  getters: {
    applications: state => state.applications,
    application: state => state.application,
    loader: state => state.loader,
    blocker: state => state.blocker,
    blockForDriver: state => state.blockForDriver,
  },
  mutations: {
    CHANGE_APPLICATION(state, payload) {
      state.application = payload
    },
    CHANGE_STATUS_RIGHT_PANEL(state, payload) {
      state.loader.rightPanel = payload
    },
    SET_BLOCK(state, payload) {
      state.blocker = payload
    },
    BLOCK_FOR_DRIVER(state, payload) {
      state.blockForDriver = payload
    },
    CLEAR_APPLICATION(state) {
      state.application = {}
    }
  },
  actions: {
    async selectedApplication({commit}, {entityUuid}) {
      commit('CHANGE_APPLICATION', {});
      commit('CHANGE_STATUS_RIGHT_PANEL', true);
      await api.advances_payment.getApplication(entityUuid)
        .then(({data}) => {
          commit('CHANGE_APPLICATION', data);
          api.advances_payment.getStatusDriver(data.entityUuid)
            .then(({clicarResponse}) => {
              if (['NEW', 'ON_DOER_SEARCH', 'BOOKED', 'ALLOCATED'].includes(clicarResponse.baseOrder.status)) {
                commit('BLOCK_FOR_DRIVER', true)
              } else {
                commit('BLOCK_FOR_DRIVER', false)
              }
            })
        })
        .catch(() => commit('CHANGE_APPLICATION', {}))
        .finally(() => commit('CHANGE_STATUS_RIGHT_PANEL', false))
    },
    async changeStatusAdvance({state, commit, dispatch}, params) {
      commit('SET_BLOCK', true);
      await api.advances_payment.changeStatusApplication(state.application.entityUuid, {
        eventType: params.event ? params.event : params,
        reasonForRefusal: params.reason,
        fileUuid: params.fileUuid
      })
        .then(() => dispatch('selectedApplication', state.application))
        .finally(() => commit('SET_BLOCK', false))
    }
  }
}
