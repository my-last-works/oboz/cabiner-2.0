import api from '@/api';
import { uuid } from "vue-uuid";
import moment from 'moment';
export default {
  namespaced: true,
  state: {
    services: [],
    subscription: {
      uuid: '',
      title: '',
      type: 'SHIPMENT_POINTS',
      fromLocations: {
        isAll: true,
        fromLocationUuids: []
      },
      toLocations: {
        isAll: true,
        toLocationUuids: []
      },
      clients: {
        isAll: true,
        clientUuids: []
      },
      services: [],
      activeFromDate: new Date(),
      activeToDate: null
    },
    subscriptions: [],
    isCreate: null,
    isDelete: null,
    listLocations: [],
    listClients: []
  },
  getters: {
    services: state => state.services,
    subscription: state => state.subscription,
    subscriptions: state => state.subscriptions,
    isCreate: state => state.isCreate,
    isDelete: state => state.isDelete,
    listLocations: state => state.listLocations,
    listClients: state => state.listClients
  },
  mutations: {
    FILL_SERVICES: (state, payload) => {
      state.services = payload;
    },
    SET_SUBSCRIPTIONS: (state, payload) => {
      state.subscriptions = payload;
    },
    SET_SUBSCRIPTION: (state, payload) => {
      state.subscription = payload;
    },
    SET_TYPE_SUBSCRIPTION: (state, payload) => {
      state.subscription.type = payload;
    },
    CLEAR_SUBSCRIPTION: (state) => {
      state.subscription = {
        uuid: '',
        title: '',
        type: 'SHIPMENT_POINTS',
        fromLocations: {
        isAll: true,
          fromLocationUuids: []
        },
        toLocations: {
          isAll: true,
          toLocationUuids: []
        },
        clients: {
          isAll: true,
          clientUuids: []
        },
        services: [],
        activeFromDate: new Date(),
        activeToDate: null
      }
    },
    SET_CREATE_FLAG: (state, payload) => {
      state.isCreate = payload;
    },
    SET_DELETE_FLAG: (state, payload) => {
      state.isDelete = payload;
    },
    SET_FROM_LOCATIONS: (state, payload) => {
      state.subscription.fromLocations.fromLocationUuids = payload;
    },
    SET_TO_LOCATIONS: (state, payload) => {
      state.subscription.toLocations.toLocationUuids = payload;
    },
    SET_STATUS_FROM_LOCATIONS: (state, payload) => {
      state.subscription.fromLocations.isAll = payload;
    },
    SET_STATUS_TO_LOCATIONS: (state, payload) => {
      state.subscription.toLocations.isAll = payload;
    },
    SET_SERVICES: (state, payload) => {
      state.subscription.services = payload;
    },
    CLEAR_FROM_LOCATIONS: (state) => {
      state.subscription.fromLocations.isAll = true;
      state.subscription.fromLocations.fromLocationUuids = [];
    },
    CLEAR_TO_LOCATIONS: (state) => {
      state.subscription.toLocations.isAll = true;
      state.subscription.toLocations.toLocationUuids = [];
    },
    CLEAR_UUID_CLIENTS: (state) => {
      state.subscription.clients.clientUuids = [];
    },
    SET_CLIENT_UUIDS: (state, payload) => {
      state.subscription.clients.clientUuids = payload;
    },
    CLEAR_PERIOD: (state) => {
      state.subscription.activeToDate = null;
    },
    SET_SERVICE_SUBSCRIPTION: (state, payload) => {
      state.subscription.services = payload;
    },
    SET_LIST_LOCATIONS: (state, payload) => {
      state.listLocations = payload
    },
    SET_LIST_CLIENTS: (state, payload) => {
      state.listClients = payload
    }
  },
  actions: {
    async getListServices ({commit}) {
      await api.subscriptions.getServiceTypeByCode()
        .then(async (services) => {
          let keys = services.map((key) => key.uuid);
          await api.subscriptions.getAdditionalInfoByServices({
            serviceTypeUuids: keys,
            isActive: true
          })
            .then((result) => {
              result = result.reduce((array, value) => {
                value = {
                  ...value,
                  title: value.resourceSubtypeTitle + ' ' + value.resourcePatternParameter,
                  // children: value.serviceUnits // todo раскоментировать и уточнить вопрос по добавлению
                };
                array.push(value);
                return array;
              }, []);

              let data = services.reduce((array, value) => {
                array.push({
                  uuid: value.uuid,
                  title: value.serviceTypeTitle,
                  list: result.filter(key => key.serviceTypeUuid === value.uuid)
                });
                return array;
              }, []);
              commit('FILL_SERVICES', data)
            })
            .catch(() => commit('FILL_SERVICES', []));
        })
        .catch(() => commit('FILL_SERVICES', []))
    },
    async showSubscriptions({commit}, uuid) {
      await api.subscriptions.showSubscriptions(uuid)
        .then((result) => {
          commit('SET_SUBSCRIPTIONS', result)
        })
        .catch(() => {
          commit('SET_SUBSCRIPTIONS', [])
        })
    },
    async createSubscription ({commit, state}, performerUuid) {

      state.subscription.uuid = state.subscription.uuid || uuid.v4()
      state.subscription.activeFromDate = moment(state.subscription.activeFromDate).format('YYYY-MM-DD');
      state.subscription.activeToDate = state.subscription.activeToDate ?
      moment(state.subscription.activeToDate).format('YYYY-MM-DD') : null;
      commit('SET_CREATE_FLAG', null);
      await api.subscriptions.createSubscription(state.subscription, performerUuid)
        .then(() => {
          commit('SET_CREATE_FLAG', true);
          commit('CLEAR_SUBSCRIPTION');
        })
        .catch((error) => {
          commit('SET_CREATE_FLAG', false);
          console.error('Ошибка при создание/редактрование подписки: ', error);
        })
    },
    async deleteSubscription ({commit}, uuid) {
      commit('SET_DELETE_FLAG', null);
      await api.subscriptions.deleteSubscriptions(uuid)
        .then(() => {
          commit('SET_DELETE_FLAG', true);
          commit('CLEAR_SUBSCRIPTION');
        })
        .catch((error) => {
          commit('SET_DELETE_FLAG', false);
          console.error('Ошибка при создание подписки: ', error);
        })
    },
    async getListLocations ({commit}) {
      await api.subscriptions.getLocations()
        .then((result) => commit('SET_LIST_LOCATIONS', result.values))
        .catch(() => commit('SET_LIST_LOCATIONS', []))
    },

    async getListClients ({commit},{ sort = 'title', direction = 'ASC', page = 0, size = 500 }) {
      await api.subscriptions.showListClients( sort, direction, page, size)
        .then((result) => {
          let _data = result.filter(key => key.roleCode === 'CLIENT');
          commit('SET_LIST_CLIENTS', _data)
        })
        .catch(() => commit('SET_LIST_CLIENTS', []))
    }
  }
}
