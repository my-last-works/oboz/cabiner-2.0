

import api from '@/api'

export default {
  namespaced: true,
  state: {
    dictionaries: {
    }
  },
  getters: {
    dictionaries: state => name => state.dictionaries[name]
  },
  mutations: {
    set(state, payload) {
      state.dictionaries = {...state.dictionaries, ...{[payload.name]: payload.data}}
    }
  },
  actions: {
    async get({commit}, dictionaries) {
      let _api = dictionaries.split('/')[0]
      let source = dictionaries.split('/')[1]
      if (source) {
        let res = await api[_api][source]();
        commit('set', { name: dictionaries, data: res.data.content });
        console.log(res);
      }
    }
  }
}