import i18n from '../../translations/lang'

export default {
  state: {
    currentLocale: 'ru'
  },
  mutations: {
      SET_LOCALE (state, payload) {
          state.currentLocale = payload
      }
  },
  getters: {
      getCurrentLocale: state => {
          return state.currentLocale
      }
  },
  actions: {
      changeLocale ({ commit }, payload) {
          i18n.loadLanguageAsync(payload).then(() => {
              commit('SET_LOCALE', payload.lang)
          })
      }
  }
}