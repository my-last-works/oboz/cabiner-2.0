import api from '../../api'

export const GET_BONUS = 'GET_BONUS';
export const SET_BONUS = 'SET_BONUS';
export const UPDATE_BONUS = 'UPDATE_BONUS';
export const DELETE_BONUS = 'DELETE_BONUS';

export default {
    state: {
        current: undefined
    },
    getters: {
        getCurrentBonusMethodology: state => {
            return state.current
        },
        getCurrentBonusMethodologyStartActivityDate: state => {
            return state.current.startActivityDt
        },
    },
    actions: {
        [GET_BONUS]: async ({ commit }, uuid) => {
            try {
                const res = await api.bonus_methodology.fetchBonusMethodologyByUuid(uuid);
                commit(SET_BONUS, res)
            } catch (ex) {
                console.log(ex)
            }
        },
        [UPDATE_BONUS]: async ({dispatch}, { uuid, payload }) => {
            try {
                const res = await api.bonus_methodology.updateBonusMethodologyByUuid(uuid, payload);
                if (!res.error) {
                    dispatch(GET_BONUS, uuid);
                }
                return(res);
            } catch (e) {
                console.log(e)
            }
        },
        [DELETE_BONUS]: async (context) => {
            const uuid = context.getters.getCurrentBonusMethodology.uuid;
            try {
                await api.bonus_methodology.deleteBonusMethodologyByUuid(uuid);
                context.commit(SET_BONUS, null);
            } catch (ex) {
                console.log(ex)
            }
        }
    },
    mutations: {
        [SET_BONUS]: (state, res) => {
            state.current = res
        },
    }
}

