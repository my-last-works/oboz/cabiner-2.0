import api from '@/api';
import { getIconByLocation } from '@/helpers';

const SET_MATH_MODAL = 'SET_MATH_MODAL';
const SET_INIT_MATH_MODAL = 'SET_INIT_MATH_MODAL';
const SET_MATH_MODEL_FIELD = 'SET_MATH_MODEL_FIELD';
const SET_MATH_MODAL_LOADING = 'SET_MATH_MODAL_LOADING';
const SET_MATH_MODAL_ERROR = 'SET_MATH_MODAL_ERROR';

const SET_BASE_PRICE_FORMULA = 'SET_BASE_PRICE_FORMULA';
const SET_BASE_PRICE_FORMULA_LOADING = 'SET_BASE_PRICE_FORMULA_LOADING';
const SET_BASE_PRICE_FORMULA_ERROR = 'SET_BASE_PRICE_FORMULA_ERROR';

const SET_BASE_PRICE_CHART = 'SET_BASE_PRICE_CHART';
const SET_BASE_PRICE_CHART_LOADING = 'SET_BASE_PRICE_CHART_LOADING';
const SET_BASE_PRICE_CHART_ERROR = 'SET_BASE_PRICE_CHART_ERROR';

const SET_AVERAGED_BASE_PRICE_CHART = 'SET_AVERAGED_BASE_PRICE_CHART';
const SET_AVERAGED_BASE_PRICE_CHART_LOADING =
  'SET_AVERAGED_BASE_PRICE_CHART_LOADING';
const SET_AVERAGED_BASE_PRICE_CHART_ERROR =
  'SET_AVERAGED_BASE_PRICE_CHART_ERROR';

const SET_AVERAGE_PRICE_FORMULA = 'SET_AVERAGE_PRICE_FORMULA';
const SET_AVERAGE_PRICE_FORMULA_LOADING = 'SET_AVERAGE_PRICE_FORMULA_LOADING';
const SET_AVERAGE_PRICE_FORMULA_ERROR = 'SET_AVERAGE_PRICE_FORMULA_ERROR';

const SET_AVERAGE_PRICE_CHART = 'SET_AVERAGE_PRICE_CHART';
const SET_AVERAGE_PRICE_CHART_LOADING = 'SET_AVERAGE_PRICE_CHART_LOADING';
const SET_AVERAGE_PRICE_CHART_ERROR = 'SET_AVERAGE_PRICE_CHART_ERROR';
const SET_SERVICES = 'SET_SERVICES';
const SET_LOCATIONS = 'SET_LOCATIONS';

const checkObjFieldsEqual = (obj1 = {}, obj2 = {}, fields = []) => {
  return fields.every(field => obj1[field] == obj2[field]);
};

const seasonWeekKeys = [...Array(52).keys()];

// Possible values of tab status are: true (if has changes) or false.
// This is needed for tab notification
// They should be removed before sending to BE
const initialMathModalStatus = {
  basePriceTabStatus: false,
  addressBasePriceTabStatus: false,
  averageTariffTabStatus: false,
  expertTariffTabStatus: false,
  expertTariffCalendarStatus: false,

  coef: false,
  bMin: false,
  bAuthor: false,
};

export default {
  namespaced: true,
  state: {
    tariffs: [],
    fixedPrice: '',
    expertPrice: '',

    isMathModalLoading: false,
    mathModalError: '', // for mathModal requests error
    initMathModal: {}, // needed for changes check
    currentMathModal: {}, // for current mathModal data
    mathModalStatus: {
      ...initialMathModalStatus,
    },

    isBasePriceFormulaLoading: false,
    basePriceFormulaError: '',
    basePriceFormula: {},

    isBasePriceChartLoading: false,
    basePriceChartError: '',
    basePriceChart: [],

    isAveragedBasePriceChartLoading: false,
    averagedBasePriceChartError: '',
    averagedBasePriceChart: [],

    isAveragePriceFormulaLoading: false,
    averagePriceFormulaError: '',
    averagePriceFormula: {},

    isAveragePriceChartLoading: false,
    averagePriceChartError: '',
    averagePriceChart: [],
    services: {},
    locations: {},
  },
  getters: {
    mathModel: state => state.currentMathModal,
    mathModelWeeks: state =>
      (state.currentMathModal && state.currentMathModal.season_week) || [],
    mathModalStatus: state => state.mathModalStatus,
    tariff: state => state.tariff,
    isMathModalLoading: state => state.isMathModalLoading,

    mathModelInvalid: state => {
      //will check mathModal isValid here
      const { coef, bMin, bAuthor } = state.mathModalStatus;
      return coef || bMin || bAuthor;
    },
    mathModelChanged: state => {
      return (
        state.mathModalStatus.basePriceTabStatus ||
        state.mathModalStatus.addressBasePriceTabStatus ||
        state.mathModalStatus.averageTariffTabStatus ||
        state.mathModalStatus.expertTariffTabStatus ||
        state.mathModalStatus.expertTariffCalendarStatus
      );
    },

    basePriceFormula: state => state.basePriceFormula,
    basePriceChart: state => state.basePriceChart,
    isBasePriceChartLoading: state => state.isBasePriceChartLoading,

    averagedBasePriceChart: state => state.averagedBasePriceChart,
    isAveragedBasePriceChartLoading: state =>
      state.isAveragedBasePriceChartLoading,

    averagePriceFormula: state => state.averagePriceFormula,
    isAveragePriceFormulaLoading: state => state.isAveragePriceFormulaLoading,

    averagePriceChart: state => state.averagePriceChart,
    isAveragePriceChartLoading: state => state.isAveragePriceChartLoading,
    services: state => state.services,
    locations: state => state.locations,
    getLocationTitleFromUuid: state => uuid => {
      return state.locations[uuid] ? state.locations[uuid].title : '...';
    },
    getLocationIconFromUuid: state => uuid => {
      return state.locations[uuid] ? state.locations[uuid].icon : '';
    },
  },
  actions: {
    getMathModel: async ({ commit }) => {
      commit(SET_MATH_MODAL_LOADING, true);
      commit(SET_MATH_MODAL_ERROR, '');

      try {
        const res = await api.base_prices.getMathModel();
        commit(SET_MATH_MODAL, res);
        const initMathModal = { ...res }; // copy of res
        initMathModal.season_week = [...res.season_week]; // copy of season_week
        commit(SET_INIT_MATH_MODAL, initMathModal);
        return res;
      } catch (error) {
        console.error(error);
        commit(SET_MATH_MODAL_ERROR, error.error_message || 'Unknown error!');
      }

      commit(SET_MATH_MODAL_LOADING, false);
    },
    updateMathModel: async ({ commit, state }) => {
      commit(SET_MATH_MODAL_LOADING, true);
      await api.base_prices.updateMathModel(state.currentMathModal);
      commit(SET_MATH_MODAL_LOADING, false);
    },
    clearMathModel: ({ commit }) => {
      commit(SET_MATH_MODAL, {});
      commit(SET_INIT_MATH_MODAL, {});
      commit('resetValidationState', { ...initialMathModalStatus });
    },
    setMathModelErrorStatus: ({ commit }, data) => {
      commit('setMathModelStatusField', data);
    },
    setMathModelField: (
      { commit, state: { initMathModal, currentMathModal } },
      { name, value }
    ) => {
      commit(SET_MATH_MODEL_FIELD, { name, value });

      // after setting field we need to check all fields except for calendar
      // to set tabs statuses, errors and anything else if needed

      switch (name) {
        case 'beta_author':
        case 'beta_min':
          commit('setMathModelStatusField', {
            name: 'basePriceTabStatus',
            value: !checkObjFieldsEqual(initMathModal, currentMathModal, [
              'beta_author',
              'beta_min',
            ]),
          });
          break;

        case 'delta':
          commit('setMathModelStatusField', {
            name: 'addressBasePriceTabStatus',
            value: !checkObjFieldsEqual(initMathModal, currentMathModal, [
              'delta',
            ]),
          });
          break;

        case 'coef_a':
        case 'coef_b':
        case 'coef_c':
        case 'nearest_interval':
        case 'recent_interval':
        case 'distant_interval':
          commit('setMathModelStatusField', {
            name: 'averageTariffTabStatus',
            value: !checkObjFieldsEqual(initMathModal, currentMathModal, [
              'coef_a',
              'coef_b',
              'coef_c',
              'nearest_interval',
              'recent_interval',
              'distant_interval',
            ]),
          });
          break;

        case 'inflation_rate':
          commit('setMathModelStatusField', {
            name: 'expertTariffTabStatus',
            value: !checkObjFieldsEqual(initMathModal, currentMathModal, [
              'inflation_rate',
            ]),
          });
          break;

        default:
          break;
      }
    },
    resetValidationState: ({ commit }) => {
      commit('resetValidationState', { ...initialMathModalStatus });
    },
    setExpertWeekRateByIndex: (
      { commit, state: { initMathModal, currentMathModal } },
      data
    ) => {
      commit('setExpertWeekRateByIndex', data);

      commit('setMathModelStatusField', {
        name: 'expertTariffCalendarStatus',
        value: !checkObjFieldsEqual(
          initMathModal.season_week,
          currentMathModal.season_week,
          seasonWeekKeys
        ),
      });
    },

    async getFixedPrice({ commit }) {
      try {
        const res = await api.base_prices.getFixedPrice();
        commit('setFixedPrice', res);
      } catch (error) {
        console.error(error);
      }
    },

    getBasePriceFormula: async ({ commit }, base_price_uuid) => {
      commit(SET_BASE_PRICE_FORMULA_LOADING, true);
      commit(SET_BASE_PRICE_FORMULA_ERROR, '');

      try {
        const statFormula = await api.base_prices.getBasePriceFormula(
          base_price_uuid
        );
        commit(SET_BASE_PRICE_FORMULA, statFormula);
      } catch (error) {
        console.error(error);
        commit(
          SET_BASE_PRICE_FORMULA_ERROR,
          error.error_message || 'Unknown error!'
        );
      }

      commit(SET_BASE_PRICE_FORMULA_LOADING, false);
    },
    getBasePriceChart: async ({ commit }, tariff_class_uuid) => {
      commit(SET_BASE_PRICE_CHART_LOADING, true);
      commit(SET_BASE_PRICE_CHART_ERROR, '');

      try {
        const statChart = await api.base_prices.getBasePriceChart(
          tariff_class_uuid
        );
        commit(SET_BASE_PRICE_CHART, statChart);
      } catch (error) {
        console.error(error);
        commit(
          SET_BASE_PRICE_CHART_ERROR,
          error.error_message || 'Unknown error!'
        );
      }

      commit(SET_BASE_PRICE_CHART_LOADING, false);
    },
    getAveragedBasePriceChart: async ({ commit }, tariff_class_uuid) => {
      commit(SET_AVERAGED_BASE_PRICE_CHART_LOADING, true);
      commit(SET_AVERAGED_BASE_PRICE_CHART_ERROR, '');

      try {
        const statChart = await api.base_prices.getAveragedBasePriceChart(
          tariff_class_uuid
        );

        commit(SET_AVERAGED_BASE_PRICE_CHART, statChart);
      } catch (error) {
        console.error(error);
        commit(
          SET_AVERAGED_BASE_PRICE_CHART_ERROR,
          error.error_message || 'Unknown error!'
        );
      }

      commit(SET_AVERAGED_BASE_PRICE_CHART_LOADING, false);
    },
    getAveragePriceFormula: async ({ commit }, averaged_price_uuid) => {
      commit(SET_AVERAGE_PRICE_FORMULA_LOADING, true);
      commit(SET_AVERAGE_PRICE_FORMULA_ERROR, '');

      try {
        const statFormula = await api.base_prices.getAveragePriceFormula(
          averaged_price_uuid
        );
        commit(SET_AVERAGE_PRICE_FORMULA, statFormula);
      } catch (error) {
        console.error(error);
        commit(
          SET_AVERAGE_PRICE_FORMULA_ERROR,
          error.error_message || 'Unknown error!'
        );
      }

      commit(SET_AVERAGE_PRICE_FORMULA_LOADING, false);
    },
    getAveragePriceChart: async ({ commit }, tariff_class_uuid) => {
      commit(SET_AVERAGE_PRICE_CHART_LOADING, true);
      commit(SET_AVERAGE_PRICE_CHART_ERROR, '');

      try {
        const statChart = await api.base_prices.getAveragePriceChart(
          tariff_class_uuid
        );
        commit(SET_AVERAGE_PRICE_CHART, statChart);
      } catch (error) {
        console.error(error);
        commit(
          SET_AVERAGE_PRICE_CHART_ERROR,
          error.error_message || 'Unknown error!'
        );
      }

      commit(SET_AVERAGE_PRICE_CHART_LOADING, false);
    },
    getServices: async ({ commit, state: { services } }) => {
      try {
        if (Object.keys(services).length == 0) {
          const newServices = await api.dictionaries.services.getServices({ size: 10000 });
          if (newServices && newServices.data && newServices.data.content) {
            let serv = newServices.data.content.reduce((newServices, service) => {
              newServices[service.uuid] = {
                serviceTypeTitle: service.serviceTypeTitle,
                resourceSpeciesTitle: service.resourceSpeciesTitle,
                resourceSubtypeTitle: service.resourceSubtypeTitle,
                resourcePatternParameter: service.resourcePatternParameter,
                serviceUnits: service.serviceUnits,
                serviceTypeUuid: service.serviceTypeUuid,
              };
              return newServices;
            }, {});
            commit(SET_SERVICES, serv);
          }
        }
      } catch (error) {
        console.error(error);
      }
    },
    getLocations: async ({ commit, state: { locations } }, uuidList) => {
      try {
        const unique = [...new Set(uuidList)].filter((el) => {
          return (!!el && !locations.hasOwnProperty(el) );
        });
        const list = await Promise.allSettled(unique.map(el => api.locations.getLocation(el)));
        const result = {};
        list && list.forEach(({ status, value }) => {
          if (status === "fulfilled") {
            let data = value.data;
            data.icon = getIconByLocation(data.location_type_uuid);
            result[data.uuid] = data;
          }
        });
        commit(SET_LOCATIONS, result);
      } catch (error) {
        commit(SET_LOCATIONS, {});
        console.error(error);
      }
    },
    // async updateFixedPrice({ commit }) {
    //   try {
    //     const res = await api.base_prices.updateFixedPrice();
    //     commit('setFixedPrice', res);
    //   } catch (error) {
    //     console.error(error)
    //   }
    // },
    // async deleteFixedPrice({ commit }) {
    //   try {
    //     const res = await api.base_prices.deleteFixedPrice();
    //     commit('setFixedPrice', res);
    //   } catch (error) {
    //     console.error(error)
    //   }
    // },
    // async getExpertPrice({ commit }) {
    //   try {
    //     const res = await api.base_prices.getExpertPrice();
    //     commit('setExpertPrice', res);
    //   } catch (error) {
    //     console.error(error)
    //   }
    // },
    // async updateExpertPrice({ commit }) {
    //   try {
    //     const res = await api.base_prices.updateExpertPrice();
    //     commit('setExpertPrice', res);
    //   } catch (error) {
    //     console.error(error)
    //   }
    // },
    // async deleteExpertPrice({ commit }) {
    //   try {
    //     const res = await api.base_prices.deleteExpertPrice();
    //     commit('setExpertPrice', res);
    //   } catch (error) {
    //     console.error(error)
    //   }
    // },
  },
  mutations: {
    [SET_MATH_MODAL]: (state, res) => {
      state.currentMathModal = res;
    },
    [SET_INIT_MATH_MODAL]: (state, res) => {
      state.initMathModal = res;
    },
    [SET_MATH_MODAL_ERROR]: (state, payload) => {
      state.mathModalError = payload;
    },
    [SET_MATH_MODAL_LOADING]: (state, boolean) => {
      state.isMathModalLoading = boolean;
    },
    setMathModelStatusField(state, { name, value }) {
      state.mathModalStatus[name] = value;
    },
    resetValidationState(state) {
      state.mathModalStatus = {
        ...initialMathModalStatus,
      };
    },
    [SET_MATH_MODEL_FIELD]: (state, { name, value }) => {
      state.currentMathModal[name] = value;
    },
    setExpertWeekRateByIndex(state, { index, value }) {
      state.currentMathModal.season_week.splice(index, 1, +value);
    },

    setFixedPrice(state, res) {
      state.fixedPrice = res;
    },

    [SET_BASE_PRICE_FORMULA_LOADING]: (state, payload) => {
      state.isBasePriceFormulaLoading = payload;
    },
    [SET_BASE_PRICE_FORMULA_ERROR]: (state, payload) => {
      state.basePriceFormulaError = payload;
    },
    [SET_BASE_PRICE_FORMULA]: (state, payload) => {
      state.basePriceFormula = payload;
    },

    [SET_BASE_PRICE_CHART_LOADING]: (state, payload) => {
      state.isBasePriceChartLoading = payload;
    },
    [SET_BASE_PRICE_CHART_ERROR]: (state, payload) => {
      state.basePriceChartError = payload;
    },
    [SET_BASE_PRICE_CHART]: (state, payload) => {
      state.basePriceChart = payload;
    },

    [SET_AVERAGED_BASE_PRICE_CHART_LOADING]: (state, payload) => {
      state.isAveragedBasePriceChartLoading = payload;
    },
    [SET_AVERAGED_BASE_PRICE_CHART_ERROR]: (state, payload) => {
      state.averagedBasePriceChartError = payload;
    },
    [SET_AVERAGED_BASE_PRICE_CHART]: (state, payload) => {
      state.averagedBasePriceChart = payload;
    },

    [SET_AVERAGE_PRICE_CHART_LOADING]: (state, payload) => {
      state.isAveragePriceChartLoading = payload;
    },
    [SET_AVERAGE_PRICE_CHART_ERROR]: (state, payload) => {
      state.averagePriceChartError = payload;
    },
    [SET_AVERAGE_PRICE_CHART]: (state, payload) => {
      state.averagePriceChart = payload;
    },

    [SET_AVERAGE_PRICE_FORMULA]: (state, payload) => {
      state.averagePriceFormula = payload;
    },
    [SET_AVERAGE_PRICE_FORMULA_ERROR]: (state, payload) => {
      state.averagePriceFormulaError = payload;
    },
    [SET_AVERAGE_PRICE_FORMULA_LOADING]: (state, payload) => {
      state.isAveragePriceFormulaLoading = payload;
    },
    [SET_SERVICES]: (state, payload) => {
      state.services = payload;
    },
    [SET_LOCATIONS]: (state, payload) => {
      state.locations = Object.assign({}, state.locations, payload);
    },
  },
};
