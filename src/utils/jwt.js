import jwtDecode from 'jwt-decode'
import {getCookie} from "./cookie";

let cookie = getCookie('auth_token')

export const tokenData =  cookie ? jwtDecode(cookie) : {}
