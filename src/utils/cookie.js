import VueCookie from 'vue-cookie'

export const getCookie = (name) => {
	try {
		const cookie = VueCookie.get(name)
		return cookie === null || cookie === undefined ? null : cookie
	} catch (e) {
		return null
	}
}
