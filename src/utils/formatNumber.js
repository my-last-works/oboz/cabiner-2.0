export default function numberWithSpaces(num, divider = 1) {
  return (num/divider).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}