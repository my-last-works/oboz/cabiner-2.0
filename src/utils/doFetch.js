import VueCookie from 'vue-cookie'

function doFetch (url, config, lang) {
  const conf = config || {}
  // add language
  const langHeader = lang || {
    'Accept-Language': 'RU'
  }
  conf.headers = { ...conf.headers, ...langHeader }
  // add auth token
  const token = VueCookie.get('auth_token')
  if (token) {
    const authHeader = {
      Authorization: `Bearer ${token}`
    }
    conf.headers = { ...conf.headers, ...authHeader }
  }
  return fetch(url, conf)
}
export default doFetch
