import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import VueCookie from 'vue-cookie'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
import './common.js'
// import i18n from './translations/lang'
import store from './store/index'
import Notify from "./services/Notifications"
import Router from 'vue-router'
import 'common-components/src/helpers/VueFilters'
// import { makeServer } from './mirageServer' // TODO: закоментировать
import './filters'
import VCharts from 'v-charts'
import 'leaflet/dist/leaflet.css';
import Vuelidate from 'vuelidate'
import vueDebounce from 'vue-debounce'
import VueMeta from 'vue-meta'
import moment from 'moment'
import uniqueTableKeys from "@/constants/uniqueTableKeys"

(async function () {
  // if (process.env.NODE_ENV === "development") {  // TODO: закоментировать
  //   makeServer()
  // }

  Vue.prototype.$eventGlobal = new Vue()
  Vue.prototype.$uniqueTableKeys = uniqueTableKeys

  Vue.config.productionTip = false;

  moment.locale('ru')
  Vue.prototype.$moment = moment

  Vue.use(Notify)
  Vue.use(VueLodash, { name: 'custom' , lodash: lodash })
  Vue.use(VueCookie)
  Vue.use(Router)
  Vue.use(VCharts)
  Vue.use(Vuelidate)
  Vue.use(vueDebounce)
  Vue.use(VueMeta)

  new Vue({
      router,
      // i18n,
      store,
      render: h => h(App)
    }).$mount('#app')
})()

