import { required } from "vuelidate/lib/validators";

export default {
    uuid: {
        value: null
    },
    title: {
        nested_fields: {
          ch: {
            value: '',
          },
          en: {
            value: ''
          },
          ru: {
            value: ''
          },
          label: {
            value: ''
          }
        }
    },
    pos: {
        value: '',
        validator: {
            required
        }
    },
    is_active: {
        value: true
    },
    type: {
        value: 'PARTICIPANT',
        validator: {
            required
        }
    }
}