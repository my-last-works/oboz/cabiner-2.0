import { required } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  denomination: {
    value: '',
    validator: {
      required
    }
  },
  shorthand: {
    value: '',
    validator: {
      required
    }
  },
  international_shorthand: {
    value: '',
    validator: {
      required
    }
  },
  description: {
    value: '',
    validator: {
      required
    }
  }
}

export default {
  add
}
