import { required } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  maskUuid: {
    value: null,
    validator: {
      required
    }

  },
  parameters: {
    value: {},
    validator: {
      required
    }
  }
}

export default {
  add
}
