import { required } from "vuelidate/lib/validators";

const add = {
    resourceSubTypeUuid: {
        value: '',
        validator: {
            required
        }
    },
    resourcePatternUuids: {
        value: [],
        validator: {}
    }
}

export default {
    add
}
