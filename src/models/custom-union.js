import { required } from "vuelidate/lib/validators";

export default {
  uuid: {
    value: null
  },
  international_name: {
    value: '',
    validator: {
      required
    }
  },
  members: {
    type: 'array',
    value: []
  },
  members_uuid: {
    type: 'array',
    value: [],
    exclude: true,
    validator: {
      required
    }
  },
  name: {
    value: '',
    validator: {
      required
    }
  },
  short_name: {
    value: '',
    validator: {
      required
    }
  }
}
