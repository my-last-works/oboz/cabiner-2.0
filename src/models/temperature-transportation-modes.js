import { required } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  denomination: {
    value: '',
    validator: {
      required
    }
  },
  mode_type: {
    value: 'COOLING',
    validator: {
      required
    }
  },
  temperature_low_limit: {
    type: 'number',
    value: -10,
    validator: {
      required
    }
  },
  temperature_high_limit: {
    type: 'number',
    value: 25,
    validator: {
      required
    }
  },
  single_max_time_out_of_borders: {
    type: 'number',
    value: 50,
    validator: {
      required
    }
  },
  sum_max_time_out_of_borders: {
    type: 'number',
    value: 300,
    validator: {
      required
    }
  },
  critical_deviation_low_limit: {
    type: 'number',
    value: -35,
    validator: {
      required
    }
  },
  critical_deviation_high_limit: {
    type: 'number',
    value: 30,
    validator: {
      required
    }
  },
  description: {
    value: ''
  },
  temperature_recorders: {
    type: 'array',
    value: [],
    validator: {
      required
    }
  },
  is_active: {
    value: true
  }
}

export default {
  add
}
