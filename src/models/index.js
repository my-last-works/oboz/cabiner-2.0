import customUnion from './custom-union';
import locations from './locations';
import temperatureRecorder from './temperature-recorder';
import temperatureTransportationModes from './temperature-transportation-modes';
import cargoTypes from './cargo-types';
import patterns from './patterns';
import forwardUnit from './forward-unit';
import serviceType from './service-type';
import documentType from './document-type';
import serviceUnit from './service-unit';
import service from './service';
import resourceType from './resource-type';
import userRoles from './user_roles';
import adminPanelModels from './admin-panel/index'
import cardFields from './card-fields';
import requisiteModel from './sb/requisite'
import consignor from './consignor'
import contract from './contract'
import user from './user'

export default {
  forwardUnit,
  customUnion,
  locations,
  temperatureRecorder,
  temperatureTransportationModes,
  cargoTypes,
  patterns,
  serviceType,
  documentType,
  serviceUnit,
  service,
  resourceType,
  userRoles,
  adminPanelModels,
  requisiteModel,
  cardFields,
  consignor,
  contract,
  user
}
