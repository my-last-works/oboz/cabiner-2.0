import { required } from "vuelidate/lib/validators";

const add = {
    uuid: {
        value: null
    },
    serviceTypeUuid: {
        value: '',
        validator: {
            required
        }
    },
    resourcePatternUuid: {
        value: '',
    },
    resourceTypeUuid: {
        value: '',
    },
    geography: {
        value: '',
        validator: {}
    },
    resourceSubtypeUuid: {
        value: '',
    },
    resourceSpeciesUuid: {
        value: '',
    },
    isActive: {
        value: true,
    },
    serviceUnits: {
        value: [],
        validator: {
            required
        }
    }
}

export default {
    add
}
