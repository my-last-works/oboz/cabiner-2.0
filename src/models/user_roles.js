import { required } from 'vuelidate/lib/validators'

export default {
  uuid: {
    value: null
  },
  userRole: {
    value: '',
    validator: {
      required
    }
  },
  contractor_role_uuid: {
    value: null,
    validator: {
      required
    }
  },
  status: {
    value: 'Активна'
  },
  functions: {
    type: 'array',
    value: []
  }
}
