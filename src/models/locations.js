import { required } from "vuelidate/lib/validators";

const main = {
  uuid: {
    value: null
  },
  location_type_uuid: {
    value: null,
    validator: {
      required
    }
  },
  coordinates: {
    nested_fields: {
      lat: {
        value: null,
        validator: {
          required
        }
      },
      lon: {
        value: null,
        validator: {
          required
        }
      }
    }
  },
  russian_name: {
    value: '',
    validator: {

    }
  },
  english_name: {
    value: '',
    validator: {

    }
  },
  local_name: {
    value: '',
		validator: {

		}
  },
  russian_address: {
      value: '',
      validator: {

      }
  },
  english_address: {
      value: '',
      validator: {

      }
  },
  local_address: {
    value: '',

  },
  short_title: {
    value: '',
    validator: {}
  },
  country_symbol_code: {
    value: '',
    validator: {}
  },
  location_radius: {
    nested_fields: {
      zero_interrogation_radius: {
        type: 'number',
        value: 0
      },
      vehicle_arrival_radius: {
        type: 'number',
        value: 0
      },
      vehicle_departure_radius: {
        type: 'number',
        value: 0
      },
      hard_binding_radius: {
        type: 'number',
        value: 0
      }
    }
  },
  rail_stations: {
    value: []
  },
  railway_terminal_uuid: {
    value: '',
    validator: {}
  },
  container_terminal_uuid: {
    value: '',
    validator: {}
  },
  is_active: {
    type: Boolean,
    value: true
  }
}

const coords = {
  coordsLocal: {
    value: [],
    validator: {
      required
    }
  },
}

export default {
  main,
  coords
}
