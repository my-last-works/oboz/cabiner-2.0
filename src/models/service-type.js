import { required, requiredIf } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  needTamProcessing: {
    type: 'boolean',
    value: false
  },
  reason_codes:{
    type: 'boolean',
    value: true
  },
  needBeruchersCreation: {
    type: 'boolean',
    value: false
  },
  isActive:{
    type: 'boolean',
    value: true
  },
  isVatCharged:{
    type: 'boolean',
    value: false
  },
  isIncludedInTariff: {
    type: 'boolean',
    value: false
  },
  activityUuid: {
    value: ' ',
    validator: {
      required
    }
  },
  title: {
    value: '',
    validator: {
      required
    }
  },
  reasons: {
    type: 'array',
    value: [],
  },
  needResources: {
    type: 'boolean',
    value: false
  },
  resourceSpecies: {
    transform: {
      byField: 'uuid'
    },
    type: 'array',
    value: [],
    exclude: (data) => !data.needResources,
    validator: {
      required: requiredIf(function (model) {
        return model.needResources;
      })
    }
  },
  geography: {
    value: '',
    validator: {
      required
    }
  },
  statusModel: {
    value: '',
    validator: {
      required
    }
  },
  category: {
    value: '',
    validator: {
      required
    }
  },
  includedInMainService: {
    type: 'boolean',
    value: false
  },
  calculateInBaseprice: {
    type: 'boolean',
    value: false
  },
  inclusionInDraft: {
    type: 'string',
    value: 'REQUIRED_AND_ON'
  },
  resourceClassCodes: {
    type: 'array',
    value: ['AUTO', 'TRAIN', 'CONTAINER']
  },
  etsngs: {
    type: 'array',
    value: []
  }
}
export default {
  add
}
