import { required } from "vuelidate/lib/validators";

export default {
    title: {
        validator: {
            required
        }
    },
    type: {
        validator: {
            required
        }
    },
    is_active: {
        validator: {
            required
        }
    },
    contractor_role_codes: {
        validator: {
            required
        }
    }
}
