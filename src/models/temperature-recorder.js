import { required } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  denomination: {
    value: '',
    validator: {
      required
    }
  },
  tracker: {
    nested_fields: {
      code: {
        value: ''
      },
      denomination: {
        value: ''
      },
      uuid: {
        value: '',
        validator: {
          required
        }
      }
    }
  },
  description: {
    value: '',
    validator: {
      required
    }
  },
  is_active: {
    type: 'boolean',
    value: true
  }
}

export default {
  add
}
