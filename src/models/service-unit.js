import { required } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  name: {
    value: '',
    validator: {
      required
    }
  },
  denomination: {
    value: '',
    validator: {
      required
    }
  },
  code: {
    value: '',
    validator: {
      required
    }
  },
  'is_active': {
    value: false
  }
}

export default {
  add
}
