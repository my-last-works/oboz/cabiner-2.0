import { required } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  name: {
    value: '',
    validator: {
      required
    }
  },
  document_class: {
    value: '',
    validator: {
      required
    }
  }
}

export default {
  add
}
