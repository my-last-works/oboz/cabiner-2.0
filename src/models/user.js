import { required, email } from 'vuelidate/lib/validators';

const add = {
  lastName: {
    value: '',
    validator: { required }
  },
  firstName: {
    value: '',
    validator: { required }
  },
  patronym: {
    value: '',
  },
  email: {
    value: '',
    validator: { required, email }
  },
  isActive: {
    value: '',
    validator: { required }
  },
  workerRoles: {
    value: '',
    validator: { required }
  }
};

export default { add };

