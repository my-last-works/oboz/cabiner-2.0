import { required, requiredIf, minLength } from "vuelidate/lib/validators";
import { uuid } from "vue-uuid";

const isCorrectPrice = (value) => {
  return Boolean(value)
}

const service = ({ isVatPayer, caste }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    uuid: {
      value: '',
    },
    geography: {
      value: 'FROM_TO'
    },
    serviceDictUuid: {
      value: '',
      validator: {
        required
      }
    },
    contractUuid: {
      value: '',
      validator: {
        required
      }
    },
    serviceUnitUuid: {
      value: '',
      validator: {
        required
      }
    },
    resourceTypeUuid: {
      value: '',
      validator: {
        required
      }
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    isVatInclude: {
      type: 'boolean',
      value: () => isVatPayer
    },
    isNds: {
      value: () => isVatPayer,
      type: 'boolean'
    },
    extraMileageTariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            isCorrectPrice: {
              method: (value) => {
                return caste !== 'SIMPLE' || isCorrectPrice(value)
              }
            }
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            isCorrectPrice: {
              method: (value) => {
                return caste !== 'SIMPLE' || isCorrectPrice(value)
              }
            }
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        }
      }
    },
    extraPointTariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            isCorrectPrice: {
              method: (value) => {
                return caste !== 'SIMPLE' || isCorrectPrice(value)
              }
            }
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            isCorrectPrice: {
              method: (value) => {
                return caste !== 'SIMPLE' || isCorrectPrice(value)
              }
            }
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        }
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            isCorrectPrice: {
              method: (value) => {
                return caste !== 'ADDITIONAL' || isCorrectPrice(value)
              }
            }
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            isCorrectPrice: {
              method: (value) => {
                return caste !== 'ADDITIONAL' || isCorrectPrice(value)
              }
            }
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'ADDITIONAL'
            })
          }
        }
      }
    }
  }
}

const serviceEdit = ({ caste, vatType, vatRate, isVatInclude }) => {
  return {
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    isVatInclude: {
      type: 'boolean',
      value: () => isVatInclude
    },
    isNds: {
      value: () => vatType !== 'NO_VAT',
      type: 'boolean'
    },
    extraMileageTariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        }
      }
    },
    extraPointTariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'SIMPLE'
            })
          }
        }
      }
    }
  }
}

const serviceAdditional = ({ isVatPayer, caste }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    uuid: {
      value: '',
    },
    serviceUuid: {
      value: ''
    },
    isVatInclude: {
      type: 'boolean',
      value: () => isVatPayer
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    isNds: {
      value: () => isVatPayer,
      type: 'boolean'
    },
    geography: {
      value: ''
    },
    extraMileageTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    extraPointTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'ADDITIONAL' && this.formData.geography === 'WITHOUT_GEOGRAPHY_BINDING'
            })
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'ADDITIONAL' && this.formData.geography === 'WITHOUT_GEOGRAPHY_BINDING'
            })
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return caste === 'ADDITIONAL' && this.formData.geography === 'WITHOUT_GEOGRAPHY_BINDING'
            })
          }
        }
      }
    }
  }
}

const serviceAdditionalEdit = ({ vatType, vatRate, isVatInclude }) => {
  return {
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    isVatInclude: {
      type: 'boolean',
      value: () => isVatInclude
    },
    isNds: {
      value: () => vatType !== 'NO_VAT',
      type: 'boolean'
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return this.formData.geography === 'WITHOUT_GEOGRAPHY_BINDING'
            })
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return this.formData.geography === 'WITHOUT_GEOGRAPHY_BINDING'
            })
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required: requiredIf(function () {
              return this.formData.geography === 'WITHOUT_GEOGRAPHY_BINDING'
            })
          }
        }
      }
    }
  }
}

const serviceMultistop = ({ isVatPayer }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    uuid: {
      value: '',
      validator: {
        required
      }
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    serviceUnitUuid: {
      value: '',
      validator: {
        required
      }
    },
    isVatInclude: {
      type: 'boolean',
      value: () => isVatPayer
    },
    isNds: {
      value: () => isVatPayer,
      type: 'boolean'
    },
    geography: {
      value: 'FROM_TO'
    }
  }
}

const serviceMultistopEdit = ({ isVatPayer }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    isVatInclude: {
      value: true,
      type: 'boolean'
    },
    geography: {
      value: 'FROM_TO'
    },
    serviceCaste: {
      value: ''
    },
    isNds: {
      value: () => isVatPayer,
      type: 'boolean'
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    }
  }
}

const serviceTracking = {
  uuid: {
    value: '',
    validator: {
      required
    }
  },
  serviceUnitUuid: {
    value: '',
    validator: {
      required
    }
  },
  isVatInclude: {
    type: 'boolean',
    value: true
  },
  vatValue: {
    value: 20,
    validator: {
      required: requiredIf(function () {
        return Boolean(this.formData.isNds)
      })
    }
  },
  gsmTariff: {
    value: null,
    validator: {
      required
    }
  },
  onboardDeviceTariff: {
    value: null,
    validator: {
      required
    }
  },
  mobileAppTariff: {
    value: null,
    validator: {
      required
    }
  },
  isNds: {
    value: true,
    type: 'boolean'
  }
}

const serviceTrackingEdit = {
  vatValue: {
    value: ''
  },
  isVatInclude: {
    value: true,
    type: 'boolean'
  },
  gsmTariff: {
    value: null,
    validator: {
      required
    }
  },
  onboardDeviceTariff: {
    value: null,
    validator: {
      required
    }
  },
  mobileAppTariff: {
    value: null,
    validator: {
      required
    }
  }
}

const directions = ({ isVatPayer }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    routeCategory: {
      value: '',
      validator: {
        required
      }
    },
    transboundary: {
      value: '',
      validator: {
        required
      }
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    serviceUuid: {
      value: ''
    },
    isVatInclude: {
      value: ''
    },
    locationFromUuid: {
      value: '',
      validator: {
        required
      }
    },
    locationToUuid: {
      value: '',
      validator: {
        required
      }
    },
    extraMileageTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    extraPointTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    }
  }
}

const directionsEdit = ({ vatType, vatRate }) => {
  return {
    routeCategory: {
      value: '',
      validator: {
        required
      }
    },
    transboundary: {
      value: '',
      validator: {
        required
      }
    },
    directionUuid: {
      value: ''
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    extraMileageTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    extraPointTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    }
  }
}

const directionsAdditional = ({ isVatPayer }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    routeCategory: {
      value: '',
      validator: {
        required
      }
    },
    transboundary: {
      value: '',
      validator: {
        required
      }
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    directionUuid: {
      value: ''
    },
    locationFromUuid: {
      value: '',
      validator: {
        required
      }
    },
    locationToUuid: {
      value: '',
      validator: {
        required: requiredIf(function () {
          return !this.formData.direction1
        }),
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    }
  }
}

const directionsAdditionalEdit = ({ vatType, vatRate, isVatInclude }) => {
  return {
    directionUuid: {
      value: ''
    },
    routeCategory: {
      value: '',
      validator: {
        required
      }
    },
    transboundary: {
      value: '',
      validator: {
        required
      }
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    isVatInclude: {
      value: isVatInclude
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    }
  }
}

const checkMultistopLocation = (value, vm) => {
  return Boolean(vm && (vm.address || vm.locationUuid))
}

//////
const directionsMultistop = ({ isVatPayer }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    serviceUuid: {
      value:  ''
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    points: {
      type: 'array',
      value: [
        {
          locationUuid: null,
          address: '',
          uuid: uuid.v4()
        },
        {
          locationUuid: null,
          address: '',
          uuid: uuid.v4()
        }
      ],
      validator: {
        minLength: minLength(2),
        $each: {
          location: {
            value: '',
            validator: {
              checkLocation: {
                method: checkMultistopLocation,
                error: 'Выберите локацию или конкретный адрес'
              }
            }
          }
        }
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    },
    extraMileageTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    extraPointTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    }
  }
}

const directionsMultistopEdit = ({ isVatPayer }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    directionUuid: {
      value:  ''
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    },
    extraMileageTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    },
    extraPointTariff: {
      nested_fields: {
        priceWithVat: {
          value: ''
        },
        priceWithoutVat: {
          value: ''
        },
        vatPrice: {
          value: ''
        }
      }
    }
  }
}

const locations = ({ isVatPayer }) => {
  const vatRate = isVatPayer ? 20 : 0
  const vatType = isVatPayer ? 'TWENTY' : 'NO_VAT'

  return {
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    serviceUuid: {
      value: ''
    },
    locationUuid: {
      value: '',
      validator: {
        required
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    }
  }
}

const locationsEdit = ({ vatType, vatRate }) => {
  return {
    locationUuid: {
      value: ''
    },
    vatRate: {
      value: vatRate,
      validator: {
        required
      }
    },
    vatType: {
      value: vatType,
      validator: {
        required
      }
    },
    tariff: {
      nested_fields: {
        priceWithVat: {
          value: '',
          validator: {
            required
          }
        },
        priceWithoutVat: {
          value: '',
          validator: {
            required
          }
        },
        vatPrice: {
          value: '',
          validator: {
            required
          }
        }
      }
    }
  }
}

export default {
  service,
  serviceEdit,
  serviceAdditional,
  serviceAdditionalEdit,
  serviceMultistop,
  serviceMultistopEdit,
  serviceTracking,
  serviceTrackingEdit,
  directions,
  directionsEdit,
  directionsAdditional,
  directionsAdditionalEdit,
  directionsMultistop,
  directionsMultistopEdit,
  locations,
  locationsEdit
}
