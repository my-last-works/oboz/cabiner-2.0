import { required } from "vuelidate/lib/validators";

export default {
    countries: {
        value: '',
        validator: {
            required
        }
    },
    classes: {
        value: '',
        validator: {
            required
        }
    },
    type: {
        value: '',
        validator: {
            required
        }
    },
    number: {
        value: '',
        validator: {
            required
        }
    },
}
