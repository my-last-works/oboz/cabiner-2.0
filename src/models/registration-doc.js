import { required } from "vuelidate/lib/validators";

export default {
    name: {
        value: '',
        validator: {
            required
        }
    },
    recognizerUuid: {
        value: [],
        validator: {
            required
        }
    },
    status: {
        value: true,
        type: 'boolean'
    }
}
