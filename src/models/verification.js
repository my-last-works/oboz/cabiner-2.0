import { required } from "vuelidate/lib/validators";

export default {
    name: {
        value: '',
        validator: {
            required
        }
    },
    periodInDay: {
        value: 180,
        validator: {
            required
        }
    }
}