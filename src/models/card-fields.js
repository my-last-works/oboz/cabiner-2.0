import { required } from "vuelidate/lib/validators";

const add = {
    field_uuids: {
        value: [],
        validator: {
            required
        }
    },
}

const edit = {
    DataSourceDomainReadSettings: {
        value: {}
    },
    AdminDataSourceSettings: {
        value: {}
    },
    ParticipantCommonDomainReadSettings: {
        value: {}
    },
    DataSourceDomainCreationSettings: {
        DataSourceSettings: {

        }
    }

}

const editResource = {
    DataSourceDomainReadSettings: {
        value: {}
    },
    AdminDataSourceSettings: {
        value: {}
    },
    ResourceCommonDomainReadSettings: {
        value: {}
    },
    DataSourceDomainCreationSettings: {
        DataSourceSettings: {

        }
    }

}


export default {
    add,
    edit,
    editResource
}
