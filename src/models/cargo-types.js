import { required } from "vuelidate/lib/validators";

const add = {
  uuid: {
    value: null
  },
  name: {
    value: '',
    validator: {
      required
    }
  },
  deleted_at: {
    value: null
  },
  conversion_scheme: {
    exclude: true,
    type: 'array',
    value: [
      {
        amount: '',
        forwarding_unit: {
          uuid: null,
          name: ''
        }
      },
      {
        amount: '',
        forwarding_unit: {
          uuid: null,
          name: ''
        }
      },
    ]
  },
  key_words: {
    type: 'array',
    value: []
  }
}

export default {
  add
}
