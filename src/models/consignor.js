import { required } from 'vuelidate/lib/validators';

const add = {
  cargoContractorTitle: {
    value: '',
    validator: { required }
  },
  cargoContractorType: {
    value: '',
    validator: { required }
  },
  clientUuid: {
    value: '',
    validator: { required }
  },
  pointAddress: {
    value: '',
    validator: { required }
  },
  tsdAddress: {
    value: '',
    validator: { required }
  },
  pointCode: {
    value: ''
  },
  pointCoordinate: {
    value: {
      lat: null,
      lon: null
    },
    validator: { required }
  }
};

export default { add };