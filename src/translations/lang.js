import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

let defaultTranslation = '';

// Ассинхронная загрузка файла локализации
const loadLanguageAsync = langParams => {
    try {
      const _lang = langParams.lang;
      const _page = langParams.page;
      const _setI18n = (msgs) => {
        const _allPageMsg = Object.assign(msgs, defaultTranslation);
        i18n.setLocaleMessage(_lang, _allPageMsg);
      };

      const loadLanguageDefaultAsync = () => {
          return new Promise(resolve => {
              if (i18n.locale !== _lang || !defaultTranslation) {
                  return import(`./locale/${_lang}/default`).then(msg => {
                      defaultTranslation = msg.default;
                      resolve()
                  }).catch(() => {
                      resolve()
                  })
              }
          })
      }

      return import(/* webpackChunkName: "lang-[request]" */ `./locale/${_lang}/${_page}`)
              .then(pageMsgs => {
                const _pageMsg = pageMsgs.default;
                loadLanguageDefaultAsync().then(() => {
                    _setI18n(_pageMsg);
                    return Promise.resolve(setI18nLanguage(_lang));
                })
                _setI18n(_pageMsg);
                return Promise.resolve(setI18nLanguage(_lang));
              })
              .catch(() => {
                loadLanguageDefaultAsync()
                return Promise.resolve()
              })
    } catch (e) {
      return Promise.resolve()
    }
};

VueI18n.prototype.loadLanguageAsync = loadLanguageAsync;

const i18n = new VueI18n({
    locale: 'en', // set locale
    fallbackLocale: 'en',
    messages: {},
    silentTranslationWarn: true
});

// Проверка текущей локализации
function setI18nLanguage (lang) {
    i18n.locale = lang ;
    document.querySelector('html').setAttribute('lang', lang);
    return lang
}

export default i18n;
