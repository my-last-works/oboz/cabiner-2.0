export const dictionary = {
  "title": "Коды ТН ВЭД",
  "modal": {
    "title": "Информация о товаре",
    "import": {
      "title": "Импорт",
      "code": "Код ТН ВЭД",
      "denomination": "Наименование",
      "import_duty": "Импортная пошлина",
      "temporary_import_duty": "Временная импортная пошлина",
      "excise_tax": "Акциз",
      "vat": "НДС"
    },
    "export": {
      "title": "Экспорт",
      "code": "Код ТН ВЭД",
      "denomination": "Наименование",
      "export_duty": "Экспортная пошлина",
      "licenses": "Лицензирование на экспорт "
    }
  }
}
