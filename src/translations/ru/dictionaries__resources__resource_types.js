export const dictionary = {
  "title": "Типы ресурсов",
  "columns": {
    "resource_species": "Тип ресурса",
    "resource_pattern": "Наименование типа ресурса",
    "status": "Статус"
  },
  "assign_title": "Назначить маску",
  "modal": {
    "title": "Назначение маски",
    "resource_species": "Вид ресурса",
    "resource_sub_type": "Подтип",
    "resource_mask": "Маска",
    "resource_mask_placeholder": "Выберите маску",
    "resource_pattern": "Шаблоны характеристик",
    "resource_pattern_placeholder": "Выберите шаблоны характеристик"
  }
}
