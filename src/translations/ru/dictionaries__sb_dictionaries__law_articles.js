export const dictionary = {
  "title": "Статьи ГК и УК",
  "columns": {
    "name": "Наименование статьи",
    "codeType": "Кодекс",
    "country": "Страна",
    "number": "№ статьи",
    "status": "Статус",
    "date": "Дата"
  },
  "modal": {
    "title": "Статья",
    "title_label": "Наименование статьи",
    "code_type": "Кодекс",
    "status": "Статус",
    "country_code": "Страна",
    "article_number": "Номер статьи"
  }
}
