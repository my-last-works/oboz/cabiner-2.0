export const dictionary = {
  "title": "Таможенные союзы",
  "add_button_title": "Добавить",
  "columns": {
    "name": "Наименование",
    "international_name": "Международное наименование",
    "abbr_name": "Сокр. наименование",
    "member_countries": "Страны-участницы",
    "status": "Статус"
  },
  "modal": {
    "title": "Таможенный союз",
    "name": "Наименование",
    "placeholder_name": "Введите наименование",
    "international_name": "Международное наименование",
    "placeholder_international_name": "Введите наименование",
    "abbr_name": "Сокр. наименование",
    "placeholder_abbr_name": "Введите наименование",
    "member_countries": "Страны-участницы",
    "placeholder_member_countries": "Выберите страны"
  }
}
