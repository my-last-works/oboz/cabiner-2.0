export const dictionary = {
  "title": "Страны",
  "columns": {
    "short_title": "Краткое наименование",
    "title": "Полное наименование",
    "regions": "Регионы",
    "number_code": "Код страны/iso",
    "symbol_code": "Aipha 3",
    "vat": "НДС, %",
    "nsp": "НсП, %",
    "status": "Статус"
  },
  "modal": {
    "title": "Страна",
    "short_title": "Краткое наименование",
    "title_eng": "Наименование на английском",
    "full_title": "Полное наименование",
    "regions": "Регионы",
    "number_code": "Код страны/iso",
    "symbol_code": "Код alpha 3",
    "vat": "НДС, %",
    "nsp": "НсП, %"
  }
}
