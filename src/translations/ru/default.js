export const dictionaryDefault = {
  "recordsNumber": "Количество записей: ",
  "notData": "Нет данных",
  "addButtonTitle": "Добавить",
  "status": {
    "active": "Активен",
    "inactive": "Неактивен"
  },
  "modalButton": {
    "close": "Закрыть",
    "cancel": "Отмена",
    "save": "Сохранить"
  },
  "temperatureMode": {
    "cooling": "Охлаждение"
  },
  "mainMenu": {
    "orders": "Заказы",
    "seal": "Пломба",
    "monitoring": "Мониторинг",
    "analytics": "Аналитика",
    "payments": "Оплаты",
    "contract": "Договор",
    "directories": "Справочники",
    "settings": "Настройки",
    "dealers": "Дилеры",
    "customers": "Клиенты",
    "tariffGroup": "Тарифные группы",
    "tariffs": "Тарифы",
    "producers": "Производители",
    "workType": "Виды работ",
    "reasons": "Причины",
    "resorces": "Ресурсы",
    "administration": "АДМИНИСТРИРОВАНИЕ",
    "orgCard": "Конфигуратор карточки участника",
    "SB": "СБ",
    "requisites": "Группы реквизитов",
    "services": "Сервисы",
    "docs": "Типы регистрационных документов",
    "verification": "Сроки действия проверки",
    "articles": "Статьи ГК и УК",
    "countries": "Страны"
  },
  "records_number": "Количество записей: ",
  "not_data": "Нет данных",
  "add_button_title": "Добавить",
  "modal_button": {
    "close": "Закрыть",
    "cancel": "Отмена",
    "save": "Сохранить"
  },
  "dropdown_menu": {
    "delete": "Удалить"
  },
  "TEMPERATURE_MODES": {
    "cooling": "Охлаждение",
    "freezing": "Заморозка",
    "room_temperature": "Комнатная температура",
    "without_special_mode": "Не нужен специальный режим"
  },
  "CATEGORY_CARGO_TYPES": {
    "no": "Нет",
    "dangerous": "Опасный груз"
  },
  "DOCUMENT_CLASS": {
    "logistic_document": "Логистический документ",
    "financial_document": "Закрывающий финансовый документ",
    "invoice": "Счет"
  },
  "STATUS_MODEL": {
    "EXTENDED": "Расширенная",
    "REDUCED": "Сокращенная"
  },
  "SERVICE_GEOGRAPHY": {
    "FROM_TO": "Откуда-Куда",
    "WHERE": "Где",
    "WITHOUT_GEOGRAPHY_BINDING": "Без привязки к географии"
  },
  "CREATED_BY": {
    "client": "Клиент",
    "carrier": "Поставщик",
    "expeditor": "Экспедитор"
  },
  "RADIUS_LABEL": {
    "zero_interrogation_radius": "Нулевой допробег",
    "hard_connect_radius": "Радиус жесткой привязки",
    "vehicle_arrival_radius": "Зачет прибытия ТС",
    "vehicle_departure_radius": "Зачет убытия ТС",
    "spot_listing": "Попадание в спотовый список",
    "futures": "Фьючерсирование"
  },
  "CODEX_NAME": {
    "criminal": {
      "short": "УК",
      "full": "Уголовный кодекс"
    },
    "civil": {
      "short": "ГК",
      "full": "Гражданский кодекс"
    }
  },
  "COUNTRY_CODE": {
    "rus": {
      "short": "РФ",
      "full": "Россия"
    }
  },
  "MODAL": {
    "titleSecond": {
      "add": "Добавление",
      "view": "Просмотр"
    }
  },
  "FORM_PLACEHOLDERS": {
    "select": "Выберите из списка",
    "selectOnMap": "Введите или укажите на карте"
  }
}
