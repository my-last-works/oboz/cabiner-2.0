export const dictionary = {
  "title": "Температурные режимы транспортировки",
  "columns": {
    "denomination": "Наименование",
    "mode_type": "Вид режима",
    "temperature_recorders": "Измерительное устройство",
    "critical_deviation_low_limit": "Штатный режим",
    "temperature_high_limit": "Критический режим",
    "single_max_time_out_of_borders": "Max Доп. разовое отклонение",
    "sum_max_time_out_of_borders": "Max Доп. общее отклонение за рейс",
    "description": "Описание",
    "status": "Статус"
  },
  "modal": {
    "add_title": "Новый температурный режим",
    "view_title": "Температурный режим",
    "denomination": "Наименование",
    "denomination_placeholder": "Введите наименование",
    "mode_type": "Вид режима",
    "normal_mode": "Штатный режим:",
    "critical_mode": "Критический режим:",
    "min_temperature": "Минимальная температура",
    "max_temperature": "Максимальная температура",
    "description": "Описание",
    "description_placeholder": "Введите описание",
    "single_max_time_out_of_borders": "Max продолжительность разового отклонения, минут",
    "sum_max_time_out_of_borders": "Max продолжит. суммарн. отклонения за рейс, часов",
    "temperature_recorders": "Измерительное устройство",
    "temperature_recorders_placeholder": "Выберите измерительное устройство"
  }
}
