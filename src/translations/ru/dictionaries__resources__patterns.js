export const dictionary = {
    "recordsNumber": "Количество записей: ",
    "title": "Шаблоны характеристик ресурсов",
    "columns": {
      "mask": "Шаблон",
      "status": "Статус"
    },
    "modal": {
      "title": "Шаблон характеристик ресурса",
      "mask": "Маска",
      "mask_placeholder": "Выберите маску",
      "parameter_0": "Первый параметр",
      "parameter_1": "Второй параметр",
      "parameter_2": "Третий параметр"
    }
  }
