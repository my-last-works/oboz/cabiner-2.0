export const dictionary = {
  "recordsNumber": "Количество записей: ",
  "title": "Температурные регистраторы",
  "columns": {
    "denomination": "Наименование",
    "certificate_number": "№ сертификата",
    "tracker_denomination": "Является трекером",
    "no_tracker": "нет",
    "description": "Описание",
    "status": "Статус"
  },
  "modal": {
    "add_title": "Добавление температурного регистратора",
    "view_title": "Температурный регистратор",
    "denomination": "Наименование",
    "denomination_placeholder": "Введите наименование",
    "certificate_number": "№ сертификата",
    "certificate_number_placeholder": "Введите номер",
    "tracker_denomination": "Является трекером",
    "tracker_denomination_placeholder": "Выберите",
    "description": "Описание",
    "description_placeholder": "Введите описание"
  }
}
