export const dictionary = {
  "title": "ВАЛЮТЫ",
  "select_currency": "Показать курс к",
  "select_currency_pretext": "за",
  "columns": {
    "name": "Наименование",
    "code": "Букв. код",
    "numeric_code": "Цифр. код",
    "unicode": "Unicode",
    "currency_rate": "Курс к ",
    "currency_rate_pretext": "на",
    "currency_rate_dynamics": "Динамика"
  },
  "currency_detail": {
    "period": {
      "title": "Период:",
      "week": "неделя",
      "month": "месяц",
      "quarter": "квартал",
      "year": "год"
    },
    "pretext": "к",
    "dynamics": {
      "columns": {
        "date": "дата",
        "course": "Курс",
        "dynamics": "динамика"
      },
      "now": "сегодня"
    }
  },
  "modal": {
    "title": "Валюта",
    "name": "Наименование",
    "code": "Букв. код",
    "numeric_code": "Цифр. код",
    "unicode": "Unicode",
    "issuing_country": "Страна эмитент",
    "destination_countries": "Страны хождения"
  }
}