export const dictionary = {
  "caption": "Доступы",
  "columns": {
    "title": "Доступ системы",
    "type": "Тип",
    "C": "C",
    "R": "R",
    "U": "U",
    "D": "D",
    "E": "E",
    "roleTitle": "Роли участников",
    "status": "Статус",
    "createdAt": "Дата создания",
    "lastReleaseDate": "Дата последнего релиза"
  },
  "statusValue": {
    "active": "Активна",
    "inactive": "Неактивна"
  },
  "modal": {
    "title": "Редактирование доступа",
    "name": "Наименование",
    "status": "Статус",
    "statusPlaceholder": "Выбрать",
    "type": "Тип",
    "roles": "Список ролей"
  },
  "crudOption": {
    "create": "Создание",
    "read": "Чтение",
    "update": "Обновление",
    "delete": "Удаление",
    "edit": "Редактирование"
  }
}
