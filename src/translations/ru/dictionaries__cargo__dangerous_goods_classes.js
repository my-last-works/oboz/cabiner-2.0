export const dictionary = {
  "title": "Классы опасных грузов",
  "columns": {
    "code": "UN код",
    "hazard_code": "Классификационный код",
    "adr_class": "Класс опасности",
    "name": "Наименование",
    "description": "Описание",
    "status": "Статус"
  },
  "modal": {
    "title": "Класс опасных грузов",
    "code": "UN код",
    "hazard_code": "Классификационный код",
    "name": "Наименование",
    "description": "Описание"
  },
  "loadingError": "Ошибка загрузки"
}