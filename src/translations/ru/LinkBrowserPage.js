export const dictionary = {
  "leftCaption": "Браузер линков",
  "rightCaption": "Информация о линке",
  "columns": {
    "name": "Наименование",
    "home": "В домене",
    "linked": "Есть линки",
    "canLink": "Возможны линки"
  },
  "columnsDetail": {
    "role": "Роль",
    "organization": "Данные участника",
    "status": "Статус линка участника",
    "contract": "Действующие договоры"
  },
  "linkStatus": {
    "currentUser": "Текущий пользователь",
    "pending": "Обработка запроса",
    "restriction": "Ограниченный доступ"
  },
  "linkType": {
    "link": "Линк",
    "linkLeft": "Публичный линк",
    "linkRight": "Публичный линк (Ваш)",
    "linkMutal": "Взаимный публичный линк"
  },
  "linkFormControl": {
    "visibility": "Видимость",
    "visibilityPlaceholder": "Виден Вам в домене",
    "publicLinkRight": "Публичный линк от Вас",
    "link": "Линк",
    "contracts": "Действующие договоры"
  },
  "formButton": {
    "removePublicLink": "Снять линк с публикации",
    "createPublicLink": "Создать публичный линк",
    "createLink": "Создать линк",
    "pending": "Запрос на публичный линк отправлен",
    "lock": "Настройки домена не позволяют вам создавать линки с данным участником",
    "process": "Система обрабатывает запрос на создание линка"
  },
  "labelPublicLinkName": {
    "user": "Публичный линк от Клиента",
    "supplier": "Публичный линк от Поставщика",
    "domain-owner": "Публичный линк от Владельца домена",
    "carrier": "Публичный линк от Перевозчика"
  },
  "linkTitle": {
    "link": "Связь с ",
    "user": "Клиентом",
    "supplier": "Поставщиком",
    "domain-owner": "Владельцем домена",
    "carrier": "Перевозчиком"
  },
  "linkActiveStatus": {
    "active": "Активен",
    "notActive": "Нет"
  },
  "rejectRequestTitle": {
    "pre": "Предыдущий запрос от",
    "post": "был отклонен"
  }
}
