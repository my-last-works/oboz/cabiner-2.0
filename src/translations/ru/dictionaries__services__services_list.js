export const dictionary = {
  "recordsNumber": "Количество записей: ",
  "columns": {
    "cardName": "Карточка",
    "fieldName": "Поле",
    "countryName": "Страна",
    "organizationForm": "Форма организации",
    "detectData": "Распознавание",
    "responseData": "Получение данных",
    "name": "Наименование",
    "status": "Статус",
    "date": "Дата"
  },

  "modal": {
    "service_geography": "География услуги",
    "service_geography_placeholder": "Выберите",
    "type": "Вид ресурса",
    "sub_type": "Подтип",
    "pattern": "Шаблон",
    "no_data": "Нет данных",
    "spec_title": "Нет"
  }
}
