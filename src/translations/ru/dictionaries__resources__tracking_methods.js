export const dictionary = {
  "title": "Методы трекинга",
  "columns": {
    "denomination": "Наименование",
    "code": "Код",
    "description": "Описание",
    "status": "Статус"
  },
  "modal": {
    "title": "Метод трекинга",
    "denomination": "Наименование",
    "code": "Код",
    "description": "Описание"
  }
}