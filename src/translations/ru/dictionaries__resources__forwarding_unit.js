export const dictionary = {
    "title": "Единицы экспедиторского учета",
    "columns": {
        "denomination": "Наименование",
        "shorthand": "Усл. обозначение",
        "international_shorthand": "Международ. усл. обозначение",
        "description": "Описание",
        "status": "Статус"
    },
    "modal": {
        "add_title": "Добавление единицы экспедиторского учета",
        "view_title": "Единица экспедиторского учета",
        "denomination": "Наименование",
        "denomination_placeholder": "Введите наименование",
        "shorthand": "Усл. обозначение",
        "shorthand_placeholder": "Введите обозначение",
        "international_shorthand": "Международ. усл. обозначение",
        "international_shorthand_placeholder": "Введите обозначение",
        "description": "Описание"
    }
}