export const dictionary = {
  "title": "Виды ресурсов",
  "columns": {
    "name": "Вид ресурса",
    "resource_class": "Класс ресурса",
    "resource_sub_types": "Подтипы",
    "status": "Статус"
  }
}