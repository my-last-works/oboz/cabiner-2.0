export const dictionary = {
  "title": "Группы реквизитов",
  "columns": {
    "name": "Наименование группы реквизитов",
    "entity": "Сущность",
    "number": "Порядковый номер",
    "status": "Статус",
    "date": "Дата"
  },
  "objectType": {
    "participant": "Пользователь",
    "resource": "Ресурс"
  },
  "status": {
    "active": "Активна",
    "inactive": "Неактивна"
  },
  "modal": {
    "title": "Группа реквизитов",
    "name": "Наименование группы реквизитов",
    "number": "Порядковый номер",
    "status": "Статус",
    "type": "Тип",
    "typePlaceholder": "Выберите тип",
  }
}