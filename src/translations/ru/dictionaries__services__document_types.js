export const dictionary = {
  "title": "Типы документов",
  "add_button_title": "Добавить",
  "columns" : {
    "name": "Тип документа",
    "document_class": "Класс",
    "status": "Статус"
  },
  "modal": {
    "add_title": "Добавление типа документа",
    "view_title": "Просмотр типа документа",
    "name": "Тип документа",
    "name_placeholder": "Введите название",
    "document_class": "Класс"
  }
}
