export const dictionary = {
  "title": "Коды ЕТСНГ",
  "columns": {
    "code": "Код ЕТСНГ",
    "denomination": "Наименование",
    "short_denomination": "Сокращ. наименование",
    "cargo_group": "Группа груза",
    "cargo_class": "Класс груза",
    "gng_code_names": "Код наименования по ГНГ",
    "status": "Статус"
  },
  "modal": {
    "title": "Элемент ЕТСНГ",
    "code": "Код ЕТСНГ",
    "denomination": "Наименование",
    "short_denomination": "Сокращенное наименование",
    "cargo_group": "Группа груза",
    "cargo_class": "Класс груза",
    "gng_code_names": "Код наименования по ГНГ"
  }
}