export const dictionary = {
  "title": "Типы грузов",
  "columns": {
    "name": "Тип груза",
    "conversion_scheme": "Коэффициенты пересчёта",
    "weight": "т",
    "volume": "м",
    "temperature_mode": "Доп.",
    "key_words": "Ключевые слова",
    "status": "Статус"
  },
  "modal": {
    "add_new": "Добавление типа груза",
    "view": "Просмотр типа груза",
    "basic_info": "Основная информация",
    "name": "Наименование типа груза",
    "name_placeholder": "Введите название",
    "amount": "Кол-во",
    "forwarding_unit": "Единица",
    "weight": "Вес, кг",
    "volume": "Объем, куб.метр",
    "temperature_mode": "Температурный режим",
    "category": "Опасный груз",
    "key_words": "Ключевые слова",
    "key_words_placeholder": "Введите ключевые слова"
  }
}
