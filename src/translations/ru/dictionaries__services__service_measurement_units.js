export const dictionary = {
  "title": "Единицы измерения услуг",
  "columns": {
    "name": "Наименование",
    "denomination": "Усл. обозначение",
    "code": "Код",
    "status": "Статус"
  },
  "modal": {
    "add_title": "Добавление единицы измерения",
    "view_title": "Просмотр единицы измерения",
    "name": "Наименование",
    "name_placeholder": "Укажите наименование",
    "denomination": "Условное обозначение",
    "denomination_placeholder": "Укажите краткое наименование",
    "code": "Код",
    "code_placeholder": "Укажите код"
  }
}
