export const dictionary = {
  "title": "Состав карточки ресурса",
  "columns": {
    "title": "Название поля",
    "class": "Класс",
    "type": "Вид",
    "subtype": "Подтип",
    "country": "Страна",
    "status": "Статус"
  },
  "status": {
    "active": "Активен",
    "inactive": "Неактивен",
    "system": "Системная"
  },
  "doc_types": {
    "block": "Блокировка",
    "closing_maintenance": "Закрытие ТО",
    "opening_maintenance": "Открытие ТО",
    "registration": "Регистрация",
    "rent": "Аренда",
    "return_from_rent": "Возврат",
    "to_warehouse_receipting": "Приемка",
    "to_warehouse_sending": "Перемещение",
    "unblock": "Разблокировка",
    "write_off": "Списание"
  },
  "editWindow": {
    "editWindowTitle": "Информация о причине",
    "editSystemWindowTitle": "Информация о системной причине",
    "newWindowTitle": "Добавление поля в карточку ресурса",
    "formControls": {
      "field_uuids": {
        "title": "Поле карточки участника",
        "placeholder": "Выберите из списка"
      },
      "doc_type": {
        "title": "Документ",
        "placeholder": "Выберите документ"
      },
      "status": {
        "title": "Статус",
        "placeholder": "Выберите статус"
      }
    }
  }
}
