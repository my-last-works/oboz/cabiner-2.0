import date from './date'
import currency from './currency'

import Vue from 'vue'

Vue.filter('date', date)
Vue.filter('currency', currency)