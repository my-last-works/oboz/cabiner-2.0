import moment from 'moment'
moment.locale('ru')

export default function(value, format = "DD.MM.YYYY") {
    if (!value) return '—'

    return moment(value).format(format)

}
