import { formatNumber } from 'common-components/src/helpers/common'

export default function(value, { dot = 0, separator = ',' } = {}) {
    return formatNumber(value, { dot, separator })
}
