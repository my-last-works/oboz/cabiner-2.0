const config = {
  "keycloakHost": window.OBOZ_CONFIG.OBOZ_API_HOST,
  "secret": window.OBOZ_CONFIG.CS,
  "sealAdminUrl": "/oboz2-front-admin-seal/",
  "sealClientUrl": "/oboz2-front-client-seal/",
  "sealDealerUrl": "/oboz2-front-dealer-seal/",
  "sealAdminPanelUrl": "/oboz2-front-admin-panel/",
  "userEntryUrl": "/",
};
export default config
