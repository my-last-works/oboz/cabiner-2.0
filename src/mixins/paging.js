import RestClient from '../api/RestClient';
import { formationParameters } from '@/helpers';
let Client = new RestClient();

export default {
  data() {
    let _self = this

    return {
      pagingOpt: {
        pageSize: 20
      },
      searchValue: '',
      remoteOperations: {
        paging: true,
        sorting: true,
        filtering: true
      },
      filters: {},
      totalCount: 0,
      currentData: [],
      dataSource: async ({ page, size, query, sort }) => {
        let filters =  _self.filters;

        let res = await Client.get(_self.api.source + formationParameters(page, size, query, sort, filters))
        if (!res || res.error) {
          return {
            data: _self.currentData
          }
        } else {
          return res.data
        }
      }
    }
  },
}
