import { formatNumber } from 'common-components/src/helpers/common.js'

export default {
    methods: {
        formatCurrency: (value)=>{
            return formatNumber(value)
        }
    }
}
