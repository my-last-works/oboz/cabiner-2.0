import { mapGetters } from "vuex"

export default {
  data () {
    return {
      userAccess: {}
    }
  },
  computed: {
    ...mapGetters({
      functionalRoles: 'functions/functionalRoles'
    })
  },
  watch: {
    functionalRoles: {
      immediate: true,
      handler (value) {
        if (value) {
          this.generateUserAccess(value)
        }
      }
    }
  },
  methods: {
    async generateUserAccess (functionalRoles) {
      let result = {}

      if (functionalRoles && Array.isArray(functionalRoles)) {
        for (let key in this.userAccessMap) {
          result[key] = this.userAccessMap[key].every((role) => {
            return Boolean(functionalRoles.find((userRole) => {
              return role === userRole.code
            }))
          })
        }
      }

      this.userAccess = result
    }
  }
}
