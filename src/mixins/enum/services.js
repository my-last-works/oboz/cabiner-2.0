import { dictionaryDefault } from './../../translations/ru/default';

export default {
    computed: {
        STATUS_MODEL () {
            return {
                EXTENDED: dictionaryDefault.STATUS_MODEL.EXTENDED,
                REDUCED: dictionaryDefault.STATUS_MODEL.REDUCED,
            }
        },
        STATUS_MODEL_OPTIONS () {
            return [
                {
                    value: "EXTENDED",
                    text: this.STATUS_MODEL.EXTENDED
                },
                {
                    value: "REDUCED",
                    text: this.STATUS_MODEL.REDUCED
                }
            ]
        },
        SERVICE_GEOGRAPHY () {
            return {
                FROM_TO: dictionaryDefault.SERVICE_GEOGRAPHY.FROM_TO,
                WHERE: dictionaryDefault.SERVICE_GEOGRAPHY.WHERE,
                WITHOUT_GEOGRAPHY_BINDING: dictionaryDefault.SERVICE_GEOGRAPHY.WITHOUT_GEOGRAPHY_BINDING
            }
        },
        SERVICE_GEOGRAPHY_OPTIONS () {
            return [
                {
                    value: "FROM_TO",
                    text: this.SERVICE_GEOGRAPHY.FROM_TO
                },
                {
                    value: "WHERE",
                    text: this.SERVICE_GEOGRAPHY.WHERE
                },
                {
                    value: "WITHOUT_GEOGRAPHY_BINDING",
                    text: this.SERVICE_GEOGRAPHY.WITHOUT_GEOGRAPHY_BINDING
                }
            ]
        },
        CREATED_BY () {
            return [
                {
                    value: "CLIENT",
                    text: dictionaryDefault.CREATED_BY.client
                }, {
                    value: "CARRIER",
                    text: dictionaryDefault.CREATED_BY.carrier
                }, {
                    value: "EXPEDITOR",
                    text: dictionaryDefault.CREATED_BY.expeditor
                }];
        }
    }
}
