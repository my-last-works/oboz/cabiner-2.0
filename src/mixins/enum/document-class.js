import { dictionaryDefault } from './../../translations/ru/default';

export default {
    computed: {
        DOCUMENT_CLASS () {
            return [{
                    text: dictionaryDefault.DOCUMENT_CLASS.logistic_document,
                    value: 'LOGISTIC'
                }, {
                    text: dictionaryDefault.DOCUMENT_CLASS.financial_document,
                    value: 'FINANCIAL_CLOSING'
                }, {
                    text: dictionaryDefault.DOCUMENT_CLASS.invoice,
                    value: 'BILL'
                }
            ]
        }
    }
}
