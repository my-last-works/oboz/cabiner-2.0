export default {
    computed: {
        CATEGORY_CARGO_TYPES () {
            return [
                {
                    value: "Нет",
                    // text: this.$t('CATEGORY_CARGO_TYPES.no')
                    text: 'Нет'
                }, {
                    value: "Опасный груз",
                    // text: this.$t('CATEGORY_CARGO_TYPES.dangerous')
                    text: 'Опасный груз'
                }
            ]
        }
    }
}