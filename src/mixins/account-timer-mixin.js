import moment from "moment";

export default {
  data() {
    return {
      countDown : '',
      warning: false,
      overTime: false
    }
  },
  methods: {
    countDownTimer (date) {
      const currentTime = moment().unix();
      const overTime = moment(date).unix();
      if (overTime > currentTime) {
        let diffTime = overTime - currentTime;
        let duration = moment.duration(diffTime * 1000, 'milliseconds');
        duration = moment.duration(duration - 1000, 'milliseconds');
        if (duration.days() > 0) {
          this.countDown = `${duration.days()} д ${duration.hours()} ч`
        } else if (duration.hours() >= 2) {
          this.countDown = `${duration.hours()} ч`
        } else if (duration.hours() === 1) {
          this.countDown = `${duration.hours()} ч ${duration.minutes()} мин`
        } else if (duration.hours() === 0 && duration.minutes() > 0) {
          if (duration.minutes() <= 5) {
            this.overTime = true
          }
          this.countDown = `${duration.minutes()} мин`
        } else if (duration.minutes() === 0) {
          this.countDown = `${duration.seconds()} сек`;
          this.warning = true
        }
      } else {
        this.countDown = 'Время вышло';
        this.warning = false;
        this.overTime = false;
      }
    },
    transformDateAndTime(date) {
      return moment(date).format('HH:mm DD.MM.YYYY')
    }
  }
}