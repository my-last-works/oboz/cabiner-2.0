import cloneDeep from 'clone-deep';
import moment from 'moment';

let tableLayoutMixin = {
    data() {
        return {
            selectedRowKeys: [],
            // translate: this.$t.bind(this),
            optionsSettings: {
                displayExpr: 'title',
                valueExpr: 'value'
            }
        }
    },
    methods: {
        // Опции фильтров для даты
        findDateOptionsFilter(field){
            let _optionsArray = [];
            this.formDataArray.forEach((item) => {
                if(item[field] !== null && item[field] !== undefined && item[field] !== '') {
                    let _status = false;
                    _optionsArray.forEach((option) =>{
                        if(option.value === item[field]) {
                            _status = true;
                        }
                    });
                    if(!_status) {
                        _optionsArray.push({
                            value: item[field],
                            title: moment(new Date(item[field])).format('DD.MM.YYYY')
                        });
                    }
                }
            });
            return _optionsArray;
        },
        // Опции фильтров
        findOptionsFilter(field){
            if (!this.formDataArray) return []
            let _optionsArray = [];
            this.formDataArray.forEach((item) => {
                if(item[field] !== null && item[field] !== undefined && item[field] !== '') {
                    if(_optionsArray.indexOf(item[field]) === -1) {
                        _optionsArray.push(item[field]);
                    }
                }
            });
            return _optionsArray;
        },
        getOptions(fields) {
            let _options = [];
            for (let field in fields) {
                _options.push({
                    value: field,
                    title: fields[field]
                });
            }
            return _options;
        },
        // Выбор строки сетки
        onSelection(item) {
            if(item[this.propertyId] === null || item[this.propertyId] === undefined) {
                this.formData = cloneDeep(this.newData);
            }
            else {
                this.formData = cloneDeep(item);
            }
            this.selectedRowKeys = [item[this.propertyId]];
            if(!this.openModalStatus) this.openModal(item[this.propertyId], this.editTitle);
        },
        addNewItem() {
            this.selectedRowKeys = [];
            this.formData = this.newData;
            this.openModal(null, this.newTitle);
        },
        // TODO
        add() {
            this.formData[this.propertyId] = 6;
            this.formDataArray.push(this.formData);
            this.close();
        },
        // TODO
        save() {
            this.formDataArray.forEach((formData) => {
                if(formData[this.propertyId] === this.formData[this.propertyId]) {
                    for (var key in this.formData) {
                        formData[key] = this.formData[key];
                    }
                }
            });
            this.close();
        }
    }
};

export default tableLayoutMixin;
