import { declOfNum } from '@/helpers';
import api from '@/api';

export default {
    computed: {
        totalElementsTitle() {
            return declOfNum(this.dataSource.length, this.titles)
        }
    },
    mounted() {
        this.onLoad();
    },
    data() {
        return {
            dataSource: [],
        }
    },
    methods: {
        onLoad(){
            api[this.api.source][this.api.method]().then(({ data }) => {
                this.dataSource = data[this.entity];
            });
        }
    }
}
