import router from '../router'

export default {
  data () {
    return {
      fromRoute: null,
      toRoute: null,
      showLeftPanel: false
    }
  },

  async beforeRouteEnter (to, from, next) {
    let allowed = true
    if (to.meta && to.meta.checkRouteParams) {
      allowed = to.meta.checkRouteParams(to)
    }

    if (allowed) {
      next(vm => {
        vm.fromRoute = from
      })
    } else {
      router.replace({ name: 'entry' })
    }
  },

  provide () {
    return {
      rootPageComponent: this
    }
  },

  mounted () {
    this.$nextTick(() => {
      this.showLeftPanel = true
    })
  },

  beforeRouteLeave (to, from, next) {
    this.toRoute = to
    this.$nextTick(() => {
      this.showLeftPanel = false
      setTimeout(() => {
        next()
      }, to.name === 'home' ? 0 : 200)
    })
  },

  computed: {
    isToHome () {
      return this.toRoute && this.toRoute.name === 'home'
    },
    pathSite() {
      return window.location.host.split('gruzi').length > 1 ? 'gruzi': 'oboz'
    },
    isGruziProduction() {
      return window.location.host.split('go.gruzi.ru').length > 1
    }
  },

  methods: {
    handleBack() {
      if (this.isGruziProduction) {
        window.location.href = "https://gruzi.ru/";
      } else {
        this.$router.push({ name: this.fromRoute?.name === "login" ? "login" : "entry" || "entry" });
      }
    }
  }
}
