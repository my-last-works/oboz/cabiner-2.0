import { getRiskIcon, getRiskClass, sortRisks, getNumberRisk } from '@/helpers/risks'

export default {
    methods: {
      getRiskIcon,
      getRiskClass,
      sortRisks,
      getNumberRisk
    }
  }
