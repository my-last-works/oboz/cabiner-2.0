import api from '@/api'
import {uuid} from "vue-uuid"
import { generateFormData } from "common-components/src/helpers/common"
import {mapGetters} from 'vuex'

export default {
    data() {
        return {
            newStatus: false,
            selectedData: {}
        }
    },
    computed: {
        ...mapGetters({
            isCreate: 'functions/create'
        }),
        existSelected () {
            return this.selectedData && typeof this.selectedData === 'object' && Object.keys(this.selectedData).length
        }
    },
    methods: {
        async onSelection (data) {
            try {
                this.newStatus = false
                let _data = await api[this.api.source][this.api.viewItem](data.uuid)
                this.selectedData = _data.data
                this.$refs.viewItem.open()
            } catch (e) {
                console.error(e);
                this.$notification({
                    type: 'system_notification_error',
                    message: 'Ошибка загрузки данных'
                })
            }
        },
        onAdd () {
            this.newStatus = true
            this.selectedData = this.name === 'Services' ? this.newData : generateFormData({ model: this.formModel })
            this.$refs.viewItem.open()
        },
        async onSave ({ formData = {}, model = {} } = {}) {
            let notificationId = new Date().getTime()+'save';
            this.$notification({type: 'system_notification_in_load', message: 'Сохранение', id: notificationId});
            try {
                let data = generateFormData({data: formData, model, assign: true, toService: true})

                if (formData.uuid) {
                    await api[this.api.source][this.api.editItem](formData.uuid, data)
                } else {
                    data.uuid = uuid.v4()
                    await api[this.api.source][this.api.createItem](data)
                }
                this.$refs.viewItem.close()
                if (this.$refs.dataGrid) {
                    this.$refs.dataGrid.$refs[this.dataGridRefName].instance.refresh(true)
                }
                if (this.$refs.obTable) {
                    this.$refs.obTable.loadData()
                }
                this.$notification({
                    type: 'system_notification_success',
                    message: 'Сохранено'
                })
            } catch (e) {
                console.error(e);
                this.$notification({
                    type: 'system_notification_error',
                    message: 'Не сохранено'
                })
            } finally {
                this.$notification({event: 'close', id: notificationId})
            }

        },
        async onDelete () {
            let notificationId = new Date().getTime()+'remove';
            this.$notification({type: 'system_notification_in_load', message: 'Удаление', id: notificationId});            await api[this.api.source][this.api.deleteItem](this.selectedData.uuid)
            try {
                await api[this.api.source][this.api.deleteItem](this.selectedData.uuid)
                if (this.$refs.dataGrid) {
                    this.$refs.dataGrid.$refs[this.dataGridRefName].instance.refresh()
                }
                if (this.$refs.obTable) {
                    this.$refs.obTable.loadData()
                }
                this.$refs.viewItem.close()
                this.$notification({
                    type: 'system_notification_success',
                    message: 'Удалено'
                })
            } catch (e) {
                this.$notification({
                    type: 'system_notification_error',
                    message: 'Не удалено'
                })
            } finally {
                this.$notification({event: 'close', id: notificationId})
            }
        },
        async onHistory () {
            this.$refs.historyItem.open()
        }
    }
}
