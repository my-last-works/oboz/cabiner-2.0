import {uuid} from "vue-uuid"
import {generateFormData} from "common-components/src/helpers/common"
import RestClient from '../../api/RestClient';

let Client = new RestClient({ isErrorThrow: true });
import {mapGetters} from 'vuex'

export default {
  data() {
    return {
      newStatus: false,
      selectedData: {}
    }
  },
  computed: {
    ...mapGetters({
      isCreate: 'functions/create'
    }),
    existSelected() {
      return this.selectedData && typeof this.selectedData === 'object' && Object.keys(this.selectedData).length
    }
  },
  methods: {
    async onSelection(data) {
      if (data && data.location_radius && !data.location_radius.hasOwnProperty('hard_binding_radius')) {
        data.location_radius.hard_binding_radius = 0
      }
      try {
        this.newStatus = false;
        let _data = await this.eiGet(data);
        if (_data && _data.location_radius && !_data.data.location_radius.hasOwnProperty('hard_binding_radius')) {
          _data.data.location_radius.hard_binding_radius = 0
        }
        this.selectedData = _data.data;
        this.$refs.viewItem.show();
        if (this.$refs['view-service']) {
          this.$refs['view-service'].hasAlert = false;
        }
      } catch (e) {
        console.error(e);
        this.$notification({
          type: 'system_notification_error',
          message: 'Ошибка загрузки данных'
        })
      }

    },
    onAdd() {
      this.newStatus = true;
      this.selectedData = this.name === 'Services' ? this.newData : generateFormData({model: this.formModel});
      this.$refs.viewItem.show();
      if (this.$refs['view-service']) {
        this.$refs['view-service'].hasAlert = false;
      }
    },
    async onSave({formData = {}, model = {}} = {}) {
      if (this.$refs['view-service']) {
        this.$refs['view-service'].hasAlert = false;
      }
      let notificationId = new Date().getTime() + 'save';
      try {
        this.$notification({type: 'system_notification_in_load', message: 'Сохранение', id: notificationId});
        let data = generateFormData({data: formData, model, assign: true, toService: true});
        if (formData.uuid) {
          if (formData.location_radius) {
            data.location_radius.hard_binding_radius = formData.location_radius.hard_binding_radius;
          }
          const editSource = typeof this.api.getEditSource === 'function' ? this.api.getEditSource(formData.uuid) : this.api.source + formData.uuid;
          await Client.put(editSource, data).then(result => this.processingSave(result.hasOwnProperty('data')));
        } else {
          data.uuid = uuid.v4();
          await Client.post(this.api.source, data).then(result => this.processingSave(result.hasOwnProperty('data')));
        }
        if (typeof this.processingResult === 'function') {
          this.processingResult(null);
        }

      } catch (e) {
        if (typeof this.processingResult === 'function') {
          this.processingResult(e);
        }
        console.error(e);
        this.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        })
      } finally {
        this.$notification({event: 'close', id: notificationId})
      }
    },
    processingSave(result) {
      if (this.$refs['view-service']) {
        this.$refs['view-service'].hasAlert = false;
      }
      if (result) {
        this.$notification({
          type: 'system_notification_success',
          message: 'Сохранено'
        });

        this.$refs.viewItem.hide();
        if (this.$refs[this.dataGridRefName]) {
          this.$refs[this.dataGridRefName].loadData()
        }
        if (this.$refs.obTable) {
          this.$refs.obTable.loadData()
        }
      } else {
        this.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        });
        if (this.$refs['view-service']) {
          this.$refs['view-service'].hasAlert = true;
        }
      }
    },
    async onDelete() {
      let notificationId = new Date().getTime() + 'remove';
      try {
        this.$notification({type: 'system_notification_in_load', message: 'Удаление', id: notificationId});
        await Client.delete(this.api.source + this.selectedData.uuid);
        if (this.$refs.dataGrid) {
          this.$refs[this.dataGridRefName].loadData()
        }
        if (this.$refs.obTable) {
          this.$refs.obTable.loadData()
        }
        this.$refs.viewItem.hide();
        this.$notification({
          type: 'system_notification_success',
          message: 'Удалено'
        })
      } catch (e) {
        this.$notification({
          type: 'system_notification_error',
          message: 'Не удалено'
        })
      } finally {
        this.$notification({event: 'close', id: notificationId})
      }

    },
    async onHistory() {
      this.$refs.historyItem.open()
    },
    async onInactive() {
      let notificationId = new Date().getTime() + 'save';
      try {
        this.$notification({type: 'system_notification_in_load', message: 'Сохранение', id: notificationId});
        let response = await Client.put(this.api.source + 'change_status/' + this.selectedData.uuid, {isActive: false});
        this.$notification({
          type: 'system_notification_success',
          message: 'Сохранено'
        });
        return response
      } catch (e) {
        console.error(e);
        this.$notification({
          type: 'system_notification_error',
          message: 'Не сохранено'
        })
      } finally {
        this.$notification({event: 'close', id: notificationId})
      }
    },
    async eiGet(data) {
      try {
        return await Client.get(this.api.source + data.uuid)
      } catch (e) {
        console.error(e);
        this.$notification({
          type: 'system_notification_error',
          message: 'Ошибка загрузки данных'
        })
      }
    },
    hideViewItemModal() {
      this.$refs.viewItem.hide();
    }
  }
}
