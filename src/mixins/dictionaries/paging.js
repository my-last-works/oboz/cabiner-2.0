import RestClient from '../../api/RestClient'
import { formationParameters } from '@/helpers'
let Client = new RestClient()

export default {
  data() {
    let _self = this

    return {
      searchValue: '',
      filters: {},
      totalCount: 0,
      currentData: [],
      dataSource: async ({ page, size, query, sort }) => {
        let filters =  _self.filters;

        let res = await Client.get(_self.api.source + formationParameters(page, size, query, sort, filters))
        if (!res || res.error) {
          return {
            data: _self.currentData
          }
        } else {
          return res.data
        }
      }
    }
  },
  methods: {
    onRefresh() {
      if (this.$refs.dataGrid) {
        this.$refs[this.dataGridRefName].loadData()
      }
      if (this.$refs.obTable) {
        this.$refs.obTable.loadData()
      }
    },
    async getObTableData({ page, size, query, sort, filters }) {
      let _sort = [];
      if (sort) {
        let params = sort.split(',');
        _sort.push({selector: params[0], desc: (params[1] == 'desc') })
      }

      let res = await Client.get(this.api.source + formationParameters(page, size, query, _sort, filters))
      if (res && res.data) {
        return res.data
      } else {
        this.$notification({type: 'system_notification_error', message: 'Ошибка загрузки данных' })
        return res
      }
    },
  }
}
