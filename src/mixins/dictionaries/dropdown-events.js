import RestClient from '@/api/RestClient'

const Client = new RestClient()

export default {

  props: {
    api: {
      type: Object,
      default: () => {}
    }
  },
  computed: {
    dropdownMenu() {
      const data = this.selectedData || this.dataSource;
      return [
        {
          title: data.is_active
            ? 'Перевести в статус «Неактивно»'
            : 'Перевести в статус «Активно»',
          className: 'color-error',
          method: async () => {
            await Client.put(this.api.source + 'change_status/' + data.uuid, { isActive: !data.is_active }).then(response => {
              if (!response || response.error) {
                this.$notification({ type: 'system_notification_error', message: 'Ошибка смены статуса' })
              } else {
                this.$notification({ type: 'system_notification_success', message: 'Статус успешно изменен' });
                data.is_active = !data.is_active;
                this.$emit('refresh')
              }
            }).catch(e => {
              console.log(e)
              this.$notification({ type: 'system_notification_error', message: 'Ошибка смены статуса' })
            })

          }
        },
        {
          title: 'Удалить',
          className: 'color-error',
          method: () => {
            this.$emit('delete')
          }
        }
      ]
    },
  }
}
