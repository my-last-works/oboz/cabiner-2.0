import api from '@/api';
import moment from "moment";
import {mapActions, mapGetters, mapMutations} from "vuex";

export const statusTitleMap = {
  NEW: 'Новый',
  ON_DOER_SEARCH: 'Поиск',
  BOOKED: 'Букирован',
  ALLOCATED: 'Аллокирован',
  CONTRACTED: 'Контрактован',
  IN_PROCESS: 'На маршруте',
  PROCESSED: 'Исполнен',
  EXECUTION_CONFIRMED: 'Исполнение подтверждено',
  INVOICING: 'Инвойсирование',
  INVOICED: 'Инвойсирован',
  FAILED: 'Сорван',
  CANCELLED: 'Отменён',
  CANCELED: 'Отменён',
  PAY_PROCESS: 'В процессе оплаты',
  PAID: 'Оплачен'
};

const iconMap = {
  administrator: 'administrator',
  moderator: 'system-moderator',
  expeditor: 'domain-owner',
  client: 'client',
  carrier: 'carrier',
  agent: 'agent',
  anonymous: 'profile',
  clicar: 'domain-owner'
};

export default {
  data () {
    return {
      params: {
        transportationWay: null
      }
    }
  },
  mounted () {
    if (this.$route.query.hasOwnProperty('tab') && this.filterButtons?.length) {
      Object.keys(this.filterButtons).forEach((key) => {
        this.filterButtons[key]._isActive = false;
        if (this.filterButtons[key].uuid === this.$route.query.tab) {
          this.filterButtons[key]._isActive = true;
        }
      });
    }
    if (!this.params.transportationWay) {
      this.params.transportationWay = this.$route.query.hasOwnProperty('transport') ? this.$route.query.transport : 'FTL'
    }
  },
  computed: {
    ...mapGetters({
      isClicar: 'user/isClicar',
      order: 'order/order',
    }),
    isOrderAll () {
      return this.$route.name === 'orders__all__orders'
    }
  },
  watch:{
    '$route.query.transport'(to) {
      this.params.transportationWay = to;
    }
  },
  methods: {
    ...mapActions({
      setUpdateLoader: 'order/setUpdateLoader'
    }),
    ...mapMutations({
      setStatusFilter: 'order/SET_STATUS_FILTER'
    }),
    transformText(text) {
      if (text) {
        if (text.length > 10) {
          text = text.substr(text.length - 10);
          return '...' + text;
        } else {
          return text;
        }
      } else {
        return '-';
      }
    },
    onChangeTabHandler({uuid}) {
      this.$router.push({
        path: this.$route.name,
        query: {
          role: uuid
        }
      })
    },
    onChangeFilterButton({uuid}) {
      if (uuid) {
        this.$router.push({
          path: this.$route.name,
          query: {
            role: this.$route.query.role,
            ...this.$route.query.hasOwnProperty('transport') ? {
              transport: this.$route.query.transport
            } : {},
            tab: uuid,
            ...this.$route.query.hasOwnProperty('from') ? {
              from: this.$route.query.from
            } : {},
            ...this.$route.query.hasOwnProperty('to') ? {
              to: this.$route.query.to
            } : {},
            ...this.$route.query.hasOwnProperty('date') ? {
              date: this.$route.query.date
            } : {},
            ...this.$route.query.hasOwnProperty('services') ? {
              services: this.$route.query.services
            } : {},
            ...this.$route.query.hasOwnProperty('resource') ? {
              resource: this.$route.query.resource
            } : {},
            ...this.$route.query.hasOwnProperty('status') ? {
              status: this.$route.query.status
            } : {},
            ...this.$route.query.hasOwnProperty('logist') ? {
              logist: this.$route.query.logist
            } : {},
            ...this.$route.query.hasOwnProperty('author') ? {
              author: this.$route.query.author
            } : {}
          }
        })
      }
    },
    async getOrders(
      {
        page,
        size,
        query,
        status,
        sort,
        url = this.filterButtons.find(key => key._isActive)?.uuid,
        params = this.$route.query
      }) {
      this.clearAdditionalItemsFilter();
      try {
        if (this.isOrderAll) {
          url = 'ALL'
        }
        let orderData = await api.orderProcess.getOrdersList({ page, size, query, status, sort, url }, params)
        const listKeys = ['serviceOptions', 'resourceTypeOptions', 'statusOptions', 'logistOptions', 'authorOptions']

        listKeys.forEach(key => {
          this.setStatusFilter({
            title: key,
            params: Object.keys(orderData[key]).length ? this.transformArray(orderData[key]) : []
          })
        })

        orderData.orders.content = orderData.orders.content.map((item) => {
          return {
            ...item,
            tariffRur: item.tariffWithVat || item.tariffRur,
            arriveOrDepartureDt: item.arriveOrDepartureDt ? moment(item.arriveOrDepartureDt).format("DD.MM.YYYY") : "Не указан",
            roleIcon: iconMap[(item.clientRoleCode || 'anonymous').toLowerCase()]
          }
        })

        return orderData.orders
      } catch (e) {
        console.error(e)
        this.$notification({type: 'system_notification_error', message: 'Ошибка загрузки данных'})
        return []
      } finally {
        this.setUpdateLoader(false)
      }
    },
    async getCarrierOrders({page, size, query, sort, url = this.filterButtons[0].uuid, params = this.$route.query}) {
      this.clearAdditionalItemsFilter();
      try {
        let results;
        await api.orderProcess.getSecondaryListByClicar({page, size, query, sort, url}, params)
          .then(res => {
            const new_res = res.data.content.map(item => {
              return {
                ...item,
                arriveOrDepartureDt: item.arriveOrDepartureDt ? moment(item.arriveOrDepartureDt).format("DD.MM.YYYY") : "Не указан",
                roleIcon: iconMap[(item.clientRoleCode || 'anonymous').toLowerCase()]
              }
            });

            let service = res.data.content.map(key => ({
              uuid: key.serviceUuid,
              title: key.serviceTitle
            }));

            const unique_service = [];

            service.forEach((item) => {
              const i = unique_service.findIndex(x => x.title === item.title);
              if (i <= -1) {
                unique_service.push(item);
              }
            });

            let resource = res.data.content.map(key => ({
              uuid: key.resourceTypeUUid,
              title: key.resourceTypeTitle
            }));

            const unique_resource = [];

            resource.forEach((item) => {
              const i = unique_resource.findIndex(x => x.title === item.title);
              if (i <= -1) {
                unique_resource.push(item);
              }
            });

            this.setStatusFilter({
              title: 'resourceTypeOptions',
              params: unique_resource
            });
            this.setStatusFilter({
              title: 'serviceOptions',
              params: unique_service
            });

            results = {
              ...res.data,
              content: new_res
            }
          })
          .catch(() => results = {})
        return results
      } catch (e) {
        console.error(e)
        this.$notification({ type: 'system_notification_error', message: 'Ошибка загрузки данных' })
      } finally {
        this.setUpdateLoader(false)
      }
    },
    onSelection (data) {
      if (data.tripUuid) {
        this.$router.push({
          name: 'order__item',
          params: { id: data.uuid },
          query: { is_suborder: true }
        })
      }
      if (data.orderTypeTitle === 'Перевозка') {
        this.$router.push({ name: 'order__item', params: { id: data.uuid } })
      }
      if (data.orderTypeTitle === 'Трекинг') {
        this.$router.push(`/order-tracking-item/${data.uuid}`)
      }
    },
    updateFilter(params) {
      this.params = params;
      this.$nextTick(() => this.$parent.$refs['ob-table'].loadData({ init: true }))
    },
    transformArray(array) {
      return array.map((key) => {
        if (key?.uuid || key?.title) {
          return key
        } else {
          return {
            uuid: key,
            title: key
          }
        }
      })
    },
    hasShipments (item) {
      if (!item) {
        return '-'
      }
      item = item.join(', ')
      return item
    },
    clearAdditionalItemsFilter () {
      this.setUpdateLoader(true)
    },
    openInNewTab ({ uuid, tripUuid, orderTypeTitle }) {
      let path

      if (tripUuid) {
        path = `order-item/${uuid}?is_suborder=true`
      } else if (orderTypeTitle) {
        if (orderTypeTitle === 'Перевозка') {
          path = `order-item/${uuid}`
        } else if (orderTypeTitle === 'Трекинг') {
          path = `order-tracking-item/${uuid}`
        }
      }

      const { href } = this.$router.resolve({ path })

      window.open(href, '_blank')
    }
  }
}
