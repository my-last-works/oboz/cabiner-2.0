FROM gitlab-01.mgmt.oboz.app:6000/oboz2/build/node:12 AS builder

WORKDIR /app

ARG NPM_REGISTRY_URL=https://registry.npmjs.org
COPY package.json /app/
RUN : \
    && npm config set registry $NPM_REGISTRY_URL \
    && npm install \
    && :

COPY . /app
RUN : \
    && npm run build \
    && find dist/ -type f \( -name '*.js' -or -name '*.css' -or -name '*.svg' \) -print0 | parallel --will-cite -0 'zopfli {}' \
    && rm -vf dist/config.js.gz \
    && :


FROM gitlab-01.mgmt.oboz.app:6000/oboz2/build/nginx-spa:latest

WORKDIR /usr/share/nginx/html

COPY --from=builder /app/dist .
COPY docker-entrypoint.sh /docker-entrypoint.d/99-oboz.sh
ENV OBOZ_CLIENT_KEYCLOAK_SECRET=f17f413f-91ba-473a-b29a-c26b90c3e93b
