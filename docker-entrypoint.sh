#!/bin/sh -e

echo "Using api host: $OBOZ_API_HOST"
echo "Using keycloak secret: $OBOZ_CLIENT_KEYCLOAK_SECRET"

cd /usr/share/nginx/html/
echo 'console.log("Setting window.OBOZ_CONFIG");window.OBOZ_CONFIG = {OBOZ_API_HOST: "'$OBOZ_API_HOST'", CS:"'$OBOZ_CLIENT_KEYCLOAK_SECRET'"};' > ./config.js
